package au.com.connetica.chatsforlife.forms;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.SpeechBubbleView;
import au.com.connetica.chatsforlife.TranscriptActivity;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.model.SharedPref;
import au.com.connetica.chatsforlife.Logger;

import static android.view.View.INVISIBLE;

/** P5. How to Assist Someone **/

public class P5Fragment extends VideoPlayingBaseFragment implements View.OnClickListener {
    private static final String TAG = "P5Fragment";

    public static final int EXOPLAYER_RESOURCE_ID = R.id.ev3;
    public static final int VIDEO_RESOURCE_ID = R.raw.ev3_720;
    public static final String SUBTITLE_FILE = "file:///android_asset/ev3.srt";
    private static final String ISFIRSTRUN = SharedPref.ISFIRSTRUN_EV3;

    private static final int TRANSCRIPT_IMAGE_ID = R.drawable.ev3cover;
    private static final int TRANSCRIPT_HTML_ID = R.string.EV3;

    private SpeechBubbleView speechView1, speechView2, speechView3, speechView4, speechView5;
    private ImageButton transcriptButton;
    private EditText p5s1editText, p5s2editTextOther, p5s3editTextOther, p5s4editText, p5s5editText;
    private Button previousButton, nextButton;
    private CheckedTextView p5_s2_i1_1, p5_s2_i1_2, p5_s2_i1_3, p5_s2_i1_4, p5_s2_i1_5;
    private CheckedTextView p5_s3_i1_1, p5_s3_i1_2, p5_s3_i1_3, p5_s3_i1_4, p5_s3_i1_5, p5_s3_i1_6, p5_s3_i1_7;

    // the fragment initialization parameters
    private static final String ARG_PARAM1 = "param1";

    private DataModel mDataModel = DataModel.get(getActivity());    // init DataModel singleton
    private boolean mPageDirty = false; // flag for whether data has changed and to save the page

    private OnFragmentInteractionListener mListener;

    public P5Fragment() { } // Required empty public constructor


    public static P5Fragment newInstance() {
        P5Fragment fragment = new P5Fragment();
        fragment.exoPlayerResourceId = EXOPLAYER_RESOURCE_ID;
        fragment.videoResourceId = VIDEO_RESOURCE_ID;
        fragment.subtitleFile = SUBTITLE_FILE;
        fragment.isFirstRunPreference = ISFIRSTRUN;

        return fragment;
    }

    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment using superclass
        View v = inflateFragment(R.layout.fragment_p5, inflater, container);

        // If portrait, show buttons, speech views, etc
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {

            /*
            mPlan = mDataModel.getPlan(mPlanId);    // load the plan from the database
            if (mPlan == null) {    // no existing plan, create a new one
                mPlan = new Plan(mPlanId);
            }
            Logger.d(TAG, "mPlan = " + mPlan);
*/
            speechView1 = (SpeechBubbleView) v.findViewById(R.id.p5SpeechView1);
            speechView2 = (SpeechBubbleView) v.findViewById(R.id.p5SpeechView2);
            speechView3 = (SpeechBubbleView) v.findViewById(R.id.p5SpeechView3);
            speechView4 = (SpeechBubbleView) v.findViewById(R.id.p5SpeechView4);
            speechView5 = (SpeechBubbleView) v.findViewById(R.id.p5SpeechView5);
            if (mDataModel.plan.getName() != null && !mDataModel.plan.getName().isEmpty()) {
                updateSectionTitlesWithName();
            }

            transcriptButton = (ImageButton) v.findViewById(R.id.p5TranscriptButton);
            transcriptButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "transcriptButton onClickListener()");
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), TranscriptActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("transcriptImage", TRANSCRIPT_IMAGE_ID);
                    intent.putExtra("transcriptHtml", TRANSCRIPT_HTML_ID);
                    startActivity(intent);
                }
            });

            EditFocusChangeListener editFocusChangeListener = new EditFocusChangeListener();
            EditTextWatcher editTextWatcher = new EditTextWatcher();

            p5s1editText = (EditText) v.findViewById(R.id.p5s1editText);
            p5s1editText.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p5s1editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            if (mDataModel.plan.getP5s1() != null) {
                p5s1editText.setText(mDataModel.plan.getP5s1());
            }
            p5s1editText.addTextChangedListener(editTextWatcher);
            p5s1editText.setOnFocusChangeListener(editFocusChangeListener);

            p5_s2_i1_1 = (CheckedTextView) v.findViewById(R.id.p5_s2_i1_1);
            p5_s2_i1_2 = (CheckedTextView) v.findViewById(R.id.p5_s2_i1_2);
            p5_s2_i1_3 = (CheckedTextView) v.findViewById(R.id.p5_s2_i1_3);
            p5_s2_i1_4 = (CheckedTextView) v.findViewById(R.id.p5_s2_i1_4);
            p5_s2_i1_5 = (CheckedTextView) v.findViewById(R.id.p5_s2_i1_5);
            p5_s2_i1_1.setOnClickListener(this);
            p5_s2_i1_2.setOnClickListener(this);
            p5_s2_i1_3.setOnClickListener(this);
            p5_s2_i1_4.setOnClickListener(this);
            p5_s2_i1_5.setOnClickListener(this);
            p5_s2_i1_1.setChecked(mDataModel.plan.getP5_s2_i1_1());
            p5_s2_i1_1.setTypeface(p5_s2_i1_1.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s2_i1_2.setChecked(mDataModel.plan.getP5_s2_i1_2());
            p5_s2_i1_2.setTypeface(p5_s2_i1_2.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s2_i1_3.setChecked(mDataModel.plan.getP5_s2_i1_3());
            p5_s2_i1_3.setTypeface(p5_s2_i1_3.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s2_i1_4.setChecked(mDataModel.plan.getP5_s2_i1_4());
            p5_s2_i1_4.setTypeface(p5_s2_i1_4.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s2_i1_5.setChecked(mDataModel.plan.getP5_s2_i1_5());
            p5_s2_i1_5.setTypeface(p5_s2_i1_5.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5s2editTextOther = (EditText) v.findViewById(R.id.p5s2editText);
            p5s2editTextOther.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p5s2editTextOther.setRawInputType(InputType.TYPE_CLASS_TEXT);
            if (mDataModel.plan.getP5_s2_other() != null) {
                p5s2editTextOther.setText(mDataModel.plan.getP5_s2_other());
            }
            p5s2editTextOther.addTextChangedListener(editTextWatcher);
            p5s2editTextOther.setOnFocusChangeListener(editFocusChangeListener);
            p5s2editTextOther.setVisibility(p5_s2_i1_5.isChecked() ? View.VISIBLE : View.INVISIBLE);

            p5_s3_i1_1 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_1);
            p5_s3_i1_2 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_2);
            p5_s3_i1_3 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_3);
            p5_s3_i1_4 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_4);
            p5_s3_i1_5 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_5);
            p5_s3_i1_6 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_6);
            p5_s3_i1_7 = (CheckedTextView) v.findViewById(R.id.p5_s3_i1_7);
            p5_s3_i1_1.setOnClickListener(this);
            p5_s3_i1_2.setOnClickListener(this);
            p5_s3_i1_3.setOnClickListener(this);
            p5_s3_i1_4.setOnClickListener(this);
            p5_s3_i1_5.setOnClickListener(this);
            p5_s3_i1_6.setOnClickListener(this);
            p5_s3_i1_7.setOnClickListener(this);
            p5_s3_i1_1.setChecked(mDataModel.plan.getP5_s3_i1_1());
            p5_s3_i1_1.setTypeface(p5_s3_i1_1.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s3_i1_2.setChecked(mDataModel.plan.getP5_s3_i1_2());
            p5_s3_i1_2.setTypeface(p5_s3_i1_2.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s3_i1_3.setChecked(mDataModel.plan.getP5_s3_i1_3());
            p5_s3_i1_3.setTypeface(p5_s3_i1_3.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s3_i1_4.setChecked(mDataModel.plan.getP5_s3_i1_4());
            p5_s3_i1_4.setTypeface(p5_s3_i1_4.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s3_i1_5.setChecked(mDataModel.plan.getP5_s3_i1_5());
            p5_s3_i1_5.setTypeface(p5_s3_i1_5.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s3_i1_6.setChecked(mDataModel.plan.getP5_s3_i1_6());
            p5_s3_i1_6.setTypeface(p5_s3_i1_6.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            p5_s3_i1_7.setChecked(mDataModel.plan.getP5_s3_i1_7());
            p5_s3_i1_7.setTypeface(p5_s3_i1_7.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);

            p5s3editTextOther = (EditText) v.findViewById(R.id.p5s3editText);
            p5s3editTextOther.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p5s3editTextOther.setRawInputType(InputType.TYPE_CLASS_TEXT);
            if (mDataModel.plan.getP5_s3_other() != null) {
                p5s3editTextOther.setText(mDataModel.plan.getP5_s3_other());
            }
            p5s3editTextOther.addTextChangedListener(editTextWatcher);
            p5s3editTextOther.setOnFocusChangeListener(editFocusChangeListener);
            p5s3editTextOther.setVisibility(p5_s3_i1_7.isChecked() ? View.VISIBLE : View.INVISIBLE);

            p5s4editText = (EditText) v.findViewById(R.id.p5s4editText);
            p5s4editText.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p5s4editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            if (mDataModel.plan.getP5s4() != null) {
                p5s4editText.setText(mDataModel.plan.getP5s4());
            }
            p5s4editText.addTextChangedListener(editTextWatcher);
            p5s4editText.setOnFocusChangeListener(editFocusChangeListener);

            p5s5editText = (EditText) v.findViewById(R.id.p5s5editText);
            p5s5editText.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p5s5editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            if (mDataModel.plan.getP5s5() != null) {
                p5s5editText.setText(mDataModel.plan.getP5s5());
            }
            p5s5editText.addTextChangedListener(editTextWatcher);
            p5s5editText.setOnFocusChangeListener(editFocusChangeListener);

            previousButton = (Button) v.findViewById(R.id.p5previousbutton);
            previousButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "previousButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }

                    if (mListener != null) {
                        mListener.previousPage(); // send data back to the activity
                    }
                }
            });

            nextButton = (Button) v.findViewById(R.id.p5nextbutton);
            nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "nextButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }

                    if (mListener != null) {
                        mListener.nextPage(); // send data back to the activity
                    }
                }
            });


        }

        toggleSystemUi();       // show system UI in portrait, hide in landscape

        return v;
    }


    public void updateSectionTitlesWithName() {
        if (getActivity() != null) {
            speechView1.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s1_title)));
            speechView2.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s2_title)));
            speechView3.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s3_title)));
            speechView4.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s4_title)));
            speechView5.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s5_title)));
        }
    }

    // Save changes to mPlan on any change to edit text
    private class EditTextWatcher implements TextWatcher {
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            mPageDirty = true;  // flag page to be saved
            if (editable == p5s1editText.getEditableText()) {
                mDataModel.plan.setP5s1(editable.toString());
                Logger.d(TAG, "afterTextChanged. p5s1 = " + mDataModel.plan.getP5s1());
            } else if (editable == p5s2editTextOther.getEditableText()) {
                mDataModel.plan.setP5_s2_other(editable.toString());
                Logger.d(TAG, "afterTextChanged. p5s2_other = " + mDataModel.plan.getP5_s2_other());
            } else if (editable == p5s3editTextOther.getEditableText()) {
                mDataModel.plan.setP5_s3_other(editable.toString());
                Logger.d(TAG, "afterTextChanged. p5s3_other = " + mDataModel.plan.getP5_s3_other());
            } else if (editable == p5s4editText.getEditableText()) {
                mDataModel.plan.setP5s4(editable.toString());
                Logger.d(TAG, "afterTextChanged. p5s4 = " + mDataModel.plan.getP5s4());
            } else if (editable == p5s5editText.getEditableText()) {
                mDataModel.plan.setP5s5(editable.toString());
                Logger.d(TAG, "afterTextChanged. p5s5 = " + mDataModel.plan.getP5s5());
            }

        }
    }

    // Save updated mPlan to the database on any lost focus from edit text field
    private class EditFocusChangeListener implements View.OnFocusChangeListener {
        public void onFocusChange(View v, boolean hasFocus) {

            pauseVideoPlayback();

            if (!hasFocus && mPageDirty) {    // save to database
                Logger.d(TAG, "editFocusChangeListener - view id: " + v.getId() + ". Saving plan to database " + mDataModel.plan);
                mDataModel.updatePlan(mDataModel.plan);
                mPageDirty = false;
            }
        }
    }

    // click listener for checkboxes - save change directly to mPlan and the database
    public void onClick(View v) {
        ((CheckedTextView) v).toggle();

        switch (v.getId()) {
            case R.id.p5_s2_i1_1:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s2_i1_1.isChecked = " + p5_s2_i1_1.isChecked());
                mDataModel.plan.setP5_s2_i1_1(p5_s2_i1_1.isChecked());
                p5_s2_i1_1.setTypeface(p5_s2_i1_1.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s2_i1_2:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s2_i1_2.isChecked = " + p5_s2_i1_2.isChecked());
                mDataModel.plan.setP5_s2_i1_2(p5_s2_i1_2.isChecked());
                p5_s2_i1_2.setTypeface(p5_s2_i1_2.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s2_i1_3:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s2_i1_3.isChecked = " + p5_s2_i1_3.isChecked());
                mDataModel.plan.setP5_s2_i1_3(p5_s2_i1_3.isChecked());
                p5_s2_i1_3.setTypeface(p5_s2_i1_3.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s2_i1_4:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s2_i1_4.isChecked = " + p5_s2_i1_4.isChecked());
                mDataModel.plan.setP5_s2_i1_4(p5_s2_i1_4.isChecked());
                p5_s2_i1_4.setTypeface(p5_s2_i1_4.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s2_i1_5:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s2_i1_5.isChecked = " + p5_s2_i1_5.isChecked());
                mDataModel.plan.setP5_s2_i1_5(p5_s2_i1_5.isChecked());
                p5_s2_i1_5.setTypeface(p5_s2_i1_5.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p5s2editTextOther.setVisibility(p5_s2_i1_5.isChecked() ? View.VISIBLE : View.INVISIBLE);
                break;



            case R.id.p5_s3_i1_1:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_1.isChecked = " + p5_s3_i1_1.isChecked());
                mDataModel.plan.setP5_s3_i1_1(p5_s3_i1_1.isChecked());
                p5_s3_i1_1.setTypeface(p5_s3_i1_1.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s3_i1_2:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_2.isChecked = " + p5_s3_i1_2.isChecked());
                mDataModel.plan.setP5_s3_i1_2(p5_s3_i1_2.isChecked());
                p5_s3_i1_2.setTypeface(p5_s3_i1_2.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s3_i1_3:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_3.isChecked = " + p5_s3_i1_3.isChecked());
                mDataModel.plan.setP5_s3_i1_3(p5_s3_i1_3.isChecked());
                p5_s3_i1_3.setTypeface(p5_s3_i1_3.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s3_i1_4:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_4.isChecked = " + p5_s3_i1_4.isChecked());
                mDataModel.plan.setP5_s3_i1_4(p5_s3_i1_4.isChecked());
                p5_s3_i1_4.setTypeface(p5_s3_i1_4.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s3_i1_5:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_5.isChecked = " + p5_s3_i1_5.isChecked());
                mDataModel.plan.setP5_s3_i1_5(p5_s3_i1_5.isChecked());
                p5_s3_i1_5.setTypeface(p5_s3_i1_5.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s3_i1_6:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_6.isChecked = " + p5_s3_i1_6.isChecked());
                mDataModel.plan.setP5_s3_i1_6(p5_s3_i1_6.isChecked());
                p5_s3_i1_6.setTypeface(p5_s3_i1_6.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p5_s3_i1_7:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p5_s3_i1_7.isChecked = " + p5_s3_i1_7.isChecked());
                mDataModel.plan.setP5_s3_i1_7(p5_s3_i1_7.isChecked());
                p5_s3_i1_7.setTypeface(p5_s3_i1_7.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p5s3editTextOther.setVisibility(p5_s3_i1_7.isChecked() ? View.VISIBLE : View.INVISIBLE);
                break;

            default: break;
        }

        // save the change immediately to the database
        Logger.d(TAG, "Saving plan to database " + mDataModel.plan);
        mDataModel.updatePlan(mDataModel.plan);
        mPageDirty = false;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri); // send data back to the activity
        }
    }

    @Override
    public void onAttach(Context context) {
        Logger.d(TAG, "onAttach()");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;    // connect to the listener in the activity
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        // Register to receive messages.
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter("event-name-changed"));
    }

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Logger.d("receiver", "Got message: " + message);

            if (getActivity() != null) {
                // received event-name-changed broadcast message
                updateSectionTitlesWithName();
            }
        }
    };

    @Override
    public void onDetach() {
        Logger.d(TAG, "onDetach()");
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDetach();
        mListener = null;
    }



    /** Fragment Lifecycle **/
/*
    // When the fragment becomes visible to the user
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Logger.d(TAG, "isVisibleToUser true");
            updateSectionTitlesWithName();
            if (mDataModel.dataChanged) {

            }
        }  else {
            Logger.d(TAG, "isVisibleToUser false");
        }
    }
*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);

        if (mPageDirty && mDataModel.plan != null) {    // save to database
            Logger.d(TAG, "onSaveInstanceState(). Saving plan to database " + mDataModel.plan);
            mDataModel.updatePlan(mDataModel.plan);
            mPageDirty = false;
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void nextPage();
        void previousPage();
    }
}
