package au.com.connetica.chatsforlife.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
import java.util.Date;

import au.com.connetica.chatsforlife.Logger;
import au.com.connetica.chatsforlife.ReminderManager;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;

/**
 * Created by peter on 16/7/17.
 */

/*
    When the device reboots, scan through the database and reschedule any reminder notifications
 */
public class OnBootReceiver extends BroadcastReceiver {
    private static final String TAG = "OnBootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Logger.d(TAG, "onReceive: Chats for life");

        DataModel mDataModel = DataModel.get(context);    // init DataModel singleton

        // Iterate through all plans, looking for chats/follow-ups to reschedule reminders for
        for (Plan plan : mDataModel.getPlans()) {
            if (plan != null && plan.getName() != null) {
                Date planDate = plan.getPlanDate();
                int reminderId = plan.getChatReminderId();

                // hadChat = false, date is later than now, reminderId exists
                if (!plan.getHadChat() && planDate.getTime() > System.currentTimeMillis() && reminderId > 0) {
                    // schedule a reminder notification
                    Calendar c = Calendar.getInstance();
                    c.setTime(planDate);

                    if (plan.getIsFollowup()) {
                        ReminderManager.setReminder(context, reminderId, "Chat with " + plan.getName(), c);
                    } else {
                        ReminderManager.setReminder(context, reminderId, "Follow-up chat with " + plan.getName(), c);
                    }

                }
            }
        }

    }

}
