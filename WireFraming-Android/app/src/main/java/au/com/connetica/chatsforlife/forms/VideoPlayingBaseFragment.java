package au.com.connetica.chatsforlife.forms;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.SettingsActivity;
import au.com.connetica.chatsforlife.SpeechBubbleView;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.model.SharedPref;
import au.com.connetica.chatsforlife.Logger;
import au.com.connetica.chatsforlife.Logger;

/**
 * Created by peter on 5/7/17.
 */

public class VideoPlayingBaseFragment extends Fragment {
    private static final String TAG = "VideoBaseFragment";

    public int videoResourceId;         // set in subclasses, or will crash with null
    public int exoPlayerResourceId;     // set in subclasses, or will crash with null
    public String subtitleFile;         // set in subclasses, or will crash with null
    public String isFirstRunPreference; // SharedPrefs string for isFirstRun boolean for this video

//    private static final String SUBTITLE_FILE = "file:///android_asset/ev2.srt";
//    private static final int VIDEO_RESOURCE_ID = R.raw.ev2_720;


    // constant fields for saving and restoring bundle
    public static final String VIDEO_RESOURCE_ID = "video_resource_id";
    public static final String EXOPLAYER_RESOURCE_ID = "exoplayer_resource_id";
    public static final String SUBTITLE_FILE = "subtitle_file";
    public static final String SKIP_CLICKED = "skip_clicked";
    public static final String PLAYWHENREADY = "play_when_ready";
    public static final String CURRENT_WINDOW_INDEX = "current_window_index";
    public static final String PLAYBACK_POSITION = "playback_position";

    // used to remember the playback position
    private boolean skipClicked;
    private long playbackPosition;
    public boolean playWhenReady;
    private int currentWindow;

    public SimpleExoPlayerView playerView;
    public SimpleExoPlayer player;

    public VideoPlayingBaseFragment() { } // Required empty public constructor

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);

        // if we have saved player state, restore it
        if (savedInstanceState != null) {
            videoResourceId = savedInstanceState.getInt(VIDEO_RESOURCE_ID, 0);
            exoPlayerResourceId = savedInstanceState.getInt(EXOPLAYER_RESOURCE_ID, 0);
            subtitleFile = savedInstanceState.getString(SUBTITLE_FILE);

            skipClicked = savedInstanceState.getBoolean(SKIP_CLICKED, false);
            playbackPosition = savedInstanceState.getLong(PLAYBACK_POSITION, 0);
            currentWindow = savedInstanceState.getInt(CURRENT_WINDOW_INDEX, 0);
            playWhenReady = savedInstanceState.getBoolean(PLAYWHENREADY, false);
        }
    }

    // Call in onCreateView of subclasses
    /* eg.
       @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                         Bundle savedInstanceState) {
            View view = inflateFragment(R.layout.my_fragment, inflater, container);
            // Do things related to this fragment
            ...
            return view;
        }
     */
    protected View inflateFragment(int resId, LayoutInflater inflater, ViewGroup container) {
        View view = inflater.inflate(resId, container, false);
        playerView = (SimpleExoPlayerView) view.findViewById(exoPlayerResourceId);
        // setHasOptionsMenu(true);
        return view;
    }


    /**** VIDEO USING EXOPLAYER2 ****/

    public void startVideoPlayback() {
        if (player != null) {
            player.setPlayWhenReady(true); // start playback
        }
    }

    public void pauseVideoPlayback() {
        if (player != null) {
            player.setPlayWhenReady(false); // pause playback
        }
    }

    private void initializePlayer() {
        Logger.d(TAG, "initializePlayer(). playbackPosition: " + String.valueOf(playbackPosition));

        player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this.getActivity()),
                new DefaultTrackSelector(), new DefaultLoadControl());

        playerView.setPlayer(player);
        player.setPlayWhenReady(playWhenReady);
        player.seekTo(currentWindow, playbackPosition);   // resume playback position

        Uri uri = RawResourceDataSource.buildRawResourceUri(videoResourceId);
        MediaSource mediaSource = buildMediaSource(uri);
        player.prepare(mediaSource, true, false);


        //playerView.setDefaultArtwork(BitmapFactory.decodeResource(getResources(),R.drawable.ev4cover));

        //playerView.setUseArtwork(true);
        playerView.setControllerShowTimeoutMs(3000); // defaults is 5 seconds

        /*
        playerView.hideController();
        playerView.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int i) {
                if (i == 0) {
                    playerView.hideController();
                }
            }
        });
*/


    }

    private MediaSource buildMediaSource(Uri uri) {
        Logger.d(TAG, "buildMediaSource()");

        DataSpec dataSpec = new DataSpec(uri);
        final RawResourceDataSource rawResourceDataSource = new RawResourceDataSource(this.getActivity());
        try {
            rawResourceDataSource.open(dataSpec);
        } catch (RawResourceDataSource.RawResourceDataSourceException e) {
            e.printStackTrace();
        }

        DataSource.Factory factory = new DataSource.Factory() {
            @Override
            public DataSource createDataSource() {
                return rawResourceDataSource;
            }
        };
        MediaSource videoSource = new ExtractorMediaSource(rawResourceDataSource.getUri(), factory,
                new DefaultExtractorsFactory(), null, null);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean showSubtitles = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SUBTITLES, true);
        if (showSubtitles) {
            // Merge in subtitle file
            Uri subtitleUri = Uri.parse(subtitleFile);
            Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP,
                    null, Format.NO_VALUE, Format.NO_VALUE, "en", null);
            MediaSource subtitleSource = new SingleSampleMediaSource(subtitleUri,
                    new DefaultDataSourceFactory(this.getActivity(), "ua"), textFormat, C.TIME_UNSET);
            MergingMediaSource mergedSource = new MergingMediaSource(videoSource, subtitleSource);
            return mergedSource;
        } else {
            return videoSource;
        }

    }


    /** Fragment Lifecycle **/

    // When the fragment becomes visible/invisible to the user
    @Override

    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        Logger.d(TAG, "isVisibleToUser "+ isVisibleToUser);

        if (isVisibleToUser) {
            // Autoplay on first run
            if (isFirstRunPreference != null && SharedPref.read(isFirstRunPreference, true)) {
                SharedPref.write(isFirstRunPreference, false);
                startVideoPlayback();
            }

        } else {
             pauseVideoPlayback();
         }
    }

    // save player state before all members are gone forever :D
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
        /*
        * A simple configuration change such as screen rotation will destroy this activity
        * so we'll save the player state here in a bundle (that we can later access in onCreate) before everything is lost
        * NOTE: we cannot save player state in onDestroy like we did in onPause and onStop
        * the reason being our activity will be recreated from scratch and we would have lost all members (e.g. variables, objects) of this activity
        */
        outState.putBoolean(SKIP_CLICKED, skipClicked);

        if (player != null) {
            outState.putInt(VIDEO_RESOURCE_ID, videoResourceId);
            outState.putInt(EXOPLAYER_RESOURCE_ID, exoPlayerResourceId);
            outState.putString(SUBTITLE_FILE, subtitleFile);

            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            outState.putLong(PLAYBACK_POSITION, playbackPosition);
            outState.putInt(CURRENT_WINDOW_INDEX, currentWindow);
            outState.putBoolean(PLAYWHENREADY, playWhenReady);
            Logger.d(TAG, outState.toString());
        }
    }

    @Override
    public void onStart() {
        Logger.d(TAG, "onStart()");
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onStop() {
        Logger.d(TAG, "onStop()");
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    public void onPause() {
        Logger.d(TAG, "onPause()");
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onResume() {
        Logger.d(TAG, "onResume()");
        super.onResume();
        toggleSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    // Toggle showing the system UI depending on landscape or portrait mode
    public void toggleSystemUi() {
        Logger.d(TAG, "toggleSystemUi()");
        if (playerView == null) { return; }

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            playerView.setSystemUiVisibility(0);
        } else { // ** LANDSCAPE - hide UI **
            playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private void releasePlayer() {
        Logger.d(TAG, "releasePlayer()");
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }

}
