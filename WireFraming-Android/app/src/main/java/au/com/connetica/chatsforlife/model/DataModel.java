package au.com.connetica.chatsforlife.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import au.com.connetica.chatsforlife.AppContext;
import au.com.connetica.chatsforlife.Logger;
import au.com.connetica.chatsforlife.model.DBHelper.PlanTableSchema;

/**
 * Created by peter on 30/4/17.
 */

/*

DataModel Singleton


 */
public class DataModel {

    // Singleton
    private static DataModel sDataModel;
    public static DataModel get(Context context) {
        if (sDataModel == null) {
            sDataModel = new DataModel(context);
        }
        return sDataModel;
    }

    public Plan plan;               // the currently selected plan (singleton)


    private Context mContext;
    private SQLiteDatabase mDatabase;   // Sqlite database of plans

    private DataModel(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new DBHelper(mContext).getWritableDatabase();   // create new db file if it doesn't already exist

/*
        // add some sample data
        Plan plan1 = new Plan();
        plan1.setName("Fred");
        plan1.setPlanDate(new Date());
        addPlan(plan1);

        Plan plan2 = new Plan();
        plan2.setName("Mary");
        plan2.setPlanDate(new Date());
        addPlan(plan2);
*/

    }

    public boolean dataChanged;         // flag for whether the data in the model has changed

    // Send Broadcast message that data has changed
    private void sendDataChangedNotification() {
        Logger.d("sender", "Broadcasting message: ");
        Intent intent = new Intent("event-data-changed");
        LocalBroadcastManager.getInstance(AppContext.getContext()).sendBroadcast(intent);
    }

    public List<Plan> getPlans() {
        List<Plan> plans = new ArrayList<>();
        PlanCursorWrapper cursor = queryPlans(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                plans.add(cursor.getPlan());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return plans;
    }

    public Plan getPlan(UUID id) {
        PlanCursorWrapper cursor = queryPlans(PlanTableSchema.COLUMN_UUID + " = ?",
                new String[] { id.toString() }
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getPlan();
        } finally {
            cursor.close();
        }
    }

    private PlanCursorWrapper queryPlans(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                PlanTableSchema.TABLE_NAME,
                null, // columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new PlanCursorWrapper(cursor);
    }

    public void addPlan(Plan plan) {
        dataChanged = true;
        ContentValues values = getContentValues(plan);
        mDatabase.insert(PlanTableSchema.TABLE_NAME, null, values);
        sendDataChangedNotification();
    }

    public void updatePlan(Plan plan) {
        dataChanged = true;
        String uuidstring = plan.getId().toString();
        ContentValues values = getContentValues(plan);
        mDatabase.update(PlanTableSchema.TABLE_NAME, values,
                PlanTableSchema.COLUMN_UUID + " = ?",
                new String[] { uuidstring });
        sendDataChangedNotification();
    }

    public void deletePlan(Plan plan) {
        dataChanged = true;
        String uuidstring = plan.getId().toString();
        ContentValues values = getContentValues(plan);
        mDatabase.delete(PlanTableSchema.TABLE_NAME,
                PlanTableSchema.COLUMN_UUID + " = ?",
                new String[] { uuidstring });
        sendDataChangedNotification();
    }

    // Store a plan in a ContentValues key-value store
    private static ContentValues getContentValues(Plan plan) {
        ContentValues values = new ContentValues();
        values.put(PlanTableSchema.COLUMN_UUID, plan.getId().toString());

        if (plan.getName() != null) {
            values.put(PlanTableSchema.COLUMN_NAME, plan.getName());
        }
        if (plan.getPlanDate() != null) {
            values.put(PlanTableSchema.COLUMN_PLAN_DATE, plan.getPlanDate().getTime());
        }
        if (plan.getFollowupDate() != null) {
            values.put(PlanTableSchema.COLUMN_FOLLOWUP_DATE, plan.getFollowupDate().getTime());
        }

        values.put(PlanTableSchema.COLUMN_HAD_CHAT, plan.getHadChat() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_IS_FOLLOWUP, plan.getIsFollowup() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_CHAT_REMINDER_ID, plan.getChatReminderId());

        if (plan.getP4s2() != null) {
            values.put(PlanTableSchema.COLUMN_P4S2, plan.getP4s2());
        }
        values.put(PlanTableSchema.COLUMN_P4_S3_I1_1, plan.getP4_s3_i1_1() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P4_S3_I1_2, plan.getP4_s3_i1_2() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P4_S3_I1_3, plan.getP4_s3_i1_3() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P4_S3_I1_4, plan.getP4_s3_i1_4() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P4_S3_I1_5, plan.getP4_s3_i1_5() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P4_S3_I1_6, plan.getP4_s3_i1_6() ? 1 : 0);
        if (plan.getP4s4() != null) {
            values.put(PlanTableSchema.COLUMN_P4S4, plan.getP4s4());
        }

        if (plan.getP5s1() != null) {
            values.put(PlanTableSchema.COLUMN_P5S1, plan.getP5s1());
        }
        values.put(PlanTableSchema.COLUMN_P5_S2_I1_1, plan.getP5_s2_i1_1() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S2_I1_2, plan.getP5_s2_i1_2() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S2_I1_3, plan.getP5_s2_i1_3() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S2_I1_4, plan.getP5_s2_i1_4() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S2_I1_5, plan.getP5_s2_i1_5() ? 1 : 0);
        if (plan.getP5_s2_other() != null) {
            values.put(PlanTableSchema.COLUMN_P5_S2_OTHER, plan.getP5_s2_other());
        }
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_1, plan.getP5_s3_i1_1() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_2, plan.getP5_s3_i1_2() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_3, plan.getP5_s3_i1_3() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_4, plan.getP5_s3_i1_4() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_5, plan.getP5_s3_i1_5() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_6, plan.getP5_s3_i1_6() ? 1 : 0);
        values.put(PlanTableSchema.COLUMN_P5_S3_I1_7, plan.getP5_s3_i1_7() ? 1 : 0);
        if (plan.getP5_s3_other() != null) {
            values.put(PlanTableSchema.COLUMN_P5_S3_OTHER, plan.getP5_s3_other());
        }
        if (plan.getP5s4() != null) {
            values.put(PlanTableSchema.COLUMN_P5S4, plan.getP5s4());
        }
        if (plan.getP5s5() != null) {
            values.put(PlanTableSchema.COLUMN_P5S5, plan.getP5s5());
        }

        if (plan.getFollowups3() != null) {
            values.put(PlanTableSchema.COLUMN_FOLLOWUPS3, plan.getFollowups3());
        }
        if (plan.getFollowups5() != null) {
            values.put(PlanTableSchema.COLUMN_FOLLOWUPS5, plan.getFollowups5());
        }

        return values;
    }



}
