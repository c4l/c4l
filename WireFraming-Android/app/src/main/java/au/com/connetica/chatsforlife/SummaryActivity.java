package au.com.connetica.chatsforlife;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import au.com.connetica.chatsforlife.forms.PlanViewPager;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;

import static android.R.id.message;

public class SummaryActivity extends AppCompatActivity {
    private static final String TAG = "SummaryActivity";
    private DataModel mDataModel = DataModel.get(this);    // init DataModel singleton

    private CardView summarycard_p4, summarycard_p5, summarycard_p6, summarycard_p7, summarycard_p8, summarycard_followup;
    private TextView summarycard_p4s2_title, summarycard_p4s2_text;
    private TextView summarycard_p4s3_title, summarycard_p4s3_text;
    private TextView summarycard_p4s4_title, summarycard_p4s4_text;
    private TextView summarycard_p4s5_title, summarycard_p4s5_text;

    private TextView summarycard_p5s1_title, summarycard_p5s1_text;
    private TextView summarycard_p5s2_title, summarycard_p5s2_text;
    private TextView summarycard_p5s3_title, summarycard_p5s3_text;
    private TextView summarycard_p5s4_title, summarycard_p5s4_text;
    private TextView summarycard_p5s5_title, summarycard_p5s5_text;

    private TextView summarycard_followups3_title, summarycard_followups3_text;
    private TextView summarycard_followups4_title, summarycard_followups4_text;
    private TextView summarycard_followups5_title, summarycard_followups5_text;

    private Button summarycard_delete_button;

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { // crisis button in toolbar menu
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_view_pager, menu);
        return true;
    }

    // Handle menu choices - crisis button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_crisis) {
            Intent intent = new Intent(getApplicationContext(), CrisisActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button


        // P4 - Setting Up Your Conversation
        summarycard_p4 = (CardView) findViewById(R.id.card_summary_p4);
        summarycard_p4s2_title = (TextView) findViewById(R.id.summarycard_p4s2_title);
        summarycard_p4s2_text = (TextView) findViewById(R.id.summarycard_p4s2_text);
        summarycard_p4s3_title = (TextView) findViewById(R.id.summarycard_p4s3_title);
        summarycard_p4s3_text = (TextView) findViewById(R.id.summarycard_p4s3_text);
        summarycard_p4s4_title = (TextView) findViewById(R.id.summarycard_p4s4_title);
        summarycard_p4s4_text = (TextView) findViewById(R.id.summarycard_p4s4_text);
        summarycard_p4s5_title = (TextView) findViewById(R.id.summarycard_p4s5_title);
        summarycard_p4s5_text = (TextView) findViewById(R.id.summarycard_p4s5_text);

        // P5 - How to Assist Someone
        summarycard_p5 = (CardView) findViewById(R.id.card_summary_p5);
        summarycard_p5s1_title = (TextView) findViewById(R.id.summarycard_p5s1_title);
        summarycard_p5s1_text = (TextView) findViewById(R.id.summarycard_p5s1_text);
        summarycard_p5s2_title = (TextView) findViewById(R.id.summarycard_p5s2_title);
        summarycard_p5s2_text = (TextView) findViewById(R.id.summarycard_p5s2_text);
        summarycard_p5s3_title = (TextView) findViewById(R.id.summarycard_p5s3_title);
        summarycard_p5s3_text = (TextView) findViewById(R.id.summarycard_p5s3_text);
        summarycard_p5s4_title = (TextView) findViewById(R.id.summarycard_p5s4_title);
        summarycard_p5s4_text = (TextView) findViewById(R.id.summarycard_p5s4_text);
        summarycard_p5s5_title = (TextView) findViewById(R.id.summarycard_p5s5_title);
        summarycard_p5s5_text = (TextView) findViewById(R.id.summarycard_p5s5_text);

        // P6 -
        summarycard_p6 = (CardView) findViewById(R.id.card_summary_p6);

        // P7 -
        summarycard_p7 = (CardView) findViewById(R.id.card_summary_p7);

        // P8 -
        summarycard_p8 = (CardView) findViewById(R.id.card_summary_p8);

        // Followup
        summarycard_followup = (CardView) findViewById(R.id.card_summary_followup);
        summarycard_followups3_title = (TextView) findViewById(R.id.summarycard_followups3_title);
        summarycard_followups3_text = (TextView) findViewById(R.id.summarycard_followups3_text);
        summarycard_followups4_title = (TextView) findViewById(R.id.summarycard_followups4_title);
        summarycard_followups4_text = (TextView) findViewById(R.id.summarycard_followups4_text);
        summarycard_followups5_title = (TextView) findViewById(R.id.summarycard_followups5_title);
        summarycard_followups5_text = (TextView) findViewById(R.id.summarycard_followups5_text);

        summarycard_delete_button = (Button) findViewById(R.id.summaryCardDeleteButton);
        summarycard_delete_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // *** Show a confirmation dialog
                new AlertDialog.Builder(SummaryActivity.this, R.style.Theme_AppCompat_Dialog_Alert)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete plan for " + mDataModel.plan.getName() + "?")
                        .setMessage("Are you sure ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mDataModel.deletePlan(mDataModel.plan);
                                finish();

                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
        });


        loadSummaryData();
        updateSectionTitlesWithName();

    }

    // Handle click on a card
    public void onClickHandler(View v) {

        if (v == summarycard_followup) {
            // launch followup activity
            Intent intent = new Intent(getApplicationContext(), FollowupActivity.class);
            intent.putExtra("planId", mDataModel.plan.getId());
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, PlanViewPager.class);
            intent.putExtra("planId", mDataModel.plan.getId());

            if (v == summarycard_p5) {
                intent.putExtra("startPage", 1);
            } else if (v == summarycard_p6) {
                intent.putExtra("startPage", 2);
            } else if (v == summarycard_p7) {
                intent.putExtra("startPage", 3);
            } else if (v == summarycard_p8) {
                intent.putExtra("startPage", 4);
            }

            startActivity(intent);
        }
    }


    private void updateSectionTitlesWithName() {
        Logger.d(TAG, "updateSectionTitlesWithName()");
        summarycard_p4s2_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s2_title)));
        summarycard_p4s3_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s3_title)));
        summarycard_p4s4_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s4_title)));
        summarycard_p4s5_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s5_title)));

        summarycard_p5s1_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s1_title)));
        summarycard_p5s2_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s2_title)));
        summarycard_p5s3_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s3_title)));
        summarycard_p5s4_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s4_title)));
        summarycard_p5s5_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.p5_s5_title)));

        summarycard_followups3_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.followup_s3_title)));
        summarycard_followups4_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.followup_s4_title)));
        summarycard_followups5_title.setText(mDataModel.plan.replaceTokenWithName(getString(R.string.followup_s5_title)));

    }

    // fill cards with data from the database
    private void loadSummaryData() {
        Logger.d(TAG, "loadSummaryData()");

        // get extras passed in - should be UUID of plan passed in
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            UUID mPlanId = (UUID) extras.get("planId");   // get the UUID passed in
            Logger.d(TAG, "Passed planID: " + mPlanId.toString());
            mDataModel.plan = mDataModel.getPlan(mPlanId);  // retrieve the current plan from the DB
        } else {
            return;
        }


        if (!TextUtils.isEmpty(mDataModel.plan.getName())) {
            getSupportActionBar().setTitle("Summary for " + mDataModel.plan.getName());
        }

        // P4 - Setting Up Your Conversation
        String p4s2 = mDataModel.plan.getP4s2();
        if (TextUtils.isEmpty(p4s2)) { // checks for null and empty String
            summarycard_p4s2_title.setVisibility(View.GONE);
            summarycard_p4s2_text.setVisibility(View.GONE);
        } else {
            summarycard_p4s2_title.setVisibility(View.VISIBLE);
            summarycard_p4s2_text.setVisibility(View.VISIBLE);
            summarycard_p4s2_text.setText(p4s2);
        }

        String p4s3 = "";
        if (mDataModel.plan.getP4_s3_i1_1()) { p4s3 += getString(R.string.p4_s3_i1_1) + ", "; }
        if (mDataModel.plan.getP4_s3_i1_2()) { p4s3 += getString(R.string.p4_s3_i1_2) + ", "; }
        if (mDataModel.plan.getP4_s3_i1_3()) { p4s3 += getString(R.string.p4_s3_i1_3) + ", "; }
        if (mDataModel.plan.getP4_s3_i1_4()) { p4s3 += getString(R.string.p4_s3_i1_4) + ", "; }
        if (mDataModel.plan.getP4_s3_i1_5()) { p4s3 += getString(R.string.p4_s3_i1_5) + ", "; }
        if (mDataModel.plan.getP4_s3_i1_6()) { p4s3 += getString(R.string.p4_s3_i1_6) + ", "; }
        if (p4s3.endsWith(", ")) {  // remove trailing ,
            p4s3 = p4s3.substring(0, p4s3.length() - 2);
        }
        if (TextUtils.isEmpty(p4s3)) { // checks for null and empty String
            summarycard_p4s3_title.setVisibility(View.GONE);
            summarycard_p4s3_text.setVisibility(View.GONE);
        } else {
            summarycard_p4s3_title.setVisibility(View.VISIBLE);
            summarycard_p4s3_text.setVisibility(View.VISIBLE);
            summarycard_p4s3_text.setText(p4s3);
        }

        String p4s4 = mDataModel.plan.getP4s4();
        if (TextUtils.isEmpty(p4s4)) { // checks for null and empty String
            summarycard_p4s4_title.setVisibility(View.GONE);
            summarycard_p4s4_text.setVisibility(View.GONE);
        } else {
            summarycard_p4s4_title.setVisibility(View.VISIBLE);
            summarycard_p4s4_text.setVisibility(View.VISIBLE);
            summarycard_p4s4_text.setText(p4s4);
        }

        Date planDate = mDataModel.plan.getPlanDate();
        if (planDate == null || planDate.getTime() <= 0) {   // a date is not scheduled
            summarycard_p4s5_title.setVisibility(View.GONE);
            summarycard_p4s5_text.setVisibility(View.GONE);

        } else {
            summarycard_p4s5_title.setVisibility(View.VISIBLE);
            summarycard_p4s5_text.setVisibility(View.VISIBLE);
            SimpleDateFormat formatter = new SimpleDateFormat("MMM d 'at' h:mm a");     //  "MMM d 'at' h:mma"
            summarycard_p4s5_text.setText(formatter.format(planDate));
        }


        // P5 - How to Assist Someone
        String p5s1 = mDataModel.plan.getP5s1();
        if (TextUtils.isEmpty(p5s1)) { // checks for null and empty String
            summarycard_p5s1_title.setVisibility(View.GONE);
            summarycard_p5s1_text.setVisibility(View.GONE);
        } else {
            summarycard_p5s1_title.setVisibility(View.VISIBLE);
            summarycard_p5s1_text.setVisibility(View.VISIBLE);
            summarycard_p5s1_text.setText(p5s1);
        }

        String p5s2 = "";
        String p5s2other = mDataModel.plan.getP5_s2_other();
        if (mDataModel.plan.getP5_s2_i1_1()) { p5s2 += getString(R.string.p5_s2_i1_1) + ", "; }
        if (mDataModel.plan.getP5_s2_i1_2()) { p5s2 += getString(R.string.p5_s2_i1_2) + ", "; }
        if (mDataModel.plan.getP5_s2_i1_3()) { p5s2 += getString(R.string.p5_s2_i1_3) + ", "; }
        if (mDataModel.plan.getP5_s2_i1_4()) { p5s2 += getString(R.string.p5_s2_i1_4) + ", "; }
        if (mDataModel.plan.getP5_s2_i1_5() && !TextUtils.isEmpty(p5s2other)) { p5s2 += p5s2other; }
        if (p5s2.endsWith(", ")) {  // remove trailing ,
            p5s2 = p5s2.substring(0, p5s2.length() - 2);
        }
        if (TextUtils.isEmpty(p5s2)) { // checks for null and empty String
            summarycard_p5s2_title.setVisibility(View.GONE);
            summarycard_p5s2_text.setVisibility(View.GONE);
        } else {
            summarycard_p5s2_title.setVisibility(View.VISIBLE);
            summarycard_p5s2_text.setVisibility(View.VISIBLE);
            summarycard_p5s2_text.setText(p5s2);
        }

        String p5s3 = "";
        String p5s3other = mDataModel.plan.getP5_s3_other();
        if (mDataModel.plan.getP5_s3_i1_1()) { p5s3 += getString(R.string.p5_s3_i1_1) + ", "; }
        if (mDataModel.plan.getP5_s3_i1_2()) { p5s3 += getString(R.string.p5_s3_i1_2) + ", "; }
        if (mDataModel.plan.getP5_s3_i1_3()) { p5s3 += getString(R.string.p5_s3_i1_3) + ", "; }
        if (mDataModel.plan.getP5_s3_i1_4()) { p5s3 += getString(R.string.p5_s3_i1_4) + ", "; }
        if (mDataModel.plan.getP5_s3_i1_5()) { p5s3 += getString(R.string.p5_s3_i1_5) + ", "; }
        if (mDataModel.plan.getP5_s3_i1_6()) { p5s3 += getString(R.string.p5_s3_i1_6) + ", "; }
        if (mDataModel.plan.getP5_s3_i1_7() && !TextUtils.isEmpty(p5s3other)) { p5s3 += p5s3other; }
        if (p5s3.endsWith(", ")) {  // remove trailing ,
            p5s3 = p5s3.substring(0, p5s3.length() - 2);
        }
        if (TextUtils.isEmpty(p5s3)) { // checks for null and empty String
            summarycard_p5s3_title.setVisibility(View.GONE);
            summarycard_p5s3_text.setVisibility(View.GONE);
        } else {
            summarycard_p5s3_title.setVisibility(View.VISIBLE);
            summarycard_p5s3_text.setVisibility(View.VISIBLE);
            summarycard_p5s3_text.setText(p5s3);
        }

        String p5s4 = mDataModel.plan.getP5s4();
        if (TextUtils.isEmpty(p5s4)) { // checks for null and empty String
            summarycard_p5s4_title.setVisibility(View.GONE);
            summarycard_p5s4_text.setVisibility(View.GONE);
        } else {
            summarycard_p5s4_title.setVisibility(View.VISIBLE);
            summarycard_p5s4_text.setVisibility(View.VISIBLE);
            summarycard_p5s4_text.setText(p5s1);
        }

        String p5s5 = mDataModel.plan.getP5s5();
        if (TextUtils.isEmpty(p5s5)) { // checks for null and empty String
            summarycard_p5s5_title.setVisibility(View.GONE);
            summarycard_p5s5_text.setVisibility(View.GONE);
        } else {
            summarycard_p5s5_title.setVisibility(View.VISIBLE);
            summarycard_p5s5_text.setVisibility(View.VISIBLE);
            summarycard_p5s5_text.setText(p5s1);
        }

        // Follow-up
        if (!mDataModel.plan.getIsFollowup()) {
            summarycard_followup.setVisibility(View.GONE);
        } else {

            String followups3 = mDataModel.plan.getFollowups3();
            if (TextUtils.isEmpty(followups3)) { // checks for null and empty String
                summarycard_followups3_title.setVisibility(View.GONE);
                summarycard_followups3_text.setVisibility(View.GONE);
            } else {
                summarycard_followups3_title.setVisibility(View.VISIBLE);
                summarycard_followups3_text.setVisibility(View.VISIBLE);
                summarycard_followups3_text.setText(followups3);
            }

            Date followupDate = mDataModel.plan.getPlanDate();
            if (followupDate == null || followupDate.getTime() <= 0) {   // a date is not scheduled
                summarycard_followups4_title.setVisibility(View.GONE);
                summarycard_followups4_text.setVisibility(View.GONE);

            } else {
                summarycard_followups4_title.setVisibility(View.VISIBLE);
                summarycard_followups4_text.setVisibility(View.VISIBLE);
                SimpleDateFormat formatter = new SimpleDateFormat("MMM d 'at' h:mm a");     //  "MMM d 'at' h:mma"
                summarycard_followups4_text.setText(formatter.format(followupDate));
            }

            String followups5 = mDataModel.plan.getFollowups5();
            if (TextUtils.isEmpty(followups5)) { // checks for null and empty String
                summarycard_followups5_title.setVisibility(View.GONE);
                summarycard_followups5_text.setVisibility(View.GONE);
            } else {
                summarycard_followups5_title.setVisibility(View.VISIBLE);
                summarycard_followups5_text.setVisibility(View.VISIBLE);
                summarycard_followups5_text.setText(followups5);
            }
        }

    }

    @Override
    public void onResume() {
        Logger.d(TAG, "onResume()");
        super.onResume();
/*
        // If model has changed, refresh the data on resume
        if (mDataModel.dataChanged) {
            loadSummaryData();
            mDataModel.dataChanged = false;
            updateSectionTitlesWithName();
        }

        */

        // Register to receive messages.
        // This is just like [[NSNotificationCenter defaultCenter] addObserver:...]
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(mNameReceiver,
                new IntentFilter("event-name-changed"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mDataReceiver,
                new IntentFilter("event-data-changed"));

    }


    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mNameReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("newName");
            Logger.d("receiver", "Got message: " + message);

            // received event-name-changed broadcast message
            loadSummaryData();
            updateSectionTitlesWithName();
        }
    };

    private BroadcastReceiver mDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.d("receiver", "Got message: " + message);
            // received event-data-changed broadcast message
            loadSummaryData();        }
    };



    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        // This is somewhat like [[NSNotificationCenter defaultCenter] removeObserver:name:object:]
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDataReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNameReceiver);
        super.onDestroy();
    }

}
