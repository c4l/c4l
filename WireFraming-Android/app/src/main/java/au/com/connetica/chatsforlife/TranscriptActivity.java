package au.com.connetica.chatsforlife;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import au.com.connetica.chatsforlife.R;

import static au.com.connetica.chatsforlife.R.id.crisisPhone1;

public class TranscriptActivity extends AppCompatActivity {
    private static final String TAG = "TranscriptActivity";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_view_pager, menu);
        return true;
    }

    // Handle menu choices - Crisis button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_crisis) {
            Intent intent = new Intent(getApplicationContext(), CrisisActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transcript);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button

        ImageView transcriptImageView = (ImageView) findViewById(R.id.transcriptImageView);
        WebView transcriptWebView = (WebView) findViewById(R.id.transcriptWebView);

        // get extras passed in
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int imageId = extras.getInt("transcriptImage");
            int htmlTextId = extras.getInt("transcriptHtml");

            String htmlString = getString(htmlTextId);
            transcriptWebView.loadData(htmlString, "text/html", null);

            transcriptImageView.setImageResource(imageId);

        }

    }

}
