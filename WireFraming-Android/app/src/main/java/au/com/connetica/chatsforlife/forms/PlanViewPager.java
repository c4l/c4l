package au.com.connetica.chatsforlife.forms;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import java.util.UUID;

import au.com.connetica.chatsforlife.CrisisActivity;
import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.SettingsActivity;
import au.com.connetica.chatsforlife.SettingsFragment;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.Logger;


public class PlanViewPager extends AppCompatActivity
implements P4Fragment.OnFragmentInteractionListener,
           P5Fragment.OnFragmentInteractionListener,
           P6Fragment.OnFragmentInteractionListener,
           P7Fragment.OnFragmentInteractionListener,
           P8Fragment.OnFragmentInteractionListener {

    private static final String TAG = "PlanViewPager";
    private DataModel mDataModel = DataModel.get(this);    // init DataModel singleton

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_plan_view_pager);

        // get extras passed in
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            UUID mPlanId = (UUID) extras.get("planId");   // get the UUID passed in
            Logger.d(TAG, "Passed planID: " + mPlanId.toString());

            mDataModel.plan = mDataModel.getPlan(mPlanId);  // retrieve the current plan from the DB

            if (mDataModel.plan == null) {
                mDataModel.plan = new Plan(mPlanId); // create a new plan
                mDataModel.addPlan(mDataModel.plan); // add it to the database
                Logger.d(TAG, "Created new plan: " + mDataModel.plan);
            }

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.home_icon); // set back button to home icon


        if (mDataModel.plan == null || mDataModel.plan.getName() == null) {
            getSupportActionBar().setTitle(getString(R.string.app_name) + " | New Plan");
        } else {
            getSupportActionBar().setTitle(getString(R.string.app_name) + " | 1 of 5");
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Logger.d(TAG, "onPageSelected position: " + position);
                getSupportActionBar().setTitle(getString(R.string.app_name) + " | " + (position+1) +" of 5");
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        if (extras != null) {
            int startPage = extras.getInt("startPage", 0);
            mViewPager.setCurrentItem(startPage);
        }


        toggleSystemUi(); // show action bar in portrait, hide in landscape


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_view_pager, menu);
        return true;
    }

    // Handle menu choices
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_crisis) {
            Intent intent = new Intent(getApplicationContext(), CrisisActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
//            return PlaceholderFragment.newInstance(position + 1);

            if (mDataModel.plan == null || mDataModel.plan.getName() == null) {     // just one page available for a new plan
                Fragment p4fragment = P4Fragment.newInstance();
                return p4fragment;
            }

            // 5 pages available for an existing plan

            switch (position) {
                case 0:
                    Fragment p4fragment = P4Fragment.newInstance();
                    return p4fragment;

                case 1:
                    Fragment p5fragment = P5Fragment.newInstance();
                    return p5fragment;
                case 2:
                    Fragment p6fragment = P6Fragment.newInstance();
                    return p6fragment;
                case 3:
                    Fragment p7fragment = P7Fragment.newInstance();
                    return p7fragment;
                case 4:
                    Fragment p8fragment = P8Fragment.newInstance();
                    return p8fragment;

            }
            return null;

        }

        @Override
        public int getCount() {
            if (mDataModel.plan == null || mDataModel.plan.getName() == null) {     // just one page available for a new plan
             return 1;
            }
            return 5;
        }

/*
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    //return "Setting Up Your Conversation";
                    return "1";
                case 1:
                    //return "How To Assist Someone";
                    return "2";
                case 2:
                    //return "Tips For Your Conversation";
                    return "3";
                case 3:
                    //return "Starting The Conversation";
                    return "4";
                case 4:
                    //return "Confident In Any Conversation";
                    return "5";
            }
            return null;
        }
*/

    }


    // Toggle showing the system UI depending on landscape or portrait mode
    private void toggleSystemUi() {
        Logger.d(TAG, "toggleSystemUi()");

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            //getSupportActionBar().show();
        } else { // ** LANDSCAPE - hide UI **
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            //getSupportActionBar().hide();
        }
    }



// OnFragmentInteractionListener

    public void onFragmentInteraction(Uri uri) {
        Logger.d(TAG, "onFragmentInteraction() " + uri.toString());
    }

    // A new plan has been created, refresh to show all pages
    public void newPlanCreated() {
        Logger.d(TAG, "newPlanCreated() ");
        //mDataModel.newPlan = false;
        getSupportActionBar().setTitle(getString(R.string.app_name) + " | 1 of 5");
        mSectionsPagerAdapter.notifyDataSetChanged();
    }

    public void nextPage() {
        int currentPage = mViewPager.getCurrentItem();
        if (currentPage < mSectionsPagerAdapter.getCount() - 1) {
            mViewPager.setCurrentItem(currentPage + 1, true);
        }
    }

    public void previousPage() {
        int currentPage = mViewPager.getCurrentItem();
        if (currentPage > 0) {
            mViewPager.setCurrentItem(currentPage - 1, true);
        }
    }

/*
    // save player state before all members are gone forever :D
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);

        outState.putBoolean("newPlan", mDataModel.newPlan);
    }
*/


}
/***

     // A placeholder fragment containing a simple view.



    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_plan_view_pager, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }


***/