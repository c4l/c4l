package au.com.connetica.chatsforlife;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.SharedPref;


public class FollowupActivity extends AppCompatActivity {
    private static final String TAG = "Followup";
    private DataModel mDataModel = DataModel.get(this);    // init DataModel singleton
    private boolean mPageDirty = false; // flag for whether data has changed and to save the page

    private SpeechBubbleView speechView1, speechView2, speechView3, speechView4, speechView5;
    private TextView textView1, textView2;
    private EditText followupEditText1, followupEditText2;
    private Button followupScheduleButton, followupAddToCalendarButton, doneButton;
    private int mYear, mMonth, mDay; // date returned from date picker

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followup);

        // get extras passed in
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            UUID mPlanId = (UUID) extras.get("planId");   // get the UUID passed in
            mDataModel.plan = mDataModel.getPlan(mPlanId);  // retrieve the current plan from the DB
            Logger.d(TAG, "Passed plan: " + mDataModel.plan);
        } else {
            return;
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button

        speechView1 = (SpeechBubbleView) findViewById(R.id.followupSpeechView1);
        textView1 = (TextView) findViewById(R.id.followupTextView1);
        speechView2 = (SpeechBubbleView) findViewById(R.id.followupSpeechView2);

        speechView3 = (SpeechBubbleView) findViewById(R.id.followupSpeechView3);
        speechView3.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.followup_s3_title)));

        followupEditText1 = (EditText) findViewById(R.id.followupEditText1);
        followupEditText1.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
        followupEditText1.setRawInputType(InputType.TYPE_CLASS_TEXT);
        if (mDataModel.plan.getFollowups3() != null) {
            followupEditText1.setText(mDataModel.plan.getFollowups3());
        }
        EditFocusChangeListener editFocusChangeListener = new EditFocusChangeListener();
        EditTextWatcher editTextWatcher = new EditTextWatcher();
        followupEditText1.addTextChangedListener(editTextWatcher);
        followupEditText1.setOnFocusChangeListener(editFocusChangeListener);

        speechView4 = (SpeechBubbleView) findViewById(R.id.followupSpeechView4);
        speechView4.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.followup_s4_title)));


        followupScheduleButton = (Button) findViewById(R.id.followupScheduleButton); // schedule button
        followupScheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        followupAddToCalendarButton  = (Button) findViewById(R.id.followupAddToCalendarButton); // add to calendar button
        followupAddToCalendarButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Logger.d(TAG, "followupAddToCalendarButton onClickListener()");

                // Add appointment to device calendar
                Date planDate = mDataModel.plan.getPlanDate();
                if (planDate != null && planDate.getTime() > 0) {   // a date exists
                    final Calendar c = Calendar.getInstance();
                    c.setTime(planDate);
                    ReminderManager.addToCalendar(getApplicationContext(), "Follow-up chat with " + mDataModel.plan.getName(), "",
                            mDataModel.plan.getP4s4(), c);
                }
            }
        });

        if (mDataModel.plan.getPlanDate() != null && mDataModel.plan.getPlanDate().getTime() != 0) {    // a date has been set
            SimpleDateFormat formatter = new SimpleDateFormat("MMM d 'at' h:mm a");     //  "MMM d 'at' h:mma"
            followupScheduleButton.setText(formatter.format(mDataModel.plan.getPlanDate()));
            followupAddToCalendarButton.setVisibility(View.VISIBLE);
        } else {
            followupAddToCalendarButton.setVisibility(View.GONE);
        }


        speechView5 = (SpeechBubbleView) findViewById(R.id.followupSpeechView5);
        followupEditText2 = (EditText) findViewById(R.id.followupEditText2);
        followupEditText2.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
        followupEditText2.setRawInputType(InputType.TYPE_CLASS_TEXT);
        if (mDataModel.plan.getFollowups5() != null) {
            followupEditText2.setText(mDataModel.plan.getFollowups5());
        }
        followupEditText2.addTextChangedListener(editTextWatcher);
        followupEditText2.setOnFocusChangeListener(editFocusChangeListener);

        doneButton = (Button) findViewById(R.id.followupDonebutton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Logger.d(TAG, "doneButton onClickListener()");
                finish();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) { // crisis button in toolbar menu
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_view_pager, menu);
        return true;
    }

    // Handle menu choices - Crisis button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_crisis) {
            Intent intent = new Intent(getApplicationContext(), CrisisActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    // Save changes to mDateModel.plan on any change to edit text
    private class EditTextWatcher implements TextWatcher {
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            mPageDirty = true;  // flag page to be saved
            if (editable == followupEditText1.getEditableText()) {
                mDataModel.plan.setFollowups3(editable.toString());
                Logger.d(TAG, "afterTextChanged. followups3 = " + mDataModel.plan.getFollowups3());
            } else if (editable == followupEditText2.getEditableText()) {
                mDataModel.plan.setFollowups5(editable.toString());
                Logger.d(TAG, "afterTextChanged. followups5 = " + mDataModel.plan.getFollowups5());
            }
        }
    }

    // Save updated plan to the database on any lost focus from edit text field
    private class EditFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && mPageDirty) {    // save to database
                Logger.d(TAG, "editFocusChangeListener - view id: " + v.getId() + ". Saving plan to database " + mDataModel.plan);
                mDataModel.updatePlan(mDataModel.plan);
                mPageDirty = false;
            }
        }

    }


    private void showDatePicker() {
        // Default value: use saved date - if not, use today's date
        final Calendar c = Calendar.getInstance();

        Date planDate = mDataModel.plan.getPlanDate();
        if (planDate != null && planDate.getTime() > 0) {   // a date is already schedule
            c.setTime(planDate);
        }
        final int mDefaultYear = c.get(Calendar.YEAR);
        final int mDefaultMonth = c.get(Calendar.MONTH);
        final int mDefaultDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                        //  mDateTime = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        showTimePicker();
                    }
                }, mDefaultYear, mDefaultMonth, mDefaultDay);
        datePickerDialog.show();
    }

    private void showTimePicker() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        Date planDate = mDataModel.plan.getPlanDate();
        if (planDate != null && planDate.getTime() > 0) {   // a date is already schedule
            c.setTime(planDate);
        }
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        // friendly format - eg. 'Jul 6 at 4:15pm'
                        c.set(mYear, mMonth, mDay,hourOfDay, minute);
                        Date newDate = c.getTime();
                        SimpleDateFormat formatter = new SimpleDateFormat("MMM d 'at' h:mm a");     //  "MMM d 'at' h:mma"
                        followupScheduleButton.setText(formatter.format(newDate));
                        followupAddToCalendarButton.setVisibility(View.VISIBLE);

                        mDataModel.plan.setPlanDate(newDate);
                        mDataModel.plan.setIsFollowup(true);
                        mDataModel.plan.setHadChat(false);

                        // Cancel any existing scheduled reminder
                        int previousReminderId = mDataModel.plan.getChatReminderId();
                        if (previousReminderId > 0 ) {  // cancel any existing scheduled reminder
                            ReminderManager.cancelReminder(getApplicationContext(), previousReminderId);
                        }

                        // Get a unique ID for the scheduled reminder
                        int reminderId = SharedPref.read(SharedPref.LAST_REMINDER_ID, 0) + 1;
                        SharedPref.write(SharedPref.LAST_REMINDER_ID, reminderId);
                        mDataModel.plan.setChatReminderId(reminderId);
                        mDataModel.updatePlan(mDataModel.plan);   //  save to the database
                        mPageDirty = false;

                        // schedule a reminder notification
                        ReminderManager.setReminder(getApplicationContext(), reminderId, "Follow-up chat with "+ mDataModel.plan.getName(), c);
                        // Send Broadcast message that name/date has changed
                        Logger.d("sender", "Broadcasting message: ");
                        Intent intent = new Intent("event-name-changed");
                        // You can also include some extra data.
                        //intent.putExtra("newName", newName);
                        LocalBroadcastManager.getInstance(getParent()).sendBroadcast(intent);



                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    // Save changes to database when activity finishes
    @Override
    public void onPause() {
        Logger.d(TAG, "onPause()");
        super.onPause();

        if (mPageDirty) {    // save to database
            Logger.d(TAG, "onPause(). Saving plan to database " + mDataModel.plan);
            mDataModel.updatePlan(mDataModel.plan);
            mPageDirty = false;
        }

    }

}
