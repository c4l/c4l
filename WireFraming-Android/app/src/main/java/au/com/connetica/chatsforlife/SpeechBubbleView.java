package au.com.connetica.chatsforlife;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import au.com.connetica.chatsforlife.R;

import static android.R.attr.bitmap;

/**
 * TODO: document your custom view class.
 */


public class SpeechBubbleView extends View {
    private static final String TAG = "SpeechBubbleView";

    // Bubble types
    public static final int PRIMARY = 0;
    public static final int SECONDARY = 1;
    public static final int TERTIARY = 2;
    public static final int TERTIARY_LARGE = 2;

    // Attributes
    private String speechText;
    private int bubbleType;

    private int contentWidth, contentHeight, bubbleWidth, bubbleHeight;
    private int paddingLeft, paddingTop, paddingRight, paddingBottom;
    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;

    public SpeechBubbleView(Context context) {
        super(context);
        init(null, 0);
    }

    public SpeechBubbleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public SpeechBubbleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.SpeechBubbleView, defStyle, 0);

        speechText = a.getString(
                R.styleable.SpeechBubbleView_speechText);

        bubbleType = a.getInt(R.styleable.SpeechBubbleView_bubbleType, 0);
        a.recycle();


        paddingLeft = getPaddingLeft();
        paddingTop = getPaddingTop();
        paddingRight = getPaddingRight();
        paddingBottom = getPaddingBottom();

        paddingLeft = 40;
        paddingRight = 40;
        paddingTop = 10;
        paddingBottom = 10;


        // Set up a default TextPaint object
        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();

    }

    private void invalidateTextPaintAndMeasurements() {

//        mTextPaint.setTextSize(mExampleDimension);
        int scaledSize = getResources().getDimensionPixelSize(R.dimen.speechFontSize);
        mTextPaint.setTextSize(scaledSize);

        mTextPaint.setColor(Color.WHITE);
        mTextWidth = mTextPaint.measureText(speechText);

/*
        if (mTextWidth < (this.getWidth() - paddingLeft - paddingRight)) {
            int newScaledSize = getResources().getDimensionPixelSize(R.dimen.speechFontSize) + 4;
            mTextPaint.setTextSize(newScaledSize);
            mTextWidth = mTextPaint.measureText(speechText);
        }
*/
        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        switch (bubbleType) {
            case 0:
                Style.drawSpeechBubble(canvas, new RectF(0, 0, getWidth(), getHeight()), Style.ResizingBehavior.AspectFit);
                break;
            case 1:
                Style.drawSpeechBubbleRed(canvas, new RectF(0, 0, getWidth(), getHeight()), Style.ResizingBehavior.AspectFit);
                break;
            case 2:
                Style.drawSpeechBubbleGrey(canvas, new RectF(0, 0, getWidth(), getHeight()), Style.ResizingBehavior.AspectFit);
                break;
            case 3:
                Style.drawSpeechBubbleGreyLarge(canvas, new RectF(0, 0, getWidth(), getHeight()), Style.ResizingBehavior.AspectFit);
                break;
        }


        // Draw the text.
        contentWidth = getWidth() - paddingLeft - paddingRight;
        contentHeight = getHeight() - paddingTop - paddingBottom;

        //Logger.d(TAG, "onDraw(). contentWidth: " + String.valueOf(contentWidth) + ". contentHeight: " + String.valueOf(contentHeight));

        /*
        canvas.drawText(speechText,
                paddingLeft + (contentWidth - mTextWidth) / 2,
                paddingTop + (contentHeight + mTextHeight) / 2,
                mTextPaint);
*/

        // init StaticLayout for text
        StaticLayout textLayout = new StaticLayout(
                speechText, mTextPaint, contentWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);

// get height of multiline text
        int textHeight = textLayout.getHeight() - paddingTop - paddingBottom;

// get position of text's top left corner
        float x = (getWidth() - contentWidth)/2;
        float y = (getHeight() - 72 - textHeight)/2;

// draw text to the Canvas center
        canvas.save();
        canvas.translate(x, y);
        textLayout.draw(canvas);
        canvas.restore();

    }

    /**
     * Gets the example string attribute value.
     *
     * @return The example string attribute value.
     */
    public String getSpeechText() {
        return speechText;
    }


    /**
     * Sets the view's example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param newSpeechText The example string attribute value to use.
     */
    public void setSpeechText(String newSpeechText) {
        speechText = newSpeechText;
        invalidateTextPaintAndMeasurements();
        invalidate();
    }


}
