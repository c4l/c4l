package au.com.connetica.chatsforlife.forms;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import au.com.connetica.chatsforlife.FollowupActivity;
import au.com.connetica.chatsforlife.Logger;
import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.ReminderManager;
import au.com.connetica.chatsforlife.SpeechBubbleView;
import au.com.connetica.chatsforlife.Style;
import au.com.connetica.chatsforlife.TranscriptActivity;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.model.SharedPref;
import au.com.connetica.chatsforlife.Logger;

/** Setting Up Your Conversation **/

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link P4Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link P4Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class P4Fragment extends VideoPlayingBaseFragment implements View.OnClickListener {
    private static final String TAG = "P4Fragment";

    public static final int EXOPLAYER_RESOURCE_ID = R.id.ev2;
    private static final int VIDEO_RESOURCE_ID = R.raw.ev2_720;
    private static final String SUBTITLE_FILE = "file:///android_asset/ev2.srt";
    private static final String ISFIRSTRUN = SharedPref.ISFIRSTRUN_EV2;

    private static final int TRANSCRIPT_IMAGE_ID = R.drawable.ev2cover;
    private static final int TRANSCRIPT_HTML_ID = R.string.EV2;

    private SpeechBubbleView speechView1, speechView2, speechView3, speechView4, speechView5;
    private ImageButton transcriptButton;
    private EditText editTextName, p4s2editText, p4s4editText;
    private TextView p4s4TextView, p4s5textView;
    private Button  p4s5Button, p4s5AddToCalendarButton, nextButton;
    private LinearLayout p4_s3_layout;
    private CheckedTextView p4_s3_i1_1, p4_s3_i1_2, p4_s3_i1_3, p4_s3_i1_4, p4_s3_i1_5, p4_s3_i1_6;

    // the fragment initialization parameters
    private static final String ARG_PARAM1 = "param1";
    //private UUID mPlanId;  // id of the current plan
    //private Plan mPlan; // the current plan
    private DataModel mDataModel = DataModel.get(getActivity());    // init DataModel singleton
    private boolean mPageDirty = false; // flag for whether data has changed and to save the page

    private int mYear, mMonth, mDay; // date returned from date picker

    private OnFragmentInteractionListener mListener;

    public P4Fragment() { } // Required empty public constructor

    public static P4Fragment newInstance() {
        P4Fragment fragment = new P4Fragment();

        fragment.exoPlayerResourceId = EXOPLAYER_RESOURCE_ID;
        fragment.videoResourceId = VIDEO_RESOURCE_ID;
        fragment.subtitleFile = SUBTITLE_FILE;
//        fragment.isFirstRunPreference = ISFIRSTRUN;   // P4 - don't set this, check first run in onCreateView

        return fragment;
    }

    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);

    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //View v = inflater.inflate(R.layout.fragment_p4, container, false);
        // Inflate the layout for this fragment using superclass
        View v = inflateFragment(R.layout.fragment_p4, inflater, container);

        // If portrait, show buttons, speech views, etc
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {

            speechView1 = (SpeechBubbleView) v.findViewById(R.id.p4SpeechView1);
            speechView2 = (SpeechBubbleView) v.findViewById(R.id.p4SpeechView2);
            speechView3 = (SpeechBubbleView) v.findViewById(R.id.p4SpeechView3);
            speechView4 = (SpeechBubbleView) v.findViewById(R.id.p4SpeechView4);
            speechView5 = (SpeechBubbleView) v.findViewById(R.id.p4SpeechView5);

            transcriptButton = (ImageButton) v.findViewById(R.id.p4TranscriptButton);
            transcriptButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "transcriptButton onClickListener()");
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), TranscriptActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("transcriptImage", TRANSCRIPT_IMAGE_ID);
                    intent.putExtra("transcriptHtml", TRANSCRIPT_HTML_ID);
                    startActivity(intent);
                }
            });

            editTextName = (EditText) v.findViewById(R.id.p4editTextName);
            EditFocusChangeListener editFocusChangeListener = new EditFocusChangeListener();
            EditTextWatcher editTextWatcher = new EditTextWatcher();
            editTextName.addTextChangedListener(editTextWatcher);
            editTextName.setOnFocusChangeListener(editFocusChangeListener);

            /**
            // Respond to tapping 'Done' button on name field
            editTextName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        // hide virtual keyboard
                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(editTextName.getWindowToken(), 0);

                        String newName = editTextName.getEditableText().toString();
                        if (newName != null && !newName.isEmpty()) {
                            if (mDataModel.newPlan) { // add new plan to the database
                                Logger.d(TAG, "Adding new plan to the database = " + mDataModel.plan);
                                mDataModel.addPlan(mDataModel.plan);
                                mDataModel.newPlan = false;
                                if (mListener != null) {    // notify listener that a new plan has been created
                                    mListener.newPlanCreated();
                                }
                            }
                            updateSectionTitlesWithName();
                            showLaterSections();
                        }
                        return true;
                    }
                    return false;
                }
            });
***/

            p4s2editText = (EditText) v.findViewById(R.id.p4s2editText);
            p4s2editText.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p4s2editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            p4s2editText.addTextChangedListener(editTextWatcher);
            p4s2editText.setOnFocusChangeListener(editFocusChangeListener);

            p4_s3_layout = (LinearLayout) v.findViewById(R.id.p4_s3_layout);
            p4_s3_i1_1 = (CheckedTextView) v.findViewById(R.id.p4_s3_i1_1);
            p4_s3_i1_2 = (CheckedTextView) v.findViewById(R.id.p4_s3_i1_2);
            p4_s3_i1_3 = (CheckedTextView) v.findViewById(R.id.p4_s3_i1_3);
            p4_s3_i1_4 = (CheckedTextView) v.findViewById(R.id.p4_s3_i1_4);
            p4_s3_i1_5 = (CheckedTextView) v.findViewById(R.id.p4_s3_i1_5);
            p4_s3_i1_6 = (CheckedTextView) v.findViewById(R.id.p4_s3_i1_6);
            p4_s3_i1_1.setOnClickListener(this);
            p4_s3_i1_2.setOnClickListener(this);
            p4_s3_i1_3.setOnClickListener(this);
            p4_s3_i1_4.setOnClickListener(this);
            p4_s3_i1_5.setOnClickListener(this);
            p4_s3_i1_6.setOnClickListener(this);

            p4s4editText = (EditText) v.findViewById(R.id.p4s4editText);
            p4s4editText.setImeOptions(EditorInfo.IME_ACTION_DONE); // change return to done button
            p4s4editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            p4s4editText.addTextChangedListener(editTextWatcher);
            p4s4editText.setOnFocusChangeListener(editFocusChangeListener);
            p4s4TextView = (TextView) v.findViewById(R.id.p4s4textView); // hint text

            p4s5Button = (Button) v.findViewById(R.id.p4s5Button); // schedule button
            p4s5Button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker();
                }
            });
            p4s5textView = (TextView) v.findViewById(R.id.p4s5textView); // hint text

            p4s5AddToCalendarButton = (Button) v.findViewById(R.id.p4s5AddToCalendarButton); // add to calendar button
            //p4s5AddToCalendarButton.setCompoundDrawables(new BitmapDrawable(getResources(), Style.imageOfCalendarCompleted()),null, null, null);  // add image to left of button
            p4s5AddToCalendarButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "nextButtonp4s5AddToCalendarButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }

                    // Add appointment to device calendar
                    Date planDate = mDataModel.plan.getPlanDate();
                    if (planDate != null && planDate.getTime() > 0) {   // a date exists
                        final Calendar c = Calendar.getInstance();
                        c.setTime(planDate);
                        ReminderManager.addToCalendar(getActivity().getApplicationContext(), "Chat with " + mDataModel.plan.getName(), "",
                                mDataModel.plan.getP4s4(), c);
                    }
                }
            });


            nextButton = (Button) v.findViewById(R.id.p4nextbutton);
            nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "nextButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }

                    if (mListener != null) {
                        mListener.nextPage(); // send data back to the activity
                    }
                }
            });


            // If new plan , hide sections until name exists
            if (mDataModel.plan.getName() == null) {
                hideLaterSections();
            } else {

                // if plan exists, fill fields with initial data
                if (mDataModel.plan.getName() != null) {
                    editTextName.setText(mDataModel.plan.getName());
                    updateSectionTitlesWithName();
                }

                if (mDataModel.plan.getP4s2() != null) {
                    p4s2editText.setText(mDataModel.plan.getP4s2());
                }

                p4_s3_i1_1.setChecked(mDataModel.plan.getP4_s3_i1_1());
                p4_s3_i1_1.setTypeface(p4_s3_i1_1.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p4_s3_i1_2.setChecked(mDataModel.plan.getP4_s3_i1_2());
                p4_s3_i1_2.setTypeface(p4_s3_i1_2.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p4_s3_i1_3.setChecked(mDataModel.plan.getP4_s3_i1_3());
                p4_s3_i1_3.setTypeface(p4_s3_i1_3.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p4_s3_i1_4.setChecked(mDataModel.plan.getP4_s3_i1_4());
                p4_s3_i1_4.setTypeface(p4_s3_i1_4.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p4_s3_i1_5.setChecked(mDataModel.plan.getP4_s3_i1_5());
                p4_s3_i1_5.setTypeface(p4_s3_i1_5.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                p4_s3_i1_6.setChecked(mDataModel.plan.getP4_s3_i1_6());
                p4_s3_i1_6.setTypeface(p4_s3_i1_6.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);

                if (mDataModel.plan.getP4s4() != null) {
                    p4s4editText.setText(mDataModel.plan.getP4s4());
                }

                if (mDataModel.plan.getPlanDate() != null && mDataModel.plan.getPlanDate().getTime() != 0) {    // a date has been set
                    SimpleDateFormat formatter = new SimpleDateFormat("MMM d 'at' h:mm a");     //  "MMM d 'at' h:mma"
                    p4s5Button.setText(formatter.format(mDataModel.plan.getPlanDate()));
                } else {
                    p4s5AddToCalendarButton.setVisibility(View.GONE);
                }

            }


            // Auto-play video on first run
            boolean isFirstRun = SharedPref.read(ISFIRSTRUN, true);
            if (isFirstRun) {
                SharedPref.write(ISFIRSTRUN, false);
                playWhenReady = true;
                if (player != null) {
                    Logger.d(TAG, "isFirstRun true - playWhenReady true");
                    player.setPlayWhenReady(true); // start playback
                }
            }

        }

        toggleSystemUi();   // show system UI in portrait, hide in landscape


        return v;
    }

    public void hideLaterSections() {
        speechView2.setVisibility(View.INVISIBLE);
        speechView3.setVisibility(View.INVISIBLE);
        speechView4.setVisibility(View.INVISIBLE);
        speechView5.setVisibility(View.INVISIBLE);
        p4s2editText.setVisibility(View.INVISIBLE);
        p4_s3_layout.setVisibility(View.INVISIBLE);
        p4s4editText.setVisibility(View.INVISIBLE);
        p4s4TextView.setVisibility(View.INVISIBLE);
        p4s5Button.setVisibility(View.INVISIBLE);
        p4s5AddToCalendarButton.setVisibility(View.GONE);
        p4s5textView.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
    }

    public void showLaterSections() {
        speechView2.setVisibility(View.VISIBLE);
        speechView3.setVisibility(View.VISIBLE);
        speechView4.setVisibility(View.VISIBLE);
        speechView5.setVisibility(View.VISIBLE);
        p4s2editText.setVisibility(View.VISIBLE);
        p4_s3_layout.setVisibility(View.VISIBLE);
        p4s4editText.setVisibility(View.VISIBLE);
        p4s4TextView.setVisibility(View.VISIBLE);
        p4s5Button.setVisibility(View.VISIBLE);
        if (mDataModel.plan.getPlanDate() != null && mDataModel.plan.getPlanDate().getTime() != 0) {    // a date has been set
            p4s5AddToCalendarButton.setVisibility(View.VISIBLE);
        } else {
            p4s5AddToCalendarButton.setVisibility(View.GONE);
        }
        p4s5textView.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);
    }

    public void updateSectionTitlesWithName() {
        speechView2.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s2_title)));
        speechView3.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s3_title)));
        speechView4.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s4_title)));
        speechView5.setSpeechText(mDataModel.plan.replaceTokenWithName(getString(R.string.p4_s5_title)));
    }

    // Save changes to plan on any change to edit text
    private class EditTextWatcher implements TextWatcher {
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            mPageDirty = true;  // flag page to be saved

            if (editable == editTextName.getEditableText()) {   // name field
                String newName = editTextName.getEditableText().toString();
                boolean mNewPlan = (mDataModel.plan.getName() == null); // new plan?

                mDataModel.plan.setName(newName);
                updateSectionTitlesWithName();

                if (mNewPlan && mListener != null) {    // if new plan, notify listener that a new plan has been created and show hidden sections
                    mDataModel.updatePlan(mDataModel.plan);
                    mPageDirty = false;
                    mListener.newPlanCreated();
                    showLaterSections();

                    // Send Broadcast message that name has changed
                    Logger.d("sender", "Broadcasting message: " + newName);
                    Intent intent = new Intent("event-name-changed");
                    // You can also include some extra data.
                    intent.putExtra("newName", newName);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                }

                Logger.d(TAG, "afterTextChanged. name = " + mDataModel.plan.getName());
            } else if (editable == p4s2editText.getEditableText()) {
                mDataModel.plan.setP4s2(editable.toString());
                Logger.d(TAG, "afterTextChanged. p4s2 = " + mDataModel.plan.getP4s2());
            } else if (editable == p4s4editText.getEditableText()) {
                mDataModel.plan.setP4s4(editable.toString());
                Logger.d(TAG, "afterTextChanged. p4s4 = " + mDataModel.plan.getP4s4());
            }
        }
    }

    // Save updated mPlan to the database on any lost focus from edit text field
    private class EditFocusChangeListener implements View.OnFocusChangeListener {
        public void onFocusChange(View v, boolean hasFocus) {

            pauseVideoPlayback();

            if (!hasFocus && mPageDirty) {    // save to database
                Logger.d(TAG, "editFocusChangeListener - view id: " + v.getId() + ". Saving plan to database " + mDataModel.plan);
                if (mDataModel.plan != null) {
                    mDataModel.updatePlan(mDataModel.plan);
                }
                mPageDirty = false;

                if (v.getId() == R.id.p4editTextName) { // name has changed
                    String newName = editTextName.getEditableText().toString();
                    if (newName != null) {
                        // Send Broadcast message that name has changed
                        Logger.d("sender", "Broadcasting message");
                        Intent intent = new Intent("event-name-changed");
                        // You can also include some extra data.
                        intent.putExtra("newName", newName);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                        updateSectionTitlesWithName();

                    }
                }



            }
        }
    }

    // click listener for checkboxes - save change directly to mPlan and the database
    public void onClick(View v) {
        ((CheckedTextView) v).toggle();

        switch (v.getId()) {
            case R.id.p4_s3_i1_1:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p4_s3_i1_1.isChecked = " + p4_s3_i1_1.isChecked());
                mDataModel.plan.setP4_s3_i1_1(p4_s3_i1_1.isChecked());
                p4_s3_i1_1.setTypeface(p4_s3_i1_1.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                break;

            case R.id.p4_s3_i1_2:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p4_s3_i1_2.isChecked = " + p4_s3_i1_2.isChecked());
                p4_s3_i1_2.setTypeface(p4_s3_i1_2.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                mDataModel.plan.setP4_s3_i1_2(p4_s3_i1_2.isChecked());
                break;

            case R.id.p4_s3_i1_3:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p4_s3_i1_3.isChecked = " + p4_s3_i1_3.isChecked());
                p4_s3_i1_3.setTypeface(p4_s3_i1_3.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                mDataModel.plan.setP4_s3_i1_3(p4_s3_i1_3.isChecked());
                break;

            case R.id.p4_s3_i1_4:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p4_s3_i1_4.isChecked = " + p4_s3_i1_4.isChecked());
                p4_s3_i1_4.setTypeface(p4_s3_i1_4.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                mDataModel.plan.setP4_s3_i1_4(p4_s3_i1_4.isChecked());
                break;

            case R.id.p4_s3_i1_5:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p4_s3_i1_5.isChecked = " + p4_s3_i1_5.isChecked());
                p4_s3_i1_5.setTypeface(p4_s3_i1_5.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                mDataModel.plan.setP4_s3_i1_5(p4_s3_i1_5.isChecked());
                break;

            case R.id.p4_s3_i1_6:
                Logger.d(TAG, "onClick() - id: " + v.getId() + ". p4_s3_i1_6.isChecked = " + p4_s3_i1_6.isChecked());
                p4_s3_i1_6.setTypeface(p4_s3_i1_6.isChecked() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
                mDataModel.plan.setP4_s3_i1_6(p4_s3_i1_6.isChecked());
                break;

            default: break;
        }

        // save the change immediately to the database
        Logger.d(TAG, "Saving plan to database " + mDataModel.plan);
        mDataModel.updatePlan(mDataModel.plan);
        mPageDirty = false;
    }

    private void showDatePicker() {
        // Default value: use saved date - if not, use today's date
        final Calendar c = Calendar.getInstance();

        Date planDate = mDataModel.plan.getPlanDate();
        if (planDate != null && planDate.getTime() > 0) {   // a date is already schedule
            c.setTime(planDate);
        }
        final int mDefaultYear = c.get(Calendar.YEAR);
        final int mDefaultMonth = c.get(Calendar.MONTH);
        final int mDefaultDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this.getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                        //  mDateTime = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        showTimePicker();
                    }
                }, mDefaultYear, mDefaultMonth, mDefaultDay);
        datePickerDialog.show();
    }

    private void showTimePicker() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        Date planDate = mDataModel.plan.getPlanDate();
        if (planDate != null && planDate.getTime() > 0) {   // a date is already scheduled
            c.setTime(planDate);
        }
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this.getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        // friendly format - eg. 'Jul 6 at 4:15pm'
                        c.set(mYear, mMonth, mDay,hourOfDay, minute);
                        Date newDate = c.getTime();
                        SimpleDateFormat formatter = new SimpleDateFormat("MMM d 'at' h:mm a");     //  "MMM d 'at' h:mma"
                        p4s5Button.setText(formatter.format(newDate));

                        p4s5AddToCalendarButton.setVisibility(View.VISIBLE);

                        mDataModel.plan.setPlanDate(newDate);
                        mDataModel.plan.setHadChat(false);
                        mDataModel.plan.setIsFollowup(false);


                        // Cancel any existing scheduled reminder
                        int previousReminderId = mDataModel.plan.getChatReminderId();
                        if (previousReminderId > 0 ) {  // cancel any existing scheduled reminder
                            ReminderManager.cancelReminder(getActivity().getApplicationContext(), previousReminderId);
                        }

                        // Get a unique ID for the new scheduled reminder
                        int reminderId = SharedPref.read(SharedPref.LAST_REMINDER_ID, 0) + 1;
                        SharedPref.write(SharedPref.LAST_REMINDER_ID, reminderId);
                        mDataModel.plan.setChatReminderId(reminderId);
                        mDataModel.updatePlan(mDataModel.plan);   //  save to the database
                        mPageDirty = false;

                        // schedule a reminder notification
                        ReminderManager.setReminder(getActivity().getApplicationContext(), reminderId, "Chat with " + mDataModel.plan.getName(), c);

                        // Send Broadcast message that name/date has changed
                        Logger.d("sender", "Broadcasting message: ");
                        Intent intent = new Intent("event-name-changed");
                        // You can also include some extra data.
                        //intent.putExtra("newName", newName);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    @Override
    public void onAttach(Context context) {
        Logger.d(TAG, "onAttach()");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;    // connect to the listener in the activity
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Logger.d(TAG, "onDetach()");
        super.onDetach();
        mListener = null;
    }




    /** Fragment Lifecycle **/
/*
    // When the fragment becomes visible/invisible to the user
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            Logger.d(TAG, "isVisibleToUser true");

        } else {
            Logger.d(TAG, "isVisibleToUser false");
        }
    }
*/

    /*
    @Override
    public void onPause() {
        Logger.d(TAG, "onPause()");
        super.onPause();

        // Back button tapped while on Name field
        if (getActivity().getCurrentFocus().getId() == R.id.p4editTextName && mPageDirty) {
            String newName = editTextName.getEditableText().toString();
            if (newName != null && !newName.isEmpty()) {
                if (mNewPlan) { // add new plan to the database
                    Logger.d(TAG, "onPause() - Adding new plan to the database = " + mPlan);
                    mPageDirty = false;
                    mDataModel.addPlan(mPlan);
                    mNewPlan = false;

                } else {
                    Logger.d(TAG, "onPause() - Updating plan " + mPlan);
                    mPageDirty = false;
                    mDataModel.updatePlan(mPlan);
                }
            }
        }
     }
*/

    // save player state before all members are gone forever :D
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);

        if (mPageDirty && mDataModel.plan != null) {    // save to database
            Logger.d(TAG, "onSaveInstanceState(). Saving plan to database " + mDataModel.plan);
            mDataModel.updatePlan(mDataModel.plan);
            mPageDirty = false;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void newPlanCreated();
        void nextPage();
        void previousPage();
    }
}
