package au.com.connetica.chatsforlife.forms;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.UUID;

import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.SpeechBubbleView;
import au.com.connetica.chatsforlife.TranscriptActivity;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.model.SharedPref;
import au.com.connetica.chatsforlife.Logger;

/** P8. Confident In Any Conversation **/

public class P8Fragment extends VideoPlayingBaseFragment {
    private static final String TAG = "P8Fragment";

    public static final int EXOPLAYER_RESOURCE_ID = R.id.ev6;
    public static final int VIDEO_RESOURCE_ID = R.raw.ev6_720;
    public static final String SUBTITLE_FILE = "file:///android_asset/ev6.srt";
    private static final String ISFIRSTRUN = SharedPref.ISFIRSTRUN_EV6;

    private static final int TRANSCRIPT_IMAGE_ID = R.drawable.ev6cover;
    private static final int TRANSCRIPT_HTML_ID = R.string.EV6;

    private Button previousButton, doneButton;
    private ImageButton transcriptButton;

    private OnFragmentInteractionListener mListener;

    public P8Fragment() { } // Required empty public constructor

    public static P8Fragment newInstance() {
        P8Fragment fragment = new P8Fragment();
        fragment.exoPlayerResourceId = EXOPLAYER_RESOURCE_ID;
        fragment.videoResourceId = VIDEO_RESOURCE_ID;
        fragment.subtitleFile = SUBTITLE_FILE;
        fragment.isFirstRunPreference = ISFIRSTRUN;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment using superclass
        View v = inflateFragment(R.layout.fragment_p8, inflater, container);

        // If portrait, show buttons, speech views, etc
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {

            transcriptButton = (ImageButton) v.findViewById(R.id.p4TranscriptButton);
            transcriptButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "transcriptButton onClickListener()");
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), TranscriptActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("transcriptImage", TRANSCRIPT_IMAGE_ID);
                    intent.putExtra("transcriptHtml", TRANSCRIPT_HTML_ID);
                    startActivity(intent);
                }
            });

            previousButton = (Button) v.findViewById(R.id.p8previousbutton);
            previousButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "previousButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }
                    if (mListener != null) {
                        mListener.previousPage(); // send data back to the activity
                    }
                }
            });

            doneButton = (Button) v.findViewById(R.id.p8donebutton);
            doneButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "doneButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }
                    getActivity().finish();
//
//                    if (mListener != null) {
//                        mListener.nextPage(); // send data back to the activity
//                    }
                }
            });

        }

        toggleSystemUi();       // show system UI in portrait, hide in landscape
        return v;
    }


    @Override
    public void onAttach(Context context) {
        Logger.d(TAG, "onAttach()");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;    // connect to the listener in the activity
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Logger.d(TAG, "onDetach()");
        super.onDetach();
        mListener = null;
    }


    /** Fragment Lifecycle **/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void nextPage();
        void previousPage();
    }
}
