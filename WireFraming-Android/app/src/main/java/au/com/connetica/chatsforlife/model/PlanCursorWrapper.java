package au.com.connetica.chatsforlife.model;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.Date;
import java.util.UUID;

import au.com.connetica.chatsforlife.model.DBHelper.PlanTableSchema;

/**
 * Custom cursor wrapper for retrieving plans from database
 *
 * Created by peter on 2/7/17.
 */

public class PlanCursorWrapper extends CursorWrapper {
    public PlanCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Plan getPlan() {
        String uuidString = getString(getColumnIndex(PlanTableSchema.COLUMN_UUID));
        String name = getString(getColumnIndex(PlanTableSchema.COLUMN_NAME));
        long planDate = getLong(getColumnIndex(PlanTableSchema.COLUMN_PLAN_DATE));
        long followupDate = getLong(getColumnIndex(PlanTableSchema.COLUMN_FOLLOWUP_DATE));

        boolean hadChat = (getInt(getColumnIndex(PlanTableSchema.COLUMN_HAD_CHAT)) == 1);
        boolean isFollowUp = (getInt(getColumnIndex(PlanTableSchema.COLUMN_IS_FOLLOWUP)) == 1);
        int chatReminderId = getInt(getColumnIndex(PlanTableSchema.COLUMN_CHAT_REMINDER_ID));

        String p4s2 = getString(getColumnIndex(PlanTableSchema.COLUMN_P4S2));
        boolean p4_s3_i1_1 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P4_S3_I1_1)) == 1);
        boolean p4_s3_i1_2 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P4_S3_I1_2)) == 1);
        boolean p4_s3_i1_3 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P4_S3_I1_3)) == 1);
        boolean p4_s3_i1_4 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P4_S3_I1_4)) == 1);
        boolean p4_s3_i1_5 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P4_S3_I1_5)) == 1);
        boolean p4_s3_i1_6 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P4_S3_I1_6)) == 1);
        String p4s4 = getString(getColumnIndex(PlanTableSchema.COLUMN_P4S4));

        String p5s1 = getString(getColumnIndex(PlanTableSchema.COLUMN_P5S1));
        boolean p5_s2_i1_1 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S2_I1_1)) == 1);
        boolean p5_s2_i1_2 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S2_I1_2)) == 1);
        boolean p5_s2_i1_3 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S2_I1_3)) == 1);
        boolean p5_s2_i1_4 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S2_I1_4)) == 1);
        boolean p5_s2_i1_5 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S2_I1_5)) == 1);
        String p5_s2_other = getString(getColumnIndex(PlanTableSchema.COLUMN_P5_S2_OTHER));
        boolean p5_s3_i1_1 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_1)) == 1);
        boolean p5_s3_i1_2 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_2)) == 1);
        boolean p5_s3_i1_3 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_3)) == 1);
        boolean p5_s3_i1_4 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_4)) == 1);
        boolean p5_s3_i1_5 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_5)) == 1);
        boolean p5_s3_i1_6 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_6)) == 1);
        boolean p5_s3_i1_7 = (getInt(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_I1_7)) == 1);
        String p5_s3_other = getString(getColumnIndex(PlanTableSchema.COLUMN_P5_S3_OTHER));
        String p5s4 = getString(getColumnIndex(PlanTableSchema.COLUMN_P5S4));
        String p5s5 = getString(getColumnIndex(PlanTableSchema.COLUMN_P5S5));

        String followups3 = getString(getColumnIndex(PlanTableSchema.COLUMN_FOLLOWUPS3));
        String followups5 = getString(getColumnIndex(PlanTableSchema.COLUMN_FOLLOWUPS5));

        Plan plan = new Plan(UUID.fromString(uuidString));
        plan.setName(name);
        plan.setPlanDate(new Date(planDate));
        plan.setFollowupDate(new Date(followupDate));
        plan.setHadChat(hadChat);
        plan.setIsFollowup(isFollowUp);
        plan.setChatReminderId(chatReminderId);

        plan.setP4s2(p4s2);
        plan.setP4_s3_i1_1(p4_s3_i1_1);
        plan.setP4_s3_i1_2(p4_s3_i1_2);
        plan.setP4_s3_i1_3(p4_s3_i1_3);
        plan.setP4_s3_i1_4(p4_s3_i1_4);
        plan.setP4_s3_i1_5(p4_s3_i1_5);
        plan.setP4_s3_i1_6(p4_s3_i1_6);
        plan.setP4s4(p4s4);

        plan.setP5s1(p5s1);
        plan.setP5_s2_i1_1(p5_s2_i1_1);
        plan.setP5_s2_i1_2(p5_s2_i1_2);
        plan.setP5_s2_i1_3(p5_s2_i1_3);
        plan.setP5_s2_i1_4(p5_s2_i1_4);
        plan.setP5_s2_i1_5(p5_s2_i1_5);
        plan.setP5_s2_other(p5_s2_other);
        plan.setP5_s3_i1_1(p5_s3_i1_1);
        plan.setP5_s3_i1_2(p5_s3_i1_2);
        plan.setP5_s3_i1_3(p5_s3_i1_3);
        plan.setP5_s3_i1_4(p5_s3_i1_4);
        plan.setP5_s3_i1_5(p5_s3_i1_5);
        plan.setP5_s3_i1_6(p5_s3_i1_6);
        plan.setP5_s3_i1_7(p5_s3_i1_7);
        plan.setP5_s3_other(p5_s3_other);
        plan.setP5s4(p5s4);
        plan.setP5s5(p5s5);

        plan.setFollowups3(followups3);
        plan.setFollowups5(followups5);

        return plan;
    }
}
