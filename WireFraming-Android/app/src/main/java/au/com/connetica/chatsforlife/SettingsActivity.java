package au.com.connetica.chatsforlife;

import android.app.FragmentManager;
import android.preference.SwitchPreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.Preference;

import org.wordpress.passcodelock.AppLockManager;
import org.wordpress.passcodelock.PasscodePreferenceFragment;

import au.com.connetica.chatsforlife.R;

public class SettingsActivity extends AppCompatActivity {
    public static final String KEY_PREF_SUBTITLES = "pref_subtitles";
    public static final String KEY_PREF_PASSCODE = "pref_passcode";
    private static final String KEY_PASSCODE_FRAGMENT = "passcode-fragment";
    private static final String KEY_PREFERENCE_FRAGMENT = "preference-fragment";
    private PasscodePreferenceFragment mPasscodePreferenceFragment;
    private SettingsFragment mSettingsFragment;

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button

        /*
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
*/

        FragmentManager fragmentManager = getFragmentManager();
        mSettingsFragment = (SettingsFragment) fragmentManager.findFragmentByTag(KEY_PREFERENCE_FRAGMENT);

        mPasscodePreferenceFragment = (PasscodePreferenceFragment) fragmentManager.findFragmentByTag(KEY_PASSCODE_FRAGMENT);
        if (mPasscodePreferenceFragment == null) {
            Bundle passcodeArgs = new Bundle();
            passcodeArgs.putBoolean(PasscodePreferenceFragment.KEY_SHOULD_INFLATE, false);
            mSettingsFragment = new SettingsFragment();
            mPasscodePreferenceFragment = new PasscodePreferenceFragment();
            mPasscodePreferenceFragment.setArguments(passcodeArgs);

            fragmentManager.beginTransaction()
                    .replace(android.R.id.content, mPasscodePreferenceFragment, KEY_PASSCODE_FRAGMENT)
                    .add(android.R.id.content, mSettingsFragment, KEY_PREFERENCE_FRAGMENT)
                    .commit();
        }
        //setContentView(R.layout.activity_settings);
    }

    @Override
    public void onStart() {
        super.onStart();

        Preference togglePreference = mSettingsFragment.findPreference(
                getString(org.wordpress.passcodelock.R.string.pref_key_passcode_toggle));
        Preference changePreference = mSettingsFragment.findPreference(
                getString(org.wordpress.passcodelock.R.string.pref_key_change_passcode));

        if (togglePreference != null && changePreference != null) {
            mPasscodePreferenceFragment.setPreferences(togglePreference, changePreference);
            ((SwitchPreference) togglePreference).setChecked(
                    AppLockManager.getInstance().getAppLock().isPasswordLocked());
        }
    }

}
