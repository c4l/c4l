package au.com.connetica.chatsforlife;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import au.com.connetica.chatsforlife.R;

import static au.com.connetica.chatsforlife.R.id.crisisPhone1;

public class CrisisActivity extends AppCompatActivity {
    private static final String TAG = "CrisisActivity";
    private TextView crisisPhone1, crisisPhone2, crisisPhone3;

    // Inflate the menu_crisis from resources
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_crisis, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // Handle menu choices - start Settings Activity
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crisis);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button


        LinearLayout layout1 = (LinearLayout) findViewById(R.id.crisisLayout1);
        crisisPhone1 = (TextView) findViewById(R.id.crisisPhone1);

        LinearLayout layout2 = (LinearLayout) findViewById(R.id.crisisLayout2);
        crisisPhone2 = (TextView) findViewById(R.id.crisisPhone2);

        LinearLayout layout3 = (LinearLayout) findViewById(R.id.crisisLayout3);
        crisisPhone3 = (TextView) findViewById(R.id.crisisPhone3);

    }


    public void onClickLayout(View v) {

        switch (v.getId()) {
            case R.id.crisisLayout1:
                Logger.d(TAG, "onClickLayout() - " + crisisPhone1.getText());
                callPhone(crisisPhone1.getText().toString());
                break;

            case R.id.crisisLayout2:
                Logger.d(TAG, "onClickLayout() - " + crisisPhone2.getText());
                callPhone(crisisPhone2.getText().toString());
                break;

            case R.id.crisisLayout3:
                Logger.d(TAG, "onClickLayout() - " + crisisPhone3.getText());
                callPhone(crisisPhone3.getText().toString());
                break;

        }
    }

    private void callPhone(String phoneNumber) {
        String number = "tel:" + phoneNumber.trim();
        Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(number));
        startActivity(callIntent);
    }
}
