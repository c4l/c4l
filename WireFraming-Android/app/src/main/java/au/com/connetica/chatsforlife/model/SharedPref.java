package au.com.connetica.chatsforlife.model;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/*
SharedPreferences Singleton

Usage: Simply call SharedPref.init() on MainActivity once
SharedPref.init(getApplicationContext());

* Write Data *
    SharedPref.write(SharedPref.NAME, "XXXX");//save string in shared preference.
    SharedPref.write(SharedPref.AGE, "25");//save int in shared preference.
    SharedPref.write(SharedPref.IS_SELECT, true);//save boolean in shared preference.

* Read Data*
    String name = SharedPref.read(SharedPref.NAME, null);//read string in shared preference.
    int age = SharedPref.read(SharedPref.AGE, 0);//read int in shared preference.
    boolean isSelect = SharedPref.read(SharedPref.IS_SELECT, false);//read boolean in shared
 */

public class SharedPref {
    private static SharedPreferences mSharedPref;
    public static final String ISFIRSTRUN = "ISFIRSTRUN";
    public static final String ISFIRSTRUN_EV2 = "ISFIRSTRUN_EV2";
    public static final String ISFIRSTRUN_EV3 = "ISFIRSTRUN_EV3";
    public static final String ISFIRSTRUN_EV4 = "ISFIRSTRUN_EV4";
    public static final String ISFIRSTRUN_EV5 = "ISFIRSTRUN_EV5";
    public static final String ISFIRSTRUN_EV6 = "ISFIRSTRUN_EV6";
    public static final String LAST_REMINDER_ID = "LAST_REMINDER_ID";

    private SharedPref() { }

    public static void init(Context context) {
        if(mSharedPref == null)
            mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
    }

    public static String read(String key, String defValue) {
        return mSharedPref.getString(key, defValue);
    }

    public static void write(String key, String value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public static boolean read(String key, boolean defValue) {
        return mSharedPref.getBoolean(key, defValue);
    }

    public static void write(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }

    public static Integer read(String key, int defValue) {
        return mSharedPref.getInt(key, defValue);
    }

    public static void write(String key, Integer value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putInt(key, value).apply();
    }


}