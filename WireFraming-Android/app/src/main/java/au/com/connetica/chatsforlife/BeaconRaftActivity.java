package au.com.connetica.chatsforlife;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class BeaconRaftActivity extends AppCompatActivity {
    private static final String TAG = "BeaconRaftActivity";

    private ImageView headerImageView;
    private WebView beaconRaftWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_raft);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button

        headerImageView = (ImageView) findViewById(R.id.headerImageView);
        beaconRaftWebView = (WebView) findViewById(R.id.beaconRaftWebView);

        // get extras passed in
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int headerImageId = extras.getInt("headerImage");
            int htmlTextId = extras.getInt("htmlTextId");

            String htmlString = getString(htmlTextId);
            beaconRaftWebView.loadData(htmlString, "text/html", null);

            headerImageView.setImageResource(headerImageId);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_plan_view_pager, menu);
        return true;
    }

    // Handle menu choices - Crisis button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_crisis) {
            Intent intent = new Intent(getApplicationContext(), CrisisActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
