package au.com.connetica.chatsforlife;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.RawResourceDataSource;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

import org.wordpress.passcodelock.AppLockManager;
import org.wordpress.passcodelock.PasscodeManagePasswordActivity;

import au.com.connetica.chatsforlife.forms.PlanViewPager;

import static au.com.connetica.chatsforlife.R.xml.preferences;

public class Welcome extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "Welcome";
    private static final int VIDEO_RESOURCE_ID = R.raw.ev1_720;
    private static final String SUBTITLE_FILE = "file:///android_asset/ev1.srt";

    // constant fields for saving and restoring bundle
    public static final String SKIP_CLICKED = "skip_clicked";
    public static final String PLAYWHENREADY = "play_when_ready";
    public static final String CURRENT_WINDOW_INDEX = "current_window_index";
    public static final String PLAYBACK_POSITION = "playback_position";

    // used to remember the playback position
    private boolean skipClicked;
    private long playbackPosition;
    private boolean playWhenReady;
    private int currentWindow;

    private SimpleExoPlayerView playerView;
    private SimpleExoPlayer player;
    //private  videoListener;

    private Button skipButton;
    private SpeechBubbleView speechView3;
    private TextView textView4;
    private Switch switch1, switch2;
    private Button nextButton;
    public static final int ENABLE_PASSLOCK  = 0;
    public static final int DISABLE_PASSLOCK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        playerView = (SimpleExoPlayerView) findViewById(R.id.welcome_video);

        // if we have saved player state, restore it
        if (savedInstanceState != null) {
            skipClicked = savedInstanceState.getBoolean(SKIP_CLICKED, false);
            playbackPosition = savedInstanceState.getLong(PLAYBACK_POSITION, 0);
            currentWindow = savedInstanceState.getInt(CURRENT_WINDOW_INDEX, 0);
            playWhenReady = savedInstanceState.getBoolean(PLAYWHENREADY, false);
        } else { //  first load
            playWhenReady = true;
        }

        if (player != null) {
            player.setPlayWhenReady(playWhenReady);  // start auto playback
        }

        // If portrait, show buttons, speech views, etc
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            skipButton = (Button) findViewById(R.id.welcomeskipbutton);
            speechView3 = (SpeechBubbleView) findViewById(R.id.welcomeSpeechView3);
            textView4 = (TextView) findViewById(R.id.welcomeTextView4);
            switch1 = (Switch) findViewById(R.id.welcomeSwitch1);   // subtitles
            switch2 = (Switch) findViewById(R.id.welcomeSwitch2);   // passcode
            nextButton = (Button) findViewById(R.id.welcomeNextbutton);

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            switch1.setChecked(sharedPref.getBoolean(SettingsActivity.KEY_PREF_SUBTITLES, true));
            switch1.setOnCheckedChangeListener(this);

            switch2.setChecked(AppLockManager.getInstance().getAppLock().isPasswordLocked());
            switch2.setOnCheckedChangeListener(this);

            // Hide controls until Skip button is clicked
            speechView3.setVisibility(View.INVISIBLE);
            textView4.setVisibility(View.INVISIBLE);
            switch1.setVisibility(View.INVISIBLE);
            switch2.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);

            if (skipClicked) {
                skipButtonClicked();
            }

            // Setup button actions
            skipButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    skipButtonClicked();
                }
            });

            nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "nextButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            });
        }

        toggleSystemUi();

    }


    public void skipButtonClicked() {
        Logger.d(TAG, "skipButtonClicked()");
        skipClicked = true;
        if (player != null) {
            player.setPlayWhenReady(false); // pause playback
        }

        /****** BETA VERSION - go straight to main menu) ****/
        /*
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
*/

        speechView3.setVisibility(View.VISIBLE);
        textView4.setVisibility(View.VISIBLE);
        switch1.setVisibility(View.VISIBLE);
        switch2.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);
        skipButton.setVisibility(View.GONE);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        if (buttonView == switch1) {    // subtitles
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(SettingsActivity.KEY_PREF_SUBTITLES, isChecked);
            editor.apply();
        } else if (buttonView == switch2) { // passcode
            /*SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getString(org.wordpress.passcodelock.R.string.pref_key_passcode_toggle), isChecked);
            editor.commit();
*/
            int type = AppLockManager.getInstance().getAppLock().isPasswordLocked()
                    ? DISABLE_PASSLOCK : ENABLE_PASSLOCK;
            Intent i = new Intent(this, PasscodeManagePasswordActivity.class);
            i.putExtra(PasscodeManagePasswordActivity.KEY_TYPE, type);
            startActivityForResult(i, type);

        }

    }



/**** VIDEO USING EXOPLAYER2 ****/

    private void initializePlayer() {

        Logger.d(TAG, "initializePlayer(). playbackPosition: " + String.valueOf(playbackPosition));

        player = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());

        playerView.setPlayer(player);
        player.setPlayWhenReady(playWhenReady);
        player.seekTo(currentWindow, playbackPosition);   // resume playback position

        Uri uri = RawResourceDataSource.buildRawResourceUri(VIDEO_RESOURCE_ID);
        MediaSource mediaSource = buildMediaSource(uri);
        player.prepare(mediaSource, true, false);

    }

    private MediaSource buildMediaSource(Uri uri) {
        Logger.d(TAG, "buildMediaSource()");

        DataSpec dataSpec = new DataSpec(uri);
        final RawResourceDataSource rawResourceDataSource = new RawResourceDataSource(this);
        try {
            rawResourceDataSource.open(dataSpec);
        } catch (RawResourceDataSource.RawResourceDataSourceException e) {
            e.printStackTrace();
        }

        DataSource.Factory factory = new DataSource.Factory() {
            @Override
            public DataSource createDataSource() {
                return rawResourceDataSource;
            }
        };
        MediaSource videoSource = new ExtractorMediaSource(rawResourceDataSource.getUri(), factory,
                new DefaultExtractorsFactory(), null, null);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean showSubtitles = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SUBTITLES, true);
        if (showSubtitles) {
            // Merge in subtitle file
            Uri subtitleUri = Uri.parse(SUBTITLE_FILE);
            Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP,
                    null, Format.NO_VALUE, Format.NO_VALUE, "en", null);
            MediaSource subtitleSource = new SingleSampleMediaSource(subtitleUri,
                    new DefaultDataSourceFactory(this, "ua"), textFormat, C.TIME_UNSET);
            MergingMediaSource mergedSource = new MergingMediaSource(videoSource, subtitleSource);
            return mergedSource;
        } else {
            return videoSource;
        }

    }


    /** View Lifecycle **/

    // save player state before all members are gone forever :D
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
        /*
        * A simple configuration change such as screen rotation will destroy this activity
        * so we'll save the player state here in a bundle (that we can later access in onCreate) before everything is lost
        * NOTE: we cannot save player state in onDestroy like we did in onPause and onStop
        * the reason being our activity will be recreated from scratch and we would have lost all members (e.g. variables, objects) of this activity
        */
        outState.putBoolean(SKIP_CLICKED, skipClicked);

        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            outState.putLong(PLAYBACK_POSITION, playbackPosition);
            outState.putInt(CURRENT_WINDOW_INDEX, currentWindow);
            outState.putBoolean(PLAYWHENREADY, playWhenReady);
            Logger.d(TAG, outState.toString());
        }
    }


    @Override
    protected void onStart() {
        Logger.d(TAG, "onStart()");
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    protected void onStop() {
        Logger.d(TAG, "onStop()");
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }


    @Override
    protected void onPause() {
        Logger.d(TAG, "onPause()");
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }


    @Override
    protected void onResume() {
        Logger.d(TAG, "onResume()");
        super.onResume();
        toggleSystemUi();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    // Toggle showing the system UI depending on landscape or portrait mode
    private void toggleSystemUi() {
        Logger.d(TAG, "toggleSystemUi()");
        if (playerView == null) { return; }

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            playerView.setSystemUiVisibility(0);
        } else { // ** LANDSCAPE - hide UI **
            playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }


    private void releasePlayer() {
        Logger.d(TAG, "releasePlayer()");
        if (player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
            player.release();
            player = null;
        }
    }


}
