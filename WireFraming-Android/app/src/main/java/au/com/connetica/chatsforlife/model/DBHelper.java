package au.com.connetica.chatsforlife.model;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by peter on 2/7/17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";
    private static final int VERSION = 13;
    private static final String DATABASE_NAME = "plans.db";

    public static class PlanTableSchema implements BaseColumns {
        public static final String TABLE_NAME = "plan";
        public static final String COLUMN_UUID = "uuid";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_PLAN_DATE = "planDate";
        public static final String COLUMN_FOLLOWUP_DATE = "followupDate";

        public static final String COLUMN_HAD_CHAT = "hadChat";
        public static final String COLUMN_IS_FOLLOWUP = "isFollowup";
        public static final String COLUMN_CHAT_REMINDER_ID = "chatReminderId";

        public static final String COLUMN_P4S2 = "p4s2";
        public static final String COLUMN_P4_S3_I1_1 = "p4s3i1_1";
        public static final String COLUMN_P4_S3_I1_2 = "p4s3i1_2";
        public static final String COLUMN_P4_S3_I1_3 = "p4s3i1_3";
        public static final String COLUMN_P4_S3_I1_4 = "p4s3i1_4";
        public static final String COLUMN_P4_S3_I1_5 = "p4s3i1_5";
        public static final String COLUMN_P4_S3_I1_6 = "p4s3i1_6";
        public static final String COLUMN_P4S4 = "p4s4";

        public static final String COLUMN_P5S1 = "p5s1";
        public static final String COLUMN_P5_S2_I1_1 = "p5s2i1_1";
        public static final String COLUMN_P5_S2_I1_2 = "p5s2i1_2";
        public static final String COLUMN_P5_S2_I1_3 = "p5s2i1_3";
        public static final String COLUMN_P5_S2_I1_4 = "p5s2i1_4";
        public static final String COLUMN_P5_S2_I1_5 = "p5s2i1_5";
        public static final String COLUMN_P5_S2_OTHER = "p5s2_other";
        public static final String COLUMN_P5_S3_I1_1 = "p5s3i1_1";
        public static final String COLUMN_P5_S3_I1_2 = "p5s3i1_2";
        public static final String COLUMN_P5_S3_I1_3 = "p5s3i1_3";
        public static final String COLUMN_P5_S3_I1_4 = "p5s3i1_4";
        public static final String COLUMN_P5_S3_I1_5 = "p5s3i1_5";
        public static final String COLUMN_P5_S3_I1_6 = "p5s3i1_6";
        public static final String COLUMN_P5_S3_I1_7 = "p5s3i1_7";
        public static final String COLUMN_P5_S3_OTHER = "p5s3_other";
        public static final String COLUMN_P5S4 = "p5s4";
        public static final String COLUMN_P5S5 = "p5s5";

        public static final String COLUMN_FOLLOWUPS3 = "followups3";
        public static final String COLUMN_FOLLOWUPS5 = "followups5";

     }

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + PlanTableSchema.TABLE_NAME + " (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    PlanTableSchema.COLUMN_UUID + " TEXT NOT NULL, " +
                    PlanTableSchema.COLUMN_NAME + " TEXT, " +
                    PlanTableSchema.COLUMN_PLAN_DATE + " TEXT, " +
                    PlanTableSchema.COLUMN_FOLLOWUP_DATE + " TEXT, " +
                    PlanTableSchema.COLUMN_HAD_CHAT + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_IS_FOLLOWUP + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_CHAT_REMINDER_ID + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4S2 + " TEXT, " +
                    PlanTableSchema.COLUMN_P4_S3_I1_1 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4_S3_I1_2 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4_S3_I1_3 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4_S3_I1_4 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4_S3_I1_5 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4_S3_I1_6 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P4S4 + " TEXT, " +
                    PlanTableSchema.COLUMN_P5S1 + " TEXT, " +
                    PlanTableSchema.COLUMN_P5_S2_I1_1 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S2_I1_2 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S2_I1_3 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S2_I1_4 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S2_I1_5 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S2_OTHER + " TEXT, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_1 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_2 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_3 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_4 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_5 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_6 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_I1_7 + " INTEGER DEFAULT 0, " +
                    PlanTableSchema.COLUMN_P5_S3_OTHER + " TEXT, " +
                    PlanTableSchema.COLUMN_P5S4 + " TEXT, " +
                    PlanTableSchema.COLUMN_P5S5 + " TEXT, " +
                    PlanTableSchema.COLUMN_FOLLOWUPS3 + " TEXT, " +
                    PlanTableSchema.COLUMN_FOLLOWUPS5 + " TEXT" +

    ")";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(SQL_CREATE_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + " ...");
/*
        if (oldVersion < 10) {  // in DB v10, added two new boolean columns
            db.execSQL("ALTER TABLE " + PlanTableSchema.TABLE_NAME  + " ADD COLUMN " + PlanTableSchema.COLUMN_HAD_CHAT + " INTEGER DEFAULT 0;");
            db.execSQL("ALTER TABLE " + PlanTableSchema.TABLE_NAME  + " ADD COLUMN " + PlanTableSchema.COLUMN_IS_FOLLOWUP + " INTEGER DEFAULT 0;");
        }

        if (oldVersion < 11) {  // in DB v11, added two new string columns for followup
            db.execSQL("ALTER TABLE " + PlanTableSchema.TABLE_NAME  + " ADD COLUMN " + PlanTableSchema.COLUMN_FOLLOWUPS3 + " TEXT;");
            db.execSQL("ALTER TABLE " + PlanTableSchema.TABLE_NAME  + " ADD COLUMN " + PlanTableSchema.COLUMN_FOLLOWUPS5 + " TEXT;");
        }
*/
        // Destructive upgrade  - in DB v12, changed name from NOT NULL to allow nulls
        // Destructive upgrade  - in DB v13, added PlanTableSchema.COLUMN_CHAT_REMINDER_ID

        db.execSQL("DROP TABLE IF EXISTS plan");
        onCreate(db);

    }

}
