package au.com.connetica.chatsforlife;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.Logger;

public class AboutActivity extends AppCompatActivity {

    // Handle back button
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button

        //getSupportActionBar().setTitle(getString(R.string.app_name) + " - About");

        // Show app name and version
        TextView textView3 = (TextView) findViewById(R.id.aboutTextView3);
        textView3.setText(getString(R.string.app_name) + " v" + BuildConfig.VERSION_NAME);




        ImageView aboutImageView1 = (ImageView) findViewById(R.id.aboutImageView1);
        ImageView aboutImageView2 = (ImageView) findViewById(R.id.aboutImageView2);
        ImageView aboutImageView3 = (ImageView) findViewById(R.id.aboutImageView3);
        ImageView aboutImageView4 = (ImageView) findViewById(R.id.aboutImageView4);

        aboutImageView1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.qmhc_url)));
                startActivity(intent);
            }
        });

        aboutImageView2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.connetica_url)));
                startActivity(intent);
            }
        });

        aboutImageView3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.usc_url)));
                startActivity(intent);
            }
        });

        aboutImageView4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(getString(R.string.engage_url)));
                startActivity(intent);
            }
        });


    }


}
