package au.com.connetica.chatsforlife.forms;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.UUID;

import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.SpeechBubbleView;
import au.com.connetica.chatsforlife.TranscriptActivity;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.model.SharedPref;
import au.com.connetica.chatsforlife.Logger;

/** P6. Tips For Your Conversation **/

public class P6Fragment extends VideoPlayingBaseFragment {
    private static final String TAG = "P6Fragment";

    public static final int EXOPLAYER_RESOURCE_ID = R.id.ev4;
    public static final int VIDEO_RESOURCE_ID = R.raw.ev4_720;
    public static final String SUBTITLE_FILE = "file:///android_asset/ev4.srt";
    private static final String ISFIRSTRUN = SharedPref.ISFIRSTRUN_EV4;

    private static final int TRANSCRIPT_IMAGE_ID = R.drawable.ev4cover;
    private static final int TRANSCRIPT_HTML_ID = R.string.EV4;

    private SpeechBubbleView speechView1, speechView2, speechView3, speechView4;
    private ImageButton transcriptButton;
    private Button previousButton, nextButton;

    /*
    // the fragment initialization parameters
    private static final String ARG_PARAM1 = "param1";
    private UUID mPlanId;  // id of the current plan
    private Plan mPlan; // the current plan
    private DataModel mDataModel = DataModel.get(getActivity());    // init DataModel singleton
*/

    private OnFragmentInteractionListener mListener;

    public P6Fragment() { } // Required empty public constructor

    public static P6Fragment newInstance() {
        P6Fragment fragment = new P6Fragment();

        fragment.exoPlayerResourceId = EXOPLAYER_RESOURCE_ID;
        fragment.videoResourceId = VIDEO_RESOURCE_ID;
        fragment.subtitleFile = SUBTITLE_FILE;
        fragment.isFirstRunPreference = ISFIRSTRUN;

        return fragment;
    }

    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);

        if (getArguments() != null) { // get the planId from the parameter
            mPlanId = UUID.fromString(getArguments().getString(ARG_PARAM1));
            Logger.d(TAG, "mPlanId = " + mPlanId);
        }
    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment using superclass
        View v = inflateFragment(R.layout.fragment_p6, inflater, container);

        // If portrait, show buttons, speech views, etc
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {

            speechView1 = (SpeechBubbleView) v.findViewById(R.id.p6SpeechView1);
            speechView2 = (SpeechBubbleView) v.findViewById(R.id.p6SpeechView2);
            speechView3 = (SpeechBubbleView) v.findViewById(R.id.p6SpeechView3);
            speechView4 = (SpeechBubbleView) v.findViewById(R.id.p6SpeechView4);

            transcriptButton = (ImageButton) v.findViewById(R.id.p6TranscriptButton);
            transcriptButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "transcriptButton onClickListener()");
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), TranscriptActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("transcriptImage", TRANSCRIPT_IMAGE_ID);
                    intent.putExtra("transcriptHtml", TRANSCRIPT_HTML_ID);
                    startActivity(intent);
                }
            });

            previousButton = (Button) v.findViewById(R.id.p6previousbutton);
            previousButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "previousButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }

                    if (mListener != null) {
                        mListener.previousPage(); // send data back to the activity
                    }
                }
            });

            nextButton = (Button) v.findViewById(R.id.p6nextbutton);
            nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "nextButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }

                    if (mListener != null) {
                        mListener.nextPage(); // send data back to the activity
                    }
                }
            });


        }

        toggleSystemUi();       // show system UI in portrait, hide in landscape
        return v;
    }


    @Override
    public void onAttach(Context context) {
        Logger.d(TAG, "onAttach()");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;    // connect to the listener in the activity
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Logger.d(TAG, "onDetach()");
        super.onDetach();
        mListener = null;
    }


    /** Fragment Lifecycle **/
/*
    // When the fragment becomes visible to the user
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Logger.d(TAG, "isVisibleToUser true");
        }  else {
            Logger.d(TAG, "isVisibleToUser false");
        }

    }
*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void nextPage();
        void previousPage();
    }
}
