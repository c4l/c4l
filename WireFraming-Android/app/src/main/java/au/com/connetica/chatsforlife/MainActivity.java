package au.com.connetica.chatsforlife;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.android.exoplayer2.util.Util;

import org.wordpress.passcodelock.AppLockManager;

import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.MainMenuRecyclerAdapter;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.forms.PlanViewPager;
import au.com.connetica.chatsforlife.model.SharedPref;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private DataModel mDataModel;

    private RecyclerView recyclerMainMenu;
    private RecyclerView.LayoutManager layoutManager;
    private MainMenuRecyclerAdapter adapter;


    // Inflate the menu_main from resources
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // Handle menu choices
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_crisis) {
            Intent intent = new Intent(getApplicationContext(), CrisisActivity.class);
            startActivity(intent);
            return true;
        } else if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return false;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppLockManager.getInstance().enableDefaultAppLockIfAvailable(this.getApplication());

        setContentView(R.layout.activity_main);

        mDataModel = DataModel.get(this); // init DataModel singleton
        //mDataModel.plan = null;

        SharedPref.init(getApplicationContext()); // init SharedPref singleton

        boolean isFirstRun = SharedPref.read(SharedPref.ISFIRSTRUN, true);
        if (isFirstRun) {  // Show welcome if first run
            SharedPref.write(SharedPref.ISFIRSTRUN, false);
            Intent intent = new Intent(getApplicationContext(), Welcome.class);
            startActivity(intent);
        }


        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PlanViewPager.class);
                intent.putExtra("planId", UUID.randomUUID());
                startActivity(intent);
            }
        });
        */

        // setup recycler view
        recyclerMainMenu = (RecyclerView) findViewById(R.id.recyclerMainMenu);
        layoutManager = new LinearLayoutManager(this);
        recyclerMainMenu.setLayoutManager(layoutManager);
        adapter = new MainMenuRecyclerAdapter(mDataModel.getPlans());   // fill with current list of plans

        // set click listener and long click listener
        adapter.setOnItemClickListener(new MainMenuRecyclerAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Logger.d(TAG, "onItemClick position: " + position);

                if (position == 0) {    // 'New Chat' button
                    Intent intent = new Intent(getApplicationContext(), PlanViewPager.class);
                    intent.putExtra("planId", UUID.randomUUID());
                    startActivity(intent);

                } else {    // Existing plan
                    final Plan selectedPlan = adapter.getItem(position);
                    // Create an intent and pass the selected planId into the activity
//                    Intent intent = new Intent(getApplicationContext(), PlanViewPager.class);
                    Intent intent = new Intent(getApplicationContext(), SummaryActivity.class);

                    intent.putExtra("planId", selectedPlan.getId());
                    startActivity(intent);
                }

            }

            @Override
            public void onItemLongClick(int position, View v) {
                Logger.d(TAG, "onItemLongClick pos = " + position);
                if (position > 0) {     // don't include 1st 'New Chat' button
                    final Plan selectedPlan = adapter.getItem(position);
                    if (selectedPlan != null) {
                        final UUID planId = selectedPlan.getId();
                        final String name = selectedPlan.getName();
                        final int pos = position;

                        // *** Show a confirmation dialog
                        new AlertDialog.Builder(MainActivity.this, R.style.Theme_AppCompat_Dialog_Alert)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Delete plan for " + name + "?")
                                .setMessage("Are you sure ?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        mDataModel.deletePlan(selectedPlan);
                                        refreshAdapterData();

                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                }
            }

            // Chat date checkedText click
            public void onCheckedTextClick(int position, View v) {
                Logger.d(TAG, "onCheckedTextClick pos = " + position);

                Plan selectedPlan = adapter.getItem(position);
                boolean newHadChat = !selectedPlan.getHadChat();
                selectedPlan.setHadChat(newHadChat);
                mDataModel.updatePlan(selectedPlan);
                Logger.d(TAG, "new hadChat = " + selectedPlan.getHadChat());
                refreshAdapterData();
            }

            // Schedule button click
            public void onButtonClick(int position, View v) {
                Logger.d(TAG, "onButtonClick pos = " + position);

                final Plan selectedPlan = adapter.getItem(position);
                Date planDate = selectedPlan.getPlanDate();

                //if (planDate == null || planDate.getTime() == 0) {  // no date scheduled - go to P4 to schedule a chat

                if (selectedPlan.getHadChat()) {

                    // launch followup activity
                    Intent intent = new Intent(getApplicationContext(), FollowupActivity.class);
                    intent.putExtra("planId", selectedPlan.getId());
                    startActivity(intent);


                } else {
                    // P4 - schedule a chat
                    Intent intent = new Intent(getApplicationContext(), PlanViewPager.class);
                    intent.putExtra("planId", selectedPlan.getId());
                    startActivity(intent);
                }


            }

        });

        recyclerMainMenu.setAdapter(adapter);


        // get extras passed in - eg. if called from OnAlarmReceiver with a scheduled notification intent, then refresh
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long reminderId = extras.getInt("reminderId");  // get the reminderId passed in
            Logger.d(TAG, "Reminder passed in reminder Id: " + reminderId);
            refreshAdapterData();
        }



    }


    /*** Activity Lifecycle and receive Broadcast Messages ****/

    // Reload the ListView adapter data from the database
    private void refreshAdapterData() {
        Logger.d(TAG, "refreshAdapterData()");
        if (adapter != null) {

            // Delete any plans with a null name
            for (Plan plan : mDataModel.getPlans()) {
                if (plan.getName() == null) {
                    mDataModel.deletePlan(plan);
                }
            }

            adapter.setPlans(mDataModel.getPlans());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        Logger.d(TAG, "onResume()");
        super.onResume();

        // If model has changed, refresh the ListView adapter on resume
        if (mDataModel.dataChanged) {
            refreshAdapterData();
            mDataModel.dataChanged = false;
        }

        // Register to receive messages.
        // This is just like [[NSNotificationCenter defaultCenter] addObserver:...]
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-event-name".
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("event-name-changed"));

    }

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("newName");
            Logger.d("receiver", "Got message: " + message);

            // received event-name-changed broadcast message
            refreshAdapterData();

        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        // This is somewhat like [[NSNotificationCenter defaultCenter] removeObserver:name:object:]
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

}
