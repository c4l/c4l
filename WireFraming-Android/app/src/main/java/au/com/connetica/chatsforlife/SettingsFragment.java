package au.com.connetica.chatsforlife;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import au.com.connetica.chatsforlife.R;


public class SettingsFragment extends PreferenceFragment {


//    private OnFragmentInteractionListener mListener;
/*
    public SettingsFragment() {
        // Required empty public constructor
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }


}
