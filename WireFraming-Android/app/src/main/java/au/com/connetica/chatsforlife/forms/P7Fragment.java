package au.com.connetica.chatsforlife.forms;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.UUID;

import au.com.connetica.chatsforlife.BeaconRaftActivity;
import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.SettingsActivity;
import au.com.connetica.chatsforlife.SpeechBubbleView;
import au.com.connetica.chatsforlife.TranscriptActivity;
import au.com.connetica.chatsforlife.model.DataModel;
import au.com.connetica.chatsforlife.model.Plan;
import au.com.connetica.chatsforlife.model.SharedPref;
import au.com.connetica.chatsforlife.Logger;

/** P7. Starting the Conversation **/

public class P7Fragment extends VideoPlayingBaseFragment {
    private static final String TAG = "P7Fragment";

    private static final int EXOPLAYER_RESOURCE_ID = R.id.ev5;
    private static final int VIDEO_RESOURCE_ID = R.raw.ev5_720;
    private static final String SUBTITLE_FILE = "file:///android_asset/ev5.srt";
    private static final String ISFIRSTRUN = SharedPref.ISFIRSTRUN_EV5;

    private static final int TRANSCRIPT_IMAGE_ID = R.drawable.ev5cover;
    private static final int TRANSCRIPT_HTML_ID = R.string.EV5;

    private static final int BEACON_HEADER_IMAGE_ID = R.drawable.beacon_large;
    private static final int BEACON_HTML_TEXT_ID = R.string.p7_s1_i1_text;

    private static final int RAFT_HEADER_IMAGE_ID = R.drawable.raft_large;
    private static final int RAFT_HTML_TEXT_ID = R.string.p7_s2_i1_text;

    private SpeechBubbleView speechView1, speechView2;
    private TextView textView1, textView2;
    private ImageButton transcriptButton, beaconButton, raftButton;
    private Button previousButton, nextButton;


    private OnFragmentInteractionListener mListener;

    public P7Fragment() { } // Required empty public constructor

    public static P7Fragment newInstance() {
        P7Fragment fragment = new P7Fragment();

        fragment.exoPlayerResourceId = EXOPLAYER_RESOURCE_ID;
        fragment.videoResourceId = VIDEO_RESOURCE_ID;
        fragment.subtitleFile = SUBTITLE_FILE;
        fragment.isFirstRunPreference = ISFIRSTRUN;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment using superclass
        View v = inflateFragment(R.layout.fragment_p7, inflater, container);

        // If portrait, show buttons, speech views, etc
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {

            speechView1 = (SpeechBubbleView) v.findViewById(R.id.p7SpeechView1);
            speechView2 = (SpeechBubbleView) v.findViewById(R.id.p7SpeechView2);
            textView1 = (TextView) v.findViewById(R.id.p7TextView1);
            textView2 = (TextView) v.findViewById(R.id.p7TextView2);

            transcriptButton = (ImageButton) v.findViewById(R.id.p7TranscriptButton);
            transcriptButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "transcriptButton onClickListener()");
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), TranscriptActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("transcriptImage", TRANSCRIPT_IMAGE_ID);
                    intent.putExtra("transcriptHtml", TRANSCRIPT_HTML_ID);
                    startActivity(intent);
                }
            });

            beaconButton = (ImageButton) v.findViewById(R.id.p7BeaconButton);
            beaconButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), BeaconRaftActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("headerImage", BEACON_HEADER_IMAGE_ID);
                    intent.putExtra("htmlTextId", BEACON_HTML_TEXT_ID);
                    startActivity(intent);
                }
            });

            raftButton = (ImageButton) v.findViewById(R.id.p7RaftButton);
            raftButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "raftButton onClickListener()");
                    pauseVideoPlayback();

                    Intent intent = new Intent(getActivity().getApplicationContext(), BeaconRaftActivity.class);
                    // send parameters with the transcript to display
                    intent.putExtra("headerImage", RAFT_HEADER_IMAGE_ID);
                    intent.putExtra("htmlTextId", RAFT_HTML_TEXT_ID);
                    startActivity(intent);
                }
            });

            previousButton = (Button) v.findViewById(R.id.p7previousbutton);
            previousButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "previousButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }
                    if (mListener != null) {
                        mListener.previousPage(); // send data back to the activity
                    }
                }
            });

            nextButton = (Button) v.findViewById(R.id.p7nextbutton);
            nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Logger.d(TAG, "nextButton onClickListener()");
                    if (player != null) {
                        player.stop();
                    }
                    if (mListener != null) {
                        mListener.nextPage(); // send data back to the activity
                    }
                }
            });

        }

        toggleSystemUi();       // show system UI in portrait, hide in landscape
        return v;
    }


    @Override
    public void onAttach(Context context) {
        Logger.d(TAG, "onAttach()");
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;    // connect to the listener in the activity
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Logger.d(TAG, "onDetach()");
        super.onDetach();
        mListener = null;
    }


    /** Fragment Lifecycle **/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Logger.d(TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void nextPage();
        void previousPage();
    }
}
