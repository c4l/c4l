package au.com.connetica.chatsforlife.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.graphics.Paint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import au.com.connetica.chatsforlife.Logger;
import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.Style;


/**
 * Created by peter on 8/7/17.
 * Recycler View with New Chat card at the top, and plan cards following
 * https://guides.codepath.com/android/Heterogenous-Layouts-inside-RecyclerView
 *
 */

public class MainMenuRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "MainMenuRecyclerAdapter";

    private static final int NEWCHAT_TYPE = 0, PLAN_TYPE = 1;

    private static ClickListener clickListener;
    private List<Plan> mPlans;

    public void setPlans(List<Plan> plans) {
        mPlans = plans;
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        MainMenuRecyclerAdapter.clickListener = clickListener;
    }

    // constructor
    public MainMenuRecyclerAdapter(List<Plan> plans) {
        mPlans = plans;
    }

    /*

    Interface for a MainMenuRecyclerAdapter.ClickListener to send onClick events back to activity/fragment

    Usage:
    mAdapter.setOnItemClickListener(new PasswordAdapter.ClickListener() {
        @Override
     public void onItemClick(int position, View v) {
         Logger.d(TAG, "onItemClick position: " + position);
     }
        @Override
        public void onItemLongClick(int position, View v) {
            Logger.d(TAG, "onItemLongClick pos = " +
        }
    });

     */
    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
        void onButtonClick(int position, View v);
        void onCheckedTextClick(int position, View v);
    }


    // inner ViewHolder class
    class PlanViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public ImageView thumbnailImageView;
        public TextView titleTextView;
        public TextView detailTextView;
        public Button cardButton;
        public CheckedTextView subtitleTextView;

        public PlanViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            thumbnailImageView = (ImageView) itemView.findViewById(R.id.mainmenucard_thumbnail);
            titleTextView = (TextView) itemView.findViewById(R.id.mainmenucard_title);
            cardButton = (Button) itemView.findViewById(R.id.mainmenucard_button);
            subtitleTextView = (CheckedTextView) itemView.findViewById(R.id.mainmenucard_subtitle);

            subtitleTextView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.onCheckedTextClick(getAdapterPosition(), v);
                    }
                }
            });

            cardButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                 if (clickListener != null) {
                     clickListener.onButtonClick(getAdapterPosition(), v);
                 }
                }
            });

        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClick(getAdapterPosition(), v);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (clickListener != null) {
                clickListener.onItemLongClick(getAdapterPosition(), v);
            }
            return false;
        }
    }

    // inner ViewHolder class
    class NewChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public NewChatViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClick(getAdapterPosition(), v);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (clickListener != null) {
                clickListener.onItemLongClick(getAdapterPosition(), v);
            }
            return false;
        }
    }

    /**
     * This method creates different RecyclerView.ViewHolder objects based on the item view type.\
     *
     * @param viewGroup ViewGroup container for the item
     * @param viewType type of view to be inflated
     * @return viewHolder to be inflated
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case NEWCHAT_TYPE:
                View v1 = inflater.inflate(R.layout.card_newchat, viewGroup, false);
                viewHolder = new NewChatViewHolder(v1);
                break;
            default:
                View v2 = inflater.inflate(R.layout.card_mainmenu, viewGroup, false);
                viewHolder = new PlanViewHolder(v2);
                break;
        }
        return viewHolder;
    }

    /**
     * This method internally calls onBindViewHolder(ViewHolder, int) to update the
     * RecyclerView.ViewHolder contents with the item at the given position
     * and also sets up some private fields to be used by RecyclerView.
     *
     * @param viewHolder The type of RecyclerView.ViewHolder to populate
     * @param position Item position in the viewgroup.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()) {

            case NEWCHAT_TYPE:
                NewChatViewHolder vh1 = (NewChatViewHolder) viewHolder;
                break;

            default:
                PlanViewHolder vh = (PlanViewHolder) viewHolder;
                Plan plan = mPlans.get(position-1);

                if (plan.getName() != null) {
                    vh.titleTextView.setText(plan.getName());
                }

                Date planDate = plan.getPlanDate();
                if (planDate == null || planDate.getTime() == 0) {  // No chat date scheduled - hide text, show 'Schedule Chat' button
                    vh.thumbnailImageView.setImageBitmap(Style.imageOfCalendarEmpty());
                    vh.cardButton.setVisibility(View.VISIBLE);
                    vh.cardButton.setText("Schedule Chat");

                    vh.subtitleTextView.setVisibility(View.GONE);  // hide and remove empty space

                } else {
                    vh.subtitleTextView.setVisibility(View.VISIBLE);
                    vh.subtitleTextView.setChecked(plan.getHadChat());

                    if (plan.getHadChat()) {    // Completed - show crossed out 'Completed', and Schedule follow-up button
                        vh.thumbnailImageView.setImageBitmap(Style.imageOfCalendarCompleted());

                        vh.subtitleTextView.setText("Completed");
                        vh.subtitleTextView.setPaintFlags(vh.subtitleTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        vh.cardButton.setVisibility(View.VISIBLE);
                        vh.cardButton.setText("Schedule follow-up");
                    } else {    // Scheduled chat or follow-up - show date and tick box
                        if (planDate.getTime() < System.currentTimeMillis()) {
                            vh.thumbnailImageView.setImageBitmap(Style.imageOfCalendarOverdue());
                        } else {
                            vh.thumbnailImageView.setImageBitmap(Style.imageOfCalendarScheduled());
                        }
                        String friendlyDate = new SimpleDateFormat("MMM d 'at' h:mm a").format(plan.getPlanDate());    //  "MMM d 'at' h:mma"
                        if (plan.getIsFollowup()) {
                            vh.subtitleTextView.setText("Follow-up " + friendlyDate);
                        } else {
                            vh.subtitleTextView.setText("Chat " + friendlyDate);
                        }
                        vh.subtitleTextView.setPaintFlags(vh.subtitleTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                        vh.cardButton.setVisibility(View.GONE); // hide and remove empty space
                    }

                }

                break;
        }
    }


    /*
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Logger.d(TAG, "onCreateViewHolder(). i = " + i);

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_mainmenu, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
*/

    /*
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
//        Logger.d(TAG, "onBindViewHolder(). i = " + i);
//        Plan plan = mPlans.get(i);
//        Logger.d(TAG, "plan = " + plan);
//        if (plan == null || viewHolder == null) { return; }

        if (i == 0) {   // 1st card - New Chat button

            viewHolder.titleTextView.setText("New Chat");
            viewHolder.titleTextView.setTextColor(Style.primaryColor);

            // remove the subtitleTextView completely
            //viewHolder.subtitleTextView.setVisibility(View.INVISIBLE);


            if (viewHolder.subtitleTextView != null) {
                ViewGroup parent = (ViewGroup) viewHolder.subtitleTextView.getParent();
                parent.removeView(viewHolder.subtitleTextView);
                viewHolder.subtitleTextView = null;
            }

            viewHolder.detailTextView.setText("");
            viewHolder.thumbnailImageView.setImageResource(R.drawable.ic_person_add_60dp);

        } else {
            Plan plan = mPlans.get(i-1);

            viewHolder.titleTextView.setText(plan.getName());

            Date planDate = plan.getPlanDate();
            if (planDate == null || planDate.getTime() == 0) {  // no date scheduled
                viewHolder.thumbnailImageView.setImageBitmap(Style.imageOfCalendarNotScheduled());
                if (viewHolder.subtitleTextView != null) {
                    viewHolder.subtitleTextView.setText("Schedule Chat");
                }
            } else {
                String friendlyDate = new SimpleDateFormat("MMM d 'at' h:mm a").format(plan.getPlanDate());    //  "MMM d 'at' h:mma"
                if (viewHolder.subtitleTextView != null) {
                    viewHolder.subtitleTextView.setText("Chat " + friendlyDate);
                }
                viewHolder.thumbnailImageView.setImageBitmap(Style.imageOfCalendarScheduled());
            }
        }
    }

    */

    public Plan getItem(int position) {
        if (position < 1 || position > mPlans.size()) {
            return null;
        }
        return mPlans.get(position-1);
    }

    @Override
    public int getItemCount() {
        return mPlans.size() + 1;   // additional 1 for 1st card New Chat button
    }

    //Returns the view type of the item at position for the purposes of view recycling.
    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return NEWCHAT_TYPE;
        } else {
            return PLAN_TYPE;
        }
    }



}
