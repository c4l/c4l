package au.com.connetica.chatsforlife.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.provider.Settings;
import android.support.v7.app.NotificationCompat;

import au.com.connetica.chatsforlife.MainActivity;
import au.com.connetica.chatsforlife.R;
import au.com.connetica.chatsforlife.Style;

import static android.R.attr.bitmap;

/**
 * Created by peter on 15/7/17.
 */
/*
    This class is called when our reminder alarm fires,
    at which point we'll create a notification and show it to eh user.

 */

public class OnAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Important: Do not do any asynchronous operations in the onReceive

        // get the data from the delivered intent
        int reminderId = intent.getIntExtra("reminderId", -1);
        String title = intent.getStringExtra("title");

        Intent mainMenuIntent = new Intent(context, MainActivity.class);    // open main menu when received
        mainMenuIntent.putExtra("reminderId", reminderId);  // pass reminderId to Main Menu

        PendingIntent pi = PendingIntent.getActivity(context, 0, mainMenuIntent, PendingIntent.FLAG_ONE_SHOT);
        // Build the Notification object using a Notification.Builder
        Notification note = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(context.getString(R.string.app_name) + " reminder")
                .setSmallIcon(R.drawable.settings_icon)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .setVibrate(new long[] {500, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setLargeIcon(Style.imageOfCalendarOverdue())
                .setTicker(title)
                .build();

        // Send the notification
        NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.notify(reminderId, note);

    }

}
