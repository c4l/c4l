package au.com.connetica.chatsforlife.model;

import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

/**
 *
 * Model Object for a conversation plan
 */

public class Plan {
    private UUID mId;
    private String mName;
    private Date mPlanDate;
    private Date mFollowupDate;
    private boolean mHadChat;
    private boolean mIsFollowup;
    private int mChatReminderId;

    private String mP4s2;
    private boolean mP4_s3_i1_1, mP4_s3_i1_2, mP4_s3_i1_3, mP4_s3_i1_4, mP4_s3_i1_5, mP4_s3_i1_6;
    private String mP4s4;

    private String mP5s1;
    private boolean mP5_s2_i1_1, mP5_s2_i1_2, mP5_s2_i1_3, mP5_s2_i1_4, mP5_s2_i1_5;
    private String mP5_s2_other;
    private boolean mP5_s3_i1_1, mP5_s3_i1_2, mP5_s3_i1_3, mP5_s3_i1_4, mP5_s3_i1_5, mP5_s3_i1_6, mP5_s3_i1_7;
    private String mP5_s3_other;
    private String mP5s4;
    private String mP5s5;

    private String mFollowups3;
    private String mFollowups5;

    public Plan() {
        this(UUID.randomUUID());
    }

    public Plan(UUID id) {
        mId = id;
    }

    // getters
    public UUID getId() {
        return mId;
    }
    public String getName() {
        return mName;
    }
    public Date getPlanDate () {
        return mPlanDate;
    }
    public Date getFollowupDate () {
        return mFollowupDate;
    }
    public boolean getHadChat () { return mHadChat; }
    public boolean getIsFollowup() { return mIsFollowup; }
    public int getChatReminderId() { return mChatReminderId; }

    public String getP4s2 () {
        return mP4s2;
    }
    public boolean getP4_s3_i1_1 () { return mP4_s3_i1_1; }
    public boolean getP4_s3_i1_2 () { return mP4_s3_i1_2; }
    public boolean getP4_s3_i1_3 () { return mP4_s3_i1_3; }
    public boolean getP4_s3_i1_4 () { return mP4_s3_i1_4; }
    public boolean getP4_s3_i1_5 () { return mP4_s3_i1_5; }
    public boolean getP4_s3_i1_6 () { return mP4_s3_i1_6; }
    public String getP4s4 () {
        return mP4s4;
    }

    public String getP5s1 () {
        return mP5s1;
    }
    public boolean getP5_s2_i1_1 () { return mP5_s2_i1_1; }
    public boolean getP5_s2_i1_2 () { return mP5_s2_i1_2; }
    public boolean getP5_s2_i1_3 () { return mP5_s2_i1_3; }
    public boolean getP5_s2_i1_4 () { return mP5_s2_i1_4; }
    public boolean getP5_s2_i1_5 () { return mP5_s2_i1_5; }
    public String getP5_s2_other () {
        return mP5_s2_other;
    }
    public boolean getP5_s3_i1_1 () { return mP5_s3_i1_1; }
    public boolean getP5_s3_i1_2 () { return mP5_s3_i1_2; }
    public boolean getP5_s3_i1_3 () { return mP5_s3_i1_3; }
    public boolean getP5_s3_i1_4 () { return mP5_s3_i1_4; }
    public boolean getP5_s3_i1_5 () { return mP5_s3_i1_5; }
    public boolean getP5_s3_i1_6 () { return mP5_s3_i1_6; }
    public boolean getP5_s3_i1_7 () { return mP5_s3_i1_7; }
    public String getP5_s3_other () {
        return mP5_s3_other;
    }
    public String getP5s4 () {
        return mP5s4;
    }
    public String getP5s5 () {
        return mP5s5;
    }

    public String getFollowups3 () {
        return mFollowups3;
    }
    public String getFollowups5 () {
        return mFollowups5;
    }


    // setters
    public void setName(String name) {
        mName = name;
    }
    public void setPlanDate(Date planDate) {
        mPlanDate = planDate;
    }
    public void setFollowupDate(Date followupDate) {
        mFollowupDate = followupDate;
    }
    public void setHadChat(boolean hadChat) {
        mHadChat = hadChat;
    }
    public void setIsFollowup(boolean isFollowup) {
        mIsFollowup = isFollowup;
    }
    public void setChatReminderId(int chatReminderId) { mChatReminderId = chatReminderId; }

    public void setP4s2(String p4s2) {
        mP4s2 = p4s2;
    }
    public void setP4_s3_i1_1(boolean p4_s3_i1_1) {
        mP4_s3_i1_1 = p4_s3_i1_1;
    }
    public void setP4_s3_i1_2(boolean p4_s3_i1_2) {
        mP4_s3_i1_2 = p4_s3_i1_2;
    }
    public void setP4_s3_i1_3(boolean p4_s3_i1_3) {
        mP4_s3_i1_3 = p4_s3_i1_3;
    }
    public void setP4_s3_i1_4(boolean p4_s3_i1_4) {
        mP4_s3_i1_4 = p4_s3_i1_4;
    }
    public void setP4_s3_i1_5(boolean p4_s3_i1_5) {
        mP4_s3_i1_5 = p4_s3_i1_5;
    }
    public void setP4_s3_i1_6(boolean p4_s3_i1_6) {
        mP4_s3_i1_6 = p4_s3_i1_6;
    }
    public void setP4s4(String p4s4) {
        mP4s4 = p4s4;
    }

    public void setP5s1(String p5s1) {
        mP5s1 = p5s1;
    }
    public void setP5_s2_i1_1(boolean p5_s2_i1_1) {
        mP5_s2_i1_1 = p5_s2_i1_1;
    }
    public void setP5_s2_i1_2(boolean p5_s2_i1_2) {
        mP5_s2_i1_2 = p5_s2_i1_2;
    }
    public void setP5_s2_i1_3(boolean p5_s2_i1_3) {
        mP5_s2_i1_3 = p5_s2_i1_3;
    }
    public void setP5_s2_i1_4(boolean p5_s2_i1_4) {
        mP5_s2_i1_4 = p5_s2_i1_4;
    }
    public void setP5_s2_i1_5(boolean p5_s2_i1_5) {
        mP5_s2_i1_5 = p5_s2_i1_5;
    }
    public void setP5_s2_other(String p5_s2_other) {
        mP5_s2_other = p5_s2_other;
    }
    public void setP5_s3_i1_1(boolean p5_s3_i1_1) {
        mP5_s3_i1_1 = p5_s3_i1_1;
    }
    public void setP5_s3_i1_2(boolean p5_s3_i1_2) {
        mP5_s3_i1_2 = p5_s3_i1_2;
    }
    public void setP5_s3_i1_3(boolean p5_s3_i1_3) {
        mP5_s3_i1_3 = p5_s3_i1_3;
    }
    public void setP5_s3_i1_4(boolean p5_s3_i1_4) {
        mP5_s3_i1_4 = p5_s3_i1_4;
    }
    public void setP5_s3_i1_5(boolean p5_s3_i1_5) {
        mP5_s3_i1_5 = p5_s3_i1_5;
    }
    public void setP5_s3_i1_6(boolean p5_s3_i1_6) {
        mP5_s3_i1_6 = p5_s3_i1_6;
    }
    public void setP5_s3_i1_7(boolean p5_s3_i1_7) {
        mP5_s3_i1_7 = p5_s3_i1_7;
    }
    public void setP5_s3_other(String p5_s3_other) {
        mP5_s3_other = p5_s3_other;
    }
    public void setP5s4(String p5s4) {
        mP5s4 = p5s4;
    }
    public void setP5s5(String p5s5) {
        mP5s5 = p5s5;
    }

    public void setFollowups3(String followups3) {
        mFollowups3 = followups3;
    }
    public void setFollowups5(String followups5) {
        mFollowups5 = followups5;
    }


    @Override
    public String toString() {
        return "Plan {" +
                "Id='" + mId + '\'' +
                ", name='" + mName + '\'' +
                ", planDate=" + mPlanDate +
                ", followupDate=" + mFollowupDate +
                ", hadChat=" + mHadChat +
                ", isFollowup=" + mIsFollowup +
                ", chatReminderId= " + mChatReminderId +
                ", p4s2=" + mP4s2 +
                ", p4s3= 1= " + mP4_s3_i1_1 + ", 2= " + mP4_s3_i1_2 + ", 3= " + mP4_s3_i1_3 + ", 4= " + mP4_s3_i1_4 + ", 5= " + mP4_s3_i1_5 + ",6= " + mP4_s3_i1_6 +
                ", p4s4=" + mP4s4 +
                ", p5s1=" + mP5s1 +
                ", p5s2= 1= " + mP5_s2_i1_1 + ", 2= " + mP5_s2_i1_2 + ", 3= " + mP5_s2_i1_3 + ", 4= " + mP5_s2_i1_4 + ", 5= " + mP5_s2_i1_5 + ", other= " + mP5_s2_other +
                ", p5s3= 1= " + mP5_s3_i1_1 + ", 2= " + mP5_s3_i1_2 + ", 3= " + mP5_s3_i1_3 + ", 4= " + mP5_s3_i1_4 + ", 5= " + mP5_s3_i1_5 + ", 6= " + mP5_s3_i1_6 +", 7= " + mP5_s3_i1_7 + ", other= " + mP5_s3_other +
                ", p5s4=" + mP5s4 +
                ", p5s5=" + mP5s5 +
                ", followups3=" + mFollowups3 +
                ", followups5=" + mFollowups5 +
                " }";
    }

    // utility method to replace %s in strings with the plan name
    public String replaceTokenWithName(String sourceString) {
        if (mName != null) {
            return sourceString.replaceAll("%s", mName);
        } else {
            return sourceString.replaceAll("%s", "");
        }
    }

}
