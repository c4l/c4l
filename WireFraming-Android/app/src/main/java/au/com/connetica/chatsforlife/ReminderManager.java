package au.com.connetica.chatsforlife;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;

import java.util.Calendar;
import java.util.Date;

import au.com.connetica.chatsforlife.receiver.OnAlarmReceiver;

/**
 * Created by peter on 15/7/17.
 */
/*
    Class to set reminders using the AlarmManager
    Reminder fires the OnAlarmReceiver class

    Usage:
    ReminderManager.setReminder(contact, taskId, title, when)

 */
public class ReminderManager {
    private static final String TAG = "ReminderManager";

    private ReminderManager() {}

    public static void setReminder(Context context, int reminderId, String title, Calendar when) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(context, OnAlarmReceiver.class);
        i.putExtra("reminderId", reminderId);
        i.putExtra("title", title);
        PendingIntent pi = PendingIntent.getBroadcast(context, reminderId, i, PendingIntent.FLAG_ONE_SHOT);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, when.getTimeInMillis(), pi);
        Logger.d(TAG,"Reminder scheduled for " + when.getTime() + ", Id: " + reminderId + ", Title: " + title);
    }

    public static void cancelReminder(Context context, int reminderId) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent i = new Intent(context, OnAlarmReceiver.class);
        i.putExtra("reminderId", reminderId);

        PendingIntent pi = PendingIntent.getBroadcast(context, reminderId, i, PendingIntent.FLAG_ONE_SHOT);
        alarmManager.cancel(pi);
        Logger.d(TAG,"Reminder cancelled for Id: " + reminderId);
    }


    public static void addToCalendar(Context context, String title, String description, String location, Calendar beginTime) {
        long endTimeInMillis = beginTime.getTimeInMillis()+60*60*1000;
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTimeInMillis)
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.DESCRIPTION, description)
                .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(CalendarContract.Events.HAS_ALARM, true);
        context.startActivity(intent);
    }


}
