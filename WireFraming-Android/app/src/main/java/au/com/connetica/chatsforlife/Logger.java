package au.com.connetica.chatsforlife;

import android.util.Log;

/**
 * Created by peter on 11/7/17.
 */

// Wrapper around Logger.d so that it doesn't activate in release builds
//    Usage: Logger.d(TAG, "Hello World");
public class Logger {

    public static void d(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

}