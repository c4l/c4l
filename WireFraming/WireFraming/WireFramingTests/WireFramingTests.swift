//
//  WireFramingTests.swift
//  WireFramingTests
//
//  Created by Peter Hunt on 18/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import XCTest
@testable import WireFraming


class WireFramingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
  
  func testPlan() {
    let plan1 = Plan(planId: 1, name: "Adam", pages: [:])
    //let plan2 = Plan(planId: 1, name: "Adam", pages: [:])
    XCTAssert(plan1.name == "Adam", "Created plan")
    
  }
  
    
}
