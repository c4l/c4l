//
//  StringsModel.swift
//  WireFraming
//
//  Created by Peter Hunt on 13/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import SWXMLHash

// Provides access to a read-only strings data resource for the App
// Currently implemented via an XML strings file included in the bundle
// Access via subscript - eg. strings["p4_s1_i1"]
// More info on SWXMLHash usage:
// https://github.com/drmohundro/SWXMLHash

class StringsModel {
  static let stringsFile = "strings.xml"     // XML file in bundle
  
  
  // Friendly typed access to values
  var app_name: String { return self["app_name"] }
  
  var welcome_s1_title: String { return self["welcome_s1_title"] }
  var welcome_s2_i1_title: String { return self["welcome_s2_i1_title"] }
  var welcome_s2_i2_title: String { return self["welcome_s2_i2_title"] }
  
  var p4_s1_title: String { return self["p4_s1_title"] }
  var p4_s1_i1_placeholder: String { return self["p4_s1_i1_placeholder"] }

  var p4_s2_title: String { return self["p4_s2_title"] }
  var p4_s2_i1_placeholder: String { return self["p4_s2_i1_placeholder"] }

  var p4_s3_title: String { return self["p4_s3_title"] }
  var p4_s3_i1_1: String { return self["p4_s3_i1_1"] }
  var p4_s3_i1_2: String { return self["p4_s3_i1_2"] }
  var p4_s3_i1_3: String { return self["p4_s3_i1_3"] }
  var p4_s3_i1_4: String { return self["p4_s3_i1_4"] }
  var p4_s3_i1_5: String { return self["p4_s3_i1_5"] }
  var p4_s3_i1_6: String { return self["p4_s3_i1_6"] }
  
  var p4_s4_title: String { return self["p4_s4_title"] }
  var p4_s4_i1_placeholder: String { return self["p4_s4_i1_placeholder"] }
  
  var p4_s5_title: String { return self["p4_s5_title"] }
  var p4_s5_i1_placeholder: String { return self["p4_s5_i1_placeholder"] }
  var p4_s5_i1_footer: String { return self["p4_s5_i1_footer"] }
  
  // *** P5 ***
  var p5_s1_title: String { return self["p5_s1_title"] }
  var p5_s1_i1_placeholder: String { return self["p5_s1_i1_placeholder"] }
  
  var p5_s2_title: String { return self["p5_s2_title"] }
  var p5_s2_i1_1: String { return self["p5_s2_i1_1"] }
  var p5_s2_i1_2: String { return self["p5_s2_i1_2"] }
  var p5_s2_i1_3: String { return self["p5_s2_i1_3"] }
  var p5_s2_i1_4: String { return self["p5_s2_i1_4"] }
  var p5_s2_i1_5: String { return self["p5_s2_i1_5"] }
  
  var p5_s3_title: String { return self["p5_s3_title"] }
  var p5_s3_i1_1: String { return self["p5_s3_i1_1"] }
  var p5_s3_i1_2: String { return self["p5_s3_i1_2"] }
  var p5_s3_i1_3: String { return self["p5_s3_i1_3"] }
  var p5_s3_i1_4: String { return self["p5_s3_i1_4"] }
  var p5_s3_i1_5: String { return self["p5_s3_i1_5"] }
  var p5_s3_i1_6: String { return self["p5_s3_i1_6"] }
  var p5_s3_i1_7: String { return self["p5_s3_i1_7"] }
  
  var p5_s4_title: String { return self["p5_s4_title"] }
  var p5_s4_i1_placeholder: String { return self["p5_s4_i1_placeholder"] }
  
  var p5_s5_title: String { return self["p5_s5_title"] }
  var p5_s5_i1_placeholder: String { return self["p5_s5_i1_placeholder"] }
  
  // *** P6 ***
  var p6_s1_title: String { return self["p6_s1_title"] }
  var p6_s1_i1_text: String { return self["p6_s1_i1_text"] }
  var p6_s1_i2_text: String { return self["p6_s1_i2_text"] }
  var p6_s1_i3_text: String { return self["p6_s1_i3_text"] }
  
  // *** P7 ***
  var p7_s1_title: String { return self["p7_s1_title"] }
  var p7_s1_i1_text: String { return self["p7_s1_i1_text"] }
  var p7_s2_title: String { return self["p7_s2_title"] }
  var p7_s2_i1_text: String { return self["p7_s2_i1_text"] }
var p7_s2_text: String { return self["p7_s2_text"] }
  
  // *** Follow-up ***
  var followup_s1_title: String { return self["followup_s1_title"] }
  var followup_s1_i1_text: String { return self["followup_s1_i1_text"] }
  var followup_s2_title: String { return self["followup_s2_title"] }
  var followup_s3_title: String { return self["followup_s3_title"] }
  var followup_s3_i1_placeholder: String { return self["followup_s3_i1_placeholder"] }
  var followup_s4_title: String { return self["followup_s4_title"] }
  var followup_s4_i1_placeholder: String { return self["followup_s4_i1_placeholder"] }
  var followup_s4_i1_footer: String { return self["followup_s4_i1_footer"] }
  var followup_s5_title: String { return self["followup_s5_title"] }
  var followup_s5_i1_placeholder: String { return self["followup_s5_i1_placeholder"] }
  
  
  private var strings: XMLIndexer? = {      // parse the XML from strings.xml file to memory
    guard let xmlPath = Bundle.main.path(forResource: stringsFile, ofType: nil),
      let data = try? Data(contentsOf: URL(fileURLWithPath: xmlPath))
      else { return nil }
    
    let parsedXml = SWXMLHash.config { config in
      // set any config options here
      }.parse(data)
    
    print("*** StringsModel Loaded ***")
    //print(parsedXml)
    return parsedXml
  }()

  /// Get a string from the XML strings resource
  ///
  /// - Parameter resource: string key to lookup
  /// - Returns: string value returned
  subscript(resource: String) -> String {
    get {
      do {
//        let value = try strings?["resources"]["string"].withAttr("name", resource).element?.text ?? ""
        let value = try strings?["resources"]["string"].withAttribute("name", resource).element?.text ?? ""
        return value
      } catch {
        return ""  // error is an IndexingError instance that you can deal with
      }
    }
  }

  
}

