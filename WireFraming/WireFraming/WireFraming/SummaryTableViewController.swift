//
//  SummaryTableViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 13/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit
import DateHelper

/*
 Summary Table View:
 - One cell/card per page
 - Tap on cell/card to go to that page
 - Settings screen
 */

class SummaryTableViewController: UITableViewController, Injectable, EventAccess {
  let summaryCellId = "SummaryCell"
  var pages: [String] = ["P4", "P5", "P6", "P7", "P8"]

  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Style.backgroundColor
    // dynamic table view cell height
    tableView.estimatedRowHeight = tableView.rowHeight
    tableView.rowHeight = UITableView.automaticDimension
    tableView.separatorStyle = .none
    let insets = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    //let insets = UIEdgeInsets(top: 0, left: 0, bottom: UIApplication.shared.keyWindow!.safeAreaInsets.bottom, right: 0.0);
    self.tableView.contentInset = insets
    
    self.navigationItem.title = "Summary for \(model.getName())"
    // Register to listen for a model change notification, and refresh the table view data
    NotificationCenter.default.addObserver(forName: .modelChangeNotification, object: nil, queue: OperationQueue.main) { [weak self] notification in
      print("Model Change Notification - reloading summary data")
      self?.tableView.reloadData()
      if let name = self?.model.getName() {
        self?.navigationItem.title = "Summary for \(name)"
      }
    }
    
    // Add settings button to the toolbar
    //let settingsImage = #imageLiteral(resourceName: "settings.png")
    //let settingsButton = UIBarButtonItem(image: settingsImage, style: .plain, target: self, action: #selector(settingsButtonTapped))
    //navigationItem.rightBarButtonItem = settingsButton
    
    // Add crisis button to the toolbar
    let crisisImage = UIImage(named: "crisis")
    let crisisButton = UIBarButtonItem(image: crisisImage, style: .plain, target: self, action: #selector(crisisButtonTapped))
    navigationItem.rightBarButtonItem = crisisButton
    
    
    // Add delete plan button to footer of Tableview
    let deleteView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 80))
    //deleteView.backgroundColor = Style.backgroundColor.highlight(withLevel: 0.3)
    let button = UIButton(frame: CGRect(x: 0, y: 0, width: 150, height: 50))
    button.center = deleteView.center
    button.setTitle("Delete Plan", for: .normal)
    button.setTitleColor(Style.highlightColor, for: .normal)
    button.setTitleColor(Style.secondaryTextColor, for: .selected)
    button.setTitleColor(Style.secondaryTextColor, for: .highlighted)
    button.titleLabel?.font = UIFont.customHeadline
    button.addTarget(self, action: #selector(deletePlanButton), for: .touchUpInside)
    deleteView.addSubview(button)
    
    //customView.snp.makeConstraints { (make) in
    //  make.centerX.equalTo(tableView)
    //}
    tableView.tableFooterView = deleteView
    
    if let isFollowup = model.plan?.isFollowup, isFollowup {
      pages.append("Followup")
    }
    
  }
  
 
  
  @objc func deletePlanButton() {
    print("delete plan")
    
    let name = model.getName()
    let alert = UIAlertController(title: "Delete Plan for \(name)", message: "Are you sure you want to delete the plan?", preferredStyle: .actionSheet)
    
    let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { [weak self] (alert: UIAlertAction!) -> Void in
      print("Delete")
      
      if let plan = self?.model.plan {
        print("Deleting plan: \(plan.debugDescription)")

        // Check if there's a notification scheduled, and cancel it
        if let chatNotificationId = self?.model.plan?.chatNotificationId, (self?.model.userSettings.isNotificationsEnabled ?? false) {
          self?.cancelNotification(withId: chatNotificationId)
        }
        
        // cancel previous CALENDAR entries
        if let chatEventId = self?.model.plan?.chatEventId, (self?.model.userSettings.isCalendarEnabled ?? false) {
          self?.deleteChatEvent(withIdentifier: chatEventId)
        }
        if let followupEventId = self?.model.plan?.followupEventId, (self?.model.userSettings.isCalendarEnabled ?? false) {
          self?.deleteChatEvent(withIdentifier: followupEventId)
        }

        // remove the plan
        if let index = self?.model.plans.index(of: plan) {
          self?.model.plans.remove(at: index)
        }
        
        self?.model.savePlans()
        
        self?.navigationController?.popViewController(animated: true)
      }
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (alert: UIAlertAction!) -> Void in
      print("Cancel")
    }
    
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    present(alert, animated: true, completion: nil)
  }
  
  
  deinit {
    print("Summary deinit")
  }
  
  // Present Settings View Controller modally
  // with a navigation bar
  func settingsButtonTapped() {
    let settingsVC = newViewController(withId: "About")
    let settingsNavController = UINavigationController(rootViewController: settingsVC)
    // Creating a navigation controller with settingsVC at the root of the navigation stack.
    settingsNavController.modalTransitionStyle = .coverVertical
    settingsNavController.modalPresentationStyle = .overCurrentContext
    settingsVC.title = "Resources"
    present(settingsNavController, animated: true, completion: nil)
  }
  
  // Present Crisis View Controller modally
  // with a navigation bar
  @objc func crisisButtonTapped() {
    print("crisisButtonTapped")
    let contactsController = ContactsTableViewController(style: .plain)
    contactsController.setDependencies(strings: strings, model: model)
    let crisisNavController = UINavigationController(rootViewController: contactsController)
    crisisNavController.modalTransitionStyle = .coverVertical
    crisisNavController.modalPresentationStyle = .overCurrentContext
    //    settingsVC.title = "Resources"
    present(crisisNavController, animated: true, completion: nil)
  }

  
  /*
  override func viewWillAppear(_ animated: Bool) {
    tableView.reloadData()
  }
  */
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}


// MARK: UITableViewDataSource
extension SummaryTableViewController {
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return pages.count
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    // Otherwise, show a summary cell, and populate it
    let cell = tableView.dequeueReusableCell(withIdentifier: summaryCellId, for: indexPath) as! SummaryCell
    
    // Configure the cell with the page info
    cell.model = self.model
    cell.strings = self.strings
    
    if let isFollowup = model.plan?.isFollowup, isFollowup {
      cell.pages.append("Followup")
      cell.coverFiles.append("promo.jpg")
      cell.videoTitles.append("followup_s1_title")
    }
    
    cell.configure(row: indexPath.row)
    
    return cell
  }
  
  
}


// MARK: UITableViewDelegate
extension SummaryTableViewController {
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


    let index = indexPath.row
    print("SummaryTableViewController select page: \(pages[index])")
   
    if pages[index] == "Followup" {
      // Schedule followup
      let followupVC = newViewController(withId: "Followup")
      let followupNavController = UINavigationController(rootViewController: followupVC)
      // Creating a navigation controller with followupVC at the root of the navigation stack.
      followupNavController.modalTransitionStyle = .coverVertical
      followupNavController.modalPresentationStyle = .overCurrentContext
      present(followupNavController, animated: true, completion: nil)
      tableView.deselectRow(at: indexPath, animated: true)
      return
    }

    
    let planPageViewController = newViewController(withId: "PlanPageViewController")
    
    // Set the initial page to open to
    if let planVC = (planPageViewController as? PlanPageViewController) {
      if index >= 0 && index < planVC.pages.count {
        planVC.initialDefaultPage = index
      }
    }
    self.navigationController?.pushViewController(planPageViewController, animated: true)
    

    
    
    //NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": index+1]) // add 1 as this is the first page
    
      /*
      model.plan = plan
      let planPageViewController = newViewController(withId: "PlanPageViewController")
      
      self.navigationController?.pushViewController(planPageViewController, animated: true)
      tableView.deselectRow(at: indexPath, animated: true)
      */
    
    
  }
  

  
  
  
}

