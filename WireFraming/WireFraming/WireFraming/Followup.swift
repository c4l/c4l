//
//  Followup.swift
//  WireFraming
//
//  Created by Peter Hunt on 26/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import KMPlaceholderTextView
import SnapKit

/*
 Followup Form - P7
 
 Section 1 - How did the conversation go? (Text label instructions)
 
 Section 2 - Let’s plan a follow up chat with your friend. (Speech bubble)
 
 Section 3 - Where should you and %s have a follow-up talk? (UITextView)
 
 Section 4 - When should you and %s talk? (DateTime Picker)
 
 Section 5 - What might you chat about at the follow-up? (UITextView)
 
 Next Button
 
 */

class Followup: ScrollingForm {
  
  var isFirstRun: Bool = false
  var speechView1, speechView2, speechView3, speechView4, speechView5 : SpeechBubbleView!
  var s1Label: UILabel!
  var s3TextView, s5TextView: KMPlaceholderTextView!
  var followupDateTextField: UITextField!
  var previousS3Text, previousS5Text: String?
  var previousChatDate: Date?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addSection1()
    addSection2()
    addSection3()
    addSection4()
    addSection5()
    
    addFinishButton()
    
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self,
                                                             action: #selector(doneButton))
    
  }
  
  // Modal - when Finish button tapped
  override func nextScreen() {
    self.dismiss(animated: true, completion: nil)
    
  }
  
  @objc func doneButton() {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("Followup viewWillDisappear")
    view.endEditing(true)
  }
  
  
  // MARK: - Section 1 - How did the conversation go? (Text label instructions)
  func addSection1() {
    speechView1 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.followup_s1_title))
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(10)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    
    
    let speechView1Large = SpeechBubbleView(type: .tertiaryLarge, text: model.replaceTokens(inString: strings.followup_s1_i1_text))
    contentView.addSubview(speechView1Large)
    speechView1Large.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(425/500.0)
    }
    lastControlBottom = speechView1Large.snp.bottom
    
    /*
    s1Label = customLabel(text: strings.followup_s1_i1_text)
    contentView.addSubview(s1Label)
    s1Label.adjustsFontSizeToFitWidth = true
    s1Label.textAlignment = .left
    //    s1Label.font = UIFont.customBodyItalic
    s1Label.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(250)
      //make.height.equalTo(s1Label.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = s1Label.snp.bottom
    */
    
  }
  
  
  // Section 2 - Let’s plan a follow up chat with your friend. (Speech bubble)
  func addSection2() {
    
    speechView2 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.followup_s2_title))
    contentView.addSubview(speechView2)
    speechView2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView2.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView2.snp.bottom
  }
  
  // Section 3 - Where should you and %s have a follow-up talk? (UITextView)
  func addSection3() {
    
    speechView3 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.followup_s3_title))
    contentView.addSubview(speechView3)
    speechView3.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView3.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView3.snp.bottom
    
    s3TextView = customTextView(placeholder: strings.followup_s3_i1_placeholder)  // KMPlaceholderTextView
    s3TextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s3TextView)
    s3TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(70)
    }
    lastControlBottom = s3TextView.snp.bottom
    s3TextView.text = model.get(.followup_s3) as? String
    
    
  }
  
  // Section 4 - When should you and %s talk? (DateTime Picker)
  func addSection4() {
    
    speechView4 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.followup_s4_title))
    contentView.addSubview(speechView4)
    speechView4.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView4.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView4.snp.bottom
    
    
    let dateTimePicker = UIDatePicker()
    dateTimePicker.datePickerMode = .dateAndTime
    dateTimePicker.addTarget(self, action: #selector(dateTimePickerChanged(datePicker:)), for: .valueChanged)
    
    followupDateTextField = customTextField(placeholder: strings.followup_s4_i1_placeholder)
    //followupDateTextField.textAlignment = .center
    //conversationDateTextField.clearButtonMode = UITextFieldViewMode.never
    if let date = model.getChatDate() {
      followupDateTextField.text = date.customFriendlyString()
      dateTimePicker.date = date
      //followupDateTextField.font = UIFont.customBody.bolded
    } else { // no date set, put placeholder text
      //followupDateTextField.font = UIFont.customBody
    }
    followupDateTextField.inputView = dateTimePicker
    followupDateTextField.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(followupDateTextField)
    followupDateTextField.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(45)
    }
    lastControlBottom = followupDateTextField.snp.bottom
    
    
    let footerLabel = customLabel(text: strings.followup_s4_i1_footer)  // Choose a time of day when they will be at their best and open to a conversation. Avoid times when they may have used alcohol or other substances
    footerLabel.numberOfLines = 0
    footerLabel.font = UIFont.customCaption1
    footerLabel.textColor = Style.secondaryTextColor
    contentView.addSubview(footerLabel)
    footerLabel.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(10)
      make.left.right.equalTo(weakSelf.contentView).inset(Const.HorizontalPadding)
    }
    self.lastControlBottom = footerLabel.snp.bottom
    
  }
  
  @objc func dateTimePickerChanged(datePicker: UIDatePicker) {
    scrollToTop(view: speechView4)
    print(datePicker.date.customFriendlyString())
    followupDateTextField.text = datePicker.date.customFriendlyString()
    //followupDateTextField.font = UIFont.customBody.bolded
    model.set(chatDate: datePicker.date)
    model.plan?.hadChat = false
    model.plan?.isFollowup = true
  }
  
  
  // Section 5 - What might you chat about at the follow-up? (UITextView)
  func addSection5() {
    
    speechView5 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.followup_s5_title))
    contentView.addSubview(speechView5)
    speechView5.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView5.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView5.snp.bottom
    
    s5TextView = customTextView(placeholder: strings.followup_s5_i1_placeholder)  // KMPlaceholderTextView
    s5TextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s5TextView)
    s5TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(80)
    }
    lastControlBottom = s5TextView.snp.bottom
    s5TextView.text = model.get(.followup_s5) as? String
    
    
  }
  
  // Animate elements in from the bottom of the screen, and set s1TextView as first responder
  func animateInSections() {
    
    speechView1.center.y += view.bounds.height
    speechView2.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView1.center.y -= weakSelf.view.bounds.height
                    weakSelf.speechView2.center.y -= weakSelf.view.bounds.height
      }, completion: { _ in
        
        //        if self.isFirstRun {
        //          self.s1TextView.becomeFirstResponder()
        //        }
        
    })
    
  }
  
  
  // Override for offset as this is a modal form
  
  override func scrollToTop(view: UIView, animated: Bool = true) {
    print("scrollToTop: \(view)")
    self.scrollView.setContentOffset(CGPoint(x:0, y: view.frame.origin.y - 60), animated: animated)
  }
  
  
}


// MARK: UITextFieldDelegate - date field
extension Followup {
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    textField.layer.borderColor = Style.primaryColor.cgColor
    
    if textField == followupDateTextField {
      scrollToTop(view: speechView4)
      previousChatDate = model.getChatDate()         // Record the current chatDate
    }
  }
  
  // Tapped "Done", on another field, or view disappeared
  func textFieldDidEndEditing(_ textField: UITextField) {
    textField.layer.borderColor = Style.secondaryTextColor.cgColor
    
    // Save date when editing of date field is finished
    if textField == followupDateTextField {
      print("Followup followupDateTextField DidEndEditing")
      
      // Check if the chat date has changed, if so reschedule or cancel notification
      if model.getChatDate() != previousChatDate {
        if let followupState = model.plan?.isFollowup {
          scheduleNotificationsAndCalendar(isFollowup: followupState)
          model.savePlans()
        }
      }
    }
  }
  
  
  func textFieldShouldClear(_ textField: UITextField) -> Bool {
    if textField == followupDateTextField {
      print("Followup textFieldShouldClear followupDateTextField")
      model.set(chatDate: nil)
      model.plan?.hadChat = true
      model.plan?.isFollowup = false
    }
    return true
  }
  
  /*
  func scheduleNotificationsAndCalendar() {
    
    /*
    // Check if there's already a notification scheduled
    if let chatNotificationId = model.plan?.chatNotificationId {
      // if there's a new date, reschedule the notification
      if let chatDate = model.getChatDate() {
        print("P4 Attempting to re-schedule a local notification for \(chatDate)")
        scheduleLocalNotification(withId: chatNotificationId, forDate: chatDate, title: "Follow-up chat with \(model.getName())", body: "Chat Notification")
      } else { // previously scheduled, but now deleted (nil), so cancel the notification
        cancelNotification(withId: chatNotificationId)
      }
    } else { // brand new notification to schedule
      if let chatDate = model.getChatDate() {
        let chatNotificationId = UUID().uuidString
        print("P4 Attempting to schedule a local notification for \(chatDate)")
        scheduleLocalNotification(withId: chatNotificationId, forDate: chatDate, title: "Follow-up chat with \(model.getName())", body: "Chat Notification")
        model.plan?.chatNotificationId = chatNotificationId
      }
    }
    
    
    // Also schedule in the calendar ...
    if let chatDate = model.getChatDate() {
      print("Attempting to schedule conversation in calendar for \(chatDate)")
      
      if let chatEventId = addConversationEvent(startDate: chatDate, title: "Follow-up chat with \(model.getName())") {
        // save the conversation eventId
        print("Returned calendar event Id: \(chatEventId)")
        model.plan?.chatEventId = chatEventId
        //TODO: Toast that it's been added to their calendar
      } else {
        // error during saving event
        print("Error scheduling conversation event")
      }
      
    }
    */
    
  }
  */




// MARK: UITextViewDelegate



override func textViewDidBeginEditing(_ textView: UITextView) {
  
  print("Followup textViewDidBeginEditing")
  
  // Scroll to the top of this section and record previous text
  switch textView {
    
  case s3TextView:
    scrollToTop(view: speechView3)
    previousS3Text = s3TextView.text
    
  case s5TextView:
    scrollToTop(view: speechView5)
    previousS5Text = s5TextView.text
    
  default:
    break
  }
  
  textView.layer.borderColor = Style.primaryColor.cgColor
  
}


override func textViewDidEndEditing(_ textView: UITextView) {
  print("Followup textViewDidEndEditing")
  textView.layer.borderColor = Style.secondaryTextColor.cgColor
  
  // Save data if changed
  switch textView {
    
  case s3TextView:
    s3TextView.text = s3TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
    if s3TextView.text != previousS3Text {
      model.set(.followup_s3, value: s3TextView.text)
      model.savePlans()
    }
    
  case s5TextView:
    s5TextView.text = s5TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
    if s5TextView.text != previousS5Text {
      model.set(.followup_s5, value: s5TextView.text)
      model.savePlans()
    }
    
  default:
    break
    
  }
  
}


func keyboardToolbar() -> UIToolbar  {
  let keyboardToolbar = UIToolbar()
  keyboardToolbar.backgroundColor = Style.secondaryColor.withAlpha(0.75)
  keyboardToolbar.sizeToFit()
  let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
  let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
  keyboardToolbar.items = [flexBarButton, doneBarButton]
  return keyboardToolbar
}

@objc func dismissKeyboard() {
  /*
   // Scroll to the top of the next section
   if s3TextView.isFirstResponder {
   scrollToTop(view: speechView4)
   } else if s5TextView.isFirstResponder {
   scrollToTop(view: speechView5)
   }
   */
  view.endEditing(true)
}

}



