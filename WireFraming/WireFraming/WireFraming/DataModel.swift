//
//  DataModel.swift
//  WireFraming
//
//  Created by Peter Hunt on 7/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation


// DataModel
// user: settings for the current user
// plans: array of all plans
// plan: current plan
// .savePlans(): save plans to disk
// .loadPlans(): load plans from disk

extension Notification.Name { // notification posted when model is changed
  static let modelChangeNotification = Notification.Name("ModelChangeNotification")
}

// Friendly typed access to keys
enum Key: String {
  case p4_s2 = "p4_s2"
  case p4_s3 = "p4_s3"
  case p4_s4 = "p4_s4"
  
  case p5_s1 = "p5_s1"
  case p5_s2 = "p5_s2"
  case p5_s2_other = "p5_s2_other"
  case p5_s3 = "p5_s3"
  case p5_s3_other = "p5_s3_other"
  case p5_s4 = "p5_s4"
  case p5_s5 = "p5_s5"
  
  case followup_s3 = "followup_s3"
  case followup_s5 = "followup_s5"
}

class DataModel {
  var isFirstLaunch: Bool = true
  var isUnlocked: Bool = false
  var userSettings: UserSettings!
  var plans: [Plan] = []    // all plans
  var plan: Plan?           // the current plan

  
  
  init() {
    
    // ** DEBUGGING first launch experience ... //
    //reset() // ** DEBUGGING - delete all saved data for the App
    
    // Load user settings or create a new user
    if let existingUserSettings = UserSettings.loadFromDefaults() {
      userSettings = existingUserSettings
      isFirstLaunch = false
    } else {
      userSettings = UserSettings()
    }

    print(userSettings.debugDescription)
    
    // Load existing plans if they exist
    loadPlans()
    
//    loadWithSampleData()
  }
  
  // save all plans to U
  func savePlans() {
    print("*** DataModel save() All Plans ***")
     print("About to save: \(plans.debugDescription)")

    let filePath = getDocumentsDirectory().appendingPathComponent(Const.PlansFileName).path
    
    if NSKeyedArchiver.archiveRootObject(plans, toFile: filePath) {
      print(plans.debugDescription)
      NotificationCenter.default.post(name: .modelChangeNotification, object: nil)
      print("Save to file successful \(filePath)")
    } else {
      print("savePlans() error writing file \(filePath)")
      
    }
//    NSKeyedArchiver.archiveRootObject(plans, toFile: filePath)
/*
    let plansData = NSKeyedArchiver.archivedData(withRootObject: plans)
    do {
      try plansData.write(to: filePath)
      print(plans.debugDescription)
      NotificationCenter.default.post(name: .modelChangeNotification, object: nil)
      print("Save successful")
    } catch {
      print("savePlans() error writing file")
    }
  */
  }
  
  // load all plans from UserDefaults
  func loadPlans() {
    print("*** DataModel load() All Plans ***")
    
    let filePath = getDocumentsDirectory().appendingPathComponent(Const.PlansFileName).path
    if FileManager.default.fileExists(atPath: filePath) {
      let plansArray = NSKeyedUnarchiver.unarchiveObject(withFile: filePath) as? [Plan]
      if plansArray == nil {
        print("Error: returned nil")
      } else {
        self.plans = plansArray!
        print(plans.debugDescription)
      }
      
    } else {
      print("Error: file doesn't exist \(filePath)")
    }
    
    
    /*
    guard let plansData = UserDefaults.standard.object(forKey: "plans") as? NSData else {
      print("'plans' not found in UserDefaults")
      return
    }
 
    let unarchiver = NSKeyedUnarchiver(forReadingWith: plansData as Data)
    do {
      if let plansArray = try unarchiver.decodeTopLevelObject() as? [Plan] {
        self.plans = plansArray
        print(plans.debugDescription)
      }
    }
    catch {
      print("Error unarchiving plans")
      return
    }
    */
    /*
    guard let plansArray = NSKeyedUnarchiver.unarchiveObject(with: plansData as Data) as? [Plan] else {
      print("Could not unarchive from plansData")
      return
    }
    */
    
  }
  
  func loadWithSampleData() {
    //Set up sample data
    plans = [Plan(planId: UUID().uuidString, name: "Fred"),
             Plan(planId: UUID().uuidString, name: "Jill"),
             Plan(planId: UUID().uuidString, name: "Joe")]
    
    print("*** DataModel Loaded with Sample Data ***")
    
    }
  
  // Delete all UserDefaults for the app
  func reset() {
    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    plans = []
    plan = nil
    isFirstLaunch = true
    userSettings = UserSettings()
  }
  
  // Create a new empty plan with a unique UUID
  // Add to model.plans
  // Use: model.plan = model.newPlan()
  func newPlan() -> Plan {
    let newPlanId = UUID().uuidString
    let newPlan = Plan(planId: newPlanId)
    plans.append(newPlan)
    return newPlan
  }
  
  
  /// Get an item from the current plan with a key, and return an Any? (Generally a String? or [String]?)
  ///
  /// - Parameter key: key to lookup
  /// - Returns: item as a String, or nil if not found or not a String
  func get(_ key: Key) -> Any? {
    guard let plan = plan, let item = plan.items[key.rawValue] else {
      print("get - no item found for \(key)")
      return nil
    }
    return item
  }
  
  
  /// Set an item in the current plan with a key, value (Generally a String or [String])
  ///
  /// - Parameter key: key to set
  /// - Parameter value: value to set (generally a String or [String])
  func set(_ key: Key, value: Any) {
    if let plan = plan {
      plan.items[key.rawValue] = value
    }
  }

  
  func getChatDate() -> Date? {
    return plan?.chatDate
  }
  
  func set(chatDate: Date?) {
    if let plan = plan {
      plan.chatDate = chatDate
    }
  }
  
  /*
  func getFollowupDate() -> Date? {
    return plan?.followupDate
  }
  
  func set(followupDate: Date?) {
    if let plan = plan {
      plan.followupDate = followupDate
    }
  }
  */
  
  // mark the page as complete in the current plan
  func markComplete(page: String) {
    guard let plan = plan else {
      return
    }
    
    if !plan.pagesComplete.contains(page) {
      plan.pagesComplete.append(page)
    }
    
  }
  
  func getName() -> String {
    return plan?.name ?? ""
  }
  
  func setName(_ name: String) {
    if let plan = plan {
      plan.name = name
    }
  }
  
  func replaceTokens(inString: String) -> String {
    return inString.replacingOccurrences(of: "%s", with: getName())
  }
  
  func planWithId(id: String) -> Plan? {
    for plan in plans {
      if plan.planId == id  {
        return plan
      }
    }
    return nil
  }

  func getDocumentsDirectory() -> URL {
    let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    return path!
  }
  
  
  
  /*** OLD ***
  
  
  /// Save page data to current plan and persist to disk
  ///
  /// - Parameters:
  ///   - page: key for the page (eg. "P4")
  ///   - withData: dictionary of data for the page
  /// - Returns: true if saved OK
  func save(page: String, withData: [String:Any]) -> Bool {
    guard let plan = plan else {
      return false
    }
    plan.pages[page] = withData
    savePlans()
//    print("*** Save Page \(page) Data: \(withData) ***")
    return true
  }

  /// Get page data from current plan
  ///
  /// - Parameter page: key for the page to retrieve (eg. "P4")
  /// - Returns: dictionary of data for the page. nil if data doesn't exist
  func get(page: String) -> [String: Any]? {
    guard let plan = plan, let pageData = plan.pages[page] else {
      print("get - no plan or pages")
      return nil
    }
    
    if pageData is Dictionary<String, Any> {
//      print("*** Load Page \(page) Data: \(pageData) ***")
      return pageData as? Dictionary<String, Any>
    } else {
      return nil
    }

  }
  
  
  /// Get an item from the current plan, and return a String
  ///
  /// - Parameter string: key to lookup
  /// - Returns: item as a String, or nil if not found or not a String
  func get(string: String) -> String? {
    guard let plan = plan, let item = plan.items[string] else {
      print("get - no item found for \(string)")
      return nil
    }
    if item is String {
      return item as? String
    } else {
      return nil
    }
  }
  
  
  
  /// Get an item from the current plan, and return a Array<String>
  ///
  /// - Parameter string: key to lookup
  /// - Returns: item as an Array<String?, or nil if not found or not an Array<String>
  func get(array: String) -> Array<String>? {
    guard let plan = plan, let item = plan.items[array] else {
      print("get - no item found for \(array)")
      return nil
    }
    if item is Array<String> {
      return item as? Array<String>
    } else {
      return nil
    }
  }
  
 ****/

 
}

