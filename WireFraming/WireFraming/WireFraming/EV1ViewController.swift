//
//  EV1ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import BMPlayer


class EV1ViewController: UIViewController, Injectable, EventAccess {
  let videoFile = "ev1_1080p.mp4"
  let subtitleFile = "ev1.srt"
  
  // Injectable
  var identifier: String!
  var strings: StringsModel!
  var model: DataModel!
  
  func setDependencies(identifier: String, strings: StringsModel, model: DataModel) {
    self.identifier = identifier
    self.strings = strings
    self.model = model
  }
  
  @IBOutlet weak var scrollView: UIScrollView!
//  @IBOutlet weak var nextButton: AnimatableButton!
  
  var player: BMPlayer!
  var speechView1, speechView2, speechView3, speechView4: SpeechBubbleView!
  var continueButton, continueButton2: UIButton!
  var approvalView: UIView!
  var contentView: UIView!
  
  /*
  @IBAction func nextButtonTap(_ sender: Any) {
    if let parentPageViewController = self.parent as? WelcomePageViewController {
      parentPageViewController.goToNextPage()
    }
  }
 */
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    UIApplication.shared.isStatusBarHidden = false
    // Allow rotation for this VC
    (UIApplication.shared.delegate as! AppDelegate).shouldRotate = true
    
    self.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
    
    let contentWidth = scrollView.frame.width
    let contentHeight: CGFloat = 1500
    contentView = UIView(frame: CGRect(x: 0, y: 0, width: contentWidth, height: contentHeight))
    scrollView.addSubview(contentView)
    scrollView.contentSize = CGSize(width: contentWidth, height: contentHeight)
    
    // DON'T change contentView's translatesAutoresizingMaskIntoConstraints,
    // which defaults to YES;
    // Set the content size of the scroll view to match the size of the content view:
//    self.scrollView.
    
    // Welcome speech bubble, intro to EV1 video, and skip/continue button
    addSubviews()      // add all subviews
    animateSection1()  // animate in section 1. section 2 animates in after skip button
  }
  
  
  // Add all views to the contentView
  func addSubviews() {
    
    speechView1 = SpeechBubbleView(type: .primary, text: "Welcome to Conversations for Life")
    self.contentView.addSubview(speechView1)
    
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(self.contentView).offset(10)
      make.left.right.equalTo(self.contentView)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    
    speechView2 = SpeechBubbleView(type:.secondary, text: "This app helps you plan a conversation with a friend. Check out this video to find out how.")
    self.contentView.addSubview(speechView2)
    
    speechView2.snp.makeConstraints { (make) in
      make.top.equalTo(speechView1.snp.bottom).offset(-15)
      make.width.equalTo(self.contentView)
      make.height.equalTo(speechView2.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    
    player = BMPlayer()
    self.contentView.addSubview(player)
    setVideoPortrait()  // setup constraints for player in portrait mode
    player.backBlock = { (isFullScreen) in
      if isFullScreen == true {
        return
      }
    }
    
    // Listen to when the player is playing or stopped
    player.playStateDidChange = { [unowned self] (isPlaying: Bool) in
      print("playStateDidChange \(isPlaying)")
      if !isPlaying {
        self.continueButton.setTitle("Next", for: .normal)
      } else {
        self.continueButton.setTitle("Skip", for: .normal)
      }
    }
    
    //Listen to when the play time changes
    player.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
      print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    
    let subtitleUrl = Bundle.main.url(forResource: subtitleFile, withExtension: nil)!
    let subtitle = BMSubtitles(url: subtitleUrl)
    
    //let coverUrl = Bundle.main.url(forResource: "1024x1024.jpg", withExtension: nil)!
    //let asset = BMPlayerResource(url: videoUrl, name: "Welcome")
    //player.setVideo(resource: asset)
    //    player.autoPlay()
    
    let asset = BMPlayerResource(name: "Welcome",
                                 definitions: [BMPlayerResourceDefinition(url: videoUrl, definition: "1080p")],
                                 cover: nil, //coverUrl,
      subtitles: subtitle)
    
    player.setVideo(resource: asset)
    
    continueButton = UIButton.custom("Skip", target: self, action: #selector(animateSection2))
    continueButton.translatesAutoresizingMaskIntoConstraints = false
    self.contentView.addSubview(continueButton)
    
    continueButton.snp.makeConstraints { (make) in
      //      make.bottom.equalTo(self.contentView.snp.bottom).offset(-60)
      make.top.equalTo(player.snp.bottom).offset(20)
      make.centerX.equalTo(self.contentView.snp.centerX)
      //      make.bottom.equalTo(contentView.snp.bottom).offset(20)
      make.width.equalTo(160)
      make.height.equalTo(50)
    }

    
    
    
    // PART 2 - After Skip/Next is tapped
    speechView3 = SpeechBubbleView(type: .primary, text: "Would you like subtitles, and to keep your plans secure with a passcode ?")
    self.contentView.addSubview(speechView3)
    speechView3.isHidden = true
    
    speechView3.snp.makeConstraints { (make) in
      make.top.equalTo(player.snp.bottom).offset(20)
      make.left.right.equalTo(self.contentView)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    
    
    // Approve Notifications and set PIN code
    approvalView = UIView()
    self.contentView.addSubview(approvalView)
    approvalView.isHidden = true

    //approvalView.backgroundColor = UIColor.brown
    approvalView.snp.makeConstraints { (make) in
      make.top.equalTo(speechView3.snp.bottom).offset(-10)
      make.left.right.equalTo(self.contentView)
      //make.height.equalTo(speechView1.snp.width).multipliedBy(200.0/500.0).priority(750)
      make.height.equalTo(120).priority(750)
    }
    
    let subTitlesLabel = UILabel()
    subTitlesLabel.text = "Subtitles"
    subTitlesLabel.textColor = Style.primaryTextColor
    subTitlesLabel.font = UIFont.customBody
    
    approvalView.addSubview(subTitlesLabel)
    subTitlesLabel.snp.makeConstraints { (make) in
      make.left.equalTo(approvalView).offset(40)
      make.top.equalTo(approvalView).offset(10)
      make.height.equalTo(40)
    }
    
    let subTitlesSwitch = UISwitch()
    model.userSettings.isSubtitlesEnabled = true
    subTitlesSwitch.isOn = model.userSettings.isSubtitlesEnabled // default to on
    subTitlesSwitch.tag = 0
    subTitlesSwitch.addTarget(self, action: #selector(switchValueDidChange(sender:)), for: .valueChanged)
    approvalView.addSubview(subTitlesSwitch)
    
    subTitlesSwitch.snp.makeConstraints { (make) in
      make.centerY.equalTo(subTitlesLabel)
      make.right.equalTo(approvalView).offset(-40)
      make.height.equalTo(40)
    }
    
    let pinLabel = UILabel()
    pinLabel.text = "Passcode / Touch ID"
    pinLabel.textColor = Style.primaryTextColor
    pinLabel.font = UIFont.customBody
    
    approvalView.addSubview(pinLabel)
    pinLabel.snp.makeConstraints { (make) in
      make.left.equalTo(approvalView).offset(40)
      make.top.equalTo(subTitlesLabel.snp.bottom).offset(20)
//      make.height.equalTo(40)
    }
    
    let pinSwitch = UISwitch()
    model.userSettings.isPasscodeEnabled = false
    pinSwitch.isOn = model.userSettings.isPasscodeEnabled // default to off
    
    pinSwitch.tag = 2
    pinSwitch.addTarget(self, action: #selector(switchValueDidChange(sender:)), for: .valueChanged)
    approvalView.addSubview(pinSwitch)
    
    pinSwitch.snp.makeConstraints { (make) in
      make.centerY.equalTo(pinLabel)
      make.right.equalTo(approvalView).offset(-40)
//      make.height.equalTo(40)
    }
    
    continueButton2 = UIButton.custom("Continue", target: self, action: #selector(nextScreen))
    contentView.addSubview(continueButton2)
    continueButton2.isHidden = true

    continueButton2.snp.makeConstraints { (make) in
      make.top.equalTo(approvalView.snp.bottom).offset(20)
      make.centerX.equalTo(contentView.snp.centerX)
      //make.bottom.equalTo(contentView.snp.bottom).offset(80)
      make.width.equalTo(160)
      make.height.equalTo(50)
    }

    view.setNeedsLayout()
    
  }
  
  override func viewDidLayoutSubviews() {
    // Manually set the contentSize of the scrollView to extend past the bottom of continueButton2
    let contentSizeY = continueButton2.frame.origin.y + continueButton2.frame.height
    if contentSizeY > 0 {
      scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: contentSizeY)
    }
    print("EV1 viewDidLayoutSubviews. scrollView contentSize: \(scrollView.contentSize)")
    
  }
  
  // Animate in Section 1 from the bottom of the screen and play the video
  func animateSection1() {
    
    speechView1.center.y += view.bounds.height
    speechView2.center.y += view.bounds.height
    player.center.y += view.bounds.height
    continueButton.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [unowned self] in
                    self.speechView1.center.y -= self.view.bounds.height
      }, completion: nil)
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: Animation.DefaultDelay,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [unowned self] in
                    self.speechView2.center.y -= self.view.bounds.height
                    
      }, completion: nil)
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: Animation.DefaultDelay * 2,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [unowned self] in
                    self.player.center.y -= self.view.bounds.height
                    self.continueButton.center.y -= self.view.bounds.height
      }, completion: { [unowned self] _ in
        self.player.play()
    })

  }
  
  // After the Skip/Next button is tapped ...
  // Animate in Section 2
  func animateSection2() {
    player.pause()
//    self.continueButton.isHidden = true
    
    speechView3.isHidden = false
    continueButton2.isHidden = false
    approvalView.isHidden = false
    
    self.scrollToTop(view: self.speechView3)
    
    speechView3.center.y += view.bounds.height
    approvalView.center.y += view.bounds.height
    continueButton2.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [unowned self] in
                    //                    self.continueButton.isHidden = true
                    self.continueButton.alpha = 0.0
                    self.speechView3.center.y -= self.view.bounds.height
                    self.approvalView.center.y -= self.view.bounds.height
                    self.continueButton2.center.y -= self.view.bounds.height
                    
      }, completion: { /*[unowned self]*/ _ in
        
        //                self.continueButton.isHidden = false
        //                self.scrollView.scrollRectToVisible(approvalView.frame, animated: true)
        //self.scrollToTop(view: self.speechView3)
        
    })
    
  }
  
  
  /// Scroll so that the view appears at the top of the scrollview
  ///
  /// - Parameter view: view to put at the top of the scrollview
  func scrollToTop(view: UIView, animated: Bool = true) {
    self.scrollView.setContentOffset(CGPoint(x:0, y: view.frame.origin.y - 40), animated: animated)
  }
  
  
  func switchValueDidChange(sender: UISwitch!) {

    if sender.tag == 0 { // Subtitles
      print("EV1 Subtitles switch changed")
      model.userSettings.isSubtitlesEnabled = sender.isOn
      
    } else if sender.tag == 1 { // Notifications
      print("EV1 Notifications switch changed")
      
      if sender.isOn {
        // Request authorisation for User Notifications
        if #available(iOS 10.0, *) {
          requestNotificationsAccess(options: [.alert, .badge, .sound]) { [weak self] allowed in
            if !allowed {
              print("Notifications access not granted")
            } else {
              self?.model.userSettings.isNotificationsEnabled = true
            }
          }
        } else {
          // Fallback on earlier versions
        }
        
      } else {
          self.model.userSettings.isNotificationsEnabled = false
      }
        
    } else if sender.tag == 2 { // Passcode
      
      print("EV1 Passcode switch changed")
      if sender.isOn {  // Set a new passcode - from Injectable
        presentModalPasscodeController(setNewPasscode: true) { [weak self] in
          self?.model.isUnlocked = true
          self?.model.userSettings.isPasscodeEnabled = true
          }
      } else {
        self.model.isUnlocked = true
        self.model.userSettings.isPasscodeEnabled = false
      }
      
    }
    
    self.model.userSettings.saveToDefaults()
    
    
  }

  
  func nextScreen() {
  /*
   // Show the main menu
   let mainMenuScreen = newViewController(withId: "MainMenu")
   navigationController?.pushViewController(mainMenuScreen, animated: false)
   */
    print("EV1 nextScreen()")
    
    // Request authorisation for User Notifications
    if #available(iOS 10.0, *) {
      requestNotificationsAccess(options: [.alert, .badge, .sound]) { [weak self] allowed in
        if !allowed {
          print("Notifications access not granted")
        } else {
          self?.model.userSettings.isNotificationsEnabled = true
          self?.model.userSettings.saveToDefaults()
        }
      }
    } else {
      // Fallback on earlier versions
    }

    
    let mainMenuScreen = newViewController(withId: "MainMenu")
    navigationController?.setViewControllers([mainMenuScreen], animated: true)
 
  }
  
  func setVideoFullScreen() {
    player.snp.remakeConstraints { (make) in
      make.left.right.top.equalTo(self.view)
      make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
    }
  }
  
  func setVideoPortrait() {
    player.snp.remakeConstraints { (make) in
      make.top.equalTo(speechView2.snp.bottom).offset(0)
      make.left.right.equalTo(self.contentView)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
      make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
    }
  }
  
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    let orientation = newCollection.verticalSizeClass
    
    switch orientation {
    case .compact:
      print("EV1 now in Landscape")  ///Excluding iPads!!!
      self.navigationController?.setNavigationBarHidden(true, animated: true)
      self.navigationController?.setToolbarHidden(true, animated: true)
      
      setVideoFullScreen()
      
    default:
      print("EV1 now in Portrait")
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      self.navigationController?.setToolbarHidden(false, animated: true)
      
      setVideoPortrait()
    }
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    print("EV1 viewWillDisappear")
    player.pause()
  }
  
  
}

