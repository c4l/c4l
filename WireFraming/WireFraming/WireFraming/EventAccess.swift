//
//  EventAccess.swift
//  WireFraming
//
//  Created by Peter Hunt on 12/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
//import AMGCalendarManager
import EventKit
import UserNotifications
import BRYXBanner
import SwiftIcons

protocol EventAccess {
  func requestReminderAccess(completion: @escaping (_ allowed: Bool) -> ())
  @available(iOS 10.0, *)
  func requestNotificationsAccess(options: UNAuthorizationOptions, completion: @escaping (_ allowed: Bool) -> ())
  func scheduleLocalNotification(withId: String, forDate: Date, title: String, body: String)
  func cancelNotification(withId: String)
}


extension EventAccess {
  
  // MARK: - Reminders Access
  // Request access to the Event Store for Reminders
  func requestReminderAccess(completion: @escaping (_ allowed: Bool) -> ()) {
    EKEventStore().requestAccess(to: EKEntityType.reminder) { (granted, error) in
      if !granted {
        print("Access to store for reminders not granted")
        completion(false)
      } else {
        completion(true)
      }
    }
  }
  
  
  // MARK: - Local Notifications Access
  // Request authorisation for User Notifications
  @available(iOS 10.0, *)
  func requestNotificationsAccess(options: UNAuthorizationOptions, completion: @escaping (_ allowed: Bool) -> ()) {
    //let options: UNAuthorizationOptions = [.alert, .sound, .badge]
    let center = UNUserNotificationCenter.current()
    center.requestAuthorization(options: options) { (granted, error) in
      if !granted {
        print("Access to notifications not granted")
        completion(false)
      } else {
        completion(true)
      }
    }
  }
  
  
  
  func scheduleLocalNotification(withId: String = UUID().uuidString, forDate: Date,
                                 title: String, body: String = "") {
    
    if #available(iOS 10.0, *) {
      let center = UNUserNotificationCenter.current()
      let content = UNMutableNotificationContent()
      content.title = title
      content.body = body
      content.categoryIdentifier = "alarm"
      //content.userInfo = ["customData": "fizzbuzz"]
      content.sound = UNNotificationSound.default
      
      let currentBadge = UIApplication.shared.applicationIconBadgeNumber
      content.badge = NSNumber(value: currentBadge + 1)
      
      let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: forDate)
      let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
      //      let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
      
      //      let uniqueID = UUID().uuidString
      
      let request = UNNotificationRequest(identifier: withId, content: content, trigger: trigger)
      
      center.add(request, withCompletionHandler: { (error) in
        if error != nil {
          // Something went wrong
          print("Error scheduling notification")
        } else {

        }
      })
      
      
      
    } else {
      
      // iOS 9
      let notification = UILocalNotification()
      notification.fireDate = forDate
      notification.alertBody = body
      notification.alertAction = title
      notification.soundName = UILocalNotificationDefaultSoundName
      notification.applicationIconBadgeNumber += 1
      notification.userInfo = ["id": withId]
      
      UIApplication.shared.scheduleLocalNotification(notification)

    }
    
  }
  
  
  func cancelNotification(withId: String) {
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [withId])
    } else {
      // iOS 9 - search for is in userInfo dictionary
      if let localNotifications = UIApplication.shared.scheduledLocalNotifications {
        for event in localNotifications {
          if let userInfoCurrent = event.userInfo as? [String:AnyObject],
            let notificationId = userInfoCurrent["id"] as? String {
            if notificationId == withId { // Cancelling local notification
              UIApplication.shared.cancelLocalNotification(event)
              break
            }
          }
        }
      }
    }
  }
  
  
}

extension EventAccess where Self: UIViewController {
  
  // MARK: - Calendar Access
  
  /// Adds an event to the users's calendar
  ///
  /// - Parameter startDate: date/time for the conversation
  /// - Returns: unique identifier for that event
  func addChatEvent(startDate: Date, title: String,
                    location: String = "", notes: String = "") -> String? {
    // Create a new EKEvent
    let event = EKEvent(eventStore: EventStore.store)
    event.startDate = startDate
    event.endDate = startDate + 3600  // 1 hour duration
    event.title = title
    event.location = location
    event.notes = notes
    event.availability = .busy
    event.addAlarm(EKAlarm(relativeOffset: -30 * 60))  // alarm 30 minutes before
        
    // Add the event to the default calendar (returns nil if an error)
    if let eventIdentifier = EventStore.addEvent(event) {
      DispatchQueue.main.async {
        let banner = Banner(title: "Scheduled in Calendar", subtitle: title + ", " + startDate.customFriendlyString(), image: UIImage.init(icon: .googleMaterialDesign(.notificationsActive), size: CGSize(width: 35, height: 35), textColor: Style.inverseTextColor), backgroundColor: Style.primaryColor)
        banner.textColor = Style.inverseTextColor
        banner.titleLabel.font = UIFont.customHeadline
        banner.detailLabel.font = UIFont.customSubheadline
        banner.dismissesOnTap = true
        banner.position = .top
        banner.show(duration: Animation.BannerDuration)
      }
      return eventIdentifier
    } else {
      return nil
    }
    
  }

  
  /// Move a chat event in the calendar
  ///
  /// - Parameters:
  ///   - eventId: unique identifier
  ///   - startDate: new startDate
  ///   - title: new title
  ///   - location: new location
  ///   - notes: new notes
  /// - Returns: true = successful move
  func moveChatEvent(withIdentifier eventId: String, startDate: Date, title: String,
                     location: String = "", notes: String = "") -> Bool {
    
    if let event = EventStore.store.event(withIdentifier: eventId) {
      event.startDate = startDate
      event.endDate = startDate + 3600  // 1 hour duration
      event.title = title
      event.location = location
      event.notes = notes
      event.availability = .busy
      //event.addAlarm(EKAlarm(relativeOffset: -30 * 60))  // alarm 30 minutes before
        
      // Add the event to the default calendar (returns nil if an error)
      if let _ = EventStore.addEvent(event) {
        
        DispatchQueue.main.async {
          let banner = Banner(title: "Re-scheduled in Calendar", subtitle: title + ", " + startDate.customFriendlyString(), image: UIImage.init(icon: .googleMaterialDesign(.notificationsActive), size: CGSize(width: 35, height: 35), textColor: Style.inverseTextColor), backgroundColor: Style.primaryColor)
          banner.textColor = Style.inverseTextColor
          banner.titleLabel.font = UIFont.customHeadline
          banner.detailLabel.font = UIFont.customSubheadline
          banner.dismissesOnTap = true
          banner.position = .top
          banner.show(duration: Animation.BannerDuration)
        }

        return true // success
      } else {
        return false
      }
      
    } else {
      return false
    }
  }

  /// Delete a chat event in the calendar
  func deleteChatEvent(withIdentifier identifier: String) {
    if let event = EventStore.getEvent(withIdentifier: identifier) {
      if EventStore.removeEvent(event) {
        
        DispatchQueue.main.async {
          let banner = Banner(title: "Removed from Calendar", subtitle: "", image: UIImage.init(icon: .googleMaterialDesign(.notificationsActive), size: CGSize(width: 35, height: 35), textColor: Style.inverseTextColor), backgroundColor: Style.primaryColor)
          banner.textColor = Style.inverseTextColor
          banner.titleLabel.font = UIFont.customHeadline
          banner.detailLabel.font = UIFont.customSubheadline
          banner.dismissesOnTap = true
          banner.position = .top
          banner.show(duration: Animation.BannerDuration)
        }
      }
      
    }
  }
  
  
}

