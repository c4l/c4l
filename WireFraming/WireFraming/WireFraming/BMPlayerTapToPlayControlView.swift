//  BMPlayerTapToPlayControlView.swift

import UIKit
//import BMPlayer

class BMPlayerTapToPlayControlView: BMPlayerControlView {
  
  override func onTapGestureTapped(_ gesture: UITapGestureRecognizer) {
    print("BMPlayercustomControlView tapped")
    super.onTapGestureTapped(gesture)
    
    // redirect tap action to play button action
    delegate?.controlView(controlView: self, didPressButton: playButton)
  }
  
  
  // Override to turn off hiding of status bar
  /**
   Implement of the control view animation, override if need's custom animation
   
   - parameter isShow: is to show the controlview
   */
  override func controlViewAnimation(isShow: Bool) {
    let alpha: CGFloat = isShow ? 1.0 : 0.0
    self.isMaskShowing = isShow
    
    //UIApplication.shared.setStatusBarHidden(!isShow, with: .fade)
    
    UIView.animate(withDuration: 0.3, animations: {
      self.topMaskView.alpha    = alpha
      self.bottomMaskView.alpha = alpha
      self.mainMaskView.backgroundColor = UIColor ( red: 0.0, green: 0.0, blue: 0.0, alpha: isShow ? 0.4 : 0.0)
      
      if isShow {
        if self.isFullscreen { self.chooseDefinitionView.alpha = 1.0 }
      } else {
        self.replayButton.isHidden = true
        self.chooseDefinitionView.snp.updateConstraints { (make) in
          make.height.equalTo(35)
        }
        self.chooseDefinitionView.alpha = 0.0
      }
      self.layoutIfNeeded()
    }) { (_) in
      if isShow {
        self.autoFadeOutControlViewWithAnimation()
      }
    }
  }

  
  
}
