//
//  PlanSummaryTableViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 28/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit

class PlanSummaryTableViewController: UITableViewController, Injectable {
  
  let pages = ["P4", "P5", "P6", "P7"]
  let cellIdentifier = "PlanItemCell"
  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.separatorStyle = .none
    
    // dynamic table view cell height
    tableView.estimatedRowHeight = tableView.rowHeight
    tableView.rowHeight = UITableViewAutomaticDimension
    
    // Register to listen for a model change notification, and refresh the table view data
    NotificationCenter.default.addObserver(forName: .modelChangeNotification, object: nil, queue: OperationQueue.main) { notification in
      print("Model Change Notification - reloading plan summary data")
      self.tableView.reloadData()
    }
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
}


// MARK: UITableViewDataSource
extension PlanSummaryTableViewController {
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2 //model.plan?.pages.count ?? 0
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PlanItemCell
    
    if indexPath.row >= 0 && indexPath.row < pages.count {
      configure(cell: cell, withPage: pages[indexPath.row])
    }
    
    return cell
  }
  
  
  /*
   [Plan ID: 1, name: Fred, , , P4: ["p4_s4_i1_4": "Surprised", "p4_s5_i1": "In the park", "p4_s3_i1": "He\'s withdrawn", "p4_s4_i1_2": "Angry", "p4_s2_i1": "A lot has been happening\n"], P5: ["p5_s3_i1_4": "Future work opportunities", "p5_s2_i1_5": "Playing sport", "p5_s4_i1": "Hopefully a lot better", "p5_s1_i1_4": "Professional Services", "p5_s1_i1_3": "Community Groups", "p5_s2_i1_6": "Going to the beach"], , pages complete: ["P4", "P5"]]
   */
  
  
  /// Populate a cell with data from a page
  ///
  /// - Parameters:
  ///   - cell: PlanItemCell to configure
  ///   - withPage: page to retrive from the model - eg. "P4"
  func configure(cell: PlanItemCell, withPage: String) {
    
    /*
    guard let currentPage = model.get(page: withPage) else { // no page data found
      return
    }
    */
    
    //print("Configure cell with page: \(withPage). Data: \(currentPage).")
    
    
    var heading1 = "", title1 = "", detail1 = "", title2 = "", detail2 = "", title3 = "", detail3 = "", title4 = "", detail4 = "", title5 = "", detail5 = ""
    
    
    //heading1 = model.getName()
    
    switch withPage {
      
    case "P4":
      heading1 = "A chat with \(model.getName())"
      title1 = model.replaceTokens(inString: strings.p4_s2_title)
      detail1 = model.get(Key.p4_s2) as? String ?? ""
      
      title2 = model.replaceTokens(inString: strings.p4_s3_title)
      if let answers = model.get(Key.p4_s3) as? [String] {
        let answerSummary = answers.joined(separator: ", ")
        detail2 = answerSummary
      }

      title3 = model.replaceTokens(inString: strings.p4_s4_title)
      detail3 = model.get(Key.p4_s4) as? String ?? ""
      
      title4 = model.replaceTokens(inString: strings.p4_s5_title)
      detail4 = model.getChatDate()?.customFriendlyString() ?? ""
      
      
    case "P5":
      heading1 = "How to Support"
      title1 = model.replaceTokens(inString: strings.p5_s1_title)
      detail1 = model.get(Key.p5_s1) as? String ?? ""

      title2 = model.replaceTokens(inString: strings.p5_s2_title)
      if let answers = model.get(Key.p5_s2) as? [String] {
        let answerSummary = answers.joined(separator: ", ")
        detail2 = answerSummary
        if answers.contains("Other") {
          if let otherText = model.get(Key.p5_s2_other) as? String {
            detail2 += ", \(otherText)"
          }
        }
      }

      title3 = model.replaceTokens(inString: strings.p5_s3_title)
      if let answers = model.get(Key.p5_s3) as? [String] {
        let answerSummary = answers.joined(separator: ", ")
        detail3 = answerSummary
        if answers.contains("Other") {
          if let otherText = model.get(Key.p5_s3_other) as? String {
            detail3 += ", \(otherText)"
          }
        }
      }
      
      title4 = model.replaceTokens(inString: strings.p5_s4_title)
      detail4 = model.get(Key.p5_s4) as? String ?? ""

      title5 = model.replaceTokens(inString: strings.p5_s5_title)
      detail5 = model.get(Key.p5_s5) as? String ?? ""

    default:
      break
    }
    
    cell.heading1.text = heading1
    cell.title1.text = title1
    cell.detail1.text = detail1
    cell.title2.text = title2
    cell.detail2.text = detail2
    cell.title3.text = title3
    cell.detail3.text = detail3
    cell.title4.text = title4
    cell.detail4.text = detail4
    cell.title5.text = title5
    cell.detail5.text = detail5
    
    for title in [cell.title1, cell.title2, cell.title3, cell.title4, cell.title5] {
      title?.font = UIFont.customBodyItalic
      title?.textColor = Style.secondaryTextColor.highlight(withLevel: 0.2)
    }
    for detail in [cell.detail1, cell.detail2, cell.detail3, cell.detail4, cell.detail5] {
      detail?.font = UIFont.customBody
      detail?.textColor = Style.inverseTextColor
    }
    
  }
  
  
  
  
  
}
