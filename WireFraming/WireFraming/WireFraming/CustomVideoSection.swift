//
//  CustomVideoSection.swift
//  WireFraming
//
//  Created by Peter Hunt on 2/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka
import BMPlayer

/*
 A custom Eureka section, with a custom header view for a video
 */

class CustomVideoSection: Section {
  
  var videoName: String!
  var videoFile: String!
  var subtitleFile: String?
  
  /*
  override init(_ header: String, _ initializer: (Section) -> ()) {
    
    videoFile = header
    super.init(header, initializer)
    
    var header = HeaderFooterView<VideoView>(.nibFile(name: "VideoView", bundle: nil))
    
    // Will be called every time the header appears on screen
    header.onSetupView = { [unowned self] view, _ in
      // Commonly used to setup texts inside the view
      // Don't change the view hierarchy or size here!
      
      view.configure(videoName: self.videoName, videoFile: self.videoFile, subtitleFile: self.subtitleFile)
      
      
    }
    
    
    self.header = header
    
  }
  */
  
  func configure(videoName: String, videoFile: String, subtitleFile: String?) {
    self.videoName = videoName
    self.videoFile = videoFile
    self.subtitleFile = subtitleFile
  }
  
  
  override required init(_ initializer: (Section) -> ()) {
    var header = HeaderFooterView<VideoView>(.nibFile(name: "VideoView", bundle: nil))
    super.init(initializer)
    
    // Will be called every time the header appears on screen
    header.onSetupView = { [unowned self] view, _ in
      // Commonly used to setup texts inside the view
      // Don't change the view hierarchy or size here!
      view.configure(videoName: self.videoName, videoFile: self.videoFile, subtitleFile: self.subtitleFile)
    }

    self.header = header
  }
  
  required init() {
    var header = HeaderFooterView<VideoView>(.nibFile(name: "VideoView", bundle: nil))
    
    // Will be called every time the header appears on screen
    header.onSetupView = { view, _ in
      // Commonly used to setup texts inside the view
      // Don't change the view hierarchy or size here!
    }
    
    super.init()
    self.header = header
  }
 
  
  /*
  
  func setupVideoPlayer() {
    
    player = BMPlayer()
    //self.contentView.addSubview(player)
    
    //setVideoPortrait()  // setup constraints for player in portrait mode
    
    player.backBlock = { (isFullScreen) in
      if isFullScreen == true {
        return
      }
    }
    
    // Listen to when the player is playing or stopped
    player.playStateDidChange = { [unowned self] (isPlaying: Bool) in
      print("playStateDidChange \(isPlaying)")
      /*
       if !isPlaying {
       self.continueButton.setTitle("Continue", for: .normal)
       } else {
       self.continueButton.setTitle("Skip", for: .normal)
       }
       */
    }
    
    //Listen to when the play time changes
    player.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
      print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    let asset = BMPlayerResource(url: videoUrl, name: "Welcome")
    player.setVideo(resource: asset)
    //    player.autoPlay()
    
  }
  
 */
  /*
   func setVideoFullScreen() {
   player.snp.remakeConstraints { (make) in
   make.left.right.top.equalTo(self.view)
   make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
   }
   }
   
   func setVideoPortrait() {
   player.snp.remakeConstraints { (make) in
   make.top.equalTo(speechView2.snp.bottom).offset(0)
   make.left.right.equalTo(self.contentView)
   // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
   make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
   }
   }
   */
  
  func bindPlayerFrameToSuperviewWidth() {
    /*
    guard let superview = player.superview else {
      print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
      return
    }
    
    player.translatesAutoresizingMaskIntoConstraints = false
    
    player.preservesSuperviewLayoutMargins = false
    superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": player]))
    
    let aspectRatioConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .height,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .width,
                                                   multiplier: (10.0 / 16.0),
                                                   constant: 0)
    aspectRatioConstraint.isActive = true
    
    player.addConstraint(aspectRatioConstraint)
     */
  }
  
  /*{
   
   didSet {
   let view = header?.viewForSection(self, type: .header)
   view?.backgroundColor = Style.backgroundColor
   view?.setNeedsLayout()
   }
   
   }*/

  
  /*
   setupVideoPlayer()
   
   if let view = header.viewForSection(self, type: .header) {
   view.addSubview(player)
   
   //bindPlayerFrameToSuperviewWidth()
   
   player.snp.remakeConstraints { (make) in
   make.top.equalTo(view.snp.top).offset(0)
   make.left.right.equalTo(view)
   // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
   make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
   }
   
   player.play()
   
   // Bring player to the front so it's on top of everything
   view.bringSubview(toFront: player)
   
   }
   */

}
