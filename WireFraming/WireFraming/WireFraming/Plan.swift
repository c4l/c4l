//
//  Plan.swift
//  WireFraming
//
//  Created by Peter Hunt on 13/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//
import Foundation


// Model Object for a conversation plan

class Plan: NSObject, NSCoding {
  var planId: String
  var name: String
  var chatDate: Date?
  var hadChat: Bool
  var isFollowup: Bool
  var chatNotificationId: String?     // unique id for notification
  var chatEventId: String?            // unique event id for chat in device calendar
  var followupEventId: String?        // unique event id for followup in device calendar
  
  
  //var pages: [String: Any]                 // eg. "P4" : ["p4_s1_i2" : "Hello World"]
  var items: [String: Any]                 // eg. ["p4_s2_i1" : "Fred", "p4_s2_i2" : "What has been happening..."]
  
  // Progress
  var currentPage: String?                 // eg. "P5"
  var pagesComplete: [String]              // eg. ["EV2", "P4", "P5"]
  
  init(planId: String, name: String = "", chatDate: Date? = nil, hadChat: Bool = false, isFollowup: Bool = false,
       chatNotificationId: String? = nil, chatEventId: String? = nil, followupEventId: String? = nil,
       /*pages: [String: Any] = [:],*/ items: [String: Any] = [:], currentPage: String? = nil, pagesComplete: [String] = []) {
    self.planId = planId
    self.name = name
    self.chatDate = chatDate
    self.hadChat = hadChat
    self.isFollowup = isFollowup
    self.chatNotificationId = chatNotificationId
    self.chatEventId = chatEventId
    self.followupEventId = followupEventId
    //self.pages = pages
    self.items = items
    self.currentPage = currentPage
    self.pagesComplete = pagesComplete
  }
  
  required convenience init?(coder decoder: NSCoder) {
    print("Plan init")
    
    guard let planId = decoder.decodeObject(forKey: "planId") as? String,
      let name = decoder.decodeObject(forKey: "name") as? String else {
      return nil
    }
    
    let chatDate = decoder.decodeObject(forKey: "chatDate") as? Date
    let hadChat = decoder.decodeBool(forKey: "hadChat")
    let isFollowup = decoder.decodeBool(forKey: "isFollowup")
    let chatNotificationId = decoder.decodeObject(forKey: "chatNotificationId") as? String
    let chatEventId = decoder.decodeObject(forKey: "chatEventId") as? String
    let followupEventId = decoder.decodeObject(forKey: "followupEventId") as? String
    //let pages = aDecoder.decodeObject(forKey: "pages") as? [String: Any] ?? [:]
    let items = decoder.decodeObject(forKey: "items") as? [String: Any] ?? [:]
    let currentPage = decoder.decodeObject(forKey: "currentPage") as? String
    let pagesComplete = decoder.decodeObject(forKey: "pagesComplete") as? [String] ?? []
    
    self.init(planId: planId, name: name,
              chatDate: chatDate, hadChat: hadChat, isFollowup: isFollowup,
              chatNotificationId: chatNotificationId, chatEventId: chatEventId, followupEventId: followupEventId,
              /*pages: pages,*/ items: items, currentPage: currentPage, pagesComplete: pagesComplete)
  }
  
  
  func encode(with coder: NSCoder) {
    
    print("Plan encode")
    print("\(self.debugDescription)")
    
    coder.encode(self.planId, forKey: "planId")
    coder.encode(self.name, forKey: "name")
    
    if let date = chatDate {
      coder.encode(date, forKey: "chatDate")
    }

    coder.encode(self.hadChat, forKey: "hadChat")
    coder.encode(self.isFollowup, forKey: "isFollowup")
    
    if let notificationId = chatNotificationId {
      coder.encode(notificationId, forKey: "chatNotificationId")
    }

    if let eventId = chatEventId {
      coder.encode(eventId, forKey: "chatEventId")
    }

    if let followupEventId = followupEventId {
      coder.encode(followupEventId, forKey: "followupEventId")
    }

    //aCoder.encode(pages, forKey: "pages")
    coder.encode(self.items, forKey: "items")
    
    if let page = currentPage {
      coder.encode(page, forKey: "currentPage")
    }

    coder.encode(self.pagesComplete, forKey: "pagesComplete")
    
  }
  
  override var debugDescription: String {
    
    let chatDate = self.chatDate == nil ? "" : "date: \(self.chatDate!)"
    
    
//    let followupDate = self.followupDate == nil ? "" : "follow up: \(self.followupDate!)"
//    let p4 = self.pages["P4"] == nil ? "" : "P4: \(self.pages["P4"]!)"
//    let p5 = self.pages["P5"] == nil ? "" : "P5: \(self.pages["P5"]!)"
    let items = self.items
    let currentPage = self.currentPage == nil ? "" : "current page: \(self.currentPage!)"
    let complete = String(format: "%.0f", percentComplete)
    
    return "Plan ID: \(planId), name: \(name), \(chatDate), had chat: \(hadChat), followup: \(isFollowup), \(items), \(currentPage), pages complete: \(pagesComplete), percent complete: \(complete)"
 
  }
  
  
  
  var percentComplete: Double {
    get {
      return 0
      /*
      var percent: Double = 0.0
      if pages.count == 0 {
        return percent
 */
      }
   /*
      
      // P4
      if let p4 = pages["P4"] as? Dictionary<String, Any> {
        // calculate percentage for each completed section on p4
        let p4Sections = ["p4_s2_i1", "p4_s4_i1", "WhenSection"]
        let p4TotalItems = Double(p4Sections.count + 1) // 4 including the 1 multiple choice section
        for section in p4Sections {
          if let _ = p4[section] {
            percent += 0.25 / p4TotalItems  // eg. 0.25 / 5 = 0.05 per section
          }
        }
        
        // multiple choice section - count if at least one is checked
        if p4["p4_s3_i1_1"] != nil || p4["p4_s3_i1_2"] != nil || p4["p4_s3_i1_3"] != nil ||
          p4["p4_s3_i1_4"] != nil || p4["p4_s3_i1_5"] != nil || p4["p4_s3_i1_6"] != nil {
          percent += 0.25 / p4TotalItems
        }
      }
      
      
      // P5
      if let p5 = pages["P5"] as? Dictionary<String, Any> {
        // calculate percentage for each completed section on p5
        let p5Sections = ["p5_s1_i1", "p5_s4_i1", "p5_s5_i1"]
        let p5TotalItems = Double(p5Sections.count + 2)   // 5 including the 2 multiple choice sections
        for section in p5Sections {
          if let _ = p5[section] {
            percent += 0.25 / p5TotalItems
          }
        }
      
        // if any item is checked, count as complete
        if ([p5["p5_s2_i1_1"], p5["p5_s2_i1_2"], p5["p5_s2_i1_3"], p5["p5_s2_i1_4"], p5["p5_s2_i1_5"], p5["p5_s2_i1_6"], p5["p5_s2_i1_7"]].flatMap { $0 }.count) > 0 {
            percent += 0.25 / p5TotalItems
        }
        if ([p5["p5_s3_i1_1"], p5["p5_s3_i1_2"], p5["p5_s3_i1_3"], p5["p5_s3_i1_4"], p5["p5_s3_i1_5"], p5["p5_s3_i1_6"], p5["p5_s3_i1_7"]].flatMap { $0 }.count) > 0 {
          percent += 0.25 / p5TotalItems
        }
      }
      
      /*
      if let p6ItemCount = (pages["P6"] as? Dictionary<String, Any>)?.count, p6ItemCount > 0 {
        percent += Double(p6ItemCount) / 2.0          // 2 total items on P6
      }
      
      if let p7ItemCount = (pages["P7"] as? Dictionary<String, Any>)?.count, p7ItemCount > 0 {
        percent += Double(p7ItemCount) / 1.0          // 1 total items on P7
      }
      */
      
      return min(percent, 1.0)   // should be between 0.0 and 1.0
    }
 */
  }
  

  
}
