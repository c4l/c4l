//
//  About.swift
//  WireFraming
//
//  Created by Peter Hunt on 25/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit


/// Contacts in a Crisis screen
class ContactsTableViewController: UITableViewController, Injectable {
  
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  var staticCells: [UITableViewCell]!
  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  // MARK: - View Configuration
  
  
  // Used programmatically (not from Storyboards)
  override func loadView() {
    super.loadView()
    
    // Static Cells: https://github.com/bmancini55/iOSExamples-StaticCells/blob/master/Swift/StaticCellsSwift/TableViewController.swift
    
    self.title = "Contacts in a Crisis"
    
    /*
     Section 0:
     
     “Lifeline
     13 11 14
     Lifeline.org.au
     
     Kids Helpline
     1800 551 800
     Kidshelpline.com.au
     
     Headspace
     1800 650 890
     eheadspace.org.au”
     
     
     
     Section 2:
     Chats for life
     
     
     */
    
    let imageColor: UIColor = Style.primaryColor
    let imageBackgroundColor: UIColor = .clear
    let imageSize = CGSize(width: 40, height: 40)
    
    /** SECTION 0 **/
    
    // construct first name cell, section 0, row 0
    let cell0 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell0.backgroundColor = Style.cardBackgroundColor
    cell0.imageView?.setIcon(icon: .fontAwesomeSolid(.phone), textColor: imageColor, backgroundColor: imageBackgroundColor, size: imageSize)
    cell0.textLabel?.text = "Lifeline"
    cell0.detailTextLabel?.text = "13 11 14"
    
    let cell1 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell1.backgroundColor = Style.cardBackgroundColor
    cell1.imageView?.setIcon(icon: .fontAwesomeSolid(.phone), textColor: imageColor, backgroundColor: imageBackgroundColor, size: imageSize)
    cell1.textLabel?.text = "Kids Helpline"
    cell1.detailTextLabel?.text = "1800 551 800"
    
    let cell2 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell2.backgroundColor = Style.cardBackgroundColor
    cell2.imageView?.setIcon(icon: .fontAwesomeSolid(.phone), textColor: imageColor, backgroundColor: imageBackgroundColor, size: imageSize)
    cell2.textLabel?.text = "Headspace"
    cell2.detailTextLabel?.text = "1800 650 890"
    
    /** SECTION 1 **/
    
    let cell3 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell3.backgroundColor = Style.cardBackgroundColor
    cell3.imageView?.setIcon(icon: .fontAwesomeBrands(.safari), textColor: imageColor, backgroundColor: imageBackgroundColor, size: imageSize)
    cell3.textLabel?.text = "Your family GP"
    cell3.detailTextLabel?.text = ""
    
    let cell4 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell4.backgroundColor = Style.cardBackgroundColor
    cell4.imageView?.setIcon(icon: .fontAwesomeBrands(.safari), textColor: imageColor, backgroundColor: imageBackgroundColor, size: imageSize)
    cell4.textLabel?.text = "At school – "
    cell4.detailTextLabel?.text = "Youth support and guidance officers"
    
    let cell5 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell5.backgroundColor = Style.cardBackgroundColor
    cell5.imageView?.setIcon(icon: .fontAwesomeBrands(.safari), textColor: imageColor, backgroundColor: imageBackgroundColor, size: imageSize)
    cell5.textLabel?.text = "At Uni – "
    cell5.detailTextLabel?.text = "Student Services"
    
    /** SECTION 2 **/
    let cell6 = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
    cell6.backgroundColor = Style.cardBackgroundColor
    cell6.imageView?.image = UIImage(named: "settings")
    cell6.textLabel?.text = "Settings"
    cell6.detailTextLabel?.text = ""
    cell6.accessoryType = .disclosureIndicator
    
    staticCells = [cell0, cell1, cell2, cell3, cell4, cell5, cell6]
    /*
     self.firstNameText = UITextField(frame: CGRectInset(self.firstNameCell.contentView.bounds, 15, 0))
     self.firstNameText.placeholder = "First Name"
     self.firstNameCell.addSubview(self.firstNameText)
     */
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Style.backgroundColor
    tableView.dataSource = self
    tableView.delegate = self
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 60
    tableView.separatorStyle = .singleLine
    
    //tableView.separatorStyle = .none
    //self.automaticallyAdjustsScrollViewInsets = true
    
    
    if let navController = navigationController, navController.isBeingPresented { // presented modally in a nav controller
      //    if let parent = self.parent, parent.isBeingPresented {
      //    if self.parent.isBeingPresented {
      self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self,
                                                               action: #selector(doneButton))
    }
  }
  
  
  @objc func doneButton() {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  // MARK: - UITableViewDelegate
  /*
   override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
   return 50.0
   }
   
   override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
   guard let header = view as? UITableViewHeaderFooterView else { return }
   header.contentView.backgroundColor = Style.backgroundColor
   
   header.textLabel?.textColor = Style.primaryTextColor
   //    header.textLabel?.backgroundColor = Style.backgroundColor
   header.textLabel?.font = UIFont.customHeadline
   header.textLabel?.frame = header.frame
   header.textLabel?.textAlignment = .center
   
   }
   */
  /*
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
   let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
   headerView.backgroundColor = .lightGray
   
   let label = UILabel()
   label.translatesAutoresizingMaskIntoConstraints = false
   label.text = "TITLE"
   headerView.addSubview(label)
   label.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 8).isActive = true
   label.rightAnchor.constraint(equalTo: headerView.rightAnchor).isActive = true
   label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
   label.heightAnchor.constraint(equalToConstant: 44).isActive = true
   
   return headerView
   
   }
   */
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    switch (indexPath.section, indexPath.row) {
      
    case (0, 0...2):
      guard let phoneLabel = staticCells[indexPath.row].detailTextLabel,
        let phoneNo = phoneLabel.text else { return }
      
      let phoneNoToCall = phoneNo.replacingOccurrences(of: " ", with: "")   // strip out spaces
      print("ContactsTableViewController didSelectRowAt \(phoneNoToCall)")
      "telprompt://\(phoneNoToCall)".openUrl()
      
      
      /*
       guard let number = URL(string: "telprompt://\(phoneNoToCall)") else { return }
       "sdfa".openUrl()
       
       if #available(iOS 10.0, *) {
       UIApplication.shared.open(number)
       } else {
       // Fallback on earlier versions
       }
       */
      tableView.deselectRow(at: indexPath, animated: true)
      
    case (2, 0):
      print("App Settings/Resources")
      
      let settingsVC = newViewController(withId: "About")
      settingsVC.title = "Resources"
      let backButton = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
      self.navigationItem.backBarButtonItem = backButton
      if let navController = navigationController {
        navController.pushViewController(settingsVC, animated: true)

      }
      
      
    default:
      tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
  }
  
  
}


// MARK: UITableViewDataSource
extension ContactsTableViewController {
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 3
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 3
    case 1:
      return 3
    case 2:
      return 1
    default:
      return 0
    }
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch (indexPath.section, indexPath.row) {
      
    case (0, _):
      return staticCells[indexPath.row]
      
    case (1, _):
      return staticCells[indexPath.row + 3]
      
    case (2, _):
      return staticCells[indexPath.row + 6]
    
    default:
      return staticCells[staticCells.count-1]
    }
    
    /*
     if indexPath.row == 0 { //model.plans.count {  // first row - show "Start new plan" cell
     let cell = tableView.dequeueReusableCell(withIdentifier: startNewPlanCellId, for: indexPath) as! StartNewPlanCell
     return cell
     }
     
     // Otherwise, show a main menu cell, and populate it
     let cell = tableView.dequeueReusableCell(withIdentifier: mainMenuCellId, for: indexPath) as! MainMenuCell
     
     
     // Other rows - configure the cell with the plan name...
     guard let plan = planAt(indexPath: indexPath) else {
     return cell
     }
     
     cell.nameLabel.text = plan.name
     
     // Conversation date
     if let date = plan.conversationDate {
     let conversationDateText = date.customFriendlyString()
     
     // if the date is overdue, show the badged calendar icon
     if Date().compare(.isLater(than: date)) {
     cell.calendarImageView.image = Style.imageOfCalendarOverdue
     cell.conversationDateLabel.text = "Overdue:\n\(conversationDateText)"
     } else {
     cell.calendarImageView.image = Style.imageOfCalendarScheduled
     cell.conversationDateLabel.text = "Chat scheduled:\n\(conversationDateText)"
     }
     } else {
     cell.conversationDateLabel.text = "Chat not scheduled"
     cell.calendarImageView.image = Style.imageOfCalendarDimmed
     }
     
     let ratioComplete = plan.percentComplete
     let percentComplete = ratioComplete * 100
     if percentComplete > 0 {
     cell.percentLabel.text = "\(String(format: "%.0f", percentComplete))% complete"
     } else {
     cell.percentLabel.isHidden = true
     }
     
     cell.progressRatio = CGFloat(ratioComplete)
     
     return cell
     */
  }
  
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 { return CGFloat.leastNonzeroMagnitude }
    return 20.0
  }
  
}






