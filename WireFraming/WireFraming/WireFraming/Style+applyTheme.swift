//
//  Style+applyTheme.swift
//  WireFraming
//
//  Created by Peter Hunt on 2/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
//import Eureka
//import IBAnimatable
//import BMPlayer

/// Static Style.applyTheme() called from AppDelegate to theme the app using appearance proxy


extension Style {
  
  static func applyTheme() {
    
    // Application tint color
    let sharedApplication = UIApplication.shared
    sharedApplication.delegate?.window??.tintColor = Style.primaryColor
    
    
    // UINavigationBar
    /*
    UINavigationBar.appearance().barTintColor = Style.primaryColor
    UINavigationBar.appearance().tintColor = Style.backgroundColor
    UINavigationBar.appearance().barStyle = .black  // sets status bar to white
    */
    
    // Smaller font on iPhone 4 inch devices
    let navBarFont = UIScreen.main.bounds.size.width > 320 ? UIFont.customTitle3 : UIFont.customBody
    UINavigationBar.appearance().titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue: Style.primaryColor,
                                                        NSAttributedString.Key.font.rawValue: navBarFont])
    
    /*
    // Sets background to a blank/empty image
    UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    // Sets shadow (line below the bar) to a blank image
    UINavigationBar.appearance().shadowImage = UIImage()
    */
    // Sets the translucent background color
    UINavigationBar.appearance().backgroundColor = Style.backgroundColor
    // Set translucent. (Default value is already true, so this can be removed if desired.)
    UINavigationBar.appearance().isTranslucent = true
    
    
    
    // UIBarButtonItem
    //    UIBarButtonItem.appearance().tintColor = Style.backgroundColor
    
    /*
    // UIToolbar
    UIToolbar.appearance().barTintColor = Style.backgroundColor
    UIToolbar.appearance().tintColor = Style.primaryColor
    */
    
    // UISwitch
    UISwitch.appearance().thumbTintColor = Style.primaryColor
    UISwitch.appearance().onTintColor = Style.primaryColor.withAlphaComponent(0.33)
    
    /*
    // UIPageControl
    UIPageControl.appearance().backgroundColor = Style.primaryColor
    UIPageControl.appearance().currentPageIndicatorTintColor = Style.tertiaryColor
    //    UIPageControl.appearance().pageIndicatorTintColor = Style.backgroundColor
    */

    //UIPageControl.appearance().backgroundColor = UINavigationBar.appearance().backgroundColor?.withAlpha(0.6)
    UIPageControl.appearance().backgroundColor = Style.cardBackgroundColor.withAlphaComponent(0.96)
    UIPageControl.appearance().currentPageIndicatorTintColor = Style.primaryColor
    UIPageControl.appearance().pageIndicatorTintColor = Style.secondaryColor

    /*
     
     UITableView
     
     @property (nonatomic) UIEdgeInsets separatorInset NS_AVAILABLE_IOS(7_0) UI_APPEARANCE_SELECTOR; // allows customization of the frame of cell separators
     
     @property (nonatomic, retain) UIColor *sectionIndexColor NS_AVAILABLE_IOS(6_0) UI_APPEARANCE_SELECTOR; // color used for text of the section index
     
     @property (nonatomic, retain) UIColor *sectionIndexBackgroundColor NS_AVAILABLE_IOS(7_0) UI_APPEARANCE_SELECTOR; // the background color of the section index while not being touched
     
     @property (nonatomic, retain) UIColor *sectionIndexTrackingBackgroundColor NS_AVAILABLE_IOS(6_0) UI_APPEARANCE_SELECTOR; // the background color of the section index while it is being touched
     
     @property (nonatomic, retain) UIColor *separatorColor UI_APPEARANCE_SELECTOR; // default is the standard separator gray
     
     @property (nonatomic, copy) UIVisualEffect *separatorEffect NS_AVAILABLE_IOS(8_0) UI_APPEARANCE_SELECTOR; // effect to apply to table separators
     UITableViewCell
     
     @property (nonatomic) UIEdgeInsets separatorInset NS_AVAILABLE_IOS(7_0) UI_APPEARANCE_SELECTOR; // allows customization of the separator frame
     
     */
    
    
    /*
    // Eureka Forms
    
    ButtonRow.defaultCellSetup = { cell, row in
      cell.tintColor = Style.primaryColor
      cell.backgroundColor = Style.backgroundColor
      cell.textLabel?.font = .customHeadline
    }
    
    
    NameRow.defaultCellSetup = { cell, row in
      cell.backgroundColor = Style.backgroundColor
    }
    
    NameRow.defaultCellUpdate = { cell, row in
      cell.textField.textColor = Style.primaryTextColor
      cell.textField.font = .customBodyItalic
    }
    
    
    TextAreaRow.defaultCellSetup = { cell, row in
      cell.textView.font = .customBodyItalic
      cell.textView.textColor = Style.primaryTextColor
      cell.textView.backgroundColor = Style.backgroundColor
      cell.placeholderLabel?.font = .customCaption1
      cell.placeholderLabel?.textColor = Style.secondaryColor
      cell.backgroundColor = Style.backgroundColor
    }
    
    SwitchRow.defaultCellSetup = { cell, row in
      cell.backgroundColor = Style.backgroundColor
      cell.textLabel?.textColor = Style.primaryTextColor
      cell.textLabel?.font = .customBody
    }
    
    ImageCheckRow<String>.defaultCellSetup =  { cell, row in
      
      let singleSelectionRows = ["p6_s1_i1_1","p6_s1_i1_2", "p6_s1_i1_3", "p6_s1_i1_4", "p6_s2_i1_1","p6_s2_i1_2", "p6_s2_i1_3"]
      if let tag = row.tag, singleSelectionRows.contains(tag) {
        cell.trueImage = UIImage(named: "selected")!
        cell.falseImage = UIImage(named: "unselected")!
      } else {
        cell.trueImage = UIImage(named: "selectedRectangle")!
        cell.falseImage = UIImage(named: "unselectedRectangle")!
      }
      
      cell.backgroundColor = Style.backgroundColor
      cell.textLabel?.font = .customBody
      cell.textLabel?.textColor = Style.primaryTextColor
      
    }
    
    
    DateTimeInlineRow.defaultCellSetup = { cell,row in
      /*
      cell.textLabel?.font = .customBody
      cell.textLabel?.textColor = Style.primaryTextColor
      
      
      cell.detailTextLabel?.font = .customBody
      cell.detailTextLabel?.textColor = Style.primaryTextColor
      */
      cell.backgroundColor = Style.backgroundColor
    }
*/
    
    
    // IBAnimatable Components
    //AnimatableButton.appearance().tintColor = Style.primaryColor
    
    
    // BMPlayer - video player
    BMPlayerConf.tintColor = Style.highlightColor
    BMPlayerConf.topBarShowInCase = .none
    BMPlayerConf.shouldAutoPlay = false
    BMPlayerConf.animateDelayTimeInterval = TimeInterval(2.0)
    
    
    
    /*
     // *** Debugging: List custom fonts
     for name in UIFont.familyNames {
     print("Custom fonts: \(name), \(UIFont.fontNames(forFamilyName: name))")
     }
     */
    
  }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
