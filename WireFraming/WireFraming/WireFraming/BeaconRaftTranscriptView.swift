//
//  BeaconView.swift
//  WireFraming
//
//  Created by Peter Hunt on 26/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit

enum BeaconRaftTranscriptType {
  case beacon, raft, transcript
}

class BeaconRaftTranscriptView: UIView {
  
  //var textLabel: UILabel!
  var viewType: BeaconRaftTranscriptType = .beacon
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    genericInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  
  private func genericInit() {
    //self.backgroundColor = .clear
    /*
    textLabel = UILabel()
    textLabel.numberOfLines = 0
    textLabel.adjustsFontSizeToFitWidth = true
    textLabel.font = UIFont.customBody
    textLabel.textColor = Style.primaryTextColor
    textLabel.textAlignment = .justified
    //    speechLabel.backgroundColor = UIColor.gray.withAlpha(0.3)
    self.addSubview(textLabel)
    */
  }
  
  /*
  override func layoutSubviews() {
    super.layoutSubviews()
    
    textLabel.frame = CGRect(x: 25, y: 260, width: self.frame.width - 50, height: self.frame.height - 350)
 
  }
  */
  /*
  override func awakeFromNib() {
  }
  */
  
  override func draw(_ rect: CGRect) {
    if viewType == .beacon {
      Style.drawBeaconLarge(frame: rect, resizing: .aspectFill)
    } else if viewType == .raft {
      Style.drawRaftLarge(frame: rect, resizing: .aspectFill)
    } else if viewType == .transcript {
      Style.drawTranscriptLarge(frame: rect, resizing: .aspectFill)
    }
    

  }
  
}
