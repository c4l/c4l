//
//  ScrollingForm.swift
//  WireFraming
//
//  Created by Peter Hunt on 23/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//


import Foundation
import UIKit
//import BMPlayer
import BEMCheckBox
import SnapKit
import KMPlaceholderTextView
import Haring


/*
  Superclass for a scrolling form.
  [view]
    [scrollView]
      [contentView]
        [subviews]
 
 */

class ScrollingForm: UIViewController, Injectable, EventAccess, UITextFieldDelegate {
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  var scrollView: UIScrollView!
  var contentView: UIView!
  var lastControlBottom, videoPlayerTop: ConstraintItem!
  
  var player: BMPlayer?
  var skipButton, transcriptButton: UIButton?  // skip video
  var backButton, nextButton, finishButton: UIButton?  // previous/next page
  var videoTitleLabel: UILabel?
  var safeInsets: UIEdgeInsets = UIEdgeInsets.zero
  var savedBackgroundColor: UIColor = .white
  
  //var activeField: UITextField?
  
  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  /*
  @IBAction func nextButtonTap(_ sender: Any) {
    if let parentPageViewController = self.parent as? WelcomePageViewController {
      parentPageViewController.goToNextPage()
    }
  }
  */
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    savedBackgroundColor = view.backgroundColor ?? .white
    UIApplication.shared.isStatusBarHidden = false
    appDelegate.shouldRotate = false     // Don't allow rotation for this VC
    
    // Add scrollView and set up to be size of the view
    scrollView = UIScrollView(frame: view.frame)
    
    view.addSubview(scrollView)
    scrollView.snp.makeConstraints { (make) in
      make.edges.equalTo(view)
    }
  
    // Add contentView
    contentView = UIView(frame: view.frame)
    scrollView.addSubview(contentView)
    contentView.snp.makeConstraints { (make) in
      make.edges.equalTo(scrollView)
      make.width.equalTo(view) // pin width to view (not scrollView)
    }
    lastControlBottom = contentView.snp.top
    videoPlayerTop = contentView.snp.top
    
    scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    
    //registerForKeyboardNotifications()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    // iOS 11 - resize scrollView to support safe insets on iPhone X, etc
    safeInsets = view.safeAreaInsets
    print("ScrollingForm viewDidLayoutSubviews: Safe Area Insets: \(safeInsets)")
    
    scrollView.snp.remakeConstraints { (make) in
      make.edges.equalTo(view).inset(safeInsets)
    }
    
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    print("ScrollingForm viewWillDisappear")
    if let videoPlayer = player {
      videoPlayer.pause()
    }
  }
  
  
  
  deinit {
    print("ScrollingForm deinit")
    //deregisterFromKeyboardNotifications()
    
  }
  
  // MARK: - Video
  func addVideoPlayer(videoName: String, videoFile: String, videoTitle: String?,
                      subtitleFile: String?, coverFile: String?,
                      withSkipButton: Bool = false, withTranscriptButton: Bool = true) {
    
//    let controller = BMPlayerCustomControlView()
//    player = BMPlayer(customControlView: controller)
//    player = CustomBMPlayer()
    player = BMPlayerCustom(customControlView: BMPlayerTapToPlayControlView())
    guard let videoPlayer = player else { return }
    
    contentView.addSubview(videoPlayer)
    
    setVideoPortrait()  // setup constraints for player in portrait mode
    lastControlBottom = videoPlayer.snp.bottom
    
    // Add a title label if there is a videoTitle set
    if let title = videoTitle {
      videoTitleLabel = customLabel(text: title)
      if let titleLabel = videoTitleLabel {
        titleLabel.font = UIFont.customTitle2
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 1
        titleLabel.adjustsFontSizeToFitWidth = true
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
          make.top.equalTo(lastControlBottom).offset(10)
          make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
          //      make.height.equalTo(transcriptButton.snp.width).multipliedBy(135.0/500.0)
        }
        lastControlBottom = titleLabel.snp.bottom
      }
    }
    
    if withTranscriptButton {
      transcriptButton = customTranscriptButton(target: self, action: #selector(transcriptButtonTapped))
      guard let button = transcriptButton else { return }
      contentView.addSubview(button)
      button.snp.makeConstraints { (make) in
        make.top.equalTo(lastControlBottom).offset(0)
        make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
        make.height.equalTo(button.snp.width).multipliedBy(135.0/500.0)
      }
      lastControlBottom = button.snp.bottom
    }
    
    if withSkipButton {
      skipButton = UIButton.custom("Skip", target: self, action: #selector(skipButtonTapped))
      guard let button = skipButton else { return }
      self.contentView.addSubview(button)
      
      button.snp.makeConstraints { (make) in
        make.top.equalTo(lastControlBottom).offset(20)
        make.centerX.equalTo(self.contentView.snp.centerX)
        make.width.equalTo(160)
        make.height.equalTo(50)
      }
      
      lastControlBottom = button.snp.bottom
    }
    
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    
    var subtitles: BMSubtitles?
    if let subtitle = subtitleFile, model.userSettings.isSubtitlesEnabled {
      let subtitleUrl = Bundle.main.url(forResource: subtitle, withExtension: nil)!
      subtitles = BMSubtitles(url: subtitleUrl)
    }
    
    var cover: URL?
    if let coverFile = coverFile {
      cover = Bundle.main.url(forResource: coverFile, withExtension: nil)! // coverURL images with some dimensions seems to mess up autolayout in landscape!!
    }
    
    let asset = BMPlayerResource(name: videoName,
                                 definitions: [BMPlayerResourceDefinition(url: videoUrl, definition: "1080p")],
                                 cover: cover,
                                 subtitles: subtitles)
    videoPlayer.setVideo(resource: asset)
    
    // Set up player callbacks
    videoPlayer.backBlock = { (isFullScreen) in
      if isFullScreen == true {
        return
      }
    }
    
    // Listen to when the player is playing or stopped
    videoPlayer.playStateDidChange = { [weak self] (isPlaying: Bool) in
      print("playStateDidChange \(isPlaying)")
      
      self?.appDelegate.shouldRotate = isPlaying // allow rotation only if video is playing
      
      if let button = self?.skipButton, withSkipButton {
        if !isPlaying {
          button.setTitle("Next", for: .normal)
        } else {
          button.setTitle("Skip", for: .normal)
        }
      }
    }
    
    // Listen to when the play time changes
    videoPlayer.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
      print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
  }
  
  
  /// Animate player in from the bottom of the screen, and autoplay the video
  func animateInVideoPlayer(withSkipButton: Bool = false, autoplay: Bool = false, withTranscriptButton: Bool = false) {
    
    guard let videoPlayer = player else { return }
    
    videoPlayer.center.y += view.bounds.height
    if let button = self.skipButton, withSkipButton {
      button.center.y += view.bounds.height
    }

    if let button2 = self.transcriptButton, withTranscriptButton {
      button2.center.y += view.bounds.height
    }

    if let titleLabel = self.videoTitleLabel {
      titleLabel.center.y += view.bounds.height
    }
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    videoPlayer.center.y -= weakSelf.view.bounds.height
                    if let button = weakSelf.skipButton, withSkipButton {
                      button.center.y -= weakSelf.view.bounds.height
                    }
                    if let button2 = weakSelf.transcriptButton, withTranscriptButton {
                      button2.center.y -= weakSelf.view.bounds.height
                    }
                    if let titleLabel = weakSelf.videoTitleLabel {
                      titleLabel.center.y -= weakSelf.view.bounds.height
                    }
                    
                    
      }, completion: { [weak self] _ in
        if autoplay {
          self?.player?.play()
        }
    })
    

  }
  
  
  func setVideoFullScreen() {
    guard let videoPlayer = player else { return }
    
    savedBackgroundColor = view.backgroundColor ?? .white
    self.view.backgroundColor = .black
    videoPlayer.snp.remakeConstraints { (make) in
      //Bottom guide
      make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin)
      //Top guide
      make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
      //Leading guide
      make.leading.equalTo(view.safeAreaLayoutGuide.snp.leadingMargin)
      //Trailing guide
      make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailingMargin)
//      make.height.equalTo(videoPlayer.snp.width).multipliedBy(9.0/16.0).priority(750)
    }
  }
  
  func setVideoPortrait() {
    guard let videoPlayer = player else { return }
    self.view.backgroundColor = savedBackgroundColor
    videoPlayer.snp.remakeConstraints { (make) in
      make.top.equalTo(videoPlayerTop)
      make.left.right.equalTo(self.contentView)
      make.height.equalTo(videoPlayer.snp.width).multipliedBy(9.0/16.0).priority(750)
    }
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    let orientation = newCollection.verticalSizeClass
    
    guard let videoPlayer = player else { return }
    
    switch orientation {
    case .compact: // landscape
      if videoPlayer.isPlaying {
        print("EV now in Landscape")  ///Excluding iPads!!!
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        //self.navigationController?.setToolbarHidden(true, animated: true)
        NotificationCenter.default.post(name: .landscapeNotification, object: nil, userInfo: nil)
        self.view.endEditing(true) /// dismiss keyboard if it's showing
        setVideoFullScreen()
      }
      
    default:  // portrait
      print("EV now in Portrait")
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      //self.navigationController?.setToolbarHidden(false, animated: true)
      NotificationCenter.default.post(name: .portraitNotification, object: nil, userInfo: nil)
      setVideoPortrait()
    }
  }
  

  
  
  /// When skip/next button under the video is tapped:
  /// pause the video, hide the button, show the name prompt
  
  @objc func skipButtonTapped() {
    if let videoPlayer = player {
      videoPlayer.pause()
    }
    
    self.skipButton?.isHidden = true
    /*
    showSection1()
    
    showSection2(hidden: true)  // add section 2 to the scrollview, but hide it until name is filled in
    
    // Scroll so section 1 is at the top
    //self.scrollView.setContentOffset(CGPoint(x:0, y: player.frame.height - 60), animated: true)
    //scrollToTop(view: speechView1, animated: true)
 */
    
  }
  
  @objc func transcriptButtonTapped() {
    print("transcriptButtonTapped")
//    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName1], title: strings[videoTitle1], coverFile: coverFile1)
  }
  
  // MARK: - Next Button
  func addNextButton() {
    
    // Important - pin the 'Next' button to the bottom of the contentView, so autolayout can calculate the contentView height OK
    
    nextButton = UIButton.custom("Next  >", target: self, action: #selector(nextScreen))
    guard let button = nextButton else { return }
    
    contentView.addSubview(button)
    button.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding * 1.5)
      make.bottom.equalTo(contentView.snp.bottom)
      make.centerX.equalTo(contentView.snp.centerX)
      //make.width.equalTo(200)
      make.height.equalTo(60)
      
      make.left.right.equalTo(self.contentView).inset(Const.HorizontalPadding)
      
    }
    
  }

  
  func addBackNextButtons() {
    
    // Important - pin the 'Next' button to the bottom of the contentView, so autolayout can calculate the contentView height OK

    backButton = UIButton.custom("<  Previous", target: self, action: #selector(previousScreen))
    guard let button1 = backButton else { return }
    
    contentView.addSubview(button1)
    button1.snp.makeConstraints { (make) in
//      make.top.greaterThanOrEqualTo(<#T##other: ConstraintRelatableTarget##ConstraintRelatableTarget#>)
      make.top.greaterThanOrEqualTo(lastControlBottom).offset(Const.VerticalPadding * 2)
      make.left.equalTo(self.contentView).inset(Const.HorizontalPadding)
      make.right.equalTo(self.contentView.snp.centerX).offset(-1)/*.offset(-Const.HorizontalPadding/2)*/
//      make.right.equalTo(self.contentView.snp.centerX).offset(-4)
      make.bottom.equalTo(contentView.snp.bottom)
      make.height.equalTo(60)
    }

    
    nextButton = UIButton.custom("Next  >", target: self, action: #selector(nextScreen))
    guard let button2 = nextButton else { return }
    
    contentView.addSubview(button2)
    button2.snp.makeConstraints { (make) in
      make.top.greaterThanOrEqualTo(lastControlBottom).offset(Const.VerticalPadding * 2)
      make.left.equalTo(self.contentView.snp.centerX).offset(1)/*.offset(Const.HorizontalPadding/2)*/
//      make.left.equalTo(self.contentView.snp.centerX).offset(4)
      make.right.equalTo(self.contentView).inset(Const.HorizontalPadding)
      make.bottom.equalTo(contentView.snp.bottom)
      make.height.equalTo(60)
    }
    
  }

  
  func addFinishButton() {
    
    finishButton = UIButton.custom("Done", target: self, action: #selector(nextScreen))
    guard let button = finishButton else { return }
    
    contentView.addSubview(button)
    button.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding * 2)
      make.bottom.equalTo(contentView.snp.bottom)
      make.left.right.equalTo(self.contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(60)
    }
    
  }

  
  
  // MARK: Standardised Custom controls
  /*
  func addBackNextControl() {
    let backNextControl = customSegmentedControl(items: ["<  Back", "Next  >"])
    //backNextControl.setIcon(icon: .fontAwesome(.chevronLeft), color: Style.inverseTextColor, iconSize: 20, forSegmentAtIndex: 0)
    //backNextControl.setIcon(icon: .fontAwesome(.chevronRight), color: Style.inverseTextColor, iconSize: 20, forSegmentAtIndex: 1)
    backNextControl.setTitleTextAttributes([NSFontAttributeName: UIFont.customBody.bolded,
                                             NSForegroundColorAttributeName: Style.inverseTextColor], for: .normal)
    contentView.addSubview(backNextControl)
    backNextControl.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.bottom.equalTo(contentView.snp.bottom)
      make.centerX.equalTo(contentView.snp.centerX)
      make.height.equalTo(62)
      make.left.right.equalTo(self.contentView).inset(Const.HorizontalPadding)
    }
  }
  
  func addNextControl() {
    let nextControl = customSegmentedControl(items: ["Next  >"])


    contentView.addSubview(nextControl)
    nextControl.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.bottom.equalTo(contentView.snp.bottom)
      make.centerX.equalTo(contentView.snp.centerX)
      make.height.equalTo(62)
      make.left.right.equalTo(self.contentView).inset(Const.HorizontalPadding)
    }
  }
  
  func customSegmentedControl(items: [Any]?) -> UISegmentedControl {
    let segmentedControl = UISegmentedControl(items: items)
    segmentedControl.frame = CGRect(x: 0, y: 0, width: 300, height: 60)
    segmentedControl.selectedSegmentIndex = -1
    segmentedControl.isMomentary = false
    segmentedControl.tintColor = Style.inverseTextColor
    segmentedControl.backgroundColor = Style.primaryColor
    segmentedControl.setTitleTextAttributes([NSFontAttributeName: UIFont.customBody.bolded,
                                             NSForegroundColorAttributeName: Style.inverseTextColor], for: .normal)
    segmentedControl.setTitleTextAttributes([NSFontAttributeName: UIFont.customBody.bolded,
                                             NSForegroundColorAttributeName: Style.primaryTextColor], for: .selected)

    
    segmentedControl.layer.cornerRadius = 5.0
    segmentedControl.clipsToBounds = true
    segmentedControl.addTarget(self, action: #selector(nextScreen), for: .valueChanged)
    
    return segmentedControl
  }
  */
  
  func customDividerView() -> UIView {
    let newView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 2))
    newView.backgroundColor = Style.backgroundColor
    newView.layer.cornerRadius = 5
    return newView
  }
  
  
  func customCheckbox(tag: Int) -> BEMCheckBox {
    let checkbox = BEMCheckBox(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    //checkbox.delegate = self
    checkbox.tag = tag
    
    checkbox.boxType = .circle
    checkbox.animationDuration = Animation.CheckboxDuration
    
    checkbox.onAnimationType = .bounce
    //    checkbox.onFillColor = Style.primaryColor
    checkbox.onTintColor = Style.highlightColor
    checkbox.onCheckColor = Style.highlightColor
    
    checkbox.offAnimationType = .fade
    checkbox.tintColor = Style.primaryColor
    //    checkbox.offFillColor = Style.secondaryColor
    
    return checkbox
  }
  
  
  func customLabel(text: String, tag: Int? = nil) -> UILabel {
    let label = UILabel()
    if let tag = tag {
      label.tag = tag
    }
    label.text = text
    label.textColor = Style.primaryTextColor
    label.font = UIFont.customBody
    label.numberOfLines = 0
    return label
  }
  
  // Single line text field with placeholder text
  func customTextField(placeholder: String) -> UITextField {
    let textField = UITextField(frame: CGRect(x: 0, y: 0, width: 300, height: 45))
    textField.delegate = self
    
    textField.placeholder = placeholder
    textField.font = UIFont.customBody
    textField.textColor = Style.primaryTextColor
    textField.backgroundColor = Style.backgroundColor
    
    textField.borderStyle = UITextField.BorderStyle.roundedRect
    textField.keyboardType = UIKeyboardType.default
    textField.returnKeyType = UIReturnKeyType.done
    textField.clearButtonMode = UITextField.ViewMode.whileEditing;
    textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
    textField.autocorrectionType = UITextAutocorrectionType.no
    
    textField.layer.borderColor = Style.secondaryTextColor.cgColor
    textField.layer.borderWidth = 0.8
    textField.layer.cornerRadius = 5
    
    return textField
  }
  
  // Multiple line text view with placeholder text
  func customTextView(placeholder: String) -> KMPlaceholderTextView {
    let textView = KMPlaceholderTextView(frame: CGRect(x: 0, y: 0, width: 300, height: 80))
    
    textView.delegate = self
    //textView.inputAccessoryView = keyboardToolbar()
    textView.font = UIFont.customBody
    textView.textColor = Style.primaryTextColor
    textView.backgroundColor = Style.backgroundColor
    
    textView.placeholderColor = Style.secondaryTextColor
    textView.placeholder = placeholder
    textView.placeholderFont = UIFont.customCaption1
    
    textView.layer.borderColor = Style.secondaryTextColor.cgColor
    textView.layer.borderWidth = 0.8
    textView.layer.cornerRadius = 5
    
    // Disable auto-correction keyboard on small 3.5" phones
    textView.autocorrectionType = UIDevice.current.isSmallerPhone() ? UITextAutocorrectionType.no : UITextAutocorrectionType.yes
    textView.spellCheckingType = UITextSpellCheckingType.yes
    textView.keyboardType = UIKeyboardType.default
    
    return textView
  }
  
  
  // Custom TRANSCRIPT button with target/action
  func customTranscriptButton(target: Any? = nil, action: Selector? = nil) -> UIButton {
    let transcriptButton = UIButton(type: .custom)
    transcriptButton.setImage(Style.imageOfTranscriptButton, for: .normal)
    
    if let target = target, let action = action {
      transcriptButton.addTarget(target, action: action, for: .touchUpInside)
    }
    return transcriptButton
  }
  
  
  // MARK: Standardised Custom View Controllers
  
  // Push a new BEACON, RAFT, or TRANSCRIPT screen on the nav stack
  // Parse the markdownString and display
  func showNewBeaconRaftTranscriptView(type: BeaconRaftTranscriptType, markdownString: String,
                                       title: String = "", coverFile: String = "") {
    
    // Setup View Controller, scrollView, and BeaconRaftTranscriptView
    let newVC = UIViewController()
    newVC.view.backgroundColor = Style.backgroundColor
    newVC.automaticallyAdjustsScrollViewInsets = true
    
    let scrollView = UIScrollView(frame: newVC.view.frame.inset(by: safeInsets))
    newVC.view.addSubview(scrollView)
    let viewWidth = newVC.view.frame.width
    scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    scrollView.contentSize = CGSize(width: viewWidth, height: 5000)  // set a large height, then resize later once label added
    
    var markdownText: String
    var headerHeight: CGFloat
    //var textLabelFrame: CGRect
    if type == .beacon || type == .raft { // header graphic custom view
      
      let newViewFrame = CGRect(x: 0, y: 0, width: viewWidth, height: viewWidth/640 * 640)  // preset image size is 640 x 640
      let newView = BeaconRaftTranscriptView(frame: newViewFrame)
      newView.viewType = type
      newView.backgroundColor = Style.backgroundColor
      scrollView.addSubview(newView)

      markdownText = markdownString
      headerHeight = newViewFrame.height - 95.0
      
    } else { // TRANSCRIPT - cover image + title
      
      let coverImageView = UIImageView(image: UIImage(named: coverFile))
      scrollView.addSubview(coverImageView)
      let width = scrollView.contentSize.width
      coverImageView.frame = CGRect(x: 0, y: 0, width: width, height: width * 9.0/16.0)
      
      markdownText = "##" + title + "\n\n" + markdownString
      headerHeight = coverImageView.frame.height + 10.0
  
    }

    
    // Add UILabel for the attributed text
    let textLabel = UILabel()
    textLabel.numberOfLines = 0
    textLabel.adjustsFontSizeToFitWidth = true
    textLabel.font = UIFont.customBody
    textLabel.textColor = Style.primaryTextColor
    textLabel.textAlignment = .left
    textLabel.frame = CGRect(x: Const.HorizontalPadding, y: headerHeight, width: scrollView.contentSize.width - 2.0 * Const.HorizontalPadding, height: scrollView.contentSize.height - headerHeight)
    scrollView.addSubview(textLabel)
    
    self.navigationController?.pushViewController(newVC, animated: true)
    
    
    /*
    // Add settings button to the navigation bar top right
    let settingsImage = #imageLiteral(resourceName: "settings.png")
    let settingsButton = UIBarButtonItem(image: settingsImage, style: .plain) { [weak self] button in
    
      let settingsVC = self?.newViewController(withId: "About")
      guard let settings = settingsVC else { return }
      let settingsNavController = UINavigationController(rootViewController: settings)
      // Creating a navigation controller with settings at the root of the navigation stack.
      settingsNavController.modalTransitionStyle = .coverVertical
      settingsNavController.modalPresentationStyle = .overCurrentContext
      settings.title = "Resources"
      self?.present(settingsNavController, animated: true, completion: nil)
    }
    newVC.navigationItem.rightBarButtonItem = settingsButton
*/
    // Add button to the navigation bar top right
    let crisisImage = UIImage(named: "crisis")
    let crisisButton = UIBarButtonItem(image: crisisImage, style: .plain) { [weak self] button in
      guard let weakSelf = self else { return }
      let contactsController = ContactsTableViewController(style: .plain)
      contactsController.setDependencies(strings: weakSelf.strings, model: weakSelf.model)
      let crisisNavController = UINavigationController(rootViewController: contactsController)
      crisisNavController.modalTransitionStyle = .coverVertical
      crisisNavController.modalPresentationStyle = .overCurrentContext
      self?.present(crisisNavController, animated: true, completion: nil)
     
    }
    newVC.navigationItem.rightBarButtonItem = crisisButton
    
    // Parse the markdown text to an NSAttributedString
    let markdownParser = MarkdownParser(font: UIFont.customBody)
    markdownParser.automaticLinkDetectionEnabled = false
    markdownParser.bold.color = Style.primaryColor.shadow(withLevel: 0.4)
    markdownParser.italic.font = UIFont.customBodyItalic
    markdownParser.header.fontIncrease = 4
  
    
    let attributedText = NSMutableAttributedString(attributedString: markdownParser.parse(markdownText))
    
    // SUPPORT !N and !B special markups to align hanging indents for bulleted and numbered lists
    // NUMBERED LIST HANGING INDENTS: Search for the first and last !N in the text, so it doesn't apply to all the text
    let wholeTextRange = NSRange(location: 0, length: attributedText.length)
    let firstNumberedRange = (attributedText.string as NSString).range(of: "!N", options: [], range: wholeTextRange)
    let lastNumberedRange = (attributedText.string as NSString).range(of: "!N", options: [.backwards], range: wholeTextRange)
    if firstNumberedRange.location != NSNotFound && lastNumberedRange.location != NSNotFound {
      // Adjust indenting for numbered lists
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.firstLineHeadIndent = 0.0
      // Calculate the amount for a numbered list headIndent based on current font
      let numberedPrefix: NSString = "1. "
      let size = numberedPrefix.size(withAttributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.customBody]))
      paragraphStyle.headIndent = size.width;
      let numberedListRange = NSRange(location: firstNumberedRange.location, length: lastNumberedRange.location - firstNumberedRange.location + 1)
      attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: numberedListRange)
      attributedText.replaceCharacters(in: lastNumberedRange, with: "")
      attributedText.replaceCharacters(in: firstNumberedRange, with: "")
    }
    
    // BULLET POINT HANGING INDENTS: Search for the first and last !B in the text, so it doesn't apply to all the text
    let wholeRange = NSRange(location: 0, length: attributedText.length)
    let firstRange = (attributedText.string as NSString).range(of: "!B", options: [], range: wholeRange)
    let lastRange = (attributedText.string as NSString).range(of: "!B", options: [.backwards], range: wholeRange)
    if firstRange.location != NSNotFound && lastRange.location != NSNotFound {
      // Adjust indenting for bullet points
      let paragraphStyle = NSMutableParagraphStyle()
      paragraphStyle.firstLineHeadIndent = 0.0
      // Calculate the amount for a bullet point headIndent based on current font
      let bulletPrefix: NSString = "• "
      let size = bulletPrefix.size(withAttributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font) : UIFont.customBody]))
      paragraphStyle.headIndent = size.width;
      
      let bulletRange = NSRange(location: firstRange.location, length: lastRange.location - firstRange.location + 1)
      attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: bulletRange)
      attributedText.replaceCharacters(in: lastRange, with: "")
      attributedText.replaceCharacters(in: firstRange, with: "")
    }
    
    textLabel.attributedText = attributedText
    textLabel.sizeToFit()
    
    
    backButton = UIButton.custom("<  Back", target: nil, action: nil)
    guard let button = backButton else { return }
    
    button.addTarget() { button in
      newVC.navigationController?.popViewController(animated: true)
    }
 
    scrollView.addSubview(button)
    button.snp.makeConstraints { (make) in
      make.top.equalTo(textLabel.snp.bottom).offset(Const.VerticalPadding)
      make.bottom.equalTo(scrollView.snp.bottom)
      make.centerX.equalTo(scrollView.snp.centerX)
      make.height.equalTo(60)
      make.left.right.equalTo(scrollView).inset(Const.HorizontalPadding)
    }

    scrollView.contentSize = CGSize(width: viewWidth, height: headerHeight + textLabel.frame.height + button.frame.height)
  }
  
  
  @objc func previousScreen() {
    print("ScrollingForm previousScreen()")
    
    if let parentPageViewController = self.parent as? PlanPageViewController {
      parentPageViewController.goToPreviousPage()
    }
    
    
  }

  @objc func nextScreen() {
    /*
     // Show the main menu
     let mainMenuScreen = newViewController(withId: "MainMenu")
     navigationController?.pushViewController(mainMenuScreen, animated: false)
     */
    print("ScrollingForm nextScreen()")
    
    if let parentPageViewController = self.parent as? PlanPageViewController {
      parentPageViewController.goToNextPage()
    }
    
    
    /*
    // Request authorisation for User Notifications
    requestNotificationsAccess(options: [.alert, .badge, .sound]) { [weak self] allowed in
      if !allowed {
        print("Notifications access not granted")
      } else {
        self?.model.userSettings.isNotificationsEnabled = true
        self?.model.userSettings.saveToDefaults()
      }
    }
    */
    /*
    let mainMenuScreen = newViewController(withId: "MainMenu")
    navigationController?.setViewControllers([mainMenuScreen], animated: true)
    */
    
  }
  
  
  /// Scroll so that the view appears at the top of the scrollview
  ///
  /// - Parameter view: view to put at the top of the scrollview
  func scrollToTop(view: UIView, animated: Bool = true) {
    print("scrollToTop: \(view)")
    self.scrollView.setContentOffset(CGPoint(x:0, y: view.frame.origin.y /*- 60*/), animated: animated)
  }
  
  
  /*
   override func viewDidLayoutSubviews() {
   super.viewDidLayoutSubviews()
   
   //print("P4  \(speechView1)  \(speechView2)  \(speechView3)")
   
   if let viewAtTop = scrolltoView {
   scrollToTop(view: viewAtTop)
   //      if viewAtTop == speechView2 {
   //        s2TextView.becomeFirstResponder()
   //      }
   }
   
   }
   */
  
  /*
  func registerForKeyboardNotifications(){
    //Adding notifies on keyboard appearing
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }
  
  func deregisterFromKeyboardNotifications(){
    //Removing notifies on keyboard appearing
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
  }
  
  func keyboardWasShown(notification: NSNotification){
    //Need to calculate keyboard exact size due to Apple suggestions
    self.scrollView.isScrollEnabled = true
    var info = notification.userInfo!
    let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
    
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    
    var aRect : CGRect = self.view.frame
    aRect.size.height -= keyboardSize!.height
    if let activeField = self.activeField {
      if (!aRect.contains(activeField.frame.origin)){
        self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
      }
    }
  }
  
  func keyboardWillBeHidden(notification: NSNotification){
    //Once keyboard disappears, restore original positions
    var info = notification.userInfo!
    let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
    self.scrollView.contentInset = contentInsets
    self.scrollView.scrollIndicatorInsets = contentInsets
    self.view.endEditing(true)
    self.scrollView.isScrollEnabled = false
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField){
    activeField = textField
  }
  
  func textFieldDidEndEditing(_ textField: UITextField){
    activeField = nil
  }
  
  
  
  */
  

  
  // MARK: Scheduling Notification and Calendar events
  
  func scheduleNotificationsAndCalendar(isFollowup: Bool = false) {
    
    guard let chatDate = model.getChatDate() else {        // Chat date may have been deleted
      
      // cancel previously scheduled NOTIFICATION
      if model.userSettings.isNotificationsEnabled {
        if let chatNotificationId = model.plan?.chatNotificationId {
          cancelNotification(withId: chatNotificationId)
          model.plan?.chatNotificationId = nil
        }
      }
      
      // cancel previous CALENDAR entry
      if model.userSettings.isCalendarEnabled {
        EventStore.requestAuthorization { [weak self] allowed in
          guard let weakSelf = self else { return }
          if allowed {       // confirm that we're authorized for calendar access
            if let chatEventId = isFollowup ? weakSelf.model.plan?.followupEventId : weakSelf.model.plan?.chatEventId {
              weakSelf.deleteChatEvent(withIdentifier: chatEventId)
              if isFollowup { weakSelf.model.plan?.followupEventId = nil } else { weakSelf.model.plan?.chatEventId = nil }
            }
          }
        }
      }
      
      model.savePlans()
      return
    }
    
    let eventTitle = isFollowup ? "Follow-up chat with \(model.getName())" : "Chat with \(model.getName())"
    
    // NOTIFICATION
    if model.userSettings.isNotificationsEnabled {
      // If there's already a notification scheduled, re-schedule it
      if let chatNotificationId = model.plan?.chatNotificationId {
        print("P4 Attempting to re-schedule a local notification for \(chatDate)")
        scheduleLocalNotification(withId: chatNotificationId, forDate: chatDate, title: eventTitle, body: "Chat Notification")
      } else { // brand new notification to schedule
        let chatNotificationId = UUID().uuidString
        print("P4 Attempting to schedule a local notification for \(chatDate)")
        scheduleLocalNotification(withId: chatNotificationId, forDate: chatDate, title: eventTitle, body: "Chat Notification")
        model.plan?.chatNotificationId = chatNotificationId
      }
    }
    
    // CALENDAR
    if model.userSettings.isCalendarEnabled {
      EventStore.requestAuthorization { [weak self] allowed in
        guard let weakSelf = self else { return }
        
        if allowed {
          let eventLocation = (weakSelf.model.get(.p4_s4) as? String) ?? ""
          
          // Check if there's already a chat scheduled in the calendar and move it
          var scheduleNewEvent = true
          if let chatEventId = isFollowup ? weakSelf.model.plan?.followupEventId : weakSelf.model.plan?.chatEventId {
            print("Attempting to move conversation in calendar for \(chatDate)")
            if weakSelf.moveChatEvent(withIdentifier: chatEventId, startDate: chatDate, title: eventTitle, location: eventLocation) {
              print("Successfully move chat event in calendar to \(chatDate)")
              scheduleNewEvent = false
            }
          }
          
          if scheduleNewEvent { // brand new calendar event to schedule, or error moving the event
            print("Attempting to schedule chat in calendar for \(chatDate)")
            if let chatEventId = weakSelf.addChatEvent(startDate: chatDate, title: eventTitle, location: eventLocation) {
              // save the conversation eventId
              print("Returned calendar event Id: \(chatEventId)")
              if isFollowup {
               weakSelf.model.plan?.followupEventId = chatEventId
              } else {
               weakSelf.model.plan?.chatEventId = chatEventId
              }
              
            } else {
              // error during saving event
              print("Error scheduling conversation event")
            }
          }
  
        }
      }
    }  // if model.userSettings.isCalendarEnabled...
    
    model.savePlans()
  }
 
  
 
// MARK: UITextFieldDelegate - Name Field

  /// Only allow maxTextFieldLength no. of characters
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    guard string.count > 0 else {
      return true
    }
    let currentText = textField.text ?? ""
    let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
    return prospectiveText.count <= Const.MaxTextFieldLength
  }
  
}


// MARK: UITextViewDelegate
extension ScrollingForm: UITextViewDelegate {
  
  func textViewDidBeginEditing(_ textView: UITextView) {
    let borderColor = Style.secondaryColor
    textView.layer.borderColor = borderColor.cgColor
  }
  
  func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
    textView.resignFirstResponder()
    return true
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
//    print("P4 textViewDidEndEditing")
    textView.layer.borderColor = Style.secondaryTextColor.cgColor
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    /*
     if text == "\n" {
     textView.resignFirstResponder()
     return true
     }
     */
    
    let currentText = textView.text ?? ""
    let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: text)
    return prospectiveText.count <= Const.MaxTextViewLength
  }
  
  
}




/*
 override func viewDidLayoutSubviews() {
 super.viewDidLayoutSubviews()
 if s2TextView
 s2TextView.setContentOffset(CGPoint.zero, animated: false)
 }
 */
/*
 override func viewDidLayoutSubviews() {
 self.scrollView.contentSize = contentView.frame.size
 }
 */


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
