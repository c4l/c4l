//
//  User.swift
//  WireFraming
//
//  Created by Peter Hunt on 13/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation

// Model Object for user settings, archived to UserDefaults

class UserSettings: NSObject {
  
  var hasEV1Autoplayed, hasEV2Autoplayed, hasEV3Autoplayed, hasEV4Autoplayed, hasEV5Autoplayed, hasEV6Autoplayed: Bool
  var hasTapTheCircleBannerShown, hasChatCompleteBannerShown: Bool
  var isSubtitlesEnabled: Bool
  var isPasscodeEnabled: Bool
  var passcode: String?
  var isNotificationsEnabled: Bool
  var isCalendarEnabled: Bool
  var EV2TimeInterval: TimeInterval
  //var isIconBadgeHidden : Bool
  
  init(hasEV1Autoplayed: Bool = false, hasEV2Autoplayed: Bool = false, hasEV3Autoplayed: Bool = false, hasEV4Autoplayed: Bool = false, hasEV5Autoplayed: Bool = false, hasEV6Autoplayed: Bool = false,
       hasTapTheCircleBannerShown: Bool = false, hasChatCompleteBannerShown: Bool = false,
       isSubtitlesEnabled: Bool = true, isPasscodeEnabled: Bool = false, passcode: String? = nil,
       isNotificationsEnabled: Bool = true, isCalendarEnabled: Bool = true, EV2TimeInterval: TimeInterval = 0.0) {
    self.hasEV1Autoplayed = hasEV1Autoplayed
    self.hasEV2Autoplayed = hasEV2Autoplayed
    self.hasEV3Autoplayed = hasEV3Autoplayed
    self.hasEV4Autoplayed = hasEV4Autoplayed
    self.hasEV5Autoplayed = hasEV5Autoplayed
    self.hasEV6Autoplayed = hasEV6Autoplayed
    self.hasTapTheCircleBannerShown = hasTapTheCircleBannerShown
    self.hasChatCompleteBannerShown = hasChatCompleteBannerShown
    self.isSubtitlesEnabled = isSubtitlesEnabled
    self.isPasscodeEnabled = isPasscodeEnabled
    self.passcode = passcode
    self.isNotificationsEnabled = isNotificationsEnabled
    self.isCalendarEnabled = isCalendarEnabled
    self.EV2TimeInterval = EV2TimeInterval
  }
  
  
  // load new UserSettings from UserDefaults
  static func loadFromDefaults() -> UserSettings? {
    print("*** User loadFromDefaults() ***")
    
    // first time app is loaded
    if UserDefaults.standard.object(forKey: "hasEV1Autoplayed") == nil {
      return nil
    }
    
    let hasEV1Autoplayed = UserDefaults.standard.bool(forKey: "hasEV1Autoplayed")
    let hasEV2Autoplayed = UserDefaults.standard.bool(forKey: "hasEV2Autoplayed")
    let hasEV3Autoplayed = UserDefaults.standard.bool(forKey: "hasEV3Autoplayed")
    let hasEV4Autoplayed = UserDefaults.standard.bool(forKey: "hasEV4Autoplayed")
    let hasEV5Autoplayed = UserDefaults.standard.bool(forKey: "hasEV5Autoplayed")
    let hasEV6Autoplayed = UserDefaults.standard.bool(forKey: "hasEV6Autoplayed")
    let hasTapTheCircleBannerShown = UserDefaults.standard.bool(forKey: "hasTapTheCircleBannerShown")
    let hasChatCompleteBannerShown = UserDefaults.standard.bool(forKey: "hasChatCompleteBannerShown")
    let isSubtitlesEnabled = UserDefaults.standard.bool(forKey: "isSubtitlesEnabled")
    let isPasscodeEnabled = UserDefaults.standard.bool(forKey: "isPasscodeEnabled")

    let passcode = UserDefaults.standard.string(forKey: "passcode")
  
    let isNotificationsEnabled = UserDefaults.standard.bool(forKey: "isNotificationsEnabled")
    let isCalendarEnabled = UserDefaults.standard.bool(forKey: "isCalendarEnabled")
    
    let EV2TimeInterval = UserDefaults.standard.double(forKey: "EV2TimeInterval")

    return UserSettings(hasEV1Autoplayed: hasEV1Autoplayed, hasEV2Autoplayed: hasEV2Autoplayed, hasEV3Autoplayed: hasEV3Autoplayed, hasEV4Autoplayed: hasEV4Autoplayed, hasEV5Autoplayed: hasEV5Autoplayed, hasEV6Autoplayed: hasEV6Autoplayed, hasTapTheCircleBannerShown: hasTapTheCircleBannerShown, hasChatCompleteBannerShown: hasChatCompleteBannerShown, isSubtitlesEnabled: isSubtitlesEnabled, isPasscodeEnabled: isPasscodeEnabled, passcode: passcode, isNotificationsEnabled: isNotificationsEnabled, isCalendarEnabled: isCalendarEnabled, EV2TimeInterval: EV2TimeInterval)
    
  }
  
  
  // save user settings to UserDefaults
  func saveToDefaults() {
    print("*** User save() ***")
    
    UserDefaults.standard.set(hasEV1Autoplayed, forKey: "hasEV1Autoplayed")
    UserDefaults.standard.set(hasEV2Autoplayed, forKey: "hasEV2Autoplayed")
    UserDefaults.standard.set(hasEV3Autoplayed, forKey: "hasEV3Autoplayed")
    UserDefaults.standard.set(hasEV4Autoplayed, forKey: "hasEV4Autoplayed")
    UserDefaults.standard.set(hasEV5Autoplayed, forKey: "hasEV5Autoplayed")
    UserDefaults.standard.set(hasEV6Autoplayed, forKey: "hasEV6Autoplayed")
    UserDefaults.standard.set(hasTapTheCircleBannerShown, forKey: "hasTapTheCircleBannerShown")
    UserDefaults.standard.set(hasChatCompleteBannerShown, forKey: "hasChatCompleteBannerShown")
    UserDefaults.standard.set(isSubtitlesEnabled, forKey: "isSubtitlesEnabled")
    UserDefaults.standard.set(isPasscodeEnabled, forKey: "isPasscodeEnabled")
    if let passcode = passcode {
      UserDefaults.standard.set(passcode, forKey: "passcode")
    }
    UserDefaults.standard.set(isNotificationsEnabled, forKey: "isNotificationsEnabled")
    UserDefaults.standard.set(isCalendarEnabled, forKey: "isCalendarEnabled")
    UserDefaults.standard.set(EV2TimeInterval, forKey: "EV2TimeInterval")
  }
  
  
  override var debugDescription: String {

    return "hasEV1Autoplayed: \(self.hasEV1Autoplayed), hasEV2Autoplayed: \(self.hasEV2Autoplayed), hasEV3Autoplayed: \(self.hasEV3Autoplayed), hasEV4Autoplayed: \(self.hasEV4Autoplayed), hasEV5Autoplayed: \(self.hasEV5Autoplayed), hasEV6Autoplayed: \(self.hasEV6Autoplayed), hasTapTheCircleBannerShown: \(hasTapTheCircleBannerShown), hasChatCompleteBannerShown: \(hasChatCompleteBannerShown), isPasscodeEnabled: \(isPasscodeEnabled), passcode: \(String(describing:passcode)), isNotificationsEnabled: \(isNotificationsEnabled), isCalendarEnabled: \(isCalendarEnabled), EV2TimeInterval: \(String(describing: EV2TimeInterval))"
    
  }
  
  
  
}
