//
//  PlanItemCell.swift
//  WireFraming
//
//  Created by Peter Hunt on 28/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

class PlanItemCell : UITableViewCell {
  
  @IBOutlet weak var heading1: UILabel!
  @IBOutlet weak var title1: UILabel!
  @IBOutlet weak var detail1: UILabel!
  @IBOutlet weak var title2: UILabel!
  @IBOutlet weak var detail2: UILabel!
  @IBOutlet weak var title3: UILabel!
  @IBOutlet weak var detail3: UILabel!
  @IBOutlet weak var detail4: UILabel!
  @IBOutlet weak var title4: UILabel!
  @IBOutlet weak var title5: UILabel!
  @IBOutlet weak var detail5: UILabel!
  
  @IBOutlet weak var backgroundCardView: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    // Customise the cell colours and set a corner radius on the backgroundCardView to make the cell look like a 'card'
    contentView.backgroundColor = Style.backgroundColor
    backgroundCardView.backgroundColor = Style.cardBackgroundColor
    backgroundCardView.layer.cornerRadius = 3.0
    backgroundCardView.layer.masksToBounds = false
    backgroundCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
    backgroundCardView.layer.shadowOffset = CGSize(width: 0, height:0)
    backgroundCardView.layer.shadowOpacity = 0.8
    
    
    heading1.font = UIFont.customFont(forStyle: .title2)
    heading1.textColor = Style.inverseTextColor
    
    title1.font = UIFont.customFont(forStyle: .subheadline)
    title1.textColor = Style.inverseTextColor

    detail1.font = UIFont.customFont(forStyle: .body)
    detail1.textColor = Style.secondaryTextColor

    title2.font = UIFont.customFont(forStyle: .subheadline)
    title2.textColor = Style.inverseTextColor
    
    detail2.font = UIFont.customFont(forStyle: .body)
    detail2.textColor = Style.secondaryTextColor
    
  }
  
  /*
  var page: [String: Any]? { // eg. ["p4_s1_i2" : "Hello World"]
    didSet {
      self.updateUI()
    }
  }
  

  
  func updateUI() {
    
    guard let currentPage = page else {
      return
    }
    
    
    titleLabel.text = "p4_s1_i2"
 
    if let detail = currentPage["p4_s2_i1"] as? String {
      detailTextLabel?.text = detail
    }
    
  }
 */
  
}
