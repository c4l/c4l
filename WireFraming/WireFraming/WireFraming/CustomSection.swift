//
//  CustomSection.swift
//  WireFraming
//
//  Created by Peter Hunt on 2/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka

/*
 A custom Eureka section, with a custom header view
 */

class CustomSection: Section {
  
  var myHeaderString: String? {
    didSet {
      let view = header?.viewForSection(self, type: .header)
      view?.backgroundColor = Style.backgroundColor
      view?.setNeedsLayout()
    }
  }
  
  override init(_ header: String, _ initializer: (Section) -> ()) {
    myHeaderString = header
    super.init(header, initializer)
    var header = HeaderFooterView<AutoSizeLabelView>(.class)
    header.height = { UITableViewAutomaticDimension }
    header.onSetupView = { v, s in
      
      /* Add an image attachment to the left of the label
       
       
      let attachment = NSTextAttachment()
      attachment.image = #imageLiteral(resourceName: "calendar.png")
      attachment.bounds = CGRect(x: -5, y: -5, width: 20, height: 20)
      let attachmentStr = NSAttributedString(attachment: attachment)
      let stringWithAttachment = NSMutableAttributedString(string: "")
      stringWithAttachment.append(attachmentStr)
      let stringText = NSMutableAttributedString(string: ((s as? CustomSection)?.myHeaderString)!)
      stringWithAttachment.append(stringText)
      v.label.attributedText = stringWithAttachment
      
      */
      
      v.label.text = (s as? CustomSection)?.myHeaderString
      v.backgroundColor = Style.backgroundColor
    }
    self.header = header
    
  }
  
  override required init(_ initializer: (Section) -> ()) {
    var header = HeaderFooterView<AutoSizeLabelView>(.class)
    header.height = { UITableViewAutomaticDimension }
    header.onSetupView = { v, s in
      v.label.text = (s as? CustomSection)?.myHeaderString
      v.backgroundColor = Style.backgroundColor
    }
    
    super.init(initializer)
  }
  
  required init() {
    var header = HeaderFooterView<AutoSizeLabelView>(.class)
    header.height = { UITableViewAutomaticDimension }
    header.onSetupView = { v, s in
      v.label.text = (s as? CustomSection)?.myHeaderString
      v.backgroundColor = Style.backgroundColor
    }
    
    super.init()
  }
  
}



class AutoSizeLabelView: UIView {
  
  lazy var label: UILabel = {
    var result = UILabel()
    result.translatesAutoresizingMaskIntoConstraints = false
    result.numberOfLines = 0
    
    result.font = .customSubheadline
    result.textColor = Style.secondaryTextColor
    result.backgroundColor = Style.backgroundColor
    
    
    return result
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(label)
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: NSLayoutFormatOptions.alignAllLastBaseline, metrics: nil, views: ["label": label]))
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[label]-|", options: NSLayoutFormatOptions.alignAllLeft, metrics: nil, views: ["label": label]))
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    label.preferredMaxLayoutWidth = frame.size.width
    superview?.setNeedsLayout()
  }
}
