//
//  P6.swift
//  WireFraming
//
//  Created by Peter Hunt on 24/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
//import KMPlaceholderTextView
//import BEMCheckBox
import SnapKit
import BMPlayer


/*
 P6 Scrolling Form
 
 Video (EV4)
 
 Skip/Next Button
 
 Video (EV5)
 
 Skip/Next Button
 
 
 BEACON
 
 RAFT
 
 
 Video (EV6)
 
 
 Next Button
 
 */

class P6: ScrollingForm {
  
  var player1, player2, player3: BMPlayer?
  var videoPlayer1Top, videoPlayer2Top, videoPlayer3Top: ConstraintItem!
  var transcriptSpeechView1: SpeechBubbleView!
  
  let videoName1 = "EV4"
  let videoFile1 = "EV4.mp4"
  let subtitleFile1 = "EV4.srt"
  let coverFile1 = "EV4.jpg"
  let videoTitle1 = "ev4_title"
  
  /*
   1. It's about your friend
   2. Take time to listen
   3. Be patient
   4. Don't be loud
   5. Don't judge
   6. Be interested
   7. Don't interrupt
   8. Ask questions
   9. Be honest
   
   
   */
  
  let videoName2 = "EV5"
  let videoFile2 = "EV5.mp4"
  let subtitleFile2 = "EV5.srt"
  let coverFile2 = "EV5.jpg"
  let videoTitle2 = "ev5_title"
  
  /*
   Questions..
   Are you Okay ?
   
   You seem a bit flat...
   
   You seem pretty tired...
   
   I've had a bad week..
   
   Lifeline 13 11 14 or 000
   
   BEACON
   RAFT
   */
  
  let videoName3 = "EV6"
  let videoFile3 = "EV6.mp4"
  let subtitleFile3 = "EV6.srt"
  let coverFile3 = "EV6.jpg"
  let videoTitle3 = "ev6_title"
  
  
  var isFirstRun: Bool = false
  var speechView1, speechView2: SpeechBubbleView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    /*
     if isFirstRun {
     addVideoPlayer(videoName: videoName1, videoFile: videoFile1, subtitleFile: subtitleFile1, coverFile: coverFile1, withSkipButton: true)
     animateInVideoPlayer(withSkipButton: true, autoplay: true)
     } else {
     addVideoPlayer(videoName: videoName1, videoFile: videoFile1, subtitleFile: subtitleFile1, coverFile: coverFile1, withSkipButton: false)
     animateInVideoPlayer(withSkipButton: true, autoplay: false)
     }
     
     if let player = player {
     videoPlayer2Top = player.snp.bottom
     
     }
     */
    
    videoPlayer1Top = contentView.snp.top
    addVideoPlayer1()
    
    videoPlayer2Top = lastControlBottom
    addVideoPlayer2(videoName: videoName2, videoFile: videoFile2, subtitleFile: subtitleFile2, coverFile: coverFile2, withSkipButton: false)
    
    addSection1()  // BEACON
    addSection2()  // RAFT
    
    videoPlayer3Top = lastControlBottom
    addVideoPlayer3(videoName: videoName3, videoFile: videoFile3, subtitleFile: subtitleFile3, coverFile: coverFile3)
    
    addBackNextButtons()
    //nextButton?.setTitle("Finish", for: .normal)
  }
  
  deinit {
    print("P6 deinit")
    if let player = player {
      player.prepareToDealloc()
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("P6 viewWillDisappear")
    
    //if let player = player {
    //  player.pause()
    // }
    
    //model.savePlans()
  }
  
  
  func newPlayerResource(videoName: String, videoFile: String,
                         subtitleFile: String?, coverFile: String?) -> BMPlayerResource {
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    var subtitles: BMSubtitles?
    if let subtitle = subtitleFile, model.userSettings.isSubtitlesEnabled {
      let subtitleUrl = Bundle.main.url(forResource: subtitle, withExtension: nil)!
      subtitles = BMSubtitles(url: subtitleUrl)
    }
    var cover: URL?
    if let coverFile = coverFile {
      cover = Bundle.main.url(forResource: coverFile, withExtension: nil)! // coverURL images with some dimensions seems to mess up autolayout in landscape!!
    }
    let asset = BMPlayerResource(name: videoName,
                                 definitions: [BMPlayerResourceDefinition(url: videoUrl, definition: "1080p")],
                                 cover: cover,
                                 subtitles: subtitles)
    return asset
  }
  
  func setPlayerResource1() {
    guard let videoPlayer = player1 else { return }
    videoPlayer.setVideo(resource: newPlayerResource(videoName: videoName1, videoFile: videoFile1, subtitleFile: subtitleFile1, coverFile: coverFile1))
  }
  
  func setPlayerResource2() {
    guard let videoPlayer = player2 else { return }
    videoPlayer.setVideo(resource: newPlayerResource(videoName: videoName2, videoFile: videoFile2, subtitleFile: subtitleFile2, coverFile: coverFile2))
  }
  
  func setPlayerResource3() {
    guard let videoPlayer = player3 else { return }
    videoPlayer.setVideo(resource: newPlayerResource(videoName: videoName3, videoFile: videoFile3, subtitleFile: subtitleFile3, coverFile: coverFile3))
  }
  
  
  func stateChange(forPlayer: Int, isPlaying: Bool) {
    guard let p1 = player1, let p2 = player2, let p3 = player3 else { return }
    
    print("P6 player #\(forPlayer) playStateDidChange \(isPlaying)")
    
    
    switch forPlayer {
      
    case 1:
      if isPlaying {
        p2.pause()
        setPlayerResource2()
        p3.pause()
        setPlayerResource3()
      }
      
    case 2:
      if isPlaying {
        p1.pause()
        setPlayerResource1()
        p3.pause()
        setPlayerResource3()
      }
      
    case 3:
      if isPlaying {
        p1.pause()
        setPlayerResource1()
        p2.pause()
        setPlayerResource2()
      }
      
    default:
      break
      
    }
    
    appDelegate.shouldRotate = p1.isPlaying || p2.isPlaying || p3.isPlaying  // allow rotation only if one is playing
    
  }
  
  
  // MARK: - Videos
  func addVideoPlayer1() {
    //let controller = BMPlayerCustomControlView()
    //player1 = BMPlayer(customControlView: controller)
    player1 = BMPlayerCustom(customControlView: BMPlayerTapToPlayControlView())
    guard let videoPlayer = player1 else { return }
    contentView.addSubview(videoPlayer)
    
    setVideoPlayersPortrait()  // setup constraints for player in portrait mode
    lastControlBottom = videoPlayer.snp.bottom
    
    setPlayerResource1()
    
    // Set up player callbacks
    videoPlayer.backBlock = { (isFullScreen) in
      if isFullScreen { return }
    }
    
    // Listen to when the player is playing or stopped
    videoPlayer.playStateDidChange = { [unowned self] (isPlaying: Bool) in
      self.stateChange(forPlayer: 1, isPlaying: isPlaying)
    }
    /*
     // Listen to when the play time changes
     videoPlayer.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
     print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
     }
     */
    
    let transcriptButton = customTranscriptButton(target: self, action: #selector(transcriptButton1Tapped))
    contentView.addSubview(transcriptButton)
    transcriptButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-5)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(transcriptButton.snp.width).multipliedBy(135.0/500.0)
    }
    lastControlBottom = transcriptButton.snp.bottom
    
  }
  
  
  // MARK: - Video
  func addVideoPlayer2(videoName: String, videoFile: String,
                       subtitleFile: String?, coverFile: String?,
                       withSkipButton: Bool = false) {
    
    //let controller = BMPlayerCustomControlView()
    //player2 = BMPlayer(customControlView: controller)
    player2 = BMPlayerCustom(customControlView: BMPlayerTapToPlayControlView())
    guard let videoPlayer = player2 else { return }
    
    contentView.addSubview(videoPlayer)
    
    setVideoPlayersPortrait()  // setup constraints for player in portrait mode
    
    lastControlBottom = videoPlayer.snp.bottom
    
    if withSkipButton {
      skipButton = UIButton.custom("Skip", target: self, action: #selector(skipButtonTapped))
      guard let button = skipButton else { return }
      self.contentView.addSubview(button)
      
      button.snp.makeConstraints { (make) in
        make.top.equalTo(lastControlBottom).offset(20)
        make.centerX.equalTo(self.contentView.snp.centerX)
        make.width.equalTo(160)
        make.height.equalTo(50)
      }
      
      lastControlBottom = button.snp.bottom
    }
    
    videoPlayer.setVideo(resource: newPlayerResource(videoName: videoName, videoFile: videoFile, subtitleFile: subtitleFile, coverFile: coverFile))
    
    // Set up player callbacks
    videoPlayer.backBlock = { (isFullScreen) in
      if isFullScreen { return }
    }
    
    // Listen to when the player is playing or stopped
    videoPlayer.playStateDidChange = { [unowned self] (isPlaying: Bool) in
      self.stateChange(forPlayer: 2, isPlaying: isPlaying)
      
      if let button = self.skipButton, withSkipButton {
        if !isPlaying {
          button.setTitle("Next", for: .normal)
        } else {
          button.setTitle("Skip", for: .normal)
        }
      }
    }
    
    // Listen to when the play time changes
    videoPlayer.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
      print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
    let transcriptButton = customTranscriptButton(target: self, action: #selector(transcriptButton2Tapped))
    contentView.addSubview(transcriptButton)
    transcriptButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-5)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(transcriptButton.snp.width).multipliedBy(135.0/500.0)
    }
    lastControlBottom = transcriptButton.snp.bottom
  }
  
  
  // MARK: - Video
  func addVideoPlayer3(videoName: String, videoFile: String,
                       subtitleFile: String?, coverFile: String?) {
    //let controller = BMPlayerCustomControlView()
    //player3 = BMPlayer(customControlView: controller)
    player3 = BMPlayerCustom(customControlView: BMPlayerTapToPlayControlView())
    guard let videoPlayer = player3 else { return }
    
    contentView.addSubview(videoPlayer)
    setVideoPlayersPortrait()  // setup constraints for player in portrait mode
    lastControlBottom = videoPlayer.snp.bottom
    
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    var subtitles: BMSubtitles?
    if let subtitle = subtitleFile, model.userSettings.isSubtitlesEnabled {
      let subtitleUrl = Bundle.main.url(forResource: subtitle, withExtension: nil)!
      subtitles = BMSubtitles(url: subtitleUrl)
    }
    
    var cover: URL?
    if let coverFile = coverFile {
      cover = Bundle.main.url(forResource: coverFile, withExtension: nil)! // coverURL images with some dimensions seems to mess up autolayout in landscape!!
    }
    
    let asset = BMPlayerResource(name: videoName,
                                 definitions: [BMPlayerResourceDefinition(url: videoUrl, definition: "1080p")],
                                 cover: cover,
                                 subtitles: subtitles)
    videoPlayer.setVideo(resource: asset)
    
    // Set up player callbacks
    videoPlayer.backBlock = { (isFullScreen) in
      if isFullScreen { return }
    }
    
    // Listen to when the player is playing or stopped
    videoPlayer.playStateDidChange = { [unowned self] (isPlaying: Bool) in
      self.stateChange(forPlayer: 3, isPlaying: isPlaying)
    }
    
    // Listen to when the play time changes
    /*
     videoPlayer.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
     print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
     }
     */
    let transcriptButton = customTranscriptButton(target: self, action: #selector(transcriptButton3Tapped))
    contentView.addSubview(transcriptButton)
    transcriptButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-5)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(transcriptButton.snp.width).multipliedBy(135.0/500.0)
    }
    lastControlBottom = transcriptButton.snp.bottom
  }
  
  
  
  func setActiveVideoPlayerFullScreen() {
    
    // player1 is active
    if let activePlayer = player1,
      let _ = activePlayer.playerLayer?.player {
      
      activePlayer.snp.remakeConstraints { (make) in
        make.left.right.top.equalTo(self.view)
        make.height.equalTo(activePlayer.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
      
      if let player2 = player2 {
        player2.snp.remakeConstraints { (make) in make.height.equalTo(0) }
      }
      
      if let player3 = player3 {
        player3.snp.remakeConstraints { (make) in make.height.equalTo(0) }
      }
      
      // player2 is active
    } else if let activePlayer = player2,
      let _ = activePlayer.playerLayer?.player {
      
      activePlayer.snp.remakeConstraints { (make) in
        make.left.right.top.equalTo(self.view)
        make.height.equalTo(activePlayer.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
      
      if let player1 = player1 {
        player1.snp.remakeConstraints { (make) in make.height.equalTo(0) }
      }
      
      if let player3 = player3 {
        player3.snp.remakeConstraints { (make) in make.height.equalTo(0) }
      }
      
      // player3 is active
    } else if let activePlayer = player3,
      let _ = activePlayer.playerLayer?.player {
      
      activePlayer.snp.remakeConstraints { (make) in
        make.left.right.top.equalTo(self.view)
        make.height.equalTo(activePlayer.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
      
      if let player1 = player1 {
        player1.snp.remakeConstraints { (make) in make.height.equalTo(0) }
      }
      
      if let player2 = player2 {
        player2.snp.remakeConstraints { (make) in make.height.equalTo(0) }
      }
      
    }
    
    
    
  }
  
  
  func setVideoPlayersPortrait() {
    
    if let player1 = player1 {
      player1.snp.remakeConstraints { (make) in
        make.top.equalTo(videoPlayer1Top)
        make.left.right.equalTo(self.contentView)
        make.height.equalTo(player1.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
    }
    
    if let player2 = player2 {
      player2.snp.remakeConstraints { (make) in
        make.top.equalTo(videoPlayer2Top).offset(20)
        make.left.right.equalTo(self.contentView)
        make.height.equalTo(player2.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
    }
    
    if let player3 = player3 {
      player3.snp.remakeConstraints { (make) in
        make.top.equalTo(videoPlayer3Top).offset(20)
        make.left.right.equalTo(self.contentView)
        make.height.equalTo(player3.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
    }
    
    
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    let orientation = newCollection.verticalSizeClass
    
    switch orientation {
    case .compact:
      print("P6 now in Landscape")  ///Excluding iPads!!!
      self.navigationController?.setNavigationBarHidden(true, animated: true)
      //self.navigationController?.setToolbarHidden(true, animated: true)
      NotificationCenter.default.post(name: .landscapeNotification, object: nil, userInfo: nil)
      self.view.endEditing(true) /// dismiss keyboard if it's showing
      setActiveVideoPlayerFullScreen()
      
    default:
      print("P6 now in Portrait")
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      //self.navigationController?.setToolbarHidden(false, animated: true)
      NotificationCenter.default.post(name: .portraitNotification, object: nil, userInfo: nil)
      setVideoPlayersPortrait()
    }
  }
  
  
  // MARK: - Section 1 - B.E.A.C.O.N.
  func addSection1() {
    speechView1 = SpeechBubbleView(type: .secondary, text: strings.p6_s1_title)
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    
    
    let beaconImage = Style.imageOfBeaconButton
    let beaconButton = UIButton(type: .custom)
    beaconButton.setImage(beaconImage, for: .normal)
    contentView.addSubview(beaconButton)
    beaconButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.centerX.equalTo(contentView)
      
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      //      make.width.equalTo(500)
      //      make.height.equalTo(135)
      make.height.equalTo(beaconButton.snp.width).multipliedBy(135.0/500.0)
      
    }
    lastControlBottom = beaconButton.snp.bottom
    beaconButton.addTarget(self, action: #selector(beaconButtonTapped), for: .touchUpInside)
    
    
  }
  
  
  // MARK: - Section 2 - R.A.F.T
  func addSection2() {
    
    speechView2 = SpeechBubbleView(type: .secondary, text: strings.p6_s2_title)
    contentView.addSubview(speechView2)
    speechView2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView2.snp.bottom
    
    
    let raftButtonImage = Style.imageOfRaftButton
    let raftButton = UIButton(type: .custom)
    raftButton.setImage(raftButtonImage, for: .normal)
    contentView.addSubview(raftButton)
    raftButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.centerX.equalTo(contentView)
      
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      //      make.width.equalTo(500)
      //      make.height.equalTo(135)
      make.height.equalTo(raftButton.snp.width).multipliedBy(135.0/500.0)
      
    }
    lastControlBottom = raftButton.snp.bottom
    raftButton.addTarget(self, action: #selector(raftButtonTapped), for: .touchUpInside)
    
  }
  
  
  // Animate elements in from the bottom of the screen, and set s1TextView as first responder
  func animateInSections() {
    
    speechView1.center.y += view.bounds.height
    speechView2.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [unowned self] in
                    self.speechView1.center.y -= self.view.bounds.height
                    self.speechView2.center.y -= self.view.bounds.height
      }, completion: { [unowned self] _ in
        
        //        if self.isFirstRun {
        //          self.s1TextView.becomeFirstResponder()
        //        }
        
    })
    
  }
  
  
  // MARK: - Button Actions
  
  /*
   // FINISH
   override func nextScreen() {
   
   // Return to Main Menu
   self.navigationController?.popToRootViewController(animated: false)
   
   /*
   // Show the main menu
   let mainMenuScreen = newViewController(withId: "MainMenu")
   navigationController?.pushViewController(mainMenuScreen, animated: false)
   */
   /*
   print("ScrollingForm nextScreen()")
   
   if let parentPageViewController = self.parent as? PlanPageViewController {
   parentPageViewController.goToNextPage()
   }
   */
   
   }
   */
  
  func transcriptButton1Tapped() {
    print("transcriptButton1Tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName1], title: strings[videoTitle1], coverFile: coverFile1)
  }
  
  func transcriptButton2Tapped() {
    print("transcriptButton2Tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName2], title: strings[videoTitle2], coverFile: coverFile2)
  }
  
  func transcriptButton3Tapped() {
    print("transcriptButton3Tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName3], title: strings[videoTitle3], coverFile: coverFile3)
  }
  
  func beaconButtonTapped() {
    print("P6 Beacon Button Tapped")
    showNewBeaconRaftTranscriptView(type: .beacon, markdownString: strings.p6_s1_i1_text)
  }
  
  func raftButtonTapped() {
    print("P6 Raft Button Touched")
    showNewBeaconRaftTranscriptView(type: .raft, markdownString: strings.p6_s2_i1_text)
  }
  
}





