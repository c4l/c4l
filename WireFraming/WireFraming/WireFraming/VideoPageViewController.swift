//
//  VideoPageViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//

/*
 UIViewController subclass to contain and playback a video
 - on rotation to a regular width size class (landscape on iPhone), video goes fullscreen
 - pauses video playback on viewWillDisappear
 
 
 
 */

import UIKit
import AVKit
import AVFoundation
//import IBAnimatable

class VideoPageViewController: UIViewController {
  
  @IBOutlet weak var playButton: AnimatableButton!
  @IBInspectable var videoURL: String = "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
  
  @IBOutlet weak var thumbImageView: UIImageView!
  
  var player: AVPlayer!
  var playerViewController = AVPlayerViewController()
  
  @IBAction func playButtonTap(_ sender: Any) {
    
    /*
    self.present(playerViewController, animated: true) {
      self.playerViewController.player!.play()
    }
    */
    
    if player.rate != 0 && player.error == nil { // currently playing
      pauseVideo()
    } else {
      playVideo()
    }
    
    
  }
  
  func pauseVideo() {
    player.pause()
    playButton.setTitle("Play", for: .normal)
  }
  
  func playVideo() {
    player.play()
    playButton.setTitle("Pause", for: .normal)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //player = AVPlayer(url: URL(string: videoURL)!)
    
    /*
     let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
     let player = AVPlayer(url: videoURL!)
     let playerLayer = AVPlayerLayer(player: player)
     playerLayer.frame = self.view.bounds
     self.view.layer.addSublayer(playerLayer)
     player.play()
     */
    
    playButton.isHidden = true
    
    let filePath = Bundle.main.path(forResource: "big_buck_bunny", ofType: "mp4")
    let videoUrl = URL(fileURLWithPath: filePath!)
    
    player = AVPlayer(url: videoUrl)

    playerViewController.player = player

    
    //let playerLayer = AVPlayerLayer(player: player)
    playerViewController.view.frame = thumbImageView.frame
    
    //playerLayer.frame = thumbImageView.bounds
    //self.thumbImageView.layer.addSublayer(playerLayer)
    
    //self.view.layer.addSublayer(playerLayer)

    self.addChildViewController(playerViewController)
    self.view.addSubview(playerViewController.view)
    playerViewController.didMove(toParentViewController: self)
    
/*
    // Create a thumbnail from the video
    // http://stackoverflow.com/questions/31779150/creating-thumbnail-from-local-video-in-swift
    
    do {


      let asset = AVURLAsset(url: videoUrl , options: nil)
      let imgGenerator = AVAssetImageGenerator(asset: asset)
      imgGenerator.appliesPreferredTrackTransform = true
      let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
      let thumbnail = UIImage(cgImage: cgImage)
      
      thumbImageView.image = thumbnail
      
    } catch let error {
      print("*** Error generating thumbnail: \(error.localizedDescription)")
    }
*/
    
    
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    // called after autolayout has done its thing
    // resize the AVPLayer layer to fit the parent layer size
    
    // iterate through all sublayers on thumbImageview
    // find an AVPlayerLayer, and resize it
    if let sublayers = self.thumbImageView.layer.sublayers{
      for layer in sublayers where layer is AVPlayerLayer{

              layer.frame = self.thumbImageView.bounds

      }
    }

  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    pauseVideo()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    if UIDevice.current.orientation.isLandscape {
      print("Landscape")
    } else {
      print("Portrait")
    }
  }
  
  
  override var shouldAutorotate: Bool {
    return true
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
