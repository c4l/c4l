//
//  URL+open.swift
//  Chats for Life
//
//  Created by Peter Hunt on 12/6/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

/// Open a URL or URL scheme. Support iOS 9 and 10
/// Example: "www.abc.net.au".openUrl()
extension String {
  func openUrl() {
    if let url = URL(string: self) {
        if #available(iOS 10, *) {
          UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
          let _ = UIApplication.shared.openURL(url)
        }
    }
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
