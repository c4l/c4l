//
//  P5ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 5/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka
import AVKit
import AVFoundation


/*
 P5: Planning How to Support
 */


class P5ViewController: MasterFormViewController {
  
  let videoName = "EV2"
  let videoFile = "ev1_1080p.mp4"
  let subtitleFile = "ev1.srt"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Get strings data for this page, and setup the form fields
    configureForm()
    
    // Injectable - load data from the model for this page
    self.loadForm()
  }
  
  
  func configureForm() {

        /*
    if !model.userSettings.isEV2Viewed {
      playVideo()
    }
    */
    
    /*
     <!-- *** Page 5  - Planning how to support *** -->
     <!-- Section 1 - Who are the people who care for &lt;first name&gt; and love them? -->
     <string name="p5_s1_title">Who are the people who care for %s and love them?</string>
     <string name="p5_s1_tag">PeopleWhoCareSection</string>
     <string name="p5_s1_i1_placeholder">Enter some people who care</string>
     <!-- Section 2 - Who else may be able to help and support ? -->
     <string name="p5_s2_title">Who else may be able to help and support %s ?</string>
     <string name="p5_s2_tag">HelpSection</string>
     <string name="p5_s2_i1_text">Select support options</string>
     <string name="p5_s2_i1_1">Family</string>
     <string name="p5_s2_i1_2">Friends</string>
     <string name="p5_s2_i1_3">Community Groups</string>
     <string name="p5_s2_i1_4">Professional Services</string>
     <string name="p5_s2_i1_5">Other</string>
     <!-- Section 3 - What could <first name> look forward to  ? -->
     <string name="p5_s3_title">What could %s look forward to ?</string>
     <string name="p5_s3_tag">LookForwardSection</string>
     <string name="p5_s3_i1_text">Select</string>
     <string name="p5_s3_i1_1">A phone call tomorrow</string>
     <string name="p5_s3_i1_2">Lunch</string>
     <string name="p5_s3_i1_3">Going for a walk</string>
     <string name="p5_s3_i1_4">A nice meal</string>
     <string name="p5_s3_i1_5">Playing sport</string>
     <string name="p5_s3_i1_6">Going to the beach</string>
     <string name="p5_s3_i1_7">Other</string>
     <!-- Section 4 - What are some of <first name> favourite places and experiences ?-->
     <string name="p5_s4_title">What are some of %s favourite places and experiences ?</string>
     <string name="p5_s4_tag">FavouritePlacesSection</string>
     <string name="p5_s4_i1_placeholder">Remind them of the beauty in the world</string>
     <!-- Section 5 - What are some actions you can suggest that %s might take to improve their wellbeing ?-->
     <string name="p5_s5_title">What are some actions you can suggest that %s might take to improve their wellbeing?</string>
     <string name="p5_s5_tag">ActionsSection</string>
     <string name="p5_s5_i1_placeholder">This might be things that they have done in the past that has helped, eg playing sport or exercising, listening to music, watching a movie or going out with friends or family.</string>
     
     */
    // Get data from the model for this page
    
    // ** Section 1 - Who are the people who care for &lt;first name&gt; and love them?
    let p5_s1_title = strings["p5_s1_title"]                     //
    let p5_s1_tag = strings["p5_s1_tag"]                         //
    let p5_s1_i1_placeholder = strings["p5_s1_i1_placeholder"]                       //
    
    //    <!-- Section 2 - Who else may be able to help and support ? -->
    let p5_s2_title = strings["p5_s2_title"]
    let p5_s2_tag = strings["p5_s2_tag"]                         //
    //let p5_s2_i1_text = strings["p5_s2_i1_text"]                       //
    let p5_s2_i1_1 = strings["p5_s2_i1_1"]                             //
    let p5_s2_i1_2 = strings["p5_s2_i1_2"]                             //
    let p5_s2_i1_3 = strings["p5_s2_i1_3"]                             //
    let p5_s2_i1_4 = strings["p5_s2_i1_4"]                             //
    let p5_s2_i1_5 = strings["p5_s2_i1_5"]                             //
    
    //  <!-- Section 3 - What could <first name> look forward to  ? -->
    let p5_s3_title = strings["p5_s3_title"]
    let p5_s3_tag = strings["p5_s3_tag"]                         //
    //let p5_s3_i1_text = strings["p5_s3_i1_text"]                       //
    let p5_s3_i1_1 = strings["p5_s3_i1_1"]                             //
    let p5_s3_i1_2 = strings["p5_s3_i1_2"]                             //
    let p5_s3_i1_3 = strings["p5_s3_i1_3"]                             //
    let p5_s3_i1_4 = strings["p5_s3_i1_4"]                             //
    let p5_s3_i1_5 = strings["p5_s3_i1_5"]                             //
    let p5_s3_i1_6 = strings["p5_s3_i1_6"]                             //
    let p5_s3_i1_7 = strings["p5_s3_i1_7"]                             //
    
    //  <!-- Section 4 - What are some of <first name> favourite places and experiences ?-->
    let p5_s4_title = strings["p5_s4_title"]                     //
    let p5_s4_tag = strings["p5_s4_tag"]                         //
    let p5_s4_i1_placeholder = strings["p5_s4_i1_placeholder"]                       //
    
    //**    <!-- Section 5 - What are some actions you can suggest that %s might take to improve their wellbeing ?-->
    let p5_s5_title = strings["p5_s5_title"]
    let p5_s5_tag = strings["p5_s5_tag"]                         //
    let p5_s5_i1_placeholder = strings["p5_s5_i1_placeholder"]                  //
    
    
    /*
    form +++ CustomSection()
      <<< ButtonRow("Explainer Video") { (row: ButtonRow) -> Void in
        row.title = "Explainer Video"
        }.onCellSelection { [weak self] (cell, row) in
          //self?.playVideo()
    }
    */
    form +++ CustomVideoSection() { section in
      section.tag = videoName
      (section as? CustomVideoSection)?.configure(videoName: videoName, videoFile: videoFile, subtitleFile: subtitleFile)
    }
    
    
    // ** Section 1
    form +++ CustomSection(model.replaceTokens(inString: p5_s1_title)) { //model.replaceTokens(inString: p5_s1_title)) {
      $0.tag = p5_s1_tag
    }
      
    <<< TextAreaRow("p5_s1_i1") {
        $0.placeholder = p5_s1_i1_placeholder
        $0.textAreaHeight = .dynamic(initialTextViewHeight: 40)
    }
    
    // ** Section 2
    form +++ SelectableSection<ListCheckRow<String>>(model.replaceTokens(inString: p5_s2_title), selectionType: .multipleSelection) {
      $0.tag = p5_s2_tag
    }
      <<< ListCheckRow<String>("p5_s2_i1_1"){ listRow in
        listRow.title = p5_s2_i1_1
        listRow.selectableValue = p5_s2_i1_1
        listRow.value = nil
      }
      
      <<< ListCheckRow<String>("p5_s2_i1_2"){ listRow in
        listRow.title = p5_s2_i1_2
        listRow.selectableValue = p5_s2_i1_2
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s2_i1_3"){ listRow in
        listRow.title = p5_s2_i1_3
        listRow.selectableValue = p5_s2_i1_3
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s2_i1_4"){ listRow in
        listRow.title = p5_s2_i1_4
        listRow.selectableValue = p5_s2_i1_4
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s2_i1_5"){ listRow in
        listRow.title = p5_s2_i1_5
        listRow.selectableValue = p5_s2_i1_5
        listRow.value = nil
    }
    
    
    // ** Section 3
    form +++ SelectableSection<ListCheckRow<String>>(model.replaceTokens(inString: p5_s3_title), selectionType: .multipleSelection) {
      $0.tag = p5_s3_tag
      }
      <<< ListCheckRow<String>("p5_s3_i1_1"){ listRow in
        listRow.title = p5_s3_i1_1
        listRow.selectableValue = p5_s3_i1_1
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s3_i1_2"){ listRow in
        listRow.title = p5_s3_i1_2
        listRow.selectableValue = p5_s3_i1_2
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s3_i1_3"){ listRow in
        listRow.title = p5_s3_i1_3
        listRow.selectableValue = p5_s3_i1_3
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s3_i1_4"){ listRow in
        listRow.title = p5_s3_i1_4
        listRow.selectableValue = p5_s3_i1_4
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s3_i1_5"){ listRow in
        listRow.title = p5_s3_i1_5
        listRow.selectableValue = p5_s3_i1_5
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s3_i1_6"){ listRow in
        listRow.title = p5_s3_i1_6
        listRow.selectableValue = p5_s3_i1_6
        listRow.value = nil
      }
      <<< ListCheckRow<String>("p5_s3_i1_7"){ listRow in
        listRow.title = p5_s3_i1_7
        listRow.selectableValue = p5_s3_i1_7
        listRow.value = nil
    }
    
    
    
    
    // ** Section 4
    form +++ CustomSection(model.replaceTokens(inString: p5_s4_title)) {
      $0.tag = p5_s4_tag
      }
      <<< TextAreaRow("p5_s4_i1") {
        $0.placeholder = p5_s4_i1_placeholder
        $0.textAreaHeight = .dynamic(initialTextViewHeight: 40)
    }
    
    // ** Section 5
    form +++ CustomSection(model.replaceTokens(inString: p5_s5_title)) {
      $0.tag = p5_s5_tag
    }
    <<< TextAreaRow("p5_s5_i1") {
      $0.placeholder = p5_s5_i1_placeholder
      $0.textAreaHeight = .dynamic(initialTextViewHeight: 40)
    }
    
    // ** Final Section - Next button
    form +++ Section("") {
      $0.hidden = false
      $0.tag = "NextButtonSection"
    }
      
    <<< ButtonRow("Next") { (row: ButtonRow) -> Void in
     row.title = "Next"
    }.onCellSelection { [weak self] (cell, row) in
     row.value = "submitted"
      self?.saveForm()
      // go to next page
      NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": 2])
    }
    
  }
  

  
  
}



extension P5ViewController {
  
  /*
  
  func playVideo() {
    let playerViewController = AVPlayerViewController()
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    let player = AVPlayer(url: videoUrl)
    playerViewController.player = player
    
    self.present(playerViewController, animated: true) {
      playerViewController.player!.play()
      self.model.userSettings.isEV1Viewed = true
    }
  }
*/
  
}

