
import UIKit
import SmileLock

class PasscodeModel {
  static var passcode: String?   // saved passcode hash

  class func match(_ password: String) -> PasscodeModel? {
    guard (password + Const.PasscodeSalt).sha256() == passcode else { return nil } // check that hash matches
    return PasscodeModel()
  }
  
  class func setNew(_ password: String) -> PasscodeModel? {
    passcode = (password + Const.PasscodeSalt).sha256()        //  save the hash of the new password
    return PasscodeModel()
  }
  
}


class PasscodeUIValidation: PasswordUIValidation<PasscodeModel> {
  
  init(in stackView: UIStackView, setNewPasscode: Bool = false, currentPasscode: String? = nil) {
    super.init(in: stackView, digit: 4)
    
    PasscodeModel.passcode = currentPasscode
    validation = { password in
      setNewPasscode ? PasscodeModel.setNew(password) : PasscodeModel.match(password)
    }
    
  }
  
  // handle Touch ID
  override func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
    if success {
      let dummyModel = PasscodeModel()
      self.success?(dummyModel)
    } else {
      passwordContainerView.clearInput()
    }
  }
  
  
}

