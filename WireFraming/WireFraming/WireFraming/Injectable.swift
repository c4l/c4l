//
//  Injectable.swift
//  WireFraming
//
//  Created by Peter Hunt on 13/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
//import Eureka

// Protocol for view controllers that require dependency injection
// identifier gives us a unique key for persistence

protocol Injectable {
  //var identifier: String! { get }
  var strings: StringsModel! { get set }  // should be defined as weak in conforming type
  var model: DataModel! { get set }       // should be defined as weak in conforming type
  
  mutating func setDependencies(strings: StringsModel, model: DataModel)
}

extension Injectable {
  /// Instantiate a new view controller from the Main storyboard, and inject the dependencies
  ///
  /// - Parameter identifier: Storyboard ID of VC to instantiate, also used as key for persistence
  /// - Returns: New view controller, with dependencies injected
  func newViewController(withId identifier: String) -> UIViewController {
    let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    
    if newVC is Injectable {
      var injectableVC = newVC as! Injectable
      injectableVC.setDependencies(strings: strings, model: model)
      return injectableVC as! UIViewController
    }
    return newVC
    
  }
  
}

/**
// Protocol extension for Eureka form view controllers
// Default implementations to support Saving and Loading form data from the model
extension Injectable where Self: FormViewController {
  
  // Default implementation for forms to save page data to model
  func saveForm() {
    // Get the value of all rows which have a Tag assigned
    // The dictionary contains the 'rowTag':value pairs.
    let formData = form.values()  // [String : Any?]
    var saveData: [String : Any] = [:]
    
    // Remove nils
    for (k, v) in formData {
      if let value = v {
        saveData[k] = value
      }
    }
    
    // only save if there is data to save
    if saveData.count > 0 {
      
      model.markComplete(page: identifier)  // mark this page as complete
      
      // save it, with the page identifier as the key (eg. "P4")
      if model.save(page: identifier, withData: saveData) {
        print("** Saved form:  \(identifier) **")
        //print(saveData)
      }
    }
    
  }
  
  
  // Default implementation for forms to load page data from model
  func loadForm() {
    
    if let newValues = model.get(page: identifier) {
      form.setValues(newValues)
      print("** Loaded form: \(identifier) **")
      //print(newValues)
    }
    
  }
  
}

**/

// Protocol extension for view controllers
// Default implementations to support presenting a passcode controller modally
extension Injectable where Self: UIViewController {
  
  func presentModalPasscodeController(setNewPasscode: Bool = false, completion: (() -> Void)?) {
    if let passcodeVC = newViewController(withId: "BlurPasscodeViewController") as? BlurPasscodeViewController {
      passcodeVC.setNewPasscode = setNewPasscode
      passcodeVC.modalPresentationStyle = .overCurrentContext
      passcodeVC.modalTransitionStyle = .coverVertical
      self.present(passcodeVC, animated: true, completion: completion)
      
    }
  }
  
}

