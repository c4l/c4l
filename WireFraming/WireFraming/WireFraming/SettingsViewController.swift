//
//  SettingsViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 20/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka
import EventKit
import UserNotifications


class SettingsViewController: MasterFormViewController, EventAccess {
  //var identifier = "Settings"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
   
    // save form if app goes to the background
    NotificationCenter.default.addObserver(self, selector: #selector(viewWillDisappear(_:)), name: Notification.Name.UIApplicationWillResignActive, object: nil)

/*
    // Request access to the Event Store for reminders
    requestReminderAccess() { allowed in
      if !allowed {
        print("Reminder access not granted")
      }
    }
  */
    
    // Request authorisation for User Notifications
    if #available(iOS 10.0, *) {
      requestNotificationsAccess(options: [.alert, .badge, .sound]) { allowed in
        if !allowed {
          print("Notifications access not granted")
        }
      }
    } else {
      // Fallback on earlier versions
    }
    
    
/* Check if notification access has been granted
     
     center.getNotificationSettings { (settings) in
 if settings.authorizationStatus != .authorized {
 // Notifications not allowed
 }
 }
 */
 
    form +++ CustomSection("Preferences") {
      $0.tag = "PreferencesSection"
    }
    
    <<< SwitchRow() {
      $0.title = "Audio"
      $0.value = model.userSettings.isAudioEnabled
    }.onChange { [weak self] row in
      if let value = row.value {
        self?.model.userSettings.isAudioEnabled = value
      }
    }

    <<< SwitchRow() {
      $0.title = "Subtitles"
      $0.value = model.userSettings.isSubtitlesEnabled
    }.onChange { [weak self] row in
      if let value = row.value {
        self?.model.userSettings.isSubtitlesEnabled = value
      }
    }

      /*
    <<< SwitchRow() {
      $0.title = "Notifications"
      $0.value = model.userSettings.isNotificationsEnabled
    }.onChange { [weak self] row in
      if let value = row.value {
        self?.model.userSettings.isNotificationsEnabled = value
          
        if value == true { // Request authorisation for User Notifications
          self?.requestNotificationsAccess(options: [.alert, .badge, .sound]) { [weak self] allowed in
            if !allowed {
              print("Notifications access not granted")
              self?.model.userSettings.isNotificationsEnabled = false
            } else {
              self?.model.userSettings.isNotificationsEnabled = true
            }
          }
        }
        
      }
    }
      */
    <<< SwitchRow() {
      $0.title = "Passcode / Touch ID"
      $0.value = model.userSettings.isPasscodeEnabled
    }.onChange { [weak self] row in
      if let value = row.value {
        self?.model.userSettings.isPasscodeEnabled = value
        
        if value == true { // Prompt to set a new passcode
          print("Set new passcode")
          self?.presentModalPasscodeController(setNewPasscode: true) { [weak self] in
            self?.model.isUnlocked = true
          }
        }
    }
      
  }
  
    /*
    <<< SwitchRow() {
      $0.title = "Hide icon reminder badges"
      $0.value = model.userSettings.isIconBadgeHidden
    }.onChange { [weak self] row in
      if let value = row.value {
        self?.model.userSettings.isIconBadgeHidden = value
      }
    }
*/
    
      
      
    +++ Section("")

      // ** DEBUGGING  - add button to reset all data **
      <<< ButtonRow("Reset") { (row: ButtonRow) -> Void in
        row.title = "Reset all Data"
        row.tag = "Reset"
        }.onCellSelection { [weak self] (cell, row) in
          self?.model.reset()
          
          let alert = UIAlertController(title: "Reset", message: "All app data has been reset", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
          self?.present(alert, animated: true, completion: nil)
      }
      
      
    <<< ButtonRow("OK") { (row: ButtonRow) -> Void in
      row.title = "OK"
      row.tag = "OK"
    }.onCellSelection { [weak self] (cell, row) in
      
//      if self?.presentingViewController?.presentedViewController == self {  // presented modally
      if self?.presentingViewController?.presentedViewController is UINavigationController {  // presented modally
        self?.dismiss(animated: true, completion: nil)
      } else {
       self?.performSegue(withIdentifier: "MainMenuSegue", sender: nil)
      }
    }
    
  }
  
  // Dependency injection for the segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "MainMenuSegue" {
      print("MainMenu Segue")
      
      // inject dependencies
      let mainMenuTableViewController = segue.destination as! MainMenuTableViewController
      mainMenuTableViewController.setDependencies(identifier: "MainMenu", strings: strings, model: model)
    }
    
    
  }
  
  
  
  // Save the form when you swipe to the next screen
  override func viewWillDisappear(_ animated: Bool) {
    model.userSettings.saveToDefaults()
  }
  
  /*
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }
  
  override var shouldAutorotate: Bool {
    return false
  }
  */
  
  
}

