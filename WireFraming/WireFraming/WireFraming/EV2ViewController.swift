//
//  ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
/*
 
 
 From superclass ...
 @IBInspectable var fileName: String!
 var playerViewController: AVPlayerViewController?
 
 */

import UIKit
import AVKit
import AVFoundation

class EV2ViewController: VideoViewController {
  
  @IBOutlet weak var containerView: UIView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let playerVC = playerViewController {
      // add playerViewController as child, and constrain to containerView
      self.addChildViewController(playerVC)
      containerView.addSubview(playerVC.view)
      playerVC.view.bindFrameToSuperviewBounds()
      playerVC.didMove(toParentViewController: self)
    }
  }


}
