//
//  PageViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//

/*
 UIViewController subclass to contain text and support text entry
 
 
 */

import UIKit
import Eureka

class HomeViewController: UIViewController {
  
// Instantiate a new StringsModel
// var strings = StringsModel()
  
  
  @IBAction func startButtonTap(_ sender: Any) {
    print("startButtonTap")
    NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": 1])
  }
  
  @IBAction func finishButtonTap(_ sender: Any) {
    print("finishButtonTap")
    NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": 0])
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Style.backgroundColor
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }
  
  override var shouldAutorotate: Bool {
    return false
  }
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
