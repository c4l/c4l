//
//  P6.swift
//  WireFraming
//
//  Created by Peter Hunt on 24/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

import SnapKit
//import BMPlayer


/*
 P6 Scrolling Form
 
 Video (EV4)
 
 Skip/Next Button

 Back/Next Button
 
 */

class P6: ScrollingForm {
  
  /*
   1. It's about your friend
   2. Take time to listen
   3. Be patient
   4. Don't be loud
   5. Don't judge
   6. Be interested
   7. Don't interrupt
   8. Ask questions
   9. Be honest
   */
  
  let videoName = "EV4"
  let videoFile = "EV4.mp4"
  let subtitleFile = "EV4.srt"
  let coverFile = "ev4cover.jpg"
  let videoTitle = "ev4_title"

  var isFirstRun: Bool = false
  var speechView1, speechView2, speechView3, speechView4 : SpeechBubbleView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    print("P6 viewDidLoad")
    
    addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: strings[videoTitle], subtitleFile: subtitleFile, coverFile: coverFile, withSkipButton: false, withTranscriptButton: true)

    addSection1()
    
    addBackNextButtons()
    
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    if !model.userSettings.hasEV4Autoplayed {
      player?.play()
      model.userSettings.hasEV4Autoplayed = true
    }
  }
  
  deinit {
    print("P6 deinit")
    if let player = player {
      player.prepareToDealloc()
    }
  }
  
  /*
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("P6 viewWillDisappear")

    if let player = player {
      player.pause()
    }
  }
  */
  
  func addSection1() {
    speechView1 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.p6_s1_title))
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(10)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    
    //speechView2 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p6_s1_i1_text))
    speechView2 = SpeechBubbleView(frame: CGRect(x: 0, y: 0, width: contentView.frame.width, height: 250))
    speechView2.bubbleType = .secondary
    speechView2.speechLabel.text = model.replaceTokens(inString: strings.p6_s1_i1_text)
    contentView.addSubview(speechView2)

    //speechView2.speechLabel.font = UIFont.customBody
    speechView2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView2.snp.width).multipliedBy(165.0/500.0)
    }
//    speechView2.speechLabel.sizeToFit()
    lastControlBottom = speechView2.snp.bottom
    
    speechView3 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p6_s1_i2_text))
    contentView.addSubview(speechView3)
    //speechView3.speechLabel.font = UIFont.customBody
    //speechView3.speechLabel.frame = CGRect(x: 2, y: 15, width: speechView3.frame.width - 4, height: speechView3.frame.height - 30)
    speechView3.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView3.snp.width).multipliedBy(165.0/500.0)
    }
//    speechView3.speechLabel.sizeToFit()
    lastControlBottom = speechView3.snp.bottom
    
    speechView4 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p6_s1_i3_text))
    contentView.addSubview(speechView4)
    //speechView4.speechLabel.frame = CGRect(x: 5, y: 15, width: speechView4.frame.width - 10, height: speechView4.frame.height - 50)
    speechView4.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView4.snp.width).multipliedBy(165.0/500.0)
    }
//    speechView4.speechLabel.sizeToFit()
    lastControlBottom = speechView4.snp.bottom

  }
  
  
  // MARK: - Button Actions
  override func transcriptButtonTapped() {
    print("P6 Transcript button tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName], title: strings[videoTitle], coverFile: coverFile)
  }
  
}
