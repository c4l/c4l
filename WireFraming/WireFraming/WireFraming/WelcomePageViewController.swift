//
//  WelcomePageViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 27/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit

class WelcomePageViewController: UIPageViewController, Injectable {
  var identifier: String!
  var strings: StringsModel!
  var model: DataModel!
  
  func setDependencies(identifier: String, strings: StringsModel, model: DataModel) {
    self.identifier = identifier
    self.strings = strings
    self.model = model
  }
  
  // Set of UIViewControllers from the Welcome storyboard
  private(set) lazy var orderedViewControllers: [UIViewController] = {
    return [
      self.newViewController(withId: "EV1"),
      self.newViewController(withId: "Settings")
    ]
  }()
  
  private func viewControllerFor(page: Int) -> UIViewController? {
    guard page >= 0, page < orderedViewControllers.count else { return nil }
    return orderedViewControllers[page]
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    dataSource = self
    delegate = self
    
    
    // load the first view controller
    if let firstViewController = viewControllerFor(page: 0) {
      setViewControllers([firstViewController],
                         direction: .forward,
                         animated: true,
                         completion: nil)
    }
    
    /*
        // Add settings button to the toolbar
    let settingsImage = #imageLiteral(resourceName: "settings.png")
    let settingsButton = UIBarButtonItem(image: settingsImage, style: .plain, target: self, action: #selector(settingsButtonTapped))
    //self.navigationItem.setLeftBarButton(leftItem, animated: false)
    
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    
    //let settingsButton = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(settingsButtonTapped))
    
    /*
     let settingsIcon = UIImage(named: "settings")
     let settingsButton = UIBarButtonItem(image: settingsIcon, style: .plain, target: self, action: #selector(settingsButtonTapped))
     */
    
    toolbarItems = [settingsButton, spacer]
*/
    UIApplication.shared.isStatusBarHidden = false
    
    /*
    if let toolbar = self.navigationController?.toolbar {
      //toolbar.barTintColor = Style.highlightColor
      //toolbar.tintColor = Style.tertiaryColor
      
      let progressImage = Style.imageOfToolbarProgress.stretchableImage(withLeftCapWidth: 2, topCapHeight: 0)
      
      toolbar.setBackgroundImage(progressImage, forToolbarPosition: .any, barMetrics: .default)
    }
    */

    
    /*
    let progressImage = Style.imageOfToolbarProgress
    let progressImageView = UIImageView(image: progressImage)
    progressImageView.contentMode = .scaleAspectFit
    
    self.view.addSubview(progressImageView)
    //player.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    
    progressImageView.snp.makeConstraints { (make) in
      make.bottom.equalTo(self.view)
      make.left.right.equalTo(self.view)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
      //make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
      make.height.equalTo(60)
    }
*/
    
  }
  
  func settingsButtonTapped() {
    
  }
  
  override var prefersStatusBarHidden: Bool {
    return false
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    let orientation = newCollection.verticalSizeClass
    
    switch orientation {
    case .compact:
      print("WelcomePageViewController now in Landscape")  ///Excluding iPads!!!
      self.navigationController?.setNavigationBarHidden(true, animated: true)
      self.navigationController?.setToolbarHidden(true, animated: true)
     
      UIPageControl.appearance().backgroundColor = .clear
      
    default:
      print("WelcomePageViewController now in Portrait")
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      self.navigationController?.setToolbarHidden(false, animated: true)
//      UIPageControl.appearance().pageControl.alpha = 1
      
    }
    
  }

}


// MARK: UIPageViewControllerDataSource
extension WelcomePageViewController : UIPageViewControllerDataSource {
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else { return nil }
    
    let previousIndex = viewControllerIndex - 1
    
    guard previousIndex >= 0 else { return nil }
    
    guard orderedViewControllers.count > previousIndex else {
      return nil
    }
    
    return orderedViewControllers[previousIndex]
  }
  
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
      return nil
    }
    
    let nextIndex = viewControllerIndex + 1
    let orderedViewControllersCount = orderedViewControllers.count
    
    guard orderedViewControllersCount != nextIndex else {
      return nil
    }
    
    guard orderedViewControllersCount > nextIndex else {
      return nil
    }
    
    return orderedViewControllers[nextIndex]
  }
  
  /*
   // Implementing these methods displays a UIPageControl 
   
  func presentationCount(for pageViewController: UIPageViewController) -> Int {
    return orderedViewControllers.count
  }
  
  
  func presentationIndex(for pageViewController: UIPageViewController) -> Int {
    guard let firstViewController = viewControllers?.first,
      let firstViewControllerIndex = orderedViewControllers.index(of: firstViewController) else {
        return 0
    }
    return firstViewControllerIndex
  }
  */
  
  func goToNextPage() {
    guard let currentViewController = self.viewControllers?.first else { return }
    guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
    
    setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
  }
  
  
  func goToPreviousPage() {
    guard let currentViewController = self.viewControllers?.first else { return }
    guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
    setViewControllers([previousViewController], direction: .reverse, animated: true, completion: nil)
  }

  
}

// MARK: UIPageViewControllerDelegate
extension WelcomePageViewController : UIPageViewControllerDelegate {

  // Set the navigation bar title for this page
  
  func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    
    if let vc = self.viewControllers?[0] {
        self.navigationItem.title = vc.navigationItem.title
    }
  }
  
}
