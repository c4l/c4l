//
//  P4ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 5/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka
import EventKit
import AVFoundation
import AVKit

/*
 P4: Planning Who to Support
 */


class P4ViewController: MasterFormViewController, EventAccess {

  let videoName = "EV2"
  let videoFile = "ev1_1080p.mp4"
  let subtitleFile = "ev1.srt"
  
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Get strings data for this page, and setup the form fields
    configureForm()
    
    // Injectable - load data from the model for this page
    self.loadForm()
    
  }
  
  
  
  func configureForm() {

    /*
    if !model.userSettings.isEV1Viewed {
      playVideo()
    }
    */
    
    // is this a new plan ?
    let isNewPlan = self.model.getName().isEmpty
    print("isNewPlan: \(isNewPlan)")
    
    // ** Section 1
    let p4_s1_title = strings["p4_s1_title"]                     // "Who would you like to support ?"
    let p4_s1_tag = strings["p4_s1_tag"]                         // "SupportSection"
    let p4_s1_i1_placeholder = strings["p4_s1_i1_placeholder"]         // "Name"
    let p4_s1_i2_title = strings["p4_s1_i2_title"]                     // "Save"
    
    // ** Section 2
    let p4_s2_title = strings["p4_s2_title"]                     // "What has been going on to make you concerned about %s ?"
    let p4_s2_tag = strings["p4_s2_tag"]                         // "ConcernedSection"
    let p4_s2_i1_placeholder = strings["p4_s2_i1_placeholder"]         // ""Enter what has been happening"
    
    // ** Section 3
    let p4_s3_title = strings["p4_s3_title"]                     // "How might %s react?"
    let p4_s3_tag = strings["p4_s3_tag"]                         // ReactionSection
    let p4_s3_i1_1 = strings["p4_s3_i1_1"]                             // Thankful
    let p4_s3_i1_2 = strings["p4_s3_i1_2"]                             // Angry
    let p4_s3_i1_3 = strings["p4_s3_i1_3"]                             // Ashamed
    let p4_s3_i1_4 = strings["p4_s3_i1_4"]                             // Surprised
    let p4_s3_i1_5 = strings["p4_s3_i1_5"]                             // Upset
    
    // ** Section 4
    let p4_s4_title = strings["p4_s4_title"]                      // Where should you and <first name> talk?
    let p4_s4_tag = strings["p4_s4_tag"]                          // WhereSection
    let p4_s4_i1_placeholder = strings["p4_s4_i1_placeholder"]          // Choose somewhere safe for you and them. Maybe a regular meeting spot?
    
    // ** Section 5
    let p4_s5_title = strings["p4_s5_title"]                      // When should you and %s talk?
    let p4_s5_tag = strings["p4_s5_tag"]                          // WhenSection
    let p4_s5_i1_placeholder = strings["p4_s5_i1_placeholder"]
    let p4_s5_i1_footer = strings["p4_s5_i1_footer"]              // Choose a time of day when they will be at their best and open to a conversation. Avoid times when they may have used alcohol or other substances
    
    // create a function to hide the following sections until 'Save' has been tapped
    let isHiddenCondition : Condition? = .function([p4_s1_i2_title], { form -> Bool in
      //return !self.isSaved(tag: p4_s1_i2_title)      // only show this section if Save has been tapped
      if isNewPlan && !self.isSaved(tag: p4_s1_i2_title) {
        return true
      } else {
        return false
      }
      
    })
    
    // a block to refresh the section headers with the new name, when the name is saved (Save button tapped)
    let refreshHeaders = {
      for (tag, title) in [(p4_s2_tag, p4_s2_title), (p4_s3_tag, p4_s3_title), (p4_s4_tag, p4_s4_title), (p4_s5_tag, p4_s5_title)] {
        if let section = self.form.sectionBy(tag: tag) {
          let newTitle = self.model.replaceTokens(inString: title)
          section.header = HeaderFooterView(title: newTitle)
          section.reload()
        }
        
      }
    }
    
    /* Custom Page Header
     form +++ Section("P4. Planning Who to Support") { section in
     var header = HeaderFooterView<UILabel>(.class)
     header.height = { 100.0 }
     header.onSetupView = {view, _ in
     view.textColor = Style.primaryColor
     view.backgroundColor = Style.backgroundColor
     view.text = "P4. Planning Who to Support"
     view.font = .customFont(forStyle: .headline)
     }
     section.header = header
     }
     */
    
    
    
    
    // ** Section 1
    /*
     form +++ Section(p4_s1_title) {
     $0.tag = p4_s1_tag
     }
     */
  
    form +++ CustomVideoSection() { section in
      section.tag = videoName
      (section as? CustomVideoSection)?.configure(videoName: videoName, videoFile: videoFile, subtitleFile: subtitleFile)
    }
    
    /*
    form +++ CustomSection()
      <<< ButtonRow("Explainer Video") { (row: ButtonRow) -> Void in
        row.title = "Explainer Video"
        }.onCellSelection { [weak self] (cell, row) in
          self?.playVideo()
      }
      
       */
      
      /*
      +++ CustomSection("") {
        $0.tag = "1"
      }
      
      +++ CustomSection("") {
        $0.tag = "2"
      }
      */
      
      +++ CustomSection(p4_s1_title) {
        $0.tag = p4_s1_tag
      }
      
      
      <<< NameRow() {
        $0.placeholder = p4_s1_i1_placeholder
        $0.add(rule: RuleMaxLength(maxLength: 12))   // restrict length to 12 chars
        $0.add(rule: RuleRequired())
        $0.validationOptions = .validatesOnChange
        if !isNewPlan {
          $0.value = model.getName()
        }
      }.onChange { row in
        if let rowValue = row.value {
          self.model.setName(rowValue)
        }
        }.cellUpdate { cell, row in  // validate length
          if !row.isValid {
            cell.titleLabel?.textColor = Style.highlightColor
          }
        }.onRowValidationChanged { cell, row in
          let rowIndex = row.indexPath!.row
          while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
            row.section?.remove(at: rowIndex + 1)
          }
          if !row.isValid {
            for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
              let labelRow = LabelRow() {
                $0.title = "Enter a name between 1 and 12 characters"
              
                //cell.titleLabel?.font = UIFont.customBody
                //cell.titleLabel?.textColor = Style.highlightColor
                
                $0.cell.height = { 30 }
              }
              row.section?.insert(labelRow, at: row.indexPath!.row + index + 1)
            }
          }
      }
      
      
      
      <<< ButtonRow(p4_s1_i2_title) { (row: ButtonRow) -> Void in
        row.title = p4_s1_i2_title
        row.tag = p4_s1_i2_title
        if !isNewPlan { row.hidden = true }
      }.onCellSelection { [weak self] (cell, row) in
        if !(self?.model.getName().isEmpty ?? false) {
          row.value = "true"
          cell.isHidden = true
            
          // Get data from the model for this page
          //self?.loadForm()
            
          refreshHeaders()
        }
      }
    
    
    // ** Section 2
    form +++ CustomSection(model.replaceTokens(inString: p4_s2_title)) {
      $0.tag = p4_s2_tag
      $0.hidden = isHiddenCondition
      }
      
      <<< TextAreaRow("p4_s2_i1") {
        $0.placeholder = p4_s2_i1_placeholder
        $0.textAreaHeight = .dynamic(initialTextViewHeight: 40)
    }
    
    // ** Section 3 - "How might %s react?"
    //    form +++ SelectableSection<ImageCheckRow<String>>(model.replaceTokens(inString:p4_s3_title), selectionType: .multipleSelection) {
    
    form +++ SelectableSection<ImageCheckRow<String>>(model.replaceTokens(inString:p4_s3_title), selectionType: .multipleSelection) { section in
      section.header = {
        // Custom Header
        var header = HeaderFooterView<AutoSizeLabelView>(.class)
        header.height = { UITableViewAutomaticDimension }
        header.onSetupView = { v, s in
          v.label.text = self.model.replaceTokens(inString:p4_s3_title)
          v.backgroundColor = Style.backgroundColor
        }
        return header
      }()
      
      section.tag = p4_s3_tag
      section.hidden = isHiddenCondition
      
      }
      
      <<< ImageCheckRow<String>("p4_s3_i1_1"){ listRow in
        listRow.tag = "p4_s3_i1_1"
        listRow.title = p4_s3_i1_1
        listRow.selectableValue = p4_s3_i1_1
        listRow.value = nil
      }
      <<< ImageCheckRow<String>("p4_s3_i1_2"){ listRow in
        listRow.tag = "p4_s3_i1_2"
        listRow.title = p4_s3_i1_2
        listRow.selectableValue = p4_s3_i1_2
        listRow.value = nil
      }
      
      <<< ImageCheckRow<String>("p4_s3_i1_3"){ listRow in
        listRow.tag = "p4_s3_i1_3"
        listRow.title = p4_s3_i1_3
        listRow.selectableValue = p4_s3_i1_3
        listRow.value = nil
      }
      
      <<< ImageCheckRow<String>("p4_s3_i1_4"){ listRow in
        listRow.tag = "p4_s3_i1_4"
        listRow.title = p4_s3_i1_4
        listRow.selectableValue = p4_s3_i1_4
        listRow.value = nil
      }
      
      <<< ImageCheckRow<String>("p4_s3_i1_5"){ listRow in
        listRow.tag = "p4_s3_i1_5"
        listRow.title = p4_s3_i1_5
        listRow.selectableValue = p4_s3_i1_5
        listRow.value = nil
    }
    
    
    
    // ** Section 4
    form +++ CustomSection(model.replaceTokens(inString: p4_s4_title)) {
      $0.tag = p4_s4_tag
      $0.hidden = isHiddenCondition
      }
      
      <<< TextAreaRow("p4_s4_i1") {
        $0.placeholder = p4_s4_i1_placeholder
        $0.tag = "p4_s4_i1"
        $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
    }
    
    // ** Section 5 - When ?
    form +++ Section(header: model.replaceTokens(inString: p4_s5_title),
                     footer:p4_s5_i1_footer) {
                      $0.tag = p4_s5_tag
                      $0.hidden = isHiddenCondition
      }
      
      <<< DateTimeInlineRow("p4_s5_i1") {
        $0.title = p4_s5_i1_placeholder
        $0.tag = p4_s5_tag
        //      $0.value = Date()
      }.onChange { [weak self] row in
        // Save changes to model
        if let conversationDate = row.value {
          self?.model.plan?.conversationDate = conversationDate
        }
      }
      
      +++ SwitchRow("Add to calendar") {
        $0.hidden = isHiddenCondition
        $0.title = $0.tag
      }.onChange { [weak self] row in
          
          // Schedule event in the user's calendar
            if row.value ?? false {
              // on
              let dateRow: DateTimeInlineRow? = self?.form.rowBy(tag: p4_s5_tag)
              if let scheduleDate = dateRow?.value {
                
                print("Attempting to schedule conversation for \(scheduleDate)")
                if let conversationEventId = self?.addConversationEvent(startDate: scheduleDate, title: "Conversation with \((self?.model.getName())!)") {
                // save the conversation eventId
                  print("Returned conversation event Id: \(conversationEventId)")
                  
                  //TODO: Toast that it's been added to their calendar
                } else {
                  // error during saving event
                  row.value = false
                  row.updateCell()
                  print("Error scheduling conversation event")
                }
                
               
                
                // Schedule a local notification
                print("Attempting to schedule a local notification for \(scheduleDate)")
                if let conversationNotificationId = self?.scheduleLocalNotification(forDate: scheduleDate, title: "Conversation with \((self?.model.getName())!)",
                  body: "Conversations for Life Notification") {
                  // save the conversation eventId
                  print("Returned notification event Id: \(conversationNotificationId)")
                }
                
                
              }
              
              
            } else {
              // off
            }
    }
    
    
    
    
    // ** Final Section - Next button
    form +++ Section() {
      $0.hidden = isHiddenCondition
    }
      
      <<< ButtonRow("Next") { (row: ButtonRow) -> Void in
        row.title = "Next"
        }.onCellSelection { [weak self] (cell, row) in
          row.value = "submitted"
          //cell.isHidden = true
          
          //self?.saveForm()
          
          
          
          let dateRow: DateTimeInlineRow? = self?.form.rowBy(tag: p4_s5_tag)
          if let scheduleDate = dateRow?.value {
            
            
            //*** Add to Reminders when 'Next' is tapped ***
            // Schedule a reminder
            let eventStore = EKEventStore()
            eventStore.requestAccess(to: EKEntityType.reminder, completion: { (granted, error) in
              if !granted {
                print("Access to store not granted")
              } else {
                
                let reminder = EKReminder(eventStore: eventStore)
                reminder.title = (self?.model.replaceTokens(inString: "Have a conversation with %s"))!
                reminder.calendar = eventStore.defaultCalendarForNewReminders()
                
                let alarm = EKAlarm(absoluteDate: scheduleDate)
                reminder.addAlarm(alarm)
                
                do {
                  try eventStore.save(reminder, commit: true)
                } catch let error {
                  print("Reminder failed with error \(error.localizedDescription)")
                }
              }
            })
            
        
            
            
          }
          
          self?.saveForm()
          // go to next page
          NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": 1])
    }
    
    
    if !isNewPlan {
      // Get data from the model for this page
      self.loadForm()
      //refreshHeaders()
    }
    
    
    
  }
  
  
  // If any value exists, return true
  func isSaved(tag: String) -> Bool {
    let row: RowOf<String>! = form.rowBy(tag: tag)
    if let _ = row.value { return true }
    return false
  }
  
  /*
  func multipleSelectorDone(_ item:UIBarButtonItem) {
    _ = navigationController?.popViewController(animated: true)
  }
  
  
  func playVideo() {
    
    let playerViewController = AVPlayerViewController()
    // setup AVPlayer
    // let filePath = Bundle.main.path(forResource: "big_buck_bunny", ofType: "mp4")
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    let player = AVPlayer(url: videoUrl)
    playerViewController.player = player
    
    /*
     self.addChildViewController(playerViewController)
     self.view.addSubview(playerViewController.view)
     playerViewController.didMove(toParentViewController: self)
     */
    //playerViewController.modalPresentationStyle = .overFullScreen
    //self.present(playerViewController, animated: true, completion: nil)
    
    self.present(playerViewController, animated: true) {
      playerViewController.player!.play()
      self.model.userSettings.isEV1Viewed = true
    }
    
  }
  
  // Save the form when you swipe to the next screen
  override func viewWillDisappear(_ animated: Bool) {
    saveForm()
  }
  */
  
  
  
}

