//
//  MainMenuCell.swift
//  WireFraming
//
//  Created by Peter Hunt on 29/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

class StartNewPlanCell : UITableViewCell {
  
  // Cache the highlight color
  static let cardBackgroundHighlightColor = Style.cardBackgroundColor.shadow(withLevel: 0.25).blended(withFraction: 0.20, of: Style.primaryColor)
  
  
  @IBOutlet weak var backgroundCardView: UIView!
  
  @IBOutlet weak var startNewPlanLabel: UILabel!
  @IBOutlet weak var startNewPlanImageView: UIImageView!
  @IBOutlet weak var disclosureImageView: UIImageView!
  
  var newPlanButton: UIButton!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  
    
    // Customise the cell colours and set a corner radius on the backgroundCardView to make the cell look like a 'card'

    contentView.backgroundColor = Style.backgroundColor     // larger cell background
    
    //backgroundCardView.backgroundColor = Style.backgroundColor // card background
    backgroundCardView.backgroundColor = Style.cardBackgroundColor // card background
    backgroundCardView.layer.cornerRadius = 6.0
    backgroundCardView.layer.masksToBounds = false
   
    
    startNewPlanLabel.textColor = Style.primaryColor
    startNewPlanLabel.font = UIFont.customTitle1
    startNewPlanLabel.text = "New Chat"
    
    startNewPlanImageView.setIcon(icon: .googleMaterialDesign(.personAdd), textColor: Style.primaryColor, backgroundColor: .clear, size: nil)
    
    disclosureImageView.setIcon(icon: .googleMaterialDesign(.chevronRight), textColor: Style.primaryColor, backgroundColor: .clear, size: nil)
    
//    startNewPlanImageView.image = UIImage.init(icon: .emoji(.airplane), size: CGSize(width: 35, height: 35), textColor: .red)
    // Image
//    startNewPlanImageView.image = Style.imageOfStartNewPlan
    /*
    // Button
    newPlanButton = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
    contentView.addSubview(newPlanButton)
 */
    /*
    newPlanButton.snp.makeConstraints { (make) in
      make.center.equalTo(contentView.snp.center)
      make.width.equalTo(160)
      make.height.equalTo(50)
    }
*/

    
//    newPlanButton.setIcon(icon: .emoji(.ferrisWheel), title: "New Plan", color: Style.inverseTextColor, forState: .normal)
    
//    newPlanButton.setIcon(prefixText: "", icon: .googleMaterialDesign(.print), postfixText: " New Plan", forState: .normal)
    
    //    newPlanButton.setIcon(icon: .emoji(.ferrisWheel), iconColor: Style.inverseTextColor, title: "New Plan", titleColor: Style.inverseTextColor, font: UIFont.customSubheadline, backgroundColor: Style.primaryColor, borderSize: 6.0, borderColor: Style.primaryColor, forState: .normal)

    /*
    newPlanButton.setIcon(prefixText: "", prefixTextFont: UIFont.customSubheadline, prefixTextColor: Style.inverseTextColor,
                          icon: .googleMaterialDesign(.personAdd), iconColor: Style.inverseTextColor,
                          postfixText: "  NEW PLAN", postfixTextFont: UIFont.customSubheadline, postfixTextColor: Style.inverseTextColor,
                          forState: .normal, iconSize: 30)
 */
  /*
    newPlanButton.titleLabel?.setIcon(prefixText: "", prefixTextFont: UIFont.customSubheadline, prefixTextColor: Style.inverseTextColor,
                                      icon: .mapicons(.bicycling), iconColor: Style.inverseTextColor, postfixText: " New Plan", postfixTextFont: UIFont.customSubheadline, postfixTextColor: Style.inverseTextColor, iconSize: 30)
    
  */
    /*
    newPlanButton.setIcon(icon: .weather(.rainMix), iconColor: Style.inverseTextColor, title: "NEW PLAN", titleColor: Style.inverseTextColor, font: UIFont.customSubheadline, backgroundColor: .clear, borderSize: 1, borderColor: Style.primaryColor, forState: .normal)
    */
    //newPlanButton.contentVerticalAlignment = .fill
//    newPlanButton.titleLabel?.textAlignment = .center
    
  }

  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    if selected {
      backgroundCardView.backgroundColor = StartNewPlanCell.cardBackgroundHighlightColor
    } else {
      backgroundCardView.backgroundColor = Style.cardBackgroundColor
    }
    
  }
 
  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    if highlighted {
      backgroundCardView.backgroundColor = StartNewPlanCell.cardBackgroundHighlightColor
    } else {
      backgroundCardView.backgroundColor = Style.cardBackgroundColor
    }
  }
  
  
  func newPlan() {
    
  }
  
}
