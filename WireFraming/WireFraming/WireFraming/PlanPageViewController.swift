//
//  AppPageViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//


/*
 
 UIPageViewController subclass for paging through multiple view controllers (UIViewControllers).
 - Responds to .changePageNotification (payload in notification.userInfo), to trigger a page change
 
 */


import UIKit

extension Notification.Name {
  static let changePageNotification = Notification.Name("ChangePageNotification")
  static let portraitNotification = Notification.Name("PortraitNotification")
  static let landscapeNotification = Notification.Name("LandscapeNotification")
}


class PlanPageViewController: UIPageViewController, Injectable {
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  var homeToolbarButton, settingsToolbarButton: UIButton?
  
  // Storyboard ID and Restoration ID of ViewControllers
  var pages: [String] = ["P4", "P5", "P6", "P7", "P8"]
  var initialDefaultPage: Int = 0   // default page to open to, when instantiated
  
  
  /*
  // Set of UIViewControllers from the Main storyboard
  private(set) lazy var orderedViewControllers: [UIViewController] = {
    return [
      self.newViewController(withId: "P4Scroll"),
      self.newViewController(withId: "P5Scroll"),
      self.newViewController(withId: "P6Scroll"),
      //self.newViewController(withId: "P4"),
      //self.newViewController(withId: "EV3"),
      //self.newViewController(withId: "P5"),
//      self.newViewController(withId: "EV4"),
//      self.newViewController(withId: "EV5"),
      //self.newViewController(withId: "P6"),
//      self.newViewController(withId: "EV6"),
      //self.newViewController(withId: "P7"),
      
      self.newViewController(withId: "Summary"),
      //self.newViewController(withId: "Followup")
      
    ]
  }()
  */
  
  
  func viewControllerFor(page: Int) -> UIViewController? {
    /*
    guard page >= 0, page < orderedViewControllers.count else { return nil }
    return orderedViewControllers[page]
 */
    guard page >= 0, page < pages.count else { return nil }
    return newViewController(withId: pages[page])
  }
  
  
  /// Return the page no. of the specified view controller
  ///
  /// - Parameter viewController: view controller
  /// - Returns: page no.
  func pageForViewController(viewController: UIViewController) -> Int? {
    guard let identifier = viewController.restorationIdentifier else {
      return nil
    }
    return pages.index(of: identifier)
  }
  

  override func viewDidLoad() {
    super.viewDidLoad()
    
    dataSource = self
    delegate = self
    
    
    // Add home icon to the navigation bar top left
    let homeImage = #imageLiteral(resourceName: "home.png")
    let leftItem = UIBarButtonItem(image: homeImage, style: .plain, target: self, action: #selector(homeButtonTapped))
    self.navigationItem.setLeftBarButton(leftItem, animated: false)

    
    // Add settings button to the navigation bar top right
    //let settingsImage = #imageLiteral(resourceName: "settings.png")
    //let settingsButton = UIBarButtonItem(image: settingsImage, style: .plain, target: self, action: #selector(settingsButtonTapped))
    //self.navigationItem.rightBarButtonItem = settingsButton
     //self.navigationController?.setNavigationBarHidden(true, animated: true)
    
    let crisisImage = UIImage(named: "crisis")
    let crisisButton = UIBarButtonItem(image: crisisImage, style: .plain, target: self, action: #selector(crisisButtonTapped))
    navigationItem.rightBarButtonItem = crisisButton
    
    self.navigationController?.setToolbarHidden(true, animated: true)
    
    /* Add Followup Page ...
    if let isFollowup = model.plan?.isFollowup, isFollowup {
      pages.append("Followup")
    }
    */
    
    
    /*
    // Add progress bar to the toolbar
    self.navigationController?.toolbarItems = nil
  
    
    if let toolbar = self.navigationController?.toolbar {
  
      toolbar.isTranslucent = true
      /*
      toolbar.isTranslucent = false
      
      let progressImage = Style.imageOfToolbarProgress //.stretchableImage(withLeftCapWidth: 2, topCapHeight: 0)
      
      toolbar.setBackgroundImage(progressImage, forToolbarPosition: .any, barMetrics: .default)
 */
      toolbar.setItems([leftItem], animated: false)
    }
*/
    
    
    // load the first view controller
    if let defaultViewController = viewControllerFor(page: initialDefaultPage) {
      setViewControllers([defaultViewController],
                         direction: .forward,
                         animated: true,
                         completion: nil)
    }
    
    
    // Register to listen for a change page notification, and change the page
    // To post a change page notification:
    // NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page":3])
    NotificationCenter.default.addObserver(forName: .changePageNotification, object: nil, queue: OperationQueue.main) { [weak self] notification in
      guard let userInfo = notification.userInfo,
        let page = userInfo["page"] as? Int else {
          print("No userInfo found in notification")
          return
      }
      
      self?.gotoPage(index: page)
    }

    NotificationCenter.default.addObserver(forName: .landscapeNotification, object: nil, queue: OperationQueue.main) { [weak self] notification in
      guard let weakSelf = self else { return }
      weakSelf.disableSwipeGesture()
      for subView in weakSelf.view.subviews {
        if subView is UIPageControl {
          subView.isHidden = true
        }
      }

    }

    NotificationCenter.default.addObserver(forName: .portraitNotification, object: nil, queue: OperationQueue.main) { [weak self] notification in
      guard let weakSelf = self else { return }
      for subView in weakSelf.view.subviews {
        if subView is UIPageControl {
          subView.isHidden = false
        }
      }
      weakSelf.enableSwipeGesture()
    }
    
    // set the correct navigation bar title for the starting page
    resetNavigationBarTitle()

  }
  
  override func viewDidLayoutSubviews() {
    //corrects scrollview frame to allow for full-screen view controller pages
    // so UIPageControl toolbar can appears translucent
    super.viewDidLayoutSubviews()
    
    for subView in self.view.subviews {
      
      if subView is UIScrollView {
        subView.frame = self.view.bounds
      
      } else if subView is UIPageControl {
 /*
        // If homeToolbarButton is already set up, exit
        if let _ = homeToolbarButton {
          return
        }
        
        print("UIPageControl found")
        
        // Add home button to the toolbar
        homeToolbarButton = UIButton(type: .custom)
        guard let button1 = homeToolbarButton else { return }
        button1.setImage(#imageLiteral(resourceName: "home.png"), for: .normal)
        button1.addTarget(self, action: #selector(returnToMainMenu), for: .touchUpInside)
        subView.addSubview(button1)
        button1.snp.makeConstraints { (make) in
          make.top.equalTo(subView).offset(2)
          make.left.equalTo(subView).inset(Const.HorizontalPadding)
          make.height.width.equalTo(34)
        }
        
        // Add settings button to the toolbar
        settingsToolbarButton = UIButton(type: .custom)
        settingsToolbarButton?.setImage(#imageLiteral(resourceName: "settings.png"), for: .normal)
        guard let button2 = settingsToolbarButton  else { return }
        button2.addTarget(self, action: #selector(settingsButtonTapped), for: .touchUpInside)
        subView.addSubview(button2)
        button2.snp.makeConstraints { (make) in
          make.top.equalTo(subView).offset(2)
          make.right.equalTo(subView).inset(Const.HorizontalPadding)
          make.height.width.equalTo(34)
        }
        */
      }
    }

  }
  
  
  func disableSwipeGesture() {
    for view in self.view.subviews {
      if let subView = view as? UIScrollView {
        subView.isScrollEnabled = false
      }
    }
  }
  
  func enableSwipeGesture() {
    for view in self.view.subviews {
      if let subView = view as? UIScrollView {
        subView.isScrollEnabled = true
      }
    }
  }
  
  
  // Present Settings View Controller modally
  // with a navigation bar
  func settingsButtonTapped() {
    
    // Pause any video playing
    if let currentViewController = self.viewControllers?.first {
      if currentViewController is ScrollingForm {
        (currentViewController as? ScrollingForm)?.player?.pause()
      }
    }
    
    let settingsVC = newViewController(withId: "About")
    let settingsNavController = UINavigationController(rootViewController: settingsVC)
    // Creating a navigation controller with settingsVC at the root of the navigation stack.
    settingsNavController.modalTransitionStyle = .coverVertical
    settingsNavController.modalPresentationStyle = .overCurrentContext
    settingsVC.title = "Resources"
    present(settingsNavController, animated: true, completion: nil)
  }
  
  // Present Crisis View Controller modally
  // with a navigation bar
  @objc func crisisButtonTapped() {
    
    // Pause any video playing
    if let currentViewController = self.viewControllers?.first {
      if currentViewController is ScrollingForm {
        (currentViewController as? ScrollingForm)?.player?.pause()
      }
    }

    let contactsController = ContactsTableViewController(style: .plain)
    contactsController.setDependencies(strings: strings, model: model)
    let crisisNavController = UINavigationController(rootViewController: contactsController)
    crisisNavController.modalTransitionStyle = .coverVertical
    crisisNavController.modalPresentationStyle = .overCurrentContext
    //    settingsVC.title = "Resources"
    present(crisisNavController, animated: true, completion: nil)
  }

  
  deinit {
    print("PlanPageViewController deinit()")
    NotificationCenter.default.removeObserver(self, name: .changePageNotification, object: nil)
  }
  
  
  func gotoPage(index: Int) {
    //guard index >= 0, index < orderedViewControllers.count else { return }
    //let newVC = orderedViewControllers[index]
    
    if let newVC = viewControllerFor(page: index) {
      setViewControllers([newVC],
                         direction: .forward,
                         animated: true,
                         completion: { [weak self] _ in self?.resetNavigationBarTitle() })
    }
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  /*
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    /*
     if let current = self.viewControllers, current[0] is VideoPageViewController {
     return UIInterfaceOrientationMask.allButUpsideDown
     }
     */
    return UIInterfaceOrientationMask.portrait
  }
  
  override var shouldAutorotate: Bool {
    /*
     if let current = self.viewControllers, current[0] is VideoPageViewController {
     return true
     }
     */
    return false
  }
  */
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

// MARK: UIPageViewControllerDataSource
extension PlanPageViewController : UIPageViewControllerDataSource {
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    /*
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else { return nil }
    
    let previousIndex = viewControllerIndex - 1
    
    guard previousIndex >= 0 else { return nil }
    
    guard orderedViewControllers.count > previousIndex else {
      return nil
    }
    
    return orderedViewControllers[previousIndex]
    */
    
    guard let currentPage = pageForViewController(viewController: viewController) else { return nil
    }
    let previousPage = currentPage - 1
    guard previousPage >= 0 else { return nil }
    return viewControllerFor(page: previousPage)
  }
  
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    
    guard let currentPage = pageForViewController(viewController: viewController) else { return nil
    }
    let nextPage = currentPage + 1
    guard nextPage < pages.count else { return nil }
    return viewControllerFor(page: nextPage)
    
    /*
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
      return nil
    }
    
    let nextIndex = viewControllerIndex + 1
    let orderedViewControllersCount = orderedViewControllers.count
    
    guard orderedViewControllersCount != nextIndex else {
      return nil
    }
    
    guard orderedViewControllersCount > nextIndex else {
      return nil
    }
    
    return orderedViewControllers[nextIndex]
 */
  }
  
  func goToNextPage() {
    guard let currentViewController = self.viewControllers?.first else { return }
    guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
    
    setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
    resetNavigationBarTitle()
  }
  
  
  func goToPreviousPage() {
    guard let currentViewController = self.viewControllers?.first else { return }
    guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
    setViewControllers([previousViewController], direction: .reverse, animated: true, completion: nil)
    resetNavigationBarTitle()
  }
  
  
  func presentationCount(for pageViewController: UIPageViewController) -> Int {
//    return orderedViewControllers.count
    return pages.count
  }
  
  
  func presentationIndex(for pageViewController: UIPageViewController) -> Int {
  
    guard let firstViewController = viewControllers?.first,
      let firstViewControllerIndex = pageForViewController(viewController: firstViewController) else {
        return 0
    }
    return firstViewControllerIndex
  }
  
  
  
}


// MARK: UIPageViewControllerDelegate
extension PlanPageViewController : UIPageViewControllerDelegate {
  
  @objc func homeButtonTapped() {
    print("PlanPageViewController homeButtonTapped")
    //self.navigationController?.popToRootViewController(animated: true)
    self.navigationController?.popViewController(animated: true)
  }
  
  
  // Set the navigation bar title for this page
  func resetNavigationBarTitle() {
    if let vc = self.viewControllers?[0] {
      if let currentPage = pageForViewController(viewController: vc) {
        self.navigationItem.title = (vc.navigationItem.title ?? "") + " | \(currentPage+1) of \(pages.count)"
      } else {
        self.navigationItem.title = vc.navigationItem.title ?? ""
      }
    }
    
  }

  
  func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    
    resetNavigationBarTitle()
    
  }
  
}

