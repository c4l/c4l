//  BMPlayerCustomControlView.swift
//  BMPlayer
//
//  Created by BrikerMan on 2017/4/4.
//  Copyright © 2017年 CocoaPods. All rights reserved.
//

import UIKit
import BMPlayer

class BMPlayerCustomControlView: BMPlayerControlView {
  
  //var playbackRateButton = UIButton(type: .custom)
  //var playRate: Float = 1.0
  
  var playTimeUIProgressView = UIProgressView()
  var playingStateLabel = UILabel()
  
  /**
   Override if need to customize UI components
   */
  override func customizeUIComponents() {
    mainMaskView.backgroundColor   = UIColor.clear
    topMaskView.backgroundColor    = UIColor.black.withAlphaComponent(0.1)
    bottomMaskView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
    //timeSlider.setThumbImage(UIImage(named: "custom_slider_thumb"), for: .normal)
    
    /*   Add a custom playback rate button to the UI
     topMaskView.addSubview(playbackRateButton)
     
     playbackRateButton.layer.cornerRadius = 2
     playbackRateButton.layer.borderWidth  = 1
     playbackRateButton.layer.borderColor  = UIColor ( red: 1.0, green: 1.0, blue: 1.0, alpha: 0.8 ).cgColor
     playbackRateButton.setTitleColor(UIColor ( red: 1.0, green: 1.0, blue: 1.0, alpha: 0.9 ), for: .normal)
     playbackRateButton.setTitle("  rate \(playRate)  ", for: .normal)
     playbackRateButton.addTarget(self, action: #selector(onPlaybackRateButtonPressed), for: .touchUpInside)
     playbackRateButton.titleLabel?.font   = UIFont.systemFont(ofSize: 12)
     playbackRateButton.isHidden = true
     playbackRateButton.snp.makeConstraints {
     $0.right.equalTo(chooseDefitionView.snp.left).offset(-5)
     $0.centerY.equalTo(chooseDefitionView)
     }
     */
    playButton.removeFromSuperview()
    currentTimeLabel.removeFromSuperview()
    totalTimeLabel.removeFromSuperview()
    timeSlider.removeFromSuperview()
    fullscreenButton.removeFromSuperview()
    
    // If needs to change position remake the constraint
    progressView.snp.remakeConstraints { (make) in
      make.bottom.left.right.equalTo(bottomMaskView)
      make.height.equalTo(4)
    }
    
    // Add new items and constraint
    bottomMaskView.addSubview(playTimeUIProgressView)
    playTimeUIProgressView.snp.makeConstraints { (make) in
      make.bottom.left.right.equalTo(bottomMaskView)
      make.height.equalTo(4)
    }
    
    playTimeUIProgressView.tintColor      = Style.primaryColor
    playTimeUIProgressView.trackTintColor = UIColor.clear
    
    topMaskView.addSubview(playingStateLabel)
    playingStateLabel.snp.makeConstraints {
      $0.top.equalTo(topMaskView).offset(20)
      $0.right.equalTo(topMaskView).offset(-20)
      //$0.left.equalTo(self).offset(10)
      //$0.bottom.equalTo(self).offset(-10)
    }
    
    playingStateLabel.backgroundColor = .clear // UIColor.black.withAlphaComponent(0.3)
    playingStateLabel.textColor = UIColor.white
    playingStateLabel.font = UIFont.customBody
    
  }
  
  
  override func updateUI(_ isForFullScreen: Bool) {
    super.updateUI(isForFullScreen)
    chooseDefitionView.isHidden = true

    topMaskView.isHidden = false

    backButton.isHidden = !isForFullScreen // show on Fullscreen
    titleLabel.isHidden = !isForFullScreen
    
    
    //playbackRateButton.isHidden = !isForFullScreen
  }
  
  override func controlViewAnimation(isShow: Bool) {
    UIView.animate(withDuration: 0.24, animations: {
      self.topMaskView.snp.remakeConstraints {
        $0.top.equalTo(self.mainMaskView).offset(isShow ? 0 : -65)
        $0.left.right.equalTo(self.mainMaskView)
        $0.height.equalTo(65)
      }
    })
    /*
    self.isMaskShowing = isShow
    //UIApplication.shared.setStatusBarHidden(!isShow, with: .fade)
    
    UIView.animate(withDuration: 0.24, animations: {
      self.topMaskView.snp.remakeConstraints {
        $0.top.equalTo(self.mainMaskView).offset(isShow ? 0 : -65)
        $0.left.right.equalTo(self.mainMaskView)
        $0.height.equalTo(65)
      }
      
      self.bottomMaskView.snp.remakeConstraints {
        $0.bottom.equalTo(self.mainMaskView).offset(isShow ? 0 : 50)
        $0.left.right.equalTo(self.mainMaskView)
        $0.height.equalTo(50)
      }
      self.layoutIfNeeded()
    }) { (_) in
      //self.autoFadeOutControlViewWithAnimation()
    }
 */
  }
  
  override func onTapGestureTapped(_ gesture: UITapGestureRecognizer) {
    print("BMPlayercustomControlView tapped")
    controlViewAnimation(isShow: true)
    // redirect tap action to play button action
    delegate?.controlView(controlView: self, didPressButton: playButton)
  }
  
  override func playTimeDidChange(currentTime: TimeInterval, totalTime: TimeInterval) {
    super.playTimeDidChange(currentTime: currentTime, totalTime: totalTime)
    playTimeUIProgressView.setProgress(Float(currentTime/totalTime), animated: true)
  }

  override func playStateDidChange(isPlaying: Bool) {
    super.playStateDidChange(isPlaying: isPlaying)
    playingStateLabel.text = isPlaying ? "Playing" : "Paused"
  }
  
  @objc func onPlaybackRateButtonPressed() {
    /*
     autoFadeOutControlViewWithAnimation()
     switch playRate {
     case 1.0:
     playRate = 1.5
     case 1.5:
     playRate = 0.5
     case 0.5:
     playRate = 1.0
     default:
     playRate = 1.0
     }
     playbackRateButton.setTitle("  rate \(playRate)  ", for: .normal)
     delegate?.controlView?(controlView: self, didChangeVideoPlaybackRate: playRate)
     */
  }
}
