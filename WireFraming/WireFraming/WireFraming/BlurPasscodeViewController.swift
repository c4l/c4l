/*

 View Controller for validating and setting a passcode (and Touch ID)
 Saves passcode to model.usersettings.passcode
 Uses SmileLock library
 
 */

import UIKit
import SmileLock

class BlurPasscodeViewController: UIViewController, Injectable {

  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  @IBOutlet weak var passcodeStackView: UIStackView!
  @IBOutlet weak var enterPasscodeLabel: UILabel!
  
  //MARK: Property
  var passcodeUIValidation: PasscodeUIValidation!
  var setNewPasscode: Bool = false   // set true to set a new passcode
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let currentPasscode = model.userSettings.passcode
    
    // trying to validate a nil password, just exit
    if !setNewPasscode && currentPasscode == nil {
      self.dismiss(animated: true, completion: nil)
    }
    
    passcodeUIValidation = PasscodeUIValidation(in: passcodeStackView, setNewPasscode: setNewPasscode, currentPasscode: currentPasscode)
    
    if setNewPasscode {
      enterPasscodeLabel.text = "Enter New Passcode"
      passcodeUIValidation.view.touchAuthenticationEnabled = false  // disable Touch ID when setting a new passcode
    }
    
    passcodeUIValidation.success = { [weak self] passcodeModel in
      print("Passcode *️⃣ success \(PasscodeModel.passcode ?? "")")
      
      if (self?.setNewPasscode)! {
        // Save it
        self?.model.userSettings.passcode = PasscodeModel.passcode   // store the passcode hash
        self?.model.userSettings.saveToDefaults()
      }
      
      self?.dismiss(animated: true, completion: nil)
    }
    
    passcodeUIValidation.failure = { //_ in
      //do not forget add [weak self] if the view controller become nil at some point during its lifetime
      print("Passcode *️⃣ failure")
    }
    
    //visual effect password UI
    passcodeUIValidation.view.rearrangeForVisualEffectView(in: self)
    passcodeUIValidation.view.deleteButtonLocalizedTitle = "Delete"
  }
}
