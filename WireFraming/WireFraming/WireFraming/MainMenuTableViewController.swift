//
//  MainMenuTableViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 13/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit
import DateHelper
import BEMCheckBox
import BRYXBanner
import SwiftIcons

/*
 Main Menu Table View:
 - List available plans
 - Add new plan
 - Settings screen
 */


class MainMenuTableViewController: UITableViewController, Injectable, EventAccess {
  
  let mainMenuCellId = "MainMenuCell"
  let startNewPlanCellId = "StartNewPlanCell"
  
  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  var sortedPlans: [Plan] {  // Show plans in reverse order to how they were created
    get {
      return model.plans.reversed()
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if !model.isUnlocked && model.userSettings.isPasscodeEnabled {
      print("Passcode enabled")
      presentModalPasscodeController { [weak self] in
        self?.model.isUnlocked = true
        self?.tableView.reloadData()
      }
    }
    
    /*
     // Transparent navigation bar
     navigationController?.setNavigationBarHidden(false, animated: true)
     navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
     navigationController?.navigationBar.shadowImage = UIImage()
     navigationController?.navigationBar.isTranslucent = true
     navigationController?.view.backgroundColor = .clear
     */
    /*
     //    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
     //    imageView.contentMode = .scaleToFill
     // 4
     //    let image = UIImage(named: "Icon-40")
     let image = UIImage(named: "Icon-40")!.resizableImage(withCapInsets: UIEdgeInsets.zero)
     
     //    imageView.image = Style.imageOfAppLogo
     //UINavigationBar.appearance().setBackgroundImage(
     //    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"navbarimg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
     
     navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
     //navigationController?.navigationItem.titleView = imageView
     */
    
    
    view.backgroundColor = Style.backgroundColor
    
    // dynamic table view cell height
    tableView.estimatedRowHeight = tableView.rowHeight
    tableView.rowHeight = UITableView.automaticDimension
    tableView.separatorStyle = .none
    
    // Register to listen for a model change notification, and refresh the table view data
    NotificationCenter.default.addObserver(forName: .modelChangeNotification, object: nil, queue: OperationQueue.main) { [weak self] notification in
      print("Model Change Notification - reloading main menu data")
      self?.tableView.reloadData()
      
      // Check if this is the first time that a chat has been completed, then show a banner telling them to follow-up
      if let bannerShown = self?.model.userSettings.hasChatCompleteBannerShown, !bannerShown {
        if let plans = self?.sortedPlans {
          for plan in plans {
            if let _ = plan.chatDate, plan.hadChat {
              DispatchQueue.main.async {
                let banner = Banner(title: "Well done for having a chat with your friend.",
                                    subtitle: "Consider scheduling a follow-up to see how they're going.",
                                    image: nil, backgroundColor: Style.primaryColor)
                banner.textColor = Style.inverseTextColor
                banner.titleLabel.font = UIFont.customHeadline
                banner.detailLabel.font = UIFont.customSubheadline
                banner.dismissesOnTap = true
                banner.position = .top
                banner.show(duration: Animation.BannerDuration * 3)
                self?.model.userSettings.hasChatCompleteBannerShown = true
                self?.model.userSettings.saveToDefaults()
              }
            }
          }
        }
      }
      
    } // NotificationCenter.default.addObserver...
    
    
    // Uncomment the following line to preserve selection between presentations
    //self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    
    //self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MainMenuCell")
    /*
     for plan in model.plans {
     print(plan.name)
     }
     */
    
    // Add settings button to the toolbar
    let settingsImage = UIImage(named: "settings")
    let settingsButton = UIBarButtonItem(image: settingsImage, style: .plain, target: self, action: #selector(settingsButtonTapped))
    
    let crisisImage = UIImage(named: "crisis")
    let crisisButton = UIBarButtonItem(image: crisisImage, style: .plain, target: self, action: #selector(crisisButtonTapped))
  
    navigationItem.leftBarButtonItem = settingsButton
    navigationItem.rightBarButtonItem = crisisButton
//    navigationItem.rightBarButtonItems = [crisisButton, settingsButton]

//     navigationItem.rightBarButtonItem = settingsButton
    //self.navigationItem.setLeftBarButton(leftItem, animated: false)
    // let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    //let settingsButton = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(settingsButtonTapped))
    // toolbarItems = [settingsButton, spacer]
    

    self.navigationController?.isToolbarHidden = true
    
  }
  
  // Present Settings View Controller modally
  // with a navigation bar
  @objc func settingsButtonTapped() {
    
    //    let settingsVC = newViewController(withId: "Settings")
    let settingsVC = newViewController(withId: "About")
    let settingsNavController = UINavigationController(rootViewController: settingsVC)
    // Creating a navigation controller with settingsVC at the root of the navigation stack.
    
    settingsNavController.modalTransitionStyle = .coverVertical
    settingsNavController.modalPresentationStyle = .overCurrentContext
    settingsVC.title = "Resources"
    present(settingsNavController, animated: true, completion: nil)
  }

  // Present Crisis View Controller modally
  // with a navigation bar
  @objc func crisisButtonTapped() {
    print("crisisButtonTapped")
    let contactsController = ContactsTableViewController(style: .plain)
    contactsController.setDependencies(strings: strings, model: model)
    let crisisNavController = UINavigationController(rootViewController: contactsController)
    crisisNavController.modalTransitionStyle = .coverVertical
    crisisNavController.modalPresentationStyle = .overCurrentContext
//    settingsVC.title = "Resources"
    present(crisisNavController, animated: true, completion: nil)
  }

  
  override func viewWillAppear(_ animated: Bool) {
    
    tableView.reloadData()
    
    if model.userSettings.hasTapTheCircleBannerShown { // only show once
      return
    }
    
    // The first time that a chat has been scheduled, show a banner telling them to tap when complete
    for plan in sortedPlans {
      if let _ = plan.chatDate, !plan.hadChat {
        DispatchQueue.main.async {
          let banner = Banner(title: "OK, you've scheduled a chat.",
                              subtitle: "Tap the circle when you've had a chat with your friend.",
                              image: nil, backgroundColor: Style.primaryColor)
          banner.textColor = Style.inverseTextColor
          banner.titleLabel.font = UIFont.customHeadline
          banner.detailLabel.font = UIFont.customSubheadline
          banner.dismissesOnTap = true
          banner.position = .top
          banner.show(duration: Animation.BannerDuration * 3)
        }
        model.userSettings.hasTapTheCircleBannerShown = true
        model.userSettings.saveToDefaults()
        return
      }
    }
    
    
    
  }
  
  
  override var prefersStatusBarHidden: Bool {
    return false
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  // MARK: - Navigation
  
  /*
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   
   if segue.identifier == "SelectPlan" || segue.identifier == "NewPlan" {
   // set the current plan or create a new one
   model.plan = planAt(indexPath: self.tableView.indexPathForSelectedRow!) ?? model.newPlan()
   
   print("SelectPlan Segue for \(model.plan?.name ?? "new plan")")
   
   // inject dependencies
   let planPageViewController = segue.destination as! PlanPageViewController
   planPageViewController.setDependencies(identifier: "PlanPageViewController", strings: strings, model: model)
   
   //self.navigationController?.setToolbarHidden(true, animated: true)
   tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
   
   }
   
   
   }
   */
  
  
  /*
   // Override to support conditional editing of the table view.
   override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
   // Return false if you do not want the specified item to be editable.
   return true
   }
   */
  
  
  /*
   // Override to support rearranging the table view.
   override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
   
   }
   */
  
  /*
   // Override to support conditional rearranging of the table view.
   override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
   // Return false if you do not want the item to be re-orderable.
   return true
   }
   */
  
  
}


// MARK: UITableViewDataSource
extension MainMenuTableViewController {
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return model.plans.count + 1  // additional 1 for "Start new plan"
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if indexPath.row == 0 { //model.plans.count {  // first row - show "Start new plan" cell
      let cell = tableView.dequeueReusableCell(withIdentifier: startNewPlanCellId, for: indexPath) as! StartNewPlanCell
      return cell
    }
    
    // Otherwise, show a main menu cell, and populate it
    let cell = tableView.dequeueReusableCell(withIdentifier: mainMenuCellId, for: indexPath) as! MainMenuCell
    
    
    // Other rows - configure the cell with the plan name...
    guard let plan = planAt(indexPath: indexPath) else {
      return cell
    }
    
    cell.model = model
    cell.plan = plan
    cell.configure()
    
    cell.scheduleCheckbox?.delegate = self
    cell.scheduleCheckbox?.tag = indexPath.row
    
    cell.scheduleButton?.addTarget(self, action: #selector(scheduleButtonPressed(_:)), for: .touchUpInside)
    cell.scheduleButton?.tag = indexPath.row
    
    
    /*
     let ratioComplete = plan.percentComplete
     let percentComplete = ratioComplete * 100
     /*
     if percentComplete > 0 {
     cell.percentLabel.text = "\(String(format: "%.0f", percentComplete))% complete"
     } else {
     cell.percentLabel.isHidden = true
     }
     */
     cell.progressRatio = CGFloat(ratioComplete)
     */
    
    
    return cell
  }
  
  
  @objc func scheduleButtonPressed(_ sender: AnyObject) {
    guard let button = sender as? UIButton else { return }
    let row = button.tag
    let indexPath = IndexPath(row: row, section: 0)
    guard let plan = planAt(indexPath: indexPath) else { return }
    
    model.plan = plan
    
    
    print(row)
    
    guard let _ = plan.chatDate else {  // No chat date - schedule chat
      // go straight to PlanPageViewController, page 0
      let planPageViewController = newViewController(withId: "PlanPageViewController")
      // Set the initial page to open to
      if let planVC = (planPageViewController as? PlanPageViewController) {
        planVC.initialDefaultPage = 0
      }
      self.navigationController?.pushViewController(planPageViewController, animated: true)
      
      return
    }
    
    
    // Schedule followup
    let followupVC = newViewController(withId: "Followup")
    //    followupVC.modalTransitionStyle = .coverVertical
    //    followupVC.modalPresentationStyle = .overCurrentContext
    
    let followupNavController = UINavigationController(rootViewController: followupVC)
    // Creating a navigation controller with followupVC at the root of the navigation stack.
    followupNavController.modalTransitionStyle = .coverVertical
    followupNavController.modalPresentationStyle = .overCurrentContext
    
    present(followupNavController, animated: true, completion: nil)
    
    
  }
  
  
  /// Helper method to retrieve the plan for an indexPath
  ///
  /// - Parameter indexPath: indexPath of the tableview
  /// - Returns: plan to show (nil if no plan for that indexpath)
  func planAt(indexPath: IndexPath) -> Plan? {
    let row = indexPath.row
    if row >= 1 && row <= model.plans.count {
      //      return model.plans[row-1]
      return sortedPlans[row-1]
    } else {
      return nil
    }
  }
  
}


// MARK: UITableViewDelegate
extension MainMenuTableViewController {
  
  /*
   
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   
   if segue.identifier == "SelectPlan" || segue.identifier == "NewPlan" {
   // set the current plan or create a new one
   model.plan = planAt(indexPath: self.tableView.indexPathForSelectedRow!) ?? model.newPlan()
   
   print("SelectPlan Segue for \(model.plan?.name ?? "new plan")")
   
   // inject dependencies
   let planPageViewController = segue.destination as! PlanPageViewController
   planPageViewController.setDependencies(identifier: "PlanPageViewController", strings: strings, model: model)
   
   //self.navigationController?.setToolbarHidden(true, animated: true)
   tableView.deselectRow(at: self.tableView.indexPathForSelectedRow!, animated: true)
   
   }
   
   
   }
   */
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if indexPath.row == 0 { // New Plan
      
      model.plan = model.newPlan()
      let planPageViewController = newViewController(withId: "PlanPageViewController")
      self.navigationController?.pushViewController(planPageViewController, animated: true)
      tableView.deselectRow(at: indexPath, animated: true)
      
      return
    }
    
    // Tapped on a plan
    if let plan = planAt(indexPath: indexPath) {
      
      model.plan = plan
      /*
       let planPageViewController = newViewController(withId: "PlanPageViewController")
       self.navigationController?.pushViewController(planPageViewController, animated: true)
       */
      let summaryTableViewController = newViewController(withId: "Summary")
      self.navigationController?.pushViewController(summaryTableViewController, animated: true)
      
      tableView.deselectRow(at: indexPath, animated: true)
      
      
    }
    
  }
  
  
  // Override to support editing the table view.
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      
      if let plan = planAt(indexPath: indexPath),
        let index = model.plans.index(of: plan) {
        
        let name = plan.name
        // Delete the row from the data source
        let alert = UIAlertController(title: "Delete Plan for \(name)", message: "Are you sure you want to delete the plan?", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { [weak self] (alert: UIAlertAction!) -> Void in
          print("Deleting plan: \(plan.debugDescription)")
          
          // Check if there's a notification scheduled, and cancel it
          if let chatNotificationId = self?.model.plan?.chatNotificationId, (self?.model.userSettings.isNotificationsEnabled ?? false) {
            self?.cancelNotification(withId: chatNotificationId)
          }
          
          // cancel previous CALENDAR entries
          if let chatEventId = self?.model.plan?.chatEventId, (self?.model.userSettings.isCalendarEnabled ?? false) {
            self?.deleteChatEvent(withIdentifier: chatEventId)
          }
          if let followupEventId = self?.model.plan?.followupEventId, (self?.model.userSettings.isCalendarEnabled ?? false) {
            self?.deleteChatEvent(withIdentifier: followupEventId)
          }
          
          // remove the plan
          self?.model.plans.remove(at: index)
          //tableView.deleteRows(at: [indexPath], with: .fade)
          self?.model.savePlans()
          
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (alert: UIAlertAction!) -> Void in
          print("Cancel")
        }
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
      }
      
    } else if editingStyle == .insert {
      // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
  }
  
  
  override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
    
    /*
     // First figure out how many sections there are
     let lastSectionIndex = self.tableView.numberOfSections - 1
     // Then grab the number of rows in the last section
     let lastRowIndex = self.tableView.numberOfRows(inSection: lastSectionIndex) - 1
     // Now just construct the index path
     let pathToLastRow = IndexPath(row: lastRowIndex, section: lastSectionIndex)
     */
    
    let startButtonPath = IndexPath(row: 0 /*model.plans.count*/, section: 0)
    
    if indexPath == startButtonPath {  // last row - "Start new plan" - don't allow delete
      return .none
    } else {
      return .delete
    }
  }
  
  /*
   override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
   
   }
   */
  
}

extension MainMenuTableViewController: BEMCheckBoxDelegate {
  
  func didTap(_ checkBox: BEMCheckBox) {
    
    let row = checkBox.tag
    let indexPath = IndexPath(row: row, section: 0)
    guard let plan = planAt(indexPath: indexPath) else { return }
    model.plan = plan
    
    print("Checkbox tapped for plan: \(plan.name), \(plan.planId)")
    
    if checkBox.on {
      plan.hadChat = true
      
      
    } else {  // off
      plan.hadChat = false
    }
    
    model.savePlans()
  }
  
}



