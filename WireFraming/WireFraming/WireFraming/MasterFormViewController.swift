//
//  MasterFormViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 3/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

/*
 
 Parent View Controller superclass for forms
 
 */

import Foundation
import Eureka
import AVFoundation
import AVKit
import TTGSnackbar


class MasterFormViewController: FormViewController, Injectable {
  
  // Video
  //var videoFile: String = "big_buck_bunny.mp4"
  //var playerViewController: AVPlayerViewController?
  //var autoPlayVideo: Bool = false
  
  
  
  // Injectable
  var identifier: String!
  var strings: StringsModel!
  var model: DataModel!
  
  func setDependencies(identifier: String, strings: StringsModel, model: DataModel) {
    self.identifier = identifier
    self.strings = strings
    self.model = model
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // save form if app goes to the background
    NotificationCenter.default.addObserver(self, selector: #selector(viewWillDisappear(_:)), name: Notification.Name.UIApplicationWillResignActive, object: nil)
    
    
    view.backgroundColor = Style.backgroundColor
    view.tintColor = Style.primaryColor
    
    tableView.backgroundColor = Style.backgroundColor
    tableView.tintColor = Style.primaryColor
    tableView.estimatedSectionHeaderHeight = 40 // without this line the autosize doesn't work.
    
    animateScroll = false
    //animateScroll = true
    // Leaves 20pt of space between the keyboard and the highlighted row after scrolling to an off screen row
    rowKeyboardSpacing = 20
//    navigationOptions = RowNavigationOptions.Disabled
    navigationOptions = RowNavigationOptions.Enabled
  }
  
  
  // Pause video and save the form when you swipe to the next screen
  override func viewWillDisappear(_ animated: Bool) {
    //pauseVideo()
    saveForm()
  }
  
  override var prefersStatusBarHidden: Bool {
    return false
  }
  
  
  
  
  
  // MARK: UITableViewDelegate
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    
    /*
     //cast the cell as your custom cell
     if let myCell = cell as? MyCustomCell
     {
     myCell.player!.play()
     }
     */
    print("MasterFormViewController - UITableViewDelegate tableView willDisplayCell method")
    
  }
  
  
  func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    /*
     //cast the cell as your custom cell
     if let myCell = cell as? MyCustomCell
     {
     myCell.player!.pause()
     }
     */
    
    print("MasterFormViewController - UITableViewDelegate tableView didEndDisplaying cell method for cell: \(cell)")
    
  }
  
  
  func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    /*
    if let headerView = view as? VideoView {
      
      if playerViewController == nil {
        let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
        let videoUrl = URL(fileURLWithPath: filePath!)
        
        let player = AVPlayer(url: videoUrl)
        playerViewController = AVPlayerViewController()
        
        if let playerVC = playerViewController {
          playerVC.player = player
          playerVC.videoGravity = AVLayerVideoGravityResizeAspectFill
          
          self.addChildViewController(playerVC)
          headerView.addSubview(playerVC.view)
          playerVC.view.bindFrameToSuperviewBounds()
          playerVC.didMove(toParentViewController: self)
          
//          playerVC.showsPlaybackControls = false
          //  playerVC.player?.play()
         
          // Observer changes to the  bounds size (ie. full screen)
          playerVC.addObserver(self, forKeyPath: "videoBounds", options: NSKeyValueObservingOptions.new, context: nil)
          
        }
      }
      
      
      
      if autoPlayVideo {
        playVideo()
      }
      
      
      
    }
 
 */
    if let videoView = view as? VideoView {
      print("MasterFormViewController willDisplayHeaderView bring videoView to front")
      
//      self.view.bringSubview(toFront: videoView)
      
      //TODO: Check if first appearance of this view, and autplay is OK for this video ...
      
      videoView.player.play()
      
      // allow rotation
      (UIApplication.shared.delegate as! AppDelegate).shouldRotate = true
    }
  }
 
  
  // Check if playerViewController is full screen
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    if keyPath == "videoBounds" {
      if let rect = change?[.newKey] as? NSValue,
         let playerRect = rect.cgRectValue as CGRect? {
           if playerRect.size == UIScreen.main.bounds.size {
             print("Video in full screen")
            // allow rotation
            (UIApplication.shared.delegate as! AppDelegate).shouldRotate = true
            
           } else {
            
             print("Video not in full screen")
            
            // disables rotation
            (UIApplication.shared.delegate as! AppDelegate).shouldRotate = false
            
            UIViewController.attemptRotationToDeviceOrientation()
           }
      }
    }
    
  }
  
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    let orientation = newCollection.verticalSizeClass
    
    switch orientation {
    case .compact:
      print("Landscape")  ///Excluding iPads!!!
      //self.playerViewController?.view.removeBindingToSuperviewBounds()
      //self.playerViewController?.view.frame = UIScreen.main.bounds
      self.navigationController?.setNavigationBarHidden(true, animated: true)
      self.navigationController?.setToolbarHidden(true, animated: true)
      
    default:
      print("Portrait")
//      self.playerViewController?.view.bindFrameToSuperviewBounds()
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      self.navigationController?.setToolbarHidden(false, animated: true)

    }
    
    super.willTransition(to: newCollection, with: coordinator)
    
  }
  
  
  
  // Pause video when scrolled off screen
  func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
    
    //print("MasterFormViewController - UITableViewDelegate tableView didEndDisplaying header method for header view: \(view)")
    /*
    if let headerView = view as? VideoView {
      pauseVideo()
      autoPlayVideo = false
    }
    */
    
    if let headerView = view as? VideoView {
      headerView.player.pause()
      // disables rotation
      (UIApplication.shared.delegate as! AppDelegate).shouldRotate = false
    }
  }
  
  /*
  func playVideo() {
    playerViewController?.player?.play()
    //(UIApplication.shared.delegate as! AppDelegate).shouldRotate = true
  }
  
  
  func pauseVideo() {
    playerViewController?.player?.pause()
    //(UIApplication.shared.delegate as! AppDelegate).shouldRotate = false
  }
  */
  
  
  
  func playVideoModal(file: String) {
    
    let playerViewController = AVPlayerViewController()
    // setup AVPlayer
    // let filePath = Bundle.main.path(forResource: "big_buck_bunny", ofType: "mp4")
    let filePath = Bundle.main.path(forResource: file, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    let player = AVPlayer(url: videoUrl)
    playerViewController.player = player
    
    /*
     self.addChildViewController(playerViewController)
     self.view.addSubview(playerViewController.view)
     playerViewController.didMove(toParentViewController: self)
     */
    //playerViewController.modalPresentationStyle = .overFullScreen
    //self.present(playerViewController, animated: true, completion: nil)
    
    self.present(playerViewController, animated: true) {
      playerViewController.player!.play()
      self.model.userSettings.isEV1Viewed = true
    }
    
  }
  
  /*
  // MARK: - Reminders Access
  
  // Request access to the Event Store for Reminders
  func requestReminderAccess(completion: @escaping (_ allowed: Bool) -> ()) {
    
    AMGCalendarManager.shared.eventStore.requestAccess(to: EKEntityType.reminder) { (granted, error) in
      if !granted {
        print("Access to store for reminders not granted")
        completion(false)
      } else {
        completion(true)
      }
    }
  
  }

  
  
  // MARK: - Calendar Access
  
  /// Adds an event to the users's calendar
  ///
  /// - Parameter startDate: date/time for the conversation
  /// - Returns: unique identifier for that event
  func addConversationEvent(startDate: Date, title: String,
                            location: String = "", notes: String = "") -> String? {
    
    var eventIdentifier: String? = nil
    
    
    AMGCalendarManager.shared.requestAuthorization() { allowed in
      
      if !allowed {  // prompt to authorise the app in Settings
        
        let alertController = UIAlertController(title: "Calendar Permission", message: "This App requires permission to schedule events in your calendar. Go to Settings ?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
          (result : UIAlertAction) -> Void in
          print("You pressed OK")
          
          let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
          UIApplication.shared.open(settingsURL!, options: [:]) { success in
            
          }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
          (result : UIAlertAction) -> Void in
          print("You pressed Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
      }
      
      
      
    }
    
    
    AMGCalendarManager.shared.createEvent { (event) in
      guard let event = event else { return }
      
      event.title = title
      event.startDate = startDate
      event.endDate = startDate + 3600  // 1 hour duration
      
      //other options
      //event.notes = "Don't forget to bring the meeting memos"
      //event.location = self.model.plan.
      event.availability = .busy
      event.addAlarm(EKAlarm(relativeOffset: -15 * 60))  // alarm 15 minutes before
      
      AMGCalendarManager.shared.saveEvent(event: event, completion: { (error) in
        eventIdentifier = event.eventIdentifier
      })
    }
    
    return eventIdentifier
  }

  
  
   // MARK: - Local Notifications Access
  
  
  // Request authorisation for User Notifications
  func requestNotificationsAccess(options: UNAuthorizationOptions, completion: @escaping (_ allowed: Bool) -> ()) {
    //let options: UNAuthorizationOptions = [.alert, .sound, .badge]
    let center = UNUserNotificationCenter.current()
    center.requestAuthorization(options: options) { (granted, error) in
      if !granted {
        print("Access to notifications not granted")
        completion(false)
      } else {
        completion(true)
      }
    }
  }

  
  
  func scheduleLocalNotification(forDate: Date, title: String,
                                 body: String = "") -> String?  {
    
    if #available(iOS 10.0, *) {
      let center = UNUserNotificationCenter.current()
      let content = UNMutableNotificationContent()
      content.title = title
      content.body = body
      content.categoryIdentifier = "alarm"
      content.userInfo = ["customData": "fizzbuzz"]
      content.sound = UNNotificationSound.default()
      
      if let currentBadge = content.badge {
        content.badge = NSNumber(value: currentBadge.intValue + 1)
      } else {
        content.badge = NSNumber(value: 1)
      }
      
      let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: forDate)
      let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
//      let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
      
      let uniqueID = UUID().uuidString
      
      let request = UNNotificationRequest(identifier: uniqueID, content: content, trigger: trigger)
      
      center.add(request, withCompletionHandler: { (error) in
        if error != nil {
          // Something went wrong
          print("Error scheduling notification")
        } else {
          
          DispatchQueue.main.async {
            let snackbar = TTGSnackbar.init(message: "Notification scheduled", duration: .middle)
            snackbar.messageTextFont = UIFont.customHeadline
            snackbar.messageTextColor = Style.inverseTextColor
            snackbar.show()
          }
          
        }
      })
      
      
      return uniqueID
      
    } else {
      
      // ios 9
      let notification = UILocalNotification()
      notification.fireDate = forDate
      notification.alertBody = body
      notification.alertAction = title
      notification.soundName = UILocalNotificationDefaultSoundName
      notification.applicationIconBadgeNumber += 1
      UIApplication.shared.scheduleLocalNotification(notification)
      
      
      return UUID().uuidString
      
      
    }
    
  }
  
  */
  
  
}
