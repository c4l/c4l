//
//  UIButton+custom.swift
//  WireFraming
//
//  Created by Peter Hunt on 12/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit


extension UIButton {
  
  /// Create a new UIButton with customised font, color, cornerRadius, etc
  ///
  /// - Parameters:
  ///   - title: Title text for the button
  ///   - action: selector to run for touchUpInside on the button. eg. #selector(animateSection2)
  /// - Returns: new UIButton
  class func custom(_ title: String, target: Any? = nil, action: Selector? = nil) -> UIButton {
    
//    let newButton = UIButton(frame: CGRect(x: 0, y: 0, width: 160, height: 50))
    let newButton = UIButton(type: .custom)
    newButton.setTitle(title, for: .normal)
//    newButton.backgroundColor = Style.primaryColor
    newButton.titleLabel?.font = UIFont.customHeadline.bolded
    newButton.titleLabel?.textColor = Style.inverseTextColor
    newButton.titleLabel?.textAlignment = .center
    
    newButton.setBackgroundColor(color: Style.primaryColor, forState: .normal)
    //newButton.setBackgroundColor(color: Style.highlightColor, forState: .highlighted)
    //newButton.setBackgroundColor(color: Style.highlightColor, forState: .selected)
    
    newButton.layer.cornerRadius = 4.0
    newButton.clipsToBounds = true
    if let target = target, let action = action {
      newButton.addTarget(target, action: action, for: .touchUpInside)
    }
    newButton.setTitleColor(Style.inverseTextColor, for: .normal)
    //newButton.setTitleColor(Style.highlightColor, for: .highlighted)
    
    return newButton
  }

  
  func setBackgroundColor(color: UIColor, forState: UIControl.State) {
    UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
    UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
    UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
    let colorImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    self.setBackgroundImage(colorImage, for: forState)
  }

}

/*
// Allows adding a closure as an action to a UIButton (or any UIControl)
// Usage:
// button.add(for: .touchUpInside) {
//  print("Hello, Closure!")
// }
// https://stackoverflow.com/questions/25919472/adding-a-closure-as-target-to-a-uibutton


class ClosureSleeve {
  let closure: ()->()
  
  init (_ closure: @escaping ()->()) {
    self.closure = closure
  }
  
  @objc func invoke () {
    closure()
  }
}

extension UIControl {
  func add (for controlEvents: UIControlEvents, _ closure: @escaping ()->()) {
    let sleeve = ClosureSleeve(closure)
    addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
  }
}

*/


// Adds closure as an action to UIButton, UIPageControl, UIGestureRecognizer, UIBarButtonItem
// Usage:
// let button = UIButton()
// button.addTarget() { button in
//  print("Button clicked")
// }
// https://gist.github.com/Moximillian/5f6d60e2cd1222e557547a42558669ae
// Free to copy, use and modify for all types of software projects


// ************** Protocol ***************
/// Closurable protocol
protocol Closurable: class {}
// restrict protocol to only classes => can refer to the class instance in the protocol extension
extension Closurable {
  
  // Create container for closure, store it and return it
  func getContainer(for closure: @escaping (Self) -> Void) -> ClosureContainer<Self> {
    weak var weakSelf = self
    let container = ClosureContainer(closure: closure, caller: weakSelf)
    // store the container so that it can be called later, we do not need to explicitly retrieve it.
    objc_setAssociatedObject(self, Unmanaged.passUnretained(self).toOpaque(), container as AnyObject, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    return container
  }
}

/// Container class for closures, so that closure can be stored as an associated object
final class ClosureContainer<T: Closurable> {
  
  var closure: (T) -> Void
  var caller: T?
  
  init(closure: @escaping (T) -> Void, caller: T?) {
    self.closure = closure
    self.caller = caller
  }
  
  // method for the target action, visible to UIKit classes via @objc
  @objc func processHandler() {
    if let caller = caller {
      closure(caller)
    }
  }
  
  // target action
  var action: Selector { return #selector(processHandler) }
}

// ************** UIKit extensions ***************
/// extension for UIButton - actions with closure
extension UIButton: Closurable {
  
  func addTarget(forControlEvents: UIControl.Event = .touchUpInside, closure: @escaping (UIButton) -> Void) {
    let container = getContainer(for: closure)
    addTarget(container, action: container.action, for: forControlEvents)
  }
}

/// extension for UIPageControl - actions with closure
extension UIPageControl: Closurable {
  
  func addTarget(forControlEvents: UIControl.Event = .valueChanged, closure: @escaping (UIPageControl) -> Void) {
    let container = getContainer(for: closure)
    addTarget(container, action: container.action, for: forControlEvents)
  }
}

/// extension for UIGestureRecognizer - actions with closure
extension UIGestureRecognizer: Closurable {
  
  convenience init(closure: @escaping (UIGestureRecognizer) -> Void) {
    self.init()
    let container = getContainer(for: closure)
    addTarget(container, action: container.action)
  }
}

/// extension for UIBarButtonItem - actions with closure
extension UIBarButtonItem: Closurable {
  
  convenience init(image: UIImage?, style: UIBarButtonItem.Style = .plain, closure: @escaping (UIBarButtonItem) -> Void) {
    self.init(image: image, style: style, target: nil, action: nil)
    let container = getContainer(for: closure)
    target = container
    action = container.action
  }
  
  convenience init(title: String?, style: UIBarButtonItem.Style = .plain, closure: @escaping (UIBarButtonItem) -> Void) {
    self.init(title: title, style: style, target: nil, action: nil)
    let container = getContainer(for: closure)
    target = container
    action = container.action
  }
}
