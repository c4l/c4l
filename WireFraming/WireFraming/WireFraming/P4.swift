//
//  P4.swift
//  WireFraming
//
//  Created by Peter Hunt on 20/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import BEMCheckBox
import SnapKit
import KMPlaceholderTextView

/*
 P4
 Video
 Skip/Next Button
 Section 1 - Who would you like to support ? (UITextField)
 Section 2 - What has been going on to make you concerned ? (UITextView)
 Section 3 - How might %s react ? (check boxes)
 Section 4 - Where should you and %s talk ? (text view)
 Section 5 - When should you talk ? (date/time picker)
 Next Button
 */

class P4: ScrollingForm {
  
  let videoName = "EV2"
  let videoFile = "EV2.mp4"
  let subtitleFile = "EV2.srt"
  let coverFile = "ev2cover.jpg"
  let videoTitle = "ev2_title"
  
  var isFirstRun: Bool = false
  var speechView1, speechView2, speechView3, speechView4, speechView5, transcriptSpeechView: SpeechBubbleView!
  var nameTextField, conversationDateTextField: UITextField!
  var s2TextView, s4TextView: KMPlaceholderTextView!
  var s3CheckboxView: UIView!
  var s3CheckItems: [String] = []
  var footerLabel5: UILabel!
  var scheduleButton: UIButton!
  
  var previousChatDate: Date?
  var previousName, previousS2Text, previousS4Text: String?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)
    
    // TESTING ONLY - create a new plan
    //model.plan = model.newPlan()
    
    isFirstRun = self.model.getName().isEmpty
    print("isFirstRun: \(isFirstRun)")
    
    // TESTING:
    //isFirstRun = false
    //model.setName("TEST PERSON")
    
    if isFirstRun {
      // First Run Experience: Show the video, auto play it, show Skip button, then prompt for Name
      //showVideoPlayer(withSkipButton: true, autoplay: true)  // skip button will then show Section 1 (name prompt)
      
      addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: strings[videoTitle], subtitleFile: subtitleFile, coverFile: coverFile, withSkipButton: true)
      animateInVideoPlayer(withSkipButton: true, autoplay: true, withTranscriptButton: true)
      
      addSection1() // Prompt for Name (Who would you like to support ?)
      addSection2()
      addSection3()
      addSection4()
      addSection5()
      addNextButton()
      
      model.userSettings.hasEV2Autoplayed = true
      
      // Until a name has been entered, hide sections and disable swipe ...
      hideSections345()
      if let parentPageViewController = self.parent as? PlanPageViewController {
        parentPageViewController.disableSwipeGesture()
      }
      
    } else {
      // Subsequent Runs: Show the video, seek up to last location (don't autoplay it or show skip button), Show the name (don't prompt), show the rest of the form
      
      addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: strings[videoTitle], subtitleFile: subtitleFile, coverFile: coverFile, withSkipButton: false)
      
      addSection1()
      addSection2()
      addSection3()
      addSection4()
      addSection5()
      addNextButton()
      
      speechView1.isHidden = false
      nameTextField.isHidden = false
      speechView2.isHidden = false
      s2TextView.isHidden = false
      
    }
    
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("P4 viewWillDisappear")
    
    view.endEditing(true)
    
  }
  
  
  deinit {
    print("P4 deinit")
    
    if let player = player {
      player.prepareToDealloc()
    }
  }
  
  
  /// When skip/next button under the video is tapped:
  /// pause the video, hide the button, show the name prompt
  
  override func skipButtonTapped() {
    super.skipButtonTapped()
    
    // Shrink skip button height
    if let button = self.skipButton {
      button.snp.updateConstraints { (make) in
        make.height.equalTo(0)
      }
    }
    
    animateSection1()
    
    //    showSection2(hidden: true)  // add section 2 to the scrollview, but hide it until name is filled in
    
    // Scroll so section 1 is at the top
    //self.scrollView.setContentOffset(CGPoint(x:0, y: player.frame.height - 60), animated: true)
    //scrollToTop(view: speechView1, animated: true)
  }
  
  // MARK: - Section 1 - Who would you like to support ?
  func addSection1() {
    speechView1 = SpeechBubbleView(type: .primary, text: strings.p4_s1_title)
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.equalTo(contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    speechView1.isHidden = true
    
    // Name field
    nameTextField = customTextField(placeholder: strings.p4_s1_i1_placeholder)
    //    nameTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 300, height: 45))
    nameTextField.delegate = self
    nameTextField.clearButtonMode = .never
    
    if model.getName() != "" {
      nameTextField.text = model.getName()
    }
    
    contentView.addSubview(nameTextField)
    nameTextField.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-5)
      make.left.equalTo(contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(45)
    }
    lastControlBottom = nameTextField.snp.bottom
    nameTextField.isHidden = true
    
  }
  
  
  func animateSection1() {
    // Animate elements in from the bottom of the screen, and set nameField as first responder if first run
    speechView1.isHidden = false
    nameTextField.isHidden = false
    
    if self.isFirstRun {
      self.nameTextField.becomeFirstResponder()
      scrollToTop(view: skipButton ?? speechView1)
    }
    

  }
  
  
  /// Called when Section1 is complete (name field filled in)
  func nameSubmitted(name: String) {
    
    /*
    if name == previousName {
      nameTextField.text = previousName
      print("P4 same as previous")
      return
    }
*/
    /*
    if name.isEmpty  {
      nameTextField.text = previousName
      print("P4 same as previous")
      return
    }
*/
    self.model.setName(name)  // update the model with the new name
    
    // Update the titles in all the sections
    speechView2.speechLabel.text = model.replaceTokens(inString: strings.p4_s2_title)
    speechView3.speechLabel.text = model.replaceTokens(inString: strings.p4_s3_title)
    speechView4.speechLabel.text = model.replaceTokens(inString: strings.p4_s4_title)
    speechView5.speechLabel.text = model.replaceTokens(inString: strings.p4_s5_title)
    
    if isFirstRun {
      // Update the title and un-hide/animate section 2
      //      speechView2.speechLabel.text = model.replaceTokens(inString: strings.p4_s2_title)
      isFirstRun = false  // first run experience is now over
      animateSection2()
      
      // Show all the rest of the sections
      //showSection3()
      //showSection4()
      //showSection5()
      //showNextButton()
      
    } else {
      // Update the titles in all the sections
      //      speechView2.speechLabel.text = model.replaceTokens(inString: strings.p4_s2_title)
      //      speechView3.speechLabel.text = model.replaceTokens(inString: strings.p4_s3_title)
      //      speechView4.speechLabel.text = model.replaceTokens(inString: strings.p4_s4_title)
      //      speechView5.speechLabel.text = model.replaceTokens(inString: strings.p4_s5_title)
    }
    
    // Select the next field, and scroll it the top
    //s2TextView.becomeFirstResponder()
    //scrollToTop(view: speechView2)
    
    model.savePlans()
  }
  
  
  // MARK: - Section 2 - What has been going on to make you concerned ? (UITextView)
  func addSection2() {
    speechView2 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p4_s2_title))
    contentView.addSubview(speechView2)
    speechView2.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(30)
      make.left.equalTo(weakSelf.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(weakSelf.contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(weakSelf.speechView2.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView2.snp.bottom
    speechView2.isHidden = true
    
    s2TextView = customTextView(placeholder: strings.p4_s2_i1_placeholder)  // KMPlaceholderTextView
    s2TextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s2TextView)
    s2TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.equalTo(contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(80)
    }
    lastControlBottom = s2TextView.snp.bottom
    s2TextView.text = model.get(.p4_s2) as? String
    s2TextView.isHidden = true
  }
  
  
  func animateSection2() {
    // Animate elements in from the bottom of the screen
    speechView2.isHidden = false
    s2TextView.isHidden = false
    speechView2.center.y += view.bounds.height
    s2TextView.center.y += view.bounds.height
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView2.center.y -= weakSelf.view.bounds.height
                    weakSelf.s2TextView.center.y -= weakSelf.view.bounds.height
      }, completion: { [weak self] _ in
        
        // Name has been submitted, show sections and enable swipe gesture
        self?.showSections345()
        if let parentPageViewController = self?.parent as? PlanPageViewController {
          parentPageViewController.enableSwipeGesture()
        }

        //        if self.isFirstRun {
        //          self.nameTextField.becomeFirstResponder()
        //        }
    })
  }
  
  // MARK: - Section 3 - How might %s react ? (check boxes)
  func addSection3() {
    // Thankful, Angry, Ashamed, Surprised, Upset
    s3CheckItems = [strings.p4_s3_i1_1, strings.p4_s3_i1_2, strings.p4_s3_i1_3, strings.p4_s3_i1_4, strings.p4_s3_i1_5, strings.p4_s3_i1_6]
    let itemsChecked = model.get(.p4_s3) as? [String] ?? []
    
    speechView3 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.p4_s3_title))
    contentView.addSubview(speechView3)
    speechView3.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.s2TextView.snp.bottom).offset(30)
      make.left.equalTo(weakSelf.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(weakSelf.contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(weakSelf.speechView3.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView3.snp.bottom

    var viewHeight: CGFloat = 0
    s3CheckboxView = UIView(frame: CGRect.zero)
    contentView.addSubview(s3CheckboxView)
    s3CheckboxView.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(0)
      make.left.right.equalTo(weakSelf.contentView)
      make.height.equalTo(400)
    }
    lastControlBottom = s3CheckboxView.snp.top

    for (i, item) in s3CheckItems.enumerated() {
      let checkboxLabel = customLabel(text: item, tag: i + 2000)
      s3CheckboxView.addSubview(checkboxLabel)
      checkboxLabel.snp.makeConstraints { (make) in
        make.left.equalTo(s3CheckboxView).offset(40)
        make.top.equalTo(lastControlBottom).offset(i == 0 ? 0 : 30) // For 1st item, less of a vertical offset
      }
      lastControlBottom = checkboxLabel.snp.bottom
      
      let checkbox = customCheckbox(tag: i + 1000)
      checkbox.delegate = self
      s3CheckboxView.addSubview(checkbox)
      checkbox.snp.makeConstraints { (make) in
        make.centerY.equalTo(checkboxLabel)
        make.right.equalTo(s3CheckboxView).offset(-40)
      }
      
      if itemsChecked.contains(item) {
        //        checkboxLabel.textColor = Style.highlightColor
        checkboxLabel.font = UIFont.customBody.bolded
        checkbox.setOn(true, animated: true)
      }
      viewHeight += checkbox.frame.height
    }
    
    s3CheckboxView.snp.updateConstraints { (make) in
      make.height.equalTo(viewHeight)
    }
    lastControlBottom = s3CheckboxView.snp.bottom

    
  }
  
  // MARK: - Section 4 - Where should you and %s talk ? (text view)
  func addSection4() {
    speechView4 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p4_s4_title))
    contentView.addSubview(speechView4)
    speechView4.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(40)
      make.left.equalTo(weakSelf.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(weakSelf.contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(weakSelf.speechView4.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView4.snp.bottom
    
    s4TextView = customTextView(placeholder: strings.p4_s4_i1_placeholder)  // KMPlaceholderTextView. Choose somewhere safe for you and them. Maybe a regular meeting spot?
    contentView.addSubview(s4TextView)
    s4TextView.inputAccessoryView = keyboardToolbar()
    s4TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.equalTo(self.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(self.contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(100)
    }
    lastControlBottom = s4TextView.snp.bottom
    
    s4TextView.text = model.get(.p4_s4) as? String
  }
  
  
  // MARK: - Section 5 - When should you talk ? (date/time picker)
  func addSection5() {
    speechView5 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.p4_s5_title))
    contentView.addSubview(speechView5)
    
    speechView5.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(40)
      make.left.equalTo(weakSelf.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(weakSelf.contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(weakSelf.speechView3.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView5.snp.bottom
    
    
    let dateTimePicker = UIDatePicker()
    dateTimePicker.datePickerMode = .dateAndTime
    dateTimePicker.addTarget(self, action: #selector(dateTimePickerChanged(datePicker:)), for: .valueChanged)
    
    conversationDateTextField = customTextField(placeholder: strings.p4_s5_i1_placeholder)
    conversationDateTextField.textAlignment = .left
    conversationDateTextField.font = UIFont.customBody
    //conversationDateTextField.clearButtonMode = UITextFieldViewMode.never
    if let date = model.getChatDate() {
      conversationDateTextField.text = date.customFriendlyString()
      dateTimePicker.date = date
      //conversationDateTextField.font = UIFont.customBody
    } else { // no date set, put placeholder text
      //conversationDateTextField.font = UIFont.customCaption1
    }
    conversationDateTextField.inputView = dateTimePicker
    conversationDateTextField.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(conversationDateTextField)
    conversationDateTextField.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.equalTo(self.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(self.contentView).offset(-Const.HorizontalPadding)
      make.height.equalTo(45)
    }
    lastControlBottom = conversationDateTextField.snp.bottom
    
    footerLabel5 = customLabel(text: strings.p4_s5_i1_footer)  // Choose a time of day when they will be at their best and open to a conversation. Avoid times when they may have used alcohol or other substances
    footerLabel5.numberOfLines = 0
    footerLabel5.font = UIFont.customCaption1
    footerLabel5.textColor = Style.secondaryTextColor
    contentView.addSubview(footerLabel5)
    footerLabel5.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(10)
      make.left.equalTo(weakSelf.contentView).offset(Const.HorizontalPadding)
      make.right.equalTo(weakSelf.contentView).offset(-Const.HorizontalPadding)
    }
    self.lastControlBottom = footerLabel5.snp.bottom
    
  }

  func hideSections345() {
    speechView3.isHidden = true
    s3CheckboxView.isHidden = true
    speechView4.isHidden = true
    s4TextView.isHidden = true
    speechView5.isHidden = true
    conversationDateTextField.isHidden = true
    footerLabel5.isHidden = true
    nextButton?.isHidden = true
  }
  
  func showSections345() {
    speechView3.isHidden = false
    s3CheckboxView.isHidden = false
    speechView4.isHidden = false
    s4TextView.isHidden = false
    speechView5.isHidden = false
    conversationDateTextField.isHidden = false
    footerLabel5.isHidden = false
    nextButton?.isHidden = false

  }
  
  
  override func transcriptButtonTapped() {
    print("P4 Transcript button tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName], title: strings[videoTitle], coverFile: coverFile)
  }
  
  
  
  @objc func dateTimePickerChanged(datePicker: UIDatePicker) {
    print(datePicker.date.customFriendlyString())
    scrollToTop(view: speechView5)
    conversationDateTextField.text = datePicker.date.customFriendlyString()
    //conversationDateTextField.font = UIFont.customBody.bolded
    model.set(chatDate: datePicker.date)
    model.plan?.hadChat = false
    model.plan?.isFollowup = false
  }
  
  /*
   /// Scroll so that the view appears at the top of the scrollview
   ///
   /// - Parameter view: view to put at the top of the scrollview
   func scrollToTop(view: UIView, animated: Bool = true) {
   self.scrollView.setContentOffset(CGPoint(x:0, y: view.frame.origin.y /*- 60*/), animated: animated)
   }
   */
  
  
  
}


extension P4: BEMCheckBoxDelegate {
  
  func didTap(_ checkBox: BEMCheckBox) {
    //print("Checkbox tapped with tag: \(checkBox.tag)")
    view.endEditing(true) // just in case another item has first responder
    scrollToTop(view: speechView3)
    // Find the label with the corresponding tag
    if let label = contentView.viewWithTag(checkBox.tag + 1000),
      let checkLabel = label as? UILabel,
      let textTapped = checkLabel.text {
      
      print("Checkbox tapped with tag: \(checkBox.tag), label: \(textTapped), on: \(checkBox.on)")
      
      // Add or remove the item
      var itemsChecked = model.get(.p4_s3) as? [String] ?? []
      if checkBox.on {
        checkLabel.font = UIFont.customBody.bolded
        if !itemsChecked.contains(textTapped) {
          itemsChecked.append(textTapped)
        }
        
      } else {  // off
        checkLabel.font = UIFont.customBody
        if let i = itemsChecked.index(of: textTapped) {
          itemsChecked.remove(at: i)
        }
        
      }
      
      model.set(.p4_s3, value: itemsChecked)
      model.savePlans()
    }
    
  }
  
  func animationDidStop(for checkBox: BEMCheckBox) {
    print("Checkbox animation stop")
  }
  
  
}


// MARK: UITextFieldDelegate - Name Field + conversationDateField
extension P4 {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == nameTextField, let name = textField.text, name.count > 0 {
      textField.resignFirstResponder()
    }
    return false
  }
  
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    textField.layer.borderColor = Style.primaryColor.cgColor
    
    if textField == nameTextField {
      print("Scrolling to nameField")
      if scrollView.contentOffset.y < 30 { // workaround for not scrolling when at the top bug
        scrollView.setContentOffset(CGPoint(x:0, y: 120), animated: false)
      }
      scrollToTop(view: speechView1)
      previousName = nameTextField.text
    }
    
    if textField == conversationDateTextField {
      scrollToTop(view: speechView5)
      previousChatDate = model.getChatDate()         // Record the current chatDate
      
    }
    
    
  }
  
  
  // Tapped "Done", on another field, or view disappeared
  func textFieldDidEndEditing(_ textField: UITextField) {
    textField.layer.borderColor = Style.secondaryTextColor.cgColor
    
    ///  Run nameSubmitted to set the model name
    if textField == nameTextField {
      
      if let newName = nameTextField.text {
        nameSubmitted(name: newName)  // show the rest of the form, now we have a name
      }
      
    }
    
    // Save date when editing of date field is finished
    if textField == conversationDateTextField {
      print("P4 conversationDateTextField DidEndEditing")
      
      // Check if the chat date has changed, if so reschedule or cancel notification
      if model.getChatDate() != previousChatDate {
        if model.userSettings.isNotificationsEnabled {
          scheduleNotificationsAndCalendar()
        }
        model.savePlans()
      }
      
    }
    
  }
  
  
  func textFieldShouldClear(_ textField: UITextField) -> Bool {
    if textField == conversationDateTextField {
      print("P4 textFieldShouldClear conversationDateTextField")
      model.set(chatDate: nil)
    }
    return true
  }
  
  
  // MARK: UITextViewDelegate
  override func textViewDidBeginEditing(_ textView: UITextView) {
    print("P4 textViewDidBeginEditing")
    textView.layer.borderColor = Style.primaryColor.cgColor
    
    // Scroll to the top of this section and record previous text
    switch textView {
      
    case s2TextView:
      scrollToTop(view: speechView2)
      previousS2Text = s2TextView.text
      
    case s4TextView:
      scrollToTop(view: speechView4)
      previousS4Text = s4TextView.text
      
    default:
      break
    }
    
    
  }
  
  override func textViewDidEndEditing(_ textView: UITextView) {
    print("P4 textViewDidEndEditing")
    textView.layer.borderColor = Style.secondaryTextColor.cgColor
    
    // Save data if changed
    switch textView {
      
    case s2TextView:
      //scrollToTop(view: speechView3)
      s2TextView.text = s2TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s2TextView.text != previousS2Text {
        model.set(.p4_s2, value: s2TextView.text)
        model.savePlans()
      }
      
    case s4TextView:
      //scrollToTop(view: speechView5)
      s4TextView.text = s4TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s4TextView.text != previousS4Text {
        model.set(.p4_s4, value: s4TextView.text)
        model.savePlans()
      }
      
    default:
      break
    }
    
    
  }
  
  func keyboardToolbar() -> UIToolbar  {
    let keyboardToolbar = UIToolbar()
    keyboardToolbar.backgroundColor = Style.secondaryColor.withAlpha(0.75)
    keyboardToolbar.sizeToFit()
    let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
    keyboardToolbar.items = [flexBarButton, doneBarButton]
    return keyboardToolbar
  }
  
  @objc func dismissKeyboard() {
    
    // Scroll to the top of the next section
    if s2TextView.isFirstResponder {
      //scrollToTop(view: speechView3)
    } else if s4TextView.isFirstResponder {
      //scrollToTop(view: speechView5)
    }
    
    view.endEditing(true)
  }
  
  
  
  
  
}



/*
 // Check if a view is on screen
 
 func scrollViewDidScroll(_ scrollView: UIScrollView) {
 let viewFrame = player.frame
 
 let container = CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
 
 
 // We may have received messages while this tableview is offscreen
 if (viewFrame.intersects(container)) {
 // Do work here
 print("view is visible")
 }
 else{
 print("nope view is not on the screen")
 }
 }
 */

