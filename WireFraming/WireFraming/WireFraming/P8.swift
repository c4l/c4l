//
//  P6.swift
//  WireFraming
//
//  Created by Peter Hunt on 24/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
//import BMPlayer


/*
 P8 Scrolling Form
 Video (EV6)
 Skip/Next Button
 Back/Finish Button
 */

class P8: ScrollingForm {
  let videoName = "EV6"
  let videoFile = "EV6.mp4"
  let subtitleFile = "EV6.srt"
  let coverFile = "ev6cover.jpg"
  let videoTitle = "ev6_title"
  var isFirstRun: Bool = false

  override func viewDidLoad() {
    super.viewDidLoad()
    print("P8 viewDidLoad")
    
    addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: strings[videoTitle], subtitleFile: subtitleFile, coverFile: coverFile, withSkipButton: false, withTranscriptButton: true)

    addBackNextButtons()
    nextButton?.setTitle("Done", for: .normal)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    if !model.userSettings.hasEV6Autoplayed {
      player?.play()
      model.userSettings.hasEV6Autoplayed = true
    }
  }
  
  deinit {
    print("P8 deinit")
    if let player = player {
      player.prepareToDealloc()
    }
  }
  
  /*
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("P8 viewWillDisappear")
    if let player = player {
      player.pause()
    }
  }
  */

  // MARK: - Button Actions
  // FINISH
  override func nextScreen() {
    // Return to Main Menu
//    self.navigationController?.popToRootViewController(animated: true)
    self.navigationController?.popViewController(animated: true)

  }
  
  override func transcriptButtonTapped() {
    print("P8 Transcript button tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName], title: strings[videoTitle], coverFile: coverFile)
  }
  
}





