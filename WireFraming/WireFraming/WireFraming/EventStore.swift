//
//  EventStore.swift
//
//
//  Static wrapper around EventKit for central access to the calendar via EKEventStore
//

import Foundation
import EventKit

public struct EventStore {
  
  public static var store = EKEventStore()
  
  //MARK: - Authorization to the calendar
  // Example: EventStore.requestAuthorization { allowed in ...
  //            if allowed {
  //              let event = EKEvent(eventStore: EventStore.store)
  //              ...
  //              let eventId = EventStore.addEvent(event: event)
  //            }
  //          }
  //
  public static func requestAuthorization(completion: @escaping (_ allowed:Bool) -> ()) {
    switch EKEventStore.authorizationStatus(for: EKEntityType.event) {
    case .authorized:
      completion(true)
    case .denied:
      completion(false)
    case .notDetermined:
      var userAllowed = false
      EventStore.store.requestAccess(to: .event, completion: { (allowed, error) -> Void in
        userAllowed = allowed
        if userAllowed {
          EventStore.store.reset() // reset the event store in order to start receiving data after the user grants access
          completion(userAllowed)
        } else {
          completion(false)
        }
      })
    default:
      completion(false)
    }
  }
  
  // Add an event to the default calendar
  // Returns a unique id for the event, or nil if an error
  public static func addEvent(_ newEvent: EKEvent) -> String? {
    newEvent.calendar = EventStore.store.defaultCalendarForNewEvents
    
    do {
      try EventStore.store.save(newEvent, span: .thisEvent, commit: true)
      return newEvent.eventIdentifier
    } catch _ {
      print("EventStore error adding event")
      return nil
    }
    
  }
  
  
  public static func getEvent(withIdentifier eventId: String) -> EKEvent? {
    let event = EventStore.store.event(withIdentifier: eventId)
    return event
  }
  
  
  public static func removeEvent(_ event: EKEvent) -> Bool {
    do {
      try EventStore.store.remove(event, span: .thisEvent)
      return true
    } catch _ {
      return false
    }
  }
  
  
  
}
