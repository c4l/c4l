//
//  ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//

/*
 
 Superclass for a ViewController for playing video
 - Pauses video on viewWillDisappear
 - Subclasses' responsibility to display playerViewController
 
 filename - video to show
 
 
 */

import UIKit
import AVKit
import AVFoundation


class VideoViewController: UIViewController {
  
  @IBInspectable var fileName: String!
  var playerViewController: AVPlayerViewController?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // load Video Player VC from storyboard
    playerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayer") as? AVPlayerViewController
    
    if let playerVC = playerViewController {
      // setup AVPlayer
      // let filePath = Bundle.main.path(forResource: "big_buck_bunny", ofType: "mp4")
      let filePath = Bundle.main.path(forResource: fileName, ofType: nil)
      let videoUrl = URL(fileURLWithPath: filePath!)
      let player = AVPlayer(url: videoUrl)
      
      
      
      playerVC.player = player
      
      // ** Show closed captions
      
      // override the default user's system preference for media selection (ie. closed caption settings)
      player.appliesMediaSelectionCriteriaAutomatically = false
      
      
      // From https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/MediaPlaybackGuide/Contents/Resources/en.lproj/RefiningTheUserExperience/RefiningTheUserExperience.html#//apple_ref/doc/uid/TP40016757-CH6-SW1
      //  retrieved an AVMediaSelectionGroup object for a particular media characteristic AVMediaCharacteristicLegible, and select it
      if let asset = player.currentItem?.asset,
        let group = asset.mediaSelectionGroup(forMediaCharacteristic: AVMediaCharacteristicLegible) {
        let options = AVMediaSelectionGroup.playableMediaSelectionOptions(from: group.options)
        
        if let option = options.first {
          player.currentItem?.select(option, in: group)
          print ("Closed Caption Subtitles Found: \(group)")
        }
      }
      
      
      // Debugging - print all asset characteristics ...
      let asset = AVAsset(url: videoUrl)
      print("Asset characteristics: \(asset.availableMediaCharacteristicsWithMediaSelectionOptions)")
      
      for characteristic in asset.availableMediaCharacteristicsWithMediaSelectionOptions {
        
        print("\(characteristic)")
        
        // Retrieve the AVMediaSelectionGroup for the specified characteristic.
        if let group = asset.mediaSelectionGroup(forMediaCharacteristic: characteristic) {
          // Print its options.
          for option in group.options {
            print("  Option: \(option.displayName)")
          }
        }
      }
      
      
      
    }
    
    // playerViewController is now setup ... subclass will add it to the view
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    playerViewController?.player?.pause()
  }
  
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    if UIDevice.current.orientation.isLandscape {
      print("Landscape")
      
    } else {
      print("Portrait")
    }
  }
  
}

