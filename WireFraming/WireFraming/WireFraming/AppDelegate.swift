//
//  AppDelegate.swift
//  WireFraming
//
//  Created by Peter Hunt on 30/3/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit
import UserNotifications

// Redefine print, so it only runs when -D DEBUG custom flag is active
func print(_ item: @autoclosure () -> Any, separator: String = " ", terminator: String = "\n") {
  #if DEBUG
    Swift.print(item(), separator:separator, terminator: terminator)
  #endif
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  internal var shouldRotate = false
  
  // Instantiate a new StringsModel and DataModel for user settings and plans
  // loads user settings if they exist
  let strings: StringsModel = StringsModel()
  let model: DataModel = DataModel()
  

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {


    
    Style.applyTheme()
    
    // Load the navigation controller, and show either the welcome screen or main menu, depending on isFirstLaunch status
    let navigationController: UINavigationController? = (self.window?.rootViewController as? UINavigationController)

    
    //model.isFirstLaunch = true      // *** Debugging - show welcome flow

    if model.isFirstLaunch {


      
      
      // TODO: TESTING ONLY
      // Uncomment ev1Screen push on first launch when finished
      
      let welcomeScreen = newViewController(withId: "Welcome")
      navigationController?.pushViewController(welcomeScreen, animated: false)
 
      /*
      model.plan = model.newPlan()
      let p4Screen = newViewController(withId: "P4Scroll")
      let p5Screen = newViewController(withId: "P5Scroll")
      let planPageViewController = newViewController(withId: "PlanPageViewController")
      navigationController?.pushViewController(planPageViewController, animated: false)
      */
      
      
      
    } else {
      /*
      // Passcode security on app launch
      if !model.isUnlocked && model.userSettings.isPasscodeEnabled {
        print("Passcode enabled")
  
        /*
        let launchScreen = UIStoryboard(name: "LaunchScreen", bundle: nil).instantiateViewController(withIdentifier: "LaunchScreen")
        navigationController?.pushViewController(launchScreen, animated: false)
 */
        self.window?.makeKeyAndVisible()
        
        if let controller = newViewController(withId: "BlurPasscodeViewController") as? BlurPasscodeViewController {
//          controller.setDependencies(strings: strings, model: model)
          //controller.modalPresentationStyle = .overCurrentContext
          
          if let window = self.window, let rootViewController = window.rootViewController {
            var currentController = rootViewController
            while let presentedController = currentController.presentedViewController {
              currentController = presentedController
            }
            currentController.present(controller, animated: false, completion: { [weak self] in
              self?.model.isUnlocked = true
              let mainMenuScreen = self?.newViewController(withId: "MainMenu")
              navigationController?.pushViewController(mainMenuScreen!, animated: false)
              
            })
            
          }
        }

      } else { */
        // Show the main menu
        let mainMenuScreen = newViewController(withId: "MainMenu")
        navigationController?.pushViewController(mainMenuScreen, animated: false)
     /* } */
    }
    
    return true
  }

  
  private func newViewController(withId identifier: String) -> UIViewController {
    let newVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    
    if newVC is Injectable {
      var injectableVC = newVC as! Injectable
      injectableVC.setDependencies(strings: strings, model: model)
      return injectableVC as! UIViewController
    }
    return newVC
  }
  
  
  // Don't allow rotation, unless view controller toggles the variable ...
  // let appDelegate = UIApplication.shared.delegate as! AppDelegate
  //  appDelegate.shouldRotate = true // or false to disable rotation
  
  func application(_ application: UIApplication,
                   supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
    return shouldRotate ? .allButUpsideDown : .portrait
  }
  
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    print("UserDefaults synchronize()")
    UserDefaults.standard.synchronize()
    
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    UIApplication.shared.applicationIconBadgeNumber = 0
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }

 
}

