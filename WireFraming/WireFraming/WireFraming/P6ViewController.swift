//
//  P6ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 19/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka


/*
P6: Conversation Skills
 */


class P6ViewController: MasterFormViewController {

  let videoName1 = "EV2"
  let videoFile1 = "ev1_1080p.mp4"
  let subtitleFile1 = "ev1.srt"

  let videoName2 = "EV2"
  let videoFile2 = "ev1_1080p.mp4"
  let subtitleFile2 = "ev1.srt"

  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Get strings data for this page, and setup the form fields
    configureForm()
    
    // Injectable - load data from the model for this page
    self.loadForm()
    
  }
  
  
  
  func configureForm() {
    
    // ** Section 1
    let p6_s1_title = strings["p6_s1_title"]                     //
    let p6_s1_tag = strings["p6_s1_tag"]                         //
    
    let p6_s1_i1_1 = strings["p6_s1_i1_1"]                             //
    let p6_s1_i1_2 = strings["p6_s1_i1_2"]                             //
    let p6_s1_i1_3 = strings["p6_s1_i1_3"]                             //
    let p6_s1_i1_4 = strings["p6_s1_i1_4"]                             //
    
    //** Section 2
    let p6_s2_title = strings["p6_s2_title"]
    let p6_s2_tag = strings["p6_s2_tag"]                         //
    let p6_s2_i1_1 = strings["p6_s2_i1_1"]                             //
    let p6_s2_i1_2 = strings["p6_s2_i1_2"]                             //
    let p6_s2_i1_3 = strings["p6_s2_i1_3"]                             //
    
    form +++ CustomVideoSection() { section in
      section.tag = videoName1
      (section as? CustomVideoSection)?.configure(videoName: videoName1, videoFile: videoFile1, subtitleFile: subtitleFile1)
    }
    
    form +++ CustomVideoSection() { section in
      section.tag = videoName2
      (section as? CustomVideoSection)?.configure(videoName: videoName2, videoFile: videoFile2, subtitleFile: subtitleFile2)
    }

    
    
    form +++ CustomSection(p6_s1_title) {
        $0.tag = p6_s1_tag
    }
    
    
    form +++ CustomSection(p6_s2_title) {
      $0.tag = p6_s2_tag
    }
    

    
    // ** Section 1
    /*
    form +++ SelectableSection<ImageCheckRow<String>>(p6_s1_title.replacingOccurrences(of: "%s", with: model.getName()), selectionType: .singleSelection(enableDeselection: true)) {
      $0.tag = p6_s1_tag
      }
      
      <<< ImageCheckRow<String>("p6_s1_i1_1"){ listRow in
        listRow.title = p6_s1_i1_1
        listRow.selectableValue = p6_s1_i1_1
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
      }
      
      <<< ImageCheckRow<String>("p6_s1_i1_2"){ listRow in
        listRow.title = p6_s1_i1_2
        listRow.selectableValue = p6_s1_i1_2
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
      }
      
      <<< ImageCheckRow<String>("p6_s1_i1_3"){ listRow in
        listRow.title = p6_s1_i1_3
        listRow.selectableValue = p6_s1_i1_3
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
      }
      
      <<< ImageCheckRow<String>("p6_s1_i1_4"){ listRow in
        listRow.title = p6_s1_i1_4
        listRow.selectableValue = p6_s1_i1_4
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
    }
    */
    
    // ** Section 2
    /*
    form +++ SelectableSection<ImageCheckRow<String>>(p6_s2_title.replacingOccurrences(of: "%s", with: model.getName()), selectionType: .singleSelection(enableDeselection: true)) {
      $0.tag = p6_s2_tag
      }
      
      <<< ImageCheckRow<String>("p6_s2_i1_1"){ listRow in
        listRow.title = p6_s2_i1_1
        listRow.selectableValue = p6_s2_i1_1
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
      }
      
      <<< ImageCheckRow<String>("p6_s2_i1_2"){ listRow in
        listRow.title = p6_s2_i1_2
        listRow.selectableValue = p6_s2_i1_2
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
      }
      
      <<< ImageCheckRow<String>("p6_s2_i1_3"){ listRow in
        listRow.title = p6_s2_i1_3
        listRow.selectableValue = p6_s2_i1_3
        listRow.value = nil
        listRow.cell.textLabel?.numberOfLines = 0
        }.cellSetup { cell, _ in
          cell.trueImage = UIImage(named: "selected")!
          cell.falseImage = UIImage(named: "unselected")!
    }
    */
    
    // ** Final Section - Next button
    form +++ Section("") {
      $0.hidden = false
      $0.tag = "NextButtonSection"
      }
      <<< ButtonRow("Next") { (row: ButtonRow) -> Void in
        row.title = "Next"
        }.onCellSelection { [weak self] (cell, row) in
          row.value = "submitted"
          
          self?.saveForm()
          // go to next page
          NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": 3])
    }
    
  }
  
  /*
  // If any value exists, return true
  func isSaved(tag: String) -> Bool {
    let row: RowOf<String>! = form.rowBy(tag: tag)
    if let _ = row.value { return true }
    return false
  }
  */
  
}


