//
//  SummaryCell.swift
//  WireFraming
//
//  Created by Peter Hunt on 29/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class SummaryCell: UITableViewCell {
  var pages: [String] = ["P4", "P5", "P6", "P7", "P8"]
  var coverFiles: [String] = ["ev2cover.jpg", "ev3cover.jpg", "ev4cover.jpg", "ev5cover.jpg","ev6cover.jpg"]
  var videoTitles: [String] = ["ev2_title", "ev3_title", "ev4_title", "ev5_title","ev6_title"]
  let horizontalPadding = 30
  
  // Cache the highlight color
  static let cardBackgroundHighlightColor = Style.cardBackgroundColor.shadow(withLevel: 0.25).blended(withFraction: 0.20, of: Style.primaryColor)
  
  @IBOutlet weak var disclosureImageView: UIImageView!
  @IBOutlet weak var thumbnail: UIImageView!
  @IBOutlet weak var backgroundCardView: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  
  weak var strings: StringsModel?
  weak var model: DataModel?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    // Customise the cell colours and set a corner radius on the backgroundCardView to make the cell look like a 'card'
    contentView.backgroundColor = Style.backgroundColor     // larger cell background
    backgroundCardView.backgroundColor = Style.cardBackgroundColor // card background
    backgroundCardView.layer.cornerRadius = 6.0
    backgroundCardView.layer.masksToBounds = false
    
    
    // Fonts and text colors
    //nameLabel.font = UIFont.customTitle1
    nameLabel.textColor = Style.primaryTextColor
    
    nameLabel.snp.makeConstraints { (make) in
      make.left.equalTo(thumbnail.snp.right).offset(10)
      make.right.equalTo(contentView.snp.right).inset(60)
//      make.top.equalTo(thumbnail.snp.top).offset(5)
      make.centerY.equalTo(thumbnail)
      //make.height.equalTo(44)
    }
    
     disclosureImageView.setIcon(icon: .googleMaterialDesign(.chevronRight), textColor: Style.primaryColor, backgroundColor: .clear, size: nil)
    
    thumbnail.layer.cornerRadius = 6.0
    thumbnail.clipsToBounds = true
    
  }
  
  
  /// Populate the cell with data from the plan
  ///
  /// - Parameter plan: plan to populate cell with
  func configure(row: Int) {
    guard let _ = model, let strings = strings else { return }
    if row < 0 || row >= pages.count { return }
    
    thumbnail.image = UIImage(named: coverFiles[row])
    nameLabel.text = strings[videoTitles[row]]
    
    // Remove all existing UILabels from the view except nameLabel
    // Remove all existing UIImageViews except thumbnail
    for view in contentView.subviews {
      if view is UILabel && view != nameLabel {
        view.removeFromSuperview()
      }
      
      if view is UIImageView && view != thumbnail && view != disclosureImageView {
        view.removeFromSuperview()
      }
    }
    
    
    
    switch row {
      
    case 0: // P4
      addP4DataLabels()
      
    case 1: // P5
      addP5DataLabels()
      
    case 3: // P7
      addBeaconRaftImageViews()
      
    case 5: // Followup
      addFollowupLabels()
      
    default:
      break
    }
    
    
  }
  
  // populate p4Data with question and answer data for p4
  func addP4DataLabels() {
    guard let model = model, let strings = strings else { return }
    var lastControlBottom: ConstraintItem = thumbnail.snp.bottom
    var p4Data: [(q: String, a: String)] = []
    
    let q1 = model.replaceTokens(inString: strings.p4_s2_title)
    let a1 = model.get(Key.p4_s2) as? String ?? ""
    if !a1.isEmpty {
      p4Data.append((q: q1, a: a1))
    }
    
    let q2 = model.replaceTokens(inString: strings.p4_s3_title)
    var a2: String = ""
    if let answers = model.get(Key.p4_s3) as? [String] {
      if !answers.isEmpty {
        a2 = answers.joined(separator: ", ")
      }
    }
    if !a2.isEmpty {
      p4Data.append((q: q2, a: a2))
    }
    
    let q3 = model.replaceTokens(inString: strings.p4_s4_title)
    let a3 = model.get(Key.p4_s4) as? String ?? ""
    if !a3.isEmpty {
      p4Data.append((q: q3, a: a3))
    }
    
    let q4 = model.replaceTokens(inString: strings.p4_s5_title)
    let a4 = model.getChatDate()?.customFriendlyString() ?? ""
    if !a4.isEmpty {
      p4Data.append((q: q4, a: a4))
    }
    
    for (i, item) in p4Data.enumerated() {
      let labelQ = labelWith(text: item.q)
      contentView.addSubview(labelQ)
      labelQ.font = UIFont.customBody
      //labelQ.textColor = Style.secondaryTextColor
      
      labelQ.snp.makeConstraints { (make) in
        make.left.equalTo(thumbnail.snp.left).offset(0)
        make.right.equalTo(contentView.snp.right).inset(horizontalPadding)
        make.top.equalTo(lastControlBottom).offset(i == 0 ? 20 : 12)  // more space for first one
        //make.height.equalTo(44)
      }
      lastControlBottom = labelQ.snp.bottom
      
      let labelA = labelWith(text: item.a)
      labelA.font = UIFont.customBody.bolded
      contentView.addSubview(labelA)
      labelA.snp.makeConstraints { (make) in
        make.left.equalTo(thumbnail.snp.left).offset(0)
        make.right.equalTo(contentView.snp.right).inset(horizontalPadding)
        make.top.equalTo(lastControlBottom).offset(2)
        
        if i == (p4Data.count-1) { // last item
          make.bottom.equalTo(contentView.snp.bottom).inset(Const.VerticalPadding)
        }
        //make.height.equalTo(44)
      }
      lastControlBottom = labelA.snp.bottom
    }
    
  }
  
  
  
  // populate p5Data with question and answer data for p4
  func addP5DataLabels() {
    guard let model = model, let strings = strings else { return }
    var lastControlBottom: ConstraintItem = thumbnail.snp.bottom
    var p5Data: [(q: String, a: String)] = []
    
    let q1 = model.replaceTokens(inString: strings.p5_s1_title)
    let a1 = model.get(Key.p5_s1) as? String ?? ""
    if !a1.isEmpty {
      p5Data.append((q: q1, a: a1))
    }
    
    let q2 = model.replaceTokens(inString: strings.p5_s2_title)
    var a2: String = ""
    if let answers = model.get(Key.p5_s2) as? [String] {
      if !answers.isEmpty {
        a2 = answers.joined(separator: ", ")
        if answers.contains("Other") {
          if let otherText = model.get(Key.p5_s2_other) as? String {
            a2 += ": \(otherText)"
          }
        }
      }
    }
    if !a2.isEmpty {
      p5Data.append((q: q2, a: a2))
    }
    
    let q3 = model.replaceTokens(inString: strings.p5_s3_title)
    var a3: String = ""
    if let answers = model.get(Key.p5_s3) as? [String] {
      if !answers.isEmpty {
        a3 = answers.joined(separator: ", ")
        if answers.contains("Other") {
          if let otherText = model.get(Key.p5_s3_other) as? String {
            a3 += ": \(otherText)"
          }
        }
      }
    }
    if !a3.isEmpty {
      p5Data.append((q: q3, a: a3))
    }
    
    let q4 = model.replaceTokens(inString: strings.p5_s4_title)
    let a4 = model.get(Key.p5_s4) as? String ?? ""
    if !a4.isEmpty {
      p5Data.append((q: q4, a: a4))
    }
    
    let q5 = model.replaceTokens(inString: strings.p5_s5_title)
    let a5 = model.get(Key.p5_s5) as? String ?? ""
    if !a5.isEmpty {
      p5Data.append((q: q5, a: a5))
    }
    
    
    for (i, item) in p5Data.enumerated() {
      let labelQ = labelWith(text: item.q)
      contentView.addSubview(labelQ)
      labelQ.font = UIFont.customBody
      //labelQ.textColor = Style.secondaryTextColor
      labelQ.snp.makeConstraints { (make) in
        make.left.equalTo(thumbnail.snp.left).offset(0)
        make.right.equalTo(contentView.snp.right).inset(horizontalPadding)
        make.top.equalTo(lastControlBottom).offset(i == 0 ? 20 : 12)
        //make.height.equalTo(44)
      }
      lastControlBottom = labelQ.snp.bottom
      
      let labelA = labelWith(text: item.a)
      labelA.font = UIFont.customBody.bolded
      contentView.addSubview(labelA)
      labelA.snp.makeConstraints { (make) in
        make.left.equalTo(thumbnail.snp.left).offset(0)
        make.right.equalTo(contentView.snp.right).inset(horizontalPadding)
        make.top.equalTo(lastControlBottom).offset(2)
        
        if i == (p5Data.count-1) { // last item
          make.bottom.equalTo(contentView.snp.bottom).inset(Const.VerticalPadding)
        }
        //make.height.equalTo(44)
      }
      lastControlBottom = labelA.snp.bottom
    }
    
  }
  
  
  // populate followUp with question and answer data
  func addFollowupLabels() {
    guard let model = model, let strings = strings else { return }
    var lastControlBottom: ConstraintItem = thumbnail.snp.bottom
    var followupData: [(q: String, a: String)] = []
    
    let q1 = model.replaceTokens(inString: strings.followup_s3_title)
    let a1 = model.get(.followup_s3) as? String ?? ""
    
    if !a1.isEmpty {
      followupData.append((q: q1, a: a1))
    }
    
    let q2 = model.replaceTokens(inString: strings.followup_s4_title)
    let a2 = model.getChatDate()?.customFriendlyString() ?? ""
    if !a2.isEmpty {
      followupData.append((q: q2, a: a2))
    }
    
    let q3 = model.replaceTokens(inString: strings.followup_s5_title)
    let a3 = model.get(.followup_s5) as? String ?? ""
    if !a3.isEmpty {
      followupData.append((q: q3, a: a3))
    }
    
    
    for (i, item) in followupData.enumerated() {
      let labelQ = labelWith(text: item.q)
      contentView.addSubview(labelQ)
      labelQ.font = UIFont.customBody
      //labelQ.textColor = Style.secondaryTextColor
      
      labelQ.snp.makeConstraints { (make) in
        make.left.equalTo(thumbnail.snp.left).offset(0)
        make.right.equalTo(contentView.snp.right).inset(horizontalPadding)
        make.top.equalTo(lastControlBottom).offset(i == 0 ? 20 : 12)  // more space for first one
        //make.height.equalTo(44)
      }
      lastControlBottom = labelQ.snp.bottom
      
      let labelA = labelWith(text: item.a)
      labelA.font = UIFont.customBody.bolded
      contentView.addSubview(labelA)
      labelA.snp.makeConstraints { (make) in
        make.left.equalTo(thumbnail.snp.left).offset(0)
        make.right.equalTo(contentView.snp.right).inset(horizontalPadding)
        make.top.equalTo(lastControlBottom).offset(2)
        
        if i == (followupData.count-1) { // last item
          make.bottom.equalTo(contentView.snp.bottom).inset(Const.VerticalPadding)
        }
        //make.height.equalTo(44)
      }
      lastControlBottom = labelA.snp.bottom
    }
    
  }

  
  
  func addBeaconRaftImageViews() {
    var lastControlBottom: ConstraintItem = thumbnail.snp.bottom
    
    let beaconImageView = UIImageView(image: Style.imageOfBeaconButton)
    contentView.addSubview(beaconImageView)
    beaconImageView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(20)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(60)
      make.height.equalTo(beaconImageView.snp.width).multipliedBy(135.0/500.0)
    }
   lastControlBottom = beaconImageView.snp.bottom

    let raftImageView = UIImageView(image: Style.imageOfRaftButton)
    contentView.addSubview(raftImageView)
    raftImageView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-5)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(60)
      make.height.equalTo(beaconImageView.snp.width).multipliedBy(135.0/500.0)
      make.bottom.equalTo(contentView.snp.bottom).inset(Const.VerticalPadding)
    }
    lastControlBottom = raftImageView.snp.bottom
    
  }
  
  
  func labelWith(text: String, tag: Int? = nil) -> UILabel {
    let label = UILabel()
    if let tag = tag {
      label.tag = tag
    }
    label.text = text
    label.textColor = Style.primaryTextColor
    label.font = UIFont.customBody

    label.numberOfLines = 0
    return label
  }
  
  
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    if selected {
      backgroundCardView.backgroundColor = MainMenuCell.cardBackgroundHighlightColor
    } else {
      backgroundCardView.backgroundColor = Style.cardBackgroundColor
    }
    
  }
  
  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    if highlighted {
      backgroundCardView.backgroundColor = MainMenuCell.cardBackgroundHighlightColor
    } else {
      backgroundCardView.backgroundColor = Style.cardBackgroundColor
    }
  }
  
  
  func parentTableView() -> UITableView? { // iterate up the view hierarchy to find the table containing this cell/view
    var parent: UIView? = self.superview
    while parent != nil {
      if let tableView = parent as? UITableView {
        return tableView
      }
      parent = parent?.superview
    }
    return nil
  }
  
}


