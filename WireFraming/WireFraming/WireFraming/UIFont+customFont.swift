//
//  UIFont+customFont.swift
//  WireFraming
//
//  Created by Peter Hunt on 2/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

// Custom fonts used throughout the App
// Respect user preferred font sizes

extension UIFont {
  
  func smallCaps() -> UIFont {
    let settings = [[convertFromUIFontDescriptorFeatureKey(UIFontDescriptor.FeatureKey.featureIdentifier): kLowerCaseType, convertFromUIFontDescriptorFeatureKey(UIFontDescriptor.FeatureKey.typeIdentifier): kLowerCaseSmallCapsSelector]]
    let attributes: [String: AnyObject] = [convertFromUIFontDescriptorAttributeName(UIFontDescriptor.AttributeName.featureSettings): settings as AnyObject, convertFromUIFontDescriptorAttributeName(UIFontDescriptor.AttributeName.name): fontName as AnyObject]
    return UIFont(descriptor: UIFontDescriptor(fontAttributes: convertToUIFontDescriptorAttributeNameDictionary(attributes)), size: pointSize)
  }
  
  /// Returns a bold version of `self`
  public var bolded: UIFont {
    return fontDescriptor.withSymbolicTraits(.traitBold)
      .map { UIFont(descriptor: $0, size: 0) } ?? self
  }
  
  /// Returns an italic version of `self`
  public var italicized: UIFont {
    return fontDescriptor.withSymbolicTraits(.traitItalic)
      .map { UIFont(descriptor: $0, size: 0) } ?? self
  }
  
  /// Returns a scaled version of `self`
  func scaled(scaleFactor: CGFloat) -> UIFont {
    let newDescriptor = fontDescriptor.withSize(fontDescriptor.pointSize * scaleFactor)
    return UIFont(descriptor: newDescriptor, size: 0)
  }
  
  /*
   static let body: UIFontTextStyle
   static let callout: UIFontTextStyle
   static let caption1: UIFontTextStyle
   static let caption2: UIFontTextStyle
   static let footnote: UIFontTextStyle
   static let headline: UIFontTextStyle
   static let subheadline: UIFontTextStyle
   static let title1: UIFontTextStyle
   static let title2: UIFontTextStyle
   static let title3: UIFontTextStyle
   */
  /*
   Custom fonts: Lato, ["Lato-Semibold", "Lato-Regular", "Lato-Hairline", "Lato-Thin", "Lato-HairlineItalic", "Lato-Medium", "Lato-ThinItalic", "Lato-LightItalic", "Lato-Italic", "Lato-Bold", "Lato-SemiboldItalic", "Lato-BoldItalic", "Lato-MediumItalic", "Lato-Black", "Lato-HeavyItalic", "Lato-Light", "Lato-BlackItalic", "Lato-Heavy"]
   */
  
  public static let customHeadline: UIFont = UIFont.customFont(forStyle: .headline)
  public static let customSubheadline: UIFont = UIFont.customFont(forStyle: .subheadline).smallCaps()
  public static let customTitle1: UIFont = UIFont.customFont(forStyle: .title1)
  public static let customTitle2: UIFont = UIFont.customFont(forStyle: .title2)
  public static let customTitle3: UIFont = UIFont.customFont(forStyle: .title3)
  public static let customCaption1: UIFont = UIFont.customFont(forStyle: .caption1)
  public static let customCaption2: UIFont = UIFont.customFont(forStyle: .caption2)
  public static let customBody: UIFont = UIFont.customFont(forStyle: .body)
  public static let customBodyItalic: UIFont = UIFont.customFont(forStyle: .body).italicized
  
  
  private static let customFonts: [UIFont.TextStyle : String] =
    [.headline : "Lato-Medium",
     .subheadline : "Lato-Medium",
     .title1 : "Lato-Medium",
     .title2 : "Lato-Medium",
     .title3 : "Lato-Semibold",
     .caption1 : "Lato-Semibold",
     .caption2 : "Lato-Regular",
     .body : "Lato-Regular"]
  
  
  /// Return the custom font for the text style
  ///
  /// - Parameter forStyle: UIFontTextStyles - .headline, .subheadline, .title1, .title2, .body
  /// - Returns: custom font for that style
  static func customFont(forStyle: UIFont.TextStyle) -> UIFont {
    let preferredDescriptor = UIFont.preferredFont(forTextStyle: forStyle)
    if let name = customFonts[forStyle],
      let font = UIFont(name: name, size: preferredDescriptor.pointSize) {
      return font
    } else {
      return preferredDescriptor
    }
  }
  
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIFontDescriptorFeatureKey(_ input: UIFontDescriptor.FeatureKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIFontDescriptorAttributeName(_ input: UIFontDescriptor.AttributeName) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIFontDescriptorAttributeNameDictionary(_ input: [String: Any]) -> [UIFontDescriptor.AttributeName: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIFontDescriptor.AttributeName(rawValue: key), value)})
}
