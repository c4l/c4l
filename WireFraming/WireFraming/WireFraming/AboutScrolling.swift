//
//  AboutScrolling.swift
//  WireFraming
//
//  Created by Peter Hunt on 24/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit


/*
 About Scrolling Form
 Back/Finish Button
 */

class AboutScrolling: ScrollingForm {
  
  var isFirstRun: Bool = false
  
  let verticalPadding = 25
  let horizontalPadding = 30
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    print("AboutScrolling viewDidLoad")
    
    addSection1()
    
    //addFinishButton()
    //nextButton?.setTitle("Done", for: .normal)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
  }
  
  deinit {
    print("AboutScrolling deinit")
  }
  
  
  
  
  func addSection1() {
    /*
     
     FUNDED BY
     
     QMHC LOGO
     
     DEVELOPED BY
     
     CONNETICA LOGO
     
     UNIVERSITY OF THE SUNSHINE COAST LOGO
     
     ENGAGE LABS LOGO
     
     
     */
    let label1 = customLabel(text: "FUNDED BY")
    contentView.addSubview(label1)
    label1.snp.makeConstraints { (make) in
      make.top.equalTo(contentView.snp.top).offset(verticalPadding)
      //make.left.equalTo(contentView).inset(Const.HorizontalPadding * 3)
      make.centerX.equalTo(contentView)
    }
    lastControlBottom = label1.snp.bottom
    
    
    let logo1 = UIButton(type: .custom)
    logo1.setImage(UIImage(named: "qmhc_logo"), for: .normal)
    logo1.imageView?.contentMode = .scaleAspectFit
    logo1.addTarget(self, action: #selector(qmhcAction), for: .touchUpInside)
    contentView.addSubview(logo1)
    logo1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(verticalPadding / 2)
      make.left.right.equalTo(contentView).inset(horizontalPadding)
    }
    lastControlBottom = logo1.snp.bottom
    
    
    let label2 = customLabel(text: "DEVELOPED BY")
    contentView.addSubview(label2)
    label2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(verticalPadding)
      //make.left.equalTo(contentView).inset(Const.HorizontalPadding * 3)
      make.centerX.equalTo(contentView)
    }
    lastControlBottom = label2.snp.bottom
    
    let logo2 = UIButton(type: .custom)
    logo2.setImage(UIImage(named: "connetica_logo"), for: .normal)
    logo2.imageView?.contentMode = .scaleAspectFit
    logo2.addTarget(self, action: #selector(conneticaAction), for: .touchUpInside)
    contentView.addSubview(logo2)
    logo2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(verticalPadding)
      make.left.right.equalTo(contentView).inset(horizontalPadding)
    }
    lastControlBottom = logo2.snp.bottom

    let uscLogo = UIButton(type: .custom)
    uscLogo.setImage(UIImage(named: "usc_logo"), for: .normal)
    uscLogo.imageView?.contentMode = .scaleAspectFit
    uscLogo.addTarget(self, action: #selector(uscAction), for: .touchUpInside)
    contentView.addSubview(uscLogo)
    uscLogo.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(verticalPadding)
      make.left.right.equalTo(contentView).inset(horizontalPadding)
    }
    lastControlBottom = uscLogo.snp.bottom
    
    
    let engageLogo = UIButton(type: .custom)
    engageLogo.setImage(UIImage(named: "engage_logo"), for: .normal)
    engageLogo.imageView?.contentMode = .scaleAspectFit
    engageLogo.addTarget(self, action: #selector(engageAction), for: .touchUpInside)
    contentView.addSubview(engageLogo)
    engageLogo.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(verticalPadding)
      make.left.right.equalTo(contentView).inset(horizontalPadding)
    }
    lastControlBottom = engageLogo.snp.bottom
    
    
    let versionLabel = customLabel(text: strings.app_name + " v" + appVersion())

    let tap = UITapGestureRecognizer(target: self, action: #selector(creditsAction))
    versionLabel.isUserInteractionEnabled = true
    versionLabel.addGestureRecognizer(tap)
    
    contentView.addSubview(versionLabel)
    versionLabel.font = UIFont.customCaption1
    versionLabel.textColor = Style.secondaryTextColor
    versionLabel.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(verticalPadding)
      make.centerX.equalTo(contentView)
      make.height.equalTo(50)
      make.bottom.equalTo(contentView.snp.bottom)
    }
    lastControlBottom = versionLabel.snp.bottom
    
    
  }
  
  @objc func qmhcAction() {
    strings["qmhc_url"].openUrl()
  }
  
  @objc func conneticaAction() {
    strings["connetica_url"].openUrl()
  }
  
  @objc func uscAction() {
    strings["usc_url"].openUrl()
  }
  
  @objc func engageAction() {
    strings["engage_url"].openUrl()
  }
  
  // Helper method - Get the app version and build as a string. eg. 4.0.2 (v4.0 build 2), or 5 (if build is 0)
  func appVersion() -> String {
    let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    let buildNo = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
    return buildNo == "0" ? "\(version)" : "\(version).\(buildNo)"
  }
  
  @objc func creditsAction() {
    print("creditsAction")
    let creditsController = newViewController(withId: "Credits")
    creditsController.title = "Credits - \(strings.app_name)"
    navigationController?.pushViewController(creditsController, animated: true)
}
    
  
  /*
   
   // MARK: - Button Actions
   // FINISH
   override func nextScreen() {
   // Return to Main Menu
   //    self.navigationController?.popToRootViewController(animated: true)
   self.navigationController?.popViewController(animated: true)
   
   }
   */
  
}





