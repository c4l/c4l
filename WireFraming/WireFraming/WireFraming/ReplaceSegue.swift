//
//  ReplaceSegue.swift
//  WireFraming
//
//  Created by Peter Hunt on 27/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit


// Custom segue to replace the view controllers in a navigation controller
open class ReplaceSegue: UIStoryboardSegue {
  
  open override func perform() {
      source.navigationController?.setViewControllers([destination], animated: true)
  }
  
}
