//
//  CustomBMPlayer.swift
//  WireFraming
//
//  Created by Peter Hunt on 4/6/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
//import BMPlayer

class BMPlayerCustom: BMPlayer {
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
//    panGesture = UIPanGestureRecognizer()
    self.removeGestureRecognizer(panGesture)
  }
  
  override public init(customControlView: BMPlayerControlView?) {
    super.init(customControlView: customControlView)
    /*
 panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panDirection(_:)))
 self.addGestureRecognizer(panGesture)
 */
//    panGesture = UIPanGestureRecognizer()
    self.removeGestureRecognizer(panGesture)
  }
 
}
