//
//  P6ViewController.swift
//  WireFraming
//
//  Created by Peter Hunt on 19/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import Eureka


/*
 P7: Follow Up
 */


class P7ViewController: MasterFormViewController, EventAccess {
  let videoName = "EV2"
  let videoFile = "ev1_1080p.mp4"
  let subtitleFile = "ev1.srt"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Get strings data for this page, and setup the form fields
    configureForm()
    
    // Injectable - load data from the model for this page
    self.loadForm()
    
  }
  
  
  func configureForm() {
    
    // ** Section 1
    let p7_s1_title = strings["p7_s1_title"]                     //
    let p7_s1_tag = strings["p7_s1_tag"]                         //
    let p7_s1_i1_placeholder = strings["p7_s1_i1_placeholder"]                             //
    
    let p7_s2_title = strings["p7_s2_title"]                     //
    let p7_s2_tag = strings["p7_s2_tag"]                         //
    let p7_s2_i1_placeholder = strings["p7_s2_i1_placeholder"]                             //
    
    let p7_s3_title = strings["p7_s3_title"]                     //
    let p7_s3_tag = strings["p7_s3_tag"]                         //
    let p7_s3_i1_placeholder = strings["p7_s3_i1_placeholder"]                             //
    
    
    
    form +++ CustomVideoSection() { section in
      section.tag = videoName
      (section as? CustomVideoSection)?.configure(videoName: videoName, videoFile: videoFile, subtitleFile: subtitleFile)
    }
    
    
    // ** Section 1
    form +++ Section(model.replaceTokens(inString: p7_s1_title)) {
      $0.tag = p7_s1_tag
      }
      <<< TextAreaRow("p7_s1_i1") {
        $0.placeholder = p7_s1_i1_placeholder
        $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
    }
    
    // ** Section 2 - When ?
    form +++ Section(model.replaceTokens(inString: p7_s2_title)) {
      $0.tag = p7_s2_tag
      }
      /*
       <<< TextAreaRow("p7_s2_i1") {
       $0.placeholder = p7_s2_i1_placeholder
       $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
       }*/
      
      <<< DateTimeInlineRow("p7_s2_i1") {
        $0.title = p7_s2_i1_placeholder
        $0.tag = p7_s2_tag
        //      $0.value = Date()
        }.onChange { [weak self] row in
          // Save changes to model
          if let followupDate = row.value {
            self?.model.plan?.followupDate = followupDate
          }
      }
      
      
      +++ SwitchRow("Add to calendar") {
        $0.title = $0.tag
        }.onChange { [weak self] row in
          
          // Schedule event in the user's calendar
          if row.value ?? false {
            // on
            let dateRow: DateTimeInlineRow? = self?.form.rowBy(tag: p7_s2_tag)
            if let scheduleDate = dateRow?.value {
              print("Attempting to schedule followup for \(scheduleDate)")
              if let conversationEventId = self?.addConversationEvent(startDate: scheduleDate, title: "Follow-up conversation with \((self?.model.getName())!)") {
                // save the conversation eventId
                print("Returned conversation event Id: \(conversationEventId)")
                
                //TODO: Toast that it's been added to their calendar
              } else {
                // error during saving event
                row.value = false
                row.updateCell()
                print("Error scheduling follow-up conversation event")
              }
            }
            
          } else {
            // off
          }
          
    }
    
    
    
    // ** Section 3
    form +++ Section(model.replaceTokens(inString: p7_s3_title)) {
      $0.tag = p7_s3_tag
      }
      <<< TextAreaRow("p7_s3_i1") {
        $0.placeholder = p7_s3_i1_placeholder
        $0.textAreaHeight = .dynamic(initialTextViewHeight: 50)
    }
    
    
    
    // ** Final Section - Next button
    form +++ Section("") {
      $0.hidden = false
      $0.tag = "NextButtonSection"
      }
      <<< ButtonRow("Next") { (row: ButtonRow) -> Void in
        row.title = "Next"
        }.onCellSelection { [weak self] (cell, row) in
          row.value = "submitted"
          self?.saveForm()
          // go to next page
          NotificationCenter.default.post(name: .changePageNotification, object: nil, userInfo:["page": 4])
    }
    
    
    
  }
  
  
}
