//
//  P5.swift
//  WireFraming
//
//  Created by Peter Hunt on 23/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import KMPlaceholderTextView
import BEMCheckBox
import SnapKit

/*
 P5 Scrolling Form
 
 Video (EV3)
 
 Skip/Next Button
 
 Section 1 - Who are the people who care for %s and love them? (UITextView)
 
 Section 2 - Who else may be able to help and support ? (checkboxes + 'other' UITextField)
 
 Section 3 - What could %s look forward to  ? (checkboxes + 'other' UITextField)
 
 Section 4 - What are some of <first name> favourite places and experiences ? (UITextView)
 
 Section 5 - What are some actions you can suggest that %s might take to improve their wellbeing ? (UITextView)
 
 Next Button
 
 */

class P5: ScrollingForm {
  
  let videoName = "EV3"
  let videoFile = "EV3.mp4"
  let subtitleFile = "EV3.srt"
  let coverFile = "ev3cover.jpg"
  let videoTitle = "ev3_title"
  
  var isFirstRun: Bool = false
  var speechView1, speechView2, speechView3, speechView4, speechView5: SpeechBubbleView!
  var s1TextView, s2OtherTextView, s3OtherTextView, s4TextView, s5TextView : KMPlaceholderTextView!
  var s2CheckItems: [String] = []
  var s3CheckItems: [String] = []
  var s2OtherCheckbox, s3OtherCheckbox: BEMCheckBox?
  
  var previousS1Text, previousS2OtherText, previousS3OtherText, previousS4Text, previousS5Text : String?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 200, right: 0)

/*
    if isFirstRun {
      addVideoPlayer(videoName: videoName, videoFile: videoFile, subtitleFile: subtitleFile, coverFile: nil, withSkipButton: true)

    } else {
      addVideoPlayer(videoName: videoName, videoFile: videoFile, subtitleFile: subtitleFile, coverFile: nil, withSkipButton: false)
      animateInVideoPlayer(withSkipButton: true, autoplay: false)
    }
  */
    
    addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: strings[videoTitle], subtitleFile: subtitleFile, coverFile: coverFile, withSkipButton: false)
    
    addSection1()
    addSection2()
    addSection3()
    addSection4()
    addSection5()
//    addBackNextControl()
    addBackNextButtons()
    
  }
  
  deinit {
    print("P5 deinit")
    if let player = player {
      player.prepareToDealloc()
    }
  }
  
  /*
  override func viewWillAppear(_ animated: Bool) {
   /*
    animateInVideoPlayer(withSkipButton: true, autoplay: false)
    animateInSections()
    */
  }
  */
  
  override func viewDidAppear(_ animated: Bool) {
    if !model.userSettings.hasEV3Autoplayed {
      player?.play()
      model.userSettings.hasEV3Autoplayed = true
    }
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("P5 viewWillDisappear")
    view.endEditing(true)
  }
  
  
  // MARK: - Section 1 - Who are the people who care for %s and love them? (UITextView)
  func addSection1() {
    speechView1 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.p5_s1_title))
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(10)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    
    s1TextView = customTextView(placeholder: strings.p5_s1_i1_placeholder)  // KMPlaceholderTextView
    s1TextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s1TextView)
    s1TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(80)
    }
    lastControlBottom = s1TextView.snp.bottom
    s1TextView.text = model.get(.p5_s1) as? String
  }

  
  // MARK: - Section 2 - Who else may be able to help and support ? (checkboxes + 'other' UITextField)
  func addSection2() {
/*
    <!-- Section 2 - Who else may be able to help and support ? -->
    <string name="p5_s2_title">Who else may be able to help and support %s ?</string>
    <string name="p5_s2_tag">HelpSection</string>
    <string name="p5_s2_i1_text">Select support options</string>
    <string name="p5_s2_i1_1">Family</string>
    <string name="p5_s2_i1_2">Friends</string>
    <string name="p5_s2_i1_3">Community Groups</string>
    <string name="p5_s2_i1_4">Professional Services</string>
    <string name="p5_s2_i1_5">Other</string>
*/
    
    s2CheckItems = [strings.p5_s2_i1_1, strings.p5_s2_i1_2, strings.p5_s2_i1_3, strings.p5_s2_i1_4, strings.p5_s2_i1_5]
    let itemsChecked = model.get(.p5_s2) as? [String] ?? []
    
    speechView2 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p5_s2_title))
    contentView.addSubview(speechView2)
    speechView2.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(weakSelf.contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(weakSelf.speechView2.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView2.snp.bottom
    
    for (i, item) in s2CheckItems.enumerated() {
      let checkboxLabel = customLabel(text: item, tag: 2000 + i)
      contentView.addSubview(checkboxLabel)
      checkboxLabel.snp.makeConstraints { (make) in
        make.left.equalTo(contentView).offset(40)
        make.top.equalTo(lastControlBottom).offset(i == 0 ? 0 : 30) // For 1st item, less of a vertical offset
      }
      lastControlBottom = checkboxLabel.snp.bottom
      
      let checkbox = customCheckbox(tag: 1000 + i)
      contentView.addSubview(checkbox)
      checkbox.delegate = self
      checkbox.snp.makeConstraints { (make) in
        make.centerY.equalTo(checkboxLabel)
        make.right.equalTo(contentView).offset(-40)
      }
      
      if itemsChecked.contains(item) {
        checkboxLabel.font = UIFont.customBody.bolded
        checkbox.setOn(true, animated: true)
      }
      
      // Keep track of the 'Other' checkbox
      if item == "Other" {
        s2OtherCheckbox = checkbox
      }
    }
    
    s2OtherTextView = customTextView(placeholder: model.replaceTokens(inString: strings.p5_s2_title))  // KMPlaceholderTextView
    s2OtherTextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s2OtherTextView)
    s2OtherTextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(30)
      make.left.equalTo(contentView).offset(40)
      make.right.equalTo(contentView).offset(-40)
      if let otherCheckbox = s2OtherCheckbox, otherCheckbox.on {
        make.height.equalTo(40)
      } else {
        make.height.equalTo(0)   // start hidden
      }
    }
    lastControlBottom = s2OtherTextView.snp.bottom
    s2OtherTextView.text = model.get(.p5_s2_other) as? String
  }

  
  // MARK: - Section 3 - What could %s look forward to  ? (checkboxes + 'other' UITextField)
  func addSection3() {
    /*
     <!-- Section 3 - What could <first name> look forward to  ? -->
     <string name="p5_s3_title">What could %s look forward to ?</string>
     <string name="p5_s3_tag">LookForwardSection</string>
     <string name="p5_s3_i1_text">Select</string>
     <string name="p5_s3_i1_1">A phone call tomorrow</string>
     <string name="p5_s3_i1_2">Lunch</string>
     <string name="p5_s3_i1_3">Going for a walk</string>
     <string name="p5_s3_i1_4">A nice meal</string>
     <string name="p5_s3_i1_5">Playing sport</string>
     <string name="p5_s3_i1_6">Going to the beach</string>
     <string name="p5_s3_i1_7">Other</string>
     */
    
    s3CheckItems = [strings.p5_s3_i1_1, strings.p5_s3_i1_2, strings.p5_s3_i1_3, strings.p5_s3_i1_4, strings.p5_s3_i1_5, strings.p5_s3_i1_6, strings.p5_s3_i1_7]
    let itemsChecked = model.get(.p5_s3) as? [String] ?? []
    
    speechView3 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.p5_s3_title))
    contentView.addSubview(speechView3)
    speechView3.snp.makeConstraints { [weak self] (make) in
      guard let weakSelf = self else { return }
      make.top.equalTo(weakSelf.lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(weakSelf.contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(weakSelf.speechView3.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView3.snp.bottom
    
    for (i, item) in s3CheckItems.enumerated() {
      let checkboxLabel = customLabel(text: item, tag: i + 4000)
      contentView.addSubview(checkboxLabel)
      checkboxLabel.snp.makeConstraints { (make) in
        make.left.equalTo(contentView).offset(40)
        make.top.equalTo(lastControlBottom).offset(i == 0 ? 0 : 30) // For 1st item, less of a vertical offset
      }
      lastControlBottom = checkboxLabel.snp.bottom
      
      let checkbox = customCheckbox(tag: i + 3000)
      contentView.addSubview(checkbox)
      checkbox.delegate = self
      checkbox.snp.makeConstraints { (make) in
        make.centerY.equalTo(checkboxLabel)
        make.right.equalTo(contentView).offset(-40)
      }
      
      if itemsChecked.contains(item) {
        checkboxLabel.font = UIFont.customBody.bolded
        checkbox.setOn(true, animated: true)
      }
      
      // Keep track of the 'Other' checkbox
      if item == "Other" {
        s3OtherCheckbox = checkbox
      }
    }
    
    s3OtherTextView = customTextView(placeholder: model.replaceTokens(inString: strings.p5_s3_title))  // KMPlaceholderTextView
    s3OtherTextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s3OtherTextView)
    s3OtherTextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(30)
      make.left.equalTo(contentView).offset(40)
      make.right.equalTo(contentView).offset(-40)
      if let otherCheckbox = s3OtherCheckbox, otherCheckbox.on {
        make.height.equalTo(40)
      } else {
        make.height.equalTo(0)   // start hidden
      }
    }
    lastControlBottom = s3OtherTextView.snp.bottom
    s3OtherTextView.text = model.get(.p5_s3_other) as? String
  }

  
  // MARK: - Section 4 - What are some of %s's favourite places and experiences ? (UITextView)
  func addSection4() {
    speechView4 = SpeechBubbleView(type: .secondary, text: model.replaceTokens(inString: strings.p5_s4_title))
    contentView.addSubview(speechView4)
    speechView4.snp.remakeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView4.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView4.snp.bottom
    
    s4TextView = customTextView(placeholder: strings.p5_s4_i1_placeholder)  // KMPlaceholderTextView
    s4TextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s4TextView)
    s4TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(80)
    }
    lastControlBottom = s4TextView.snp.bottom
    s4TextView.text = model.get(.p5_s4) as? String
  }
  
  
  // MARK: - Section 5 - What are some actions you can suggest that %s might take to improve their wellbeing ? (UITextView)
  func addSection5() {
    speechView5 = SpeechBubbleView(type: .primary, text: model.replaceTokens(inString: strings.p5_s5_title))
    contentView.addSubview(speechView5)
    speechView5.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView5.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView5.snp.bottom
    
    s5TextView = customTextView(placeholder: strings.p5_s5_i1_placeholder)  // KMPlaceholderTextView
    s5TextView.inputAccessoryView = keyboardToolbar()
    contentView.addSubview(s5TextView)
    s5TextView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(80)
    }
    lastControlBottom = s5TextView.snp.bottom
    s5TextView.text = model.get(.p5_s5) as? String
  }
  
  
  // Animate elements in from the bottom of the screen, and set s1TextView as first responder
  func animateInSections() {
    
    speechView1.center.y += view.bounds.height
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView1.center.y -= weakSelf.view.bounds.height
      }, completion: { [weak self] _ in
        guard let weakSelf = self else { return }
        if weakSelf.isFirstRun {
          weakSelf.s1TextView.becomeFirstResponder()
        }
    })

    s1TextView.center.y += view.bounds.height
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0.1,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.s1TextView.center.y -= weakSelf.view.bounds.height
      }, completion: { _ in })

    
    speechView2.center.y += view.bounds.height
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0.2,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView2.center.y -= weakSelf.view.bounds.height
      }, completion: { _ in
    })
    
    
  }
  
  override func transcriptButtonTapped() {
    print("P5 Transcript button tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName], title: strings[videoTitle], coverFile: coverFile)
  }
  
}


// MARK: BEMCheckBoxDelegate
extension P5: BEMCheckBoxDelegate {
  
  func didTap(_ checkBox: BEMCheckBox) {
    view.endEditing(true) // just in case another item has first responder
    
    // Find the label with the corresponding tag
    if let label = contentView.viewWithTag(checkBox.tag + 1000),
      let checkLabel = label as? UILabel,
      let textTapped = checkLabel.text {
      
      print("Checkbox tapped with tag: \(checkBox.tag), label: \(textTapped), on: \(checkBox.on)")
      
      // Section 2 checkboxes are tagged 1000-1099. Section 3 checkboxes are tagged 3000-3099
      var key: Key?
      if (1000...1099).contains(checkBox.tag) {
        key = .p5_s2
        
        if textTapped == "Other" {  // show or hide the 'Other' text box
          UIView.animate(withDuration: TimeInterval(Animation.CheckboxDuration)) { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.s2OtherTextView.snp.updateConstraints { (make) in
              make.height.equalTo(checkBox.on ? 40 : 0)
            }
            weakSelf.contentView.layoutIfNeeded()
          }
          if checkBox.on { s2OtherTextView.becomeFirstResponder() }
        } else {
          scrollToTop(view: speechView2)
        }
        
      } else if (3000...3099).contains(checkBox.tag) {
        key = .p5_s3
        
        if textTapped == "Other" {  // show or hide the 'Other' text box
          UIView.animate(withDuration: TimeInterval(Animation.CheckboxDuration)) { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.s3OtherTextView.snp.updateConstraints { (make) in
              make.height.equalTo(checkBox.on ? 40 : 0)
            }
            weakSelf.contentView.layoutIfNeeded()
          }
          if checkBox.on { s3OtherTextView.becomeFirstResponder() }
        } else {
          scrollToTop(view: speechView3)
        }
        
      }

      guard let sectionKey = key else { return } // make sure it is in section 2 or 3
      var itemsChecked = model.get(sectionKey) as? [String] ?? [] // load current selections for section 2 or 3 from model

      // Add or remove the item from itemsChecked
      if checkBox.on {
        checkLabel.font = UIFont.customBody.bolded
        if !itemsChecked.contains(textTapped) {
          itemsChecked.append(textTapped)
        }
      } else {  // off
        checkLabel.font = UIFont.customBody
        if let i = itemsChecked.index(of: textTapped) {
          itemsChecked.remove(at: i)
        }
      }
      
      model.set(sectionKey, value: itemsChecked)   // save change to model
      model.savePlans()
    }
  }
  
  func animationDidStop(for checkBox: BEMCheckBox) {
    print("Checkbox animation stop")
  }
  
}


// MARK: UITextViewDelegate
extension P5 {

  override func textViewDidBeginEditing(_ textView: UITextView) {
    
    print("P5 textViewDidBeginEditing")
    
    // Scroll to the top of this section, and record previous text
    switch textView {
      
    case s1TextView:
      scrollToTop(view: speechView1)
      previousS1Text = s1TextView.text
      
    case s2OtherTextView:
      if let s2Checkbox = s2OtherCheckbox {
        scrollToTop(view: s2Checkbox)
      }
      previousS2OtherText = s2OtherTextView.text
    
    case s3OtherTextView:
      if let s3Checkbox = s3OtherCheckbox {
        scrollToTop(view: s3Checkbox)
      }
      previousS3OtherText = s3OtherTextView.text
    
    case s4TextView:
      scrollToTop(view: speechView4)
      previousS4Text = s4TextView.text
      
    case s5TextView:
      scrollToTop(view: speechView5)
      previousS5Text = s5TextView.text
      
    default:
      break
    }
    
    textView.layer.borderColor = Style.primaryColor.cgColor
    
  }
  
  override func textViewDidEndEditing(_ textView: UITextView) {
    print("P5 textViewDidEndEditing")
    textView.layer.borderColor = Style.secondaryTextColor.cgColor
    // Save data if changed
    switch textView {
      
    case s1TextView:
      s1TextView.text = s1TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s1TextView.text != previousS1Text {
        model.set(.p5_s1, value: s1TextView.text)
        model.savePlans()
      }

    case s2OtherTextView:
      s2OtherTextView.text = s2OtherTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s2OtherTextView.text != previousS2OtherText {
        model.set(.p5_s2_other, value: s2OtherTextView.text)
        model.savePlans()
      }

    case s3OtherTextView:
      s3OtherTextView.text = s3OtherTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s3OtherTextView.text != previousS3OtherText {
        model.set(.p5_s3_other, value: s3OtherTextView.text)
        model.savePlans()
      }

    case s4TextView:
      s4TextView.text = s4TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s4TextView.text != previousS4Text {
        model.set(.p5_s4, value: s4TextView.text)
        model.savePlans()
      }
      
    case s5TextView:
      s5TextView.text = s5TextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
      if s5TextView.text != previousS5Text {
        model.set(.p5_s5, value: s5TextView.text)
        model.savePlans()
      }
      
    default:
      break
    }
    
  }
  
  func keyboardToolbar() -> UIToolbar  {
    let keyboardToolbar = UIToolbar()
    keyboardToolbar.backgroundColor = Style.secondaryColor.withAlpha(0.75)
    keyboardToolbar.sizeToFit()
    let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
    keyboardToolbar.items = [flexBarButton, doneBarButton]
    return keyboardToolbar
  }
  
  @objc func dismissKeyboard() {
    
    // Scroll to the top of the next section
    if s1TextView.isFirstResponder {
      //scrollToTop(view: speechView2)
    } else if s4TextView.isFirstResponder {
      //scrollToTop(view: speechView5)
    }
    
    view.endEditing(true)
  }
  
}





