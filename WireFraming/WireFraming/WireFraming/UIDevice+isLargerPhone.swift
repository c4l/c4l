//
//  UIDevice+isLargerPhone.swift
//  WireFraming
//
//  Created by Peter Hunt on 3/6/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {
  func isLargerPhone() -> Bool {
    let threePointFiveInchScreenHeight: CGFloat = 480
    let dimensionToCheck = max(UIScreen.main.bounds.size.height, UIScreen.main.bounds.size.width)
    let isLargerPhone = dimensionToCheck > threePointFiveInchScreenHeight && userInterfaceIdiom == .phone
    
    return isLargerPhone
  }
  
  func isSmallerPhone() -> Bool {
    return !isLargerPhone() && userInterfaceIdiom == .phone
  }
}
