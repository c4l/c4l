//
//  P6.swift
//  WireFraming
//
//  Created by Peter Hunt on 24/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
//import BMPlayer


/*
 P7 Scrolling Form
 
 Video (EV5)
 Video Summary ...
 Questions..
 Are you Okay ?
 
 You seem a bit flat...
 
 You seem pretty tired...
 
 I've had a bad week..
 
 Lifeline 13 11 14 or 000
 
 BEACON
 RAFT
 
 --
 
 Skip/Next Button
 
 BEACON
 
 RAFT
 
 Next Button
 */


class P7: ScrollingForm {
  let videoName = "EV5"
  let videoFile = "EV5.mp4"
  let subtitleFile = "EV5.srt"
  let coverFile = "ev5cover.jpg"
  let videoTitle = "ev5_title"
  
  var isFirstRun: Bool = false
  var speechView1, speechView2: SpeechBubbleView!
  var transcriptSpeechView1: SpeechBubbleView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: strings[videoTitle], subtitleFile: subtitleFile, coverFile: coverFile, withSkipButton: false, withTranscriptButton: true)
    addSection1()  // BEACON
    addSection2()  // RAFT
    addBackNextButtons()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    if !model.userSettings.hasEV5Autoplayed {
      player?.play()
      model.userSettings.hasEV5Autoplayed = true
    }
  }
  
  deinit {
    print("P7 deinit")
    if let player = player {
      player.prepareToDealloc()
    }
  }
  
  /*
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    print("P7 viewWillDisappear")
    if let player = player {
      player.pause()
    }
  }
  */
  
  // MARK: - Section 1 - B.E.A.C.O.N.
  func addSection1() {
    speechView1 = SpeechBubbleView(type: .secondary, text: strings.p7_s1_title)
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    
    let beaconImage = Style.imageOfBeaconButton
    let beaconButton = UIButton(type: .custom)
    beaconButton.setImage(beaconImage, for: .normal)
    contentView.addSubview(beaconButton)
    beaconButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(beaconButton.snp.width).multipliedBy(135.0/500.0)
    }
    lastControlBottom = beaconButton.snp.bottom
    beaconButton.addTarget(self, action: #selector(beaconButtonTapped), for: .touchUpInside)
  }
  
  // MARK: - Section 2 - R.A.F.T
  func addSection2() {
    speechView2 = SpeechBubbleView(type: .secondary, text: strings.p7_s2_title)
    contentView.addSubview(speechView2)
    speechView2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView2.snp.bottom

    
    let raftButtonImage = Style.imageOfRaftButton
    let raftButton = UIButton(type: .custom)
    raftButton.setImage(raftButtonImage, for: .normal)
    contentView.addSubview(raftButton)
    raftButton.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.centerX.equalTo(contentView)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(raftButton.snp.width).multipliedBy(135.0/500.0)
    }
    lastControlBottom = raftButton.snp.bottom
    raftButton.addTarget(self, action: #selector(raftButtonTapped), for: .touchUpInside)
    
    
    let raftLabel = customLabel(text: strings.p7_s2_text)
    contentView.addSubview(raftLabel)
    raftLabel.numberOfLines = 0
    raftLabel.font = UIFont.customHeadline
    raftLabel.sizeToFit()

    raftLabel.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.equalTo(contentView).inset(Const.HorizontalPadding * 4)
      //make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = raftLabel.snp.bottom
    
    
  }
  
  // Animate elements in from the bottom of the screen, and set s1TextView as first responder
  func animateInSections() {
    
    speechView1.center.y += view.bounds.height
    speechView2.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView1.center.y -= weakSelf.view.bounds.height
                    weakSelf.speechView2.center.y -= weakSelf.view.bounds.height
      }, completion: { _ in
        
        //        if self.isFirstRun {
        //          self.s1TextView.becomeFirstResponder()
        //        }
    })
  }
  
  // MARK: - Button Actions
  override func transcriptButtonTapped() {
    print("P7 Transcript button tapped")
    showNewBeaconRaftTranscriptView(type: .transcript, markdownString: strings[videoName], title: strings[videoTitle], coverFile: coverFile)
  }
  
  @objc func beaconButtonTapped() {
    print("P7 Beacon Button Tapped")
    showNewBeaconRaftTranscriptView(type: .beacon, markdownString: strings.p7_s1_i1_text)
  }
  
  @objc func raftButtonTapped() {
    print("P7 Raft Button Touched")
    showNewBeaconRaftTranscriptView(type: .raft, markdownString: strings.p7_s2_i1_text)
  }
  
}





