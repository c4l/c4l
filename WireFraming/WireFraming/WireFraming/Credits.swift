//
//  AboutScrolling.swift
//  WireFraming
//
//  Created by Peter Hunt on 24/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit


/*
 
 Credits Scrolling Form

 */

class Credits: ScrollingForm {
    
    var isFirstRun: Bool = false
    
    let verticalPadding = 4
    let horizontalPadding = 30
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Credits viewDidLoad")
        
        addContent()
        loopCredits()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    deinit {
        print("Credits deinit")
    }
    
   
    func loopCredits() {
        
        let frame = contentView.frame
        //let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - frame.size.height);

        scrollView.contentOffset = CGPoint(x: 0, y: -(frame.size.height - 240))
        
        UIView.animate(withDuration: 10.0) {
            [weak self] in
            self?.scrollView.contentOffset = CGPoint(x:0, y: frame.size.height + 140)
        }
        
        /*
        UIView.animate(withDuration: 10.0, animations: {
            
            [weak self] in
//            self?.scrollView.contentOffset = bottomOffset
            self?.scrollView.contentOffset = CGPoint.zero
        }, completion: { [weak self] _ in
            //self?.scrollView.contentOffset = CGPoint.zero
        
        })
        */
        
    }
    
    
    func addContent() {
        /*
         
         product manager
         DR CHRISTIAN JONES
         
         development
         PETER HUNT
         
         art
         THOMAS HAMLYN-HARRIS
         
         video
         BEN KING
         
         */
        
        
        
        let logo1 = UIButton(type: .custom)
        logo1.setImage(UIImage(named: "iTunesArtwork"), for: .normal)
//        logo1.setImage(Style.imageOfAppIconLarge, for: .normal)
        logo1.imageView?.contentMode = .scaleAspectFit
        logo1.addTarget(self, action: #selector(qmhcAction), for: .touchUpInside)
        contentView.addSubview(logo1)
        logo1.snp.makeConstraints { (make) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
        }
        lastControlBottom = logo1.snp.bottom
        
        
        
        let label1 = customLabel(text: "product manager")
        contentView.addSubview(label1)
        label1.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label1.snp.bottom
        
        let label2 = customLabel(text: "DR CHRISTIAN JONES")
        contentView.addSubview(label2)
        label2.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label2.snp.bottom
        
        
        let label3 = customLabel(text: "development")
        contentView.addSubview(label3)
        label3.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding * 3)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label3.snp.bottom
        
        let label4 = customLabel(text: "PETER HUNT")
        contentView.addSubview(label4)
        label4.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            //make.left.equalTo(contentView).inset(Const.HorizontalPadding * 3)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label4.snp.bottom
        
        let label5 = customLabel(text: "art")
        contentView.addSubview(label5)
        label5.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding * 3)
            //make.left.equalTo(contentView).inset(Const.HorizontalPadding * 3)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label5.snp.bottom
        
        let label6 = customLabel(text: "THOMAS HAMLYN-HARRIS")
        contentView.addSubview(label6)
        label6.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            //make.left.equalTo(contentView).inset(Const.HorizontalPadding * 3)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label6.snp.bottom
        
        let label7 = customLabel(text: "video")
        contentView.addSubview(label7)
        label7.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding * 3)
            make.centerX.equalTo(contentView)
        }
        lastControlBottom = label7.snp.bottom
        
        let label8 = customLabel(text: "BEN KING")
        contentView.addSubview(label8)
        label8.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            make.centerX.equalTo(contentView)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        lastControlBottom = label8.snp.bottom
        

        
        /*
        let logo2 = UIButton(type: .custom)
        logo2.setImage(UIImage(named: "connetica_logo"), for: .normal)
        logo2.imageView?.contentMode = .scaleAspectFit
        logo2.addTarget(self, action: #selector(conneticaAction), for: .touchUpInside)
        contentView.addSubview(logo2)
        logo2.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            make.left.right.equalTo(contentView).inset(horizontalPadding)
        }
        lastControlBottom = logo2.snp.bottom
        
        let uscLogo = UIButton(type: .custom)
        uscLogo.setImage(UIImage(named: "usc_logo"), for: .normal)
        uscLogo.imageView?.contentMode = .scaleAspectFit
        uscLogo.addTarget(self, action: #selector(uscAction), for: .touchUpInside)
        contentView.addSubview(uscLogo)
        uscLogo.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            make.left.right.equalTo(contentView).inset(horizontalPadding)
        }
        lastControlBottom = uscLogo.snp.bottom
        
        
        let engageLogo = UIButton(type: .custom)
        engageLogo.setImage(UIImage(named: "engage_logo"), for: .normal)
        engageLogo.imageView?.contentMode = .scaleAspectFit
        engageLogo.addTarget(self, action: #selector(engageAction), for: .touchUpInside)
        contentView.addSubview(engageLogo)
        engageLogo.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            make.left.right.equalTo(contentView).inset(horizontalPadding)
        }
        lastControlBottom = engageLogo.snp.bottom
        
        
        let versionLabel = customLabel(text: strings.app_name + " v" + appVersion())
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(engageAction))
        versionLabel.isUserInteractionEnabled = true
        versionLabel.addGestureRecognizer(tap)
        
        contentView.addSubview(versionLabel)
        versionLabel.font = UIFont.customCaption1
        versionLabel.textColor = Style.secondaryTextColor
        versionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(lastControlBottom).offset(verticalPadding)
            make.centerX.equalTo(contentView)
            make.height.equalTo(50)
            make.bottom.equalTo(contentView.snp.bottom)
        }
        lastControlBottom = versionLabel.snp.bottom
        */
        
    }
    
  @objc func qmhcAction() {
        strings["qmhc_url"].openUrl()
    }
    
    func conneticaAction() {
        strings["connetica_url"].openUrl()
    }
    
    func uscAction() {
        strings["usc_url"].openUrl()
    }
    
    func engageAction() {
        strings["engage_url"].openUrl()
    }
    
    // Helper method - Get the app version and build as a string. eg. 4.0.2 (v4.0 build 2), or 5 (if build is 0)
    func appVersion() -> String {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let buildNo = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        return buildNo == "0" ? "\(version)" : "\(version).\(buildNo)"
    }
    
    func creditsAction() {
        print("creditsAction")
        let creditsController = newViewController(withId: "Credits")
        creditsController.title = "Credits - \(strings.app_name)"
        presentingViewController?.navigationController?.pushViewController(creditsController, animated: true)
    }
    
    
    /*
     
     // MARK: - Button Actions
     // FINISH
     override func nextScreen() {
     // Return to Main Menu
     //    self.navigationController?.popToRootViewController(animated: true)
     self.navigationController?.popViewController(animated: true)
     
     }
     */
    
}





