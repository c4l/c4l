//
//  MainMenuCell.swift
//  WireFraming
//
//  Created by Peter Hunt on 29/4/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import BEMCheckBox

class MainMenuCell : UITableViewCell {
  
  // Cache the highlight color
  static let cardBackgroundHighlightColor = Style.cardBackgroundColor.shadow(withLevel: 0.25).blended(withFraction: 0.20, of: Style.primaryColor)
  
  @IBOutlet weak var backgroundCardView: UIView!
  @IBOutlet weak var calendarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!

  //@IBOutlet weak var progressView: ProgressView!
  //@IBOutlet weak var percentLabel: UILabel!
  //@IBOutlet weak var conversationDateImageView: UIImageView!
  //@IBOutlet weak var conversationDateLabel: UILabel!
  
  var scheduleLabel: UILabel!
  var scheduleCheckbox: BEMCheckBox!
  var scheduleButton: UIButton?   // button to schedule a chat or followup
  weak var model: DataModel?
  weak var plan: Plan?            // the plan for this cell
  
  var progressRatio: CGFloat = 0.06 {
    didSet {
      if oldValue != progressRatio {
        //progressView.progress = progressRatio
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()

    // Customise the cell colours and set a corner radius on the backgroundCardView to make the cell look like a 'card'
    contentView.backgroundColor = Style.backgroundColor     // larger cell background
    backgroundCardView.backgroundColor = Style.cardBackgroundColor // card background
    backgroundCardView.layer.cornerRadius = 6.0
    backgroundCardView.layer.masksToBounds = false
    
    /*
    // Shadow
//    backgroundCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
    backgroundCardView.layer.shadowOffset = CGSize(width: 2, height: 2) //%%% this shadow will hang slightly down and to the right
    backgroundCardView.layer.shadowOpacity = 0.5
    backgroundCardView.layer.shadowRadius = 2
    */
//    let path = UIBezierPath(rect: backgroundCardView.bounds)
//    backgroundCardView.layer.shadowPath = path.cgPath
    
    
    // Fonts and text colors
    //nameLabel.font = UIFont.customTitle1
    nameLabel.textColor = Style.primaryTextColor

    //percentLabel.font = UIFont.customBody
    //percentLabel.textColor = Style.primaryTextColor
    //conversationDateLabel.font = UIFont.customBody
    //conversationDateLabel.textColor = Style.primaryTextColor
    //progressView.progress = progressRatio
  
    
    calendarImageView.image = Style.imageOfCalendarDimmed
    
    
    // Add checkbox
    scheduleCheckbox = BEMCheckBox(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
    //scheduleCheckbox.delegate = self
    //checkbox.tag = tag
    scheduleCheckbox.boxType = .circle
    scheduleCheckbox.animationDuration = Animation.CheckboxDuration
    scheduleCheckbox.onAnimationType = .bounce
    //    checkbox.onFillColor = Style.primaryColor
    scheduleCheckbox.onTintColor = Style.primaryColor
    scheduleCheckbox.onCheckColor = Style.primaryColor
    scheduleCheckbox.offAnimationType = .fade
    scheduleCheckbox.tintColor = Style.primaryColor
    contentView.addSubview(scheduleCheckbox)
    scheduleCheckbox.snp.makeConstraints { (make) in
      make.top.equalTo(calendarImageView.snp.bottom).offset(10)
//      make.centerY.equalTo(scheduleLabel)
      make.centerX.equalTo(calendarImageView)
      make.height.equalTo(44)
      //      make.right.equalTo(contentView.snp.right).inset(3 * Const.HorizontalPadding)
    }
    
    // Add label for checkbox
    scheduleLabel = UILabel()
    scheduleLabel.textColor = Style.primaryTextColor
    scheduleLabel.font = UIFont.customBody
    scheduleLabel.numberOfLines = 0
    contentView.addSubview(scheduleLabel)
    scheduleLabel.snp.makeConstraints { (make) in
      make.left.equalTo(nameLabel.snp.left).offset(0)
//      make.top.equalTo(calendarImageView.snp.bottom).offset(20)
      make.centerY.equalTo(scheduleCheckbox)
      make.right.equalTo(contentView.snp.right).inset(50)
//      make.height.equalTo(44)
      //make.bottom.equalTo(cell.contentView.snp.bottom).inset(Const.VerticalPadding)
    }

    
    
    // Add button - schedule chat or followup button
    scheduleButton = UIButton.custom("Schedule chat", target: nil, action: nil)
    guard let button = scheduleButton else { return }
    contentView.addSubview(button)
    button.snp.makeConstraints { (make) in
      make.top.equalTo(scheduleCheckbox.snp.bottom).offset(20)
      make.left.right.equalTo(contentView).inset(3 * Const.HorizontalPadding)
      make.height.equalTo(60)
      make.bottom.equalTo(contentView.snp.bottom).inset(Const.VerticalPadding)
    }
    
    
  }
  
  func hideScheduleButton() {
    guard let button = scheduleButton else { return }
    button.snp.updateConstraints { (make) in
      make.height.equalTo(0)
      //make.top.equalTo(scheduleLabel.snp.bottom).offset(0)
    }
  }

  func showScheduleButton() {
    guard let button = scheduleButton else { return }
    button.snp.updateConstraints { (make) in
      make.height.equalTo(60)
      //make.top.equalTo(scheduleLabel.snp.bottom).offset(20)
    }
  }

  func hideScheduleLabelAndCheckbox() {
    //scheduleLabel.snp.updateConstraints { (make) in
    //make.height.equalTo(0)
    //}
    scheduleCheckbox.snp.updateConstraints { (make) in
      make.height.equalTo(0)
    }
    scheduleLabel.isHidden = true
    scheduleCheckbox.isHidden = true
    
    guard let button = scheduleButton else { return }
    button.snp.updateConstraints { (make) in
      make.top.equalTo(scheduleCheckbox.snp.bottom).offset(0)
    }
    
  }
  
  func showScheduleLabelAndCheckbox() {
    //scheduleLabel.snp.updateConstraints { (make) in
    //  make.height.equalTo(44)
    //}
    scheduleCheckbox.snp.updateConstraints { (make) in
      make.height.equalTo(44)
    }
    scheduleLabel.isHidden = false
    scheduleCheckbox.isHidden = false
    
    guard let button = scheduleButton else { return }
    button.snp.updateConstraints { (make) in
      make.top.equalTo(scheduleCheckbox.snp.bottom).offset(20)
    }
    
  }
  
  
  /// Populate the cell with data from the plan
  ///
  /// - Parameter plan: plan to populate cell with
  func configure() {
    guard let plan = plan else { return }
    nameLabel.text = plan.name
    
    /*
 Possible states for a plan:
     
     chatDate
     hadChat
     isFollowup
     
     - Not scheduled (no chatDate, hadChat=false, isFollowup = false:
     Show Button - "Schedule Chat" - link to page to schedule the chat
     
     - Chat or Followup Scheduled (Conversation Date, hadChat = false)
     Show tick box (unticked) "Chat/Followup on Day at Time". (depending on isFollowup). 
     When ticked, set hadConversation = true, change icon and show button.
     
     - Conversation Had (Conversation Date, hadChat = true)
     Show tick box (ticked), with text strike-out "Chat/Followup on Day at Time" (depending on isFollowup)
     Show button "Schedule Followup"
     
     
     
 */
    
    guard let chatDate = plan.chatDate else {
      /*
       - Not scheduled (no chatDate, hadChat=false, hadfollowup = false:
       Show Button - "Schedule Chat" - link to page to schedule the chat
       */
      
      calendarImageView.image = Style.imageOfCalendarNotScheduled
      
      hideScheduleLabelAndCheckbox()
      showScheduleButton()
      if let button = self.scheduleButton {
        button.setTitle("Schedule chat", for: .normal)
      }
      return
    }

    

    
    if plan.hadChat {
      /*
      - Conversation Had (Conversation Date, hadChat = true)
      Show tick box (ticked), with text strike-out "Chat/Followup on Day at Time" (depending on isFollowup)
      Show button "Schedule Followup"
      */
      calendarImageView.image = Style.imageOfCalendarCompleted
      
      
      //let scheduledText = plan.isFollowup ? "Followup " + chatDate.customFriendlyString() : "Chat " + chatDate.customFriendlyString()
      let scheduledText = "Completed"
      let attributedText = NSMutableAttributedString(string: scheduledText)
      attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, attributedText.length))
      scheduleLabel.textColor = Style.secondaryTextColor
      scheduleLabel.attributedText = attributedText
      
      scheduleCheckbox.on = true
      if let button = self.scheduleButton {
        button.setTitle("Schedule follow-up", for: .normal)
      }
      
      showScheduleLabelAndCheckbox()
      showScheduleButton()
      
      
    } else {
      /*
      - Chat or Followup Scheduled (Conversation Date, hadChat = false)
      Show tick box (unticked) "Chat/Followup on Day at Time". (depending on isFollowup).
      When ticked, set hadConversation = true, change icon and show button.
      */
      
      if let date = plan.chatDate {
        // if the date is overdue, show the badged calendar icon
        if Date().compare(.isLater(than: date)) {
          calendarImageView.image = Style.imageOfCalendarOverdue
        } else {
          calendarImageView.image = Style.imageOfCalendarScheduled
        }
      } else {
        calendarImageView.image = Style.imageOfCalendarNotScheduled
      }

      var scheduledText: String
      if plan.isFollowup {
        scheduledText = "Follow-up " + chatDate.customFriendlyString()
      } else {
        scheduledText = "Chat " + chatDate.customFriendlyString()
      }
      
      scheduleLabel.textColor = Style.primaryTextColor
      scheduleLabel.text = scheduledText
      
      scheduleCheckbox.on = false
      
      showScheduleLabelAndCheckbox()
      hideScheduleButton()
    }
    
    
    
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    if selected {
      backgroundCardView.backgroundColor = MainMenuCell.cardBackgroundHighlightColor
    } else {
      backgroundCardView.backgroundColor = Style.cardBackgroundColor
    }
    
  }
  
  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    if highlighted {
      backgroundCardView.backgroundColor = MainMenuCell.cardBackgroundHighlightColor
    } else {
      backgroundCardView.backgroundColor = Style.cardBackgroundColor
    }
  }
  
}


/*
extension MainMenuCell: BEMCheckBoxDelegate {
  
  func parentTableView() -> UITableView? { // iterate up the view hierarchy to find the table containing this cell/view
    var parent: UIView? = self.superview
    while parent != nil {
      if let tableView = parent as? UITableView {
        return tableView
      }
      parent = parent?.superview
    }
    return nil
  }
  
  func didTap(_ checkBox: BEMCheckBox) {
    guard let model = model,
          let plan = plan else { return }
    print("Checkbox tapped for plan: \(plan.name), \(plan.planId)")
 
    if checkBox.on {
      plan.hadChat = true
    } else {  // off
      plan.hadChat = false
    }
    
    model.planWithId(id: plan.planId)?.hadChat = plan.hadChat
    model.savePlans()
    
//    self.configure()
    
    /*
    
    // Find the label with the corresponding tag
    if let label = contentView.viewWithTag(checkBox.tag + 1000),
      let checkLabel = label as? UILabel,
      let textTapped = checkLabel.text {
      
      print("Checkbox tapped with tag: \(checkBox.tag), label: \(textTapped), on: \(checkBox.on)")
      
      // Add or remove the item
      var itemsChecked = model.get(.p4_s3) as? [String] ?? []
      if checkBox.on {
        checkLabel.font = UIFont.customBody.bolded
        if !itemsChecked.contains(textTapped) {
          itemsChecked.append(textTapped)
        }
        
      } else {  // off
        checkLabel.font = UIFont.customBody
        if let i = itemsChecked.index(of: textTapped) {
          itemsChecked.remove(at: i)
        }
        
      }
      
    }
    */
    
    
  }
  
  /*
  func animationDidStop(for checkBox: BEMCheckBox) {
    print("Checkbox animation stop")
    
    if let tableView = parentTableView() {
      tableView.reloadData()
    }
  }
  */
  
  
}
*/


