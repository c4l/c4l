//
//  Date+customFriendlyString.swift
//  WireFraming
//
//  Created by Peter Hunt on 5/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation

extension Date {
  
  /// Return a custom formatted human friendly string for the date
  ///
  /// - Returns: Examples: Tomorrow at 3:00 PM, May 22nd at 11:15 PM, Today at 8:30 PM
  func customFriendlyString() -> String {
    // if yesterday, today, or tomorrow, then use toStringWithRelativeTime() for friendly result
    // otherwise use "Date at Time" style
    if self.compare(.isYesterday) {
      return "yesterday at " + self.toString(format: .custom("h:mma"))
    } else if self.compare(.isToday) {
      return "today at " + self.toString(format: .custom("h:mma"))
    }  else if self.compare(.isTomorrow) {
      return "tomorrow at " + self.toString(format: .custom("h:mma"))
    } else {
      return self.toString(format: .custom("MMM d 'at' h:mma"))
    }

    
    /*
    if self.compare(.isYesterday) || self.compare(.isToday) || self.compare(.isTomorrow) {
      return self.toStringWithRelativeTime().capitalized + " at " + self.toString(format: .custom("h:mm a"))
//      return self.toStringWithRelativeTime()
    } else {
      return self.toString(format: .custom("MMM d 'at' h:mm a"))
//      return self.toString(format: .custom("MMM d"))
    }
    */
  }
  
}
