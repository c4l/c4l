//
//  Welcome.swift
//  WireFraming
//
//  Created by Peter Hunt on 27/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
//import AMGCalendarManager

/*
 Welcome Scrolling Form
 
 Section1 - Welcome to Chat for Life. This app helps you plan a conversation...
 
 Video (EV1)
 
 Skip/Next Button
 
 Section 2 - Switches for subtitles and passcode
 
 Next Button
 
 */

class Welcome: ScrollingForm {
  
  let videoName = "EV1"
  let videoFile = "EV1.mp4"
  let subtitleFile = "EV1.srt"
  let coverFile = "ev1cover.jpg"
  
  var isFirstRun: Bool = false
  var speechView1,speechView2, speechView3: SpeechBubbleView!
  var approvalSpeechView: SpeechBubbleView?
  var approvalView: UIView!
  var okButton: UIButton?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //model.isFirstLaunch = true //***** TESTING ONLY
    self.navigationController?.setToolbarHidden(true, animated: false)
    
    addSection1()
    
    // Add the video player underneath Section 1
    videoPlayerTop = lastControlBottom
    addVideoPlayer(videoName: videoName, videoFile: videoFile, videoTitle: nil, subtitleFile: subtitleFile, coverFile: nil, withSkipButton: true, withTranscriptButton: false)
   
    if model.isFirstLaunch {
      addApprovalSection()
      approvalSpeechView?.isHidden = true
      okButton?.isHidden = true
    }
    
    addSection2()
    addNextButton()
    
    speechView3.isHidden = true
    approvalView.isHidden = true
    nextButton?.isHidden = true
    
    animateSection1()
    // after section 1 has animated in, calls animateInVideoPlayer(withSkipButton: true, autoplay: true)
    // then skipButtonTapped animates in section 2 and next button
    
  }
  
  
  // MARK: - Section 1 - Welcome speech bubbles
  //  "Welcome to \(strings.app_name)"
  // "This app helps you plan a conversation with a friend. Check out this video to find out how."
  
  func addSection1() {
    
    speechView1 = SpeechBubbleView(type: .primary, text: "Welcome to \(strings.app_name)")
    contentView.addSubview(speechView1)
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(contentView).offset(10)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView1.snp.bottom
    
    speechView2 = SpeechBubbleView(type: .secondary, text: strings.welcome_s1_title)
    contentView.addSubview(speechView2)
    speechView2.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-15)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView2.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView2.snp.bottom
    
  }
  
  func animateSection1() {
    speechView1.center.y += view.bounds.height
    speechView2.center.y += view.bounds.height
    player?.isHidden = true
    skipButton?.isHidden = true
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView1.center.y -= weakSelf.view.bounds.height
      }, completion: nil)
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: Animation.DefaultDelay,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.speechView2.center.y -= weakSelf.view.bounds.height
      }, completion: { [weak self] _ in
        guard let weakSelf = self else { return }
        weakSelf.player?.isHidden = false
        weakSelf.skipButton?.isHidden = false
        weakSelf.animateInVideoPlayer(withSkipButton: true, autoplay: true)
    })
    
    
  }
  
  /// When skip/next button under the video is tapped:
  /// pause the video, hide the button,
  override func skipButtonTapped() {
    if let videoPlayer = player {
      videoPlayer.pause()
    }
    
    if model.isFirstLaunch {
      animateApprovalSection()
    } else {
     animateSection2()
    }
    
  }
  
  // MARK: - Approval Section - First Launch Only - After Skip/Next is tapped
  func addApprovalSection() {
    
    approvalSpeechView = SpeechBubbleView(type: .primary, text: strings.welcome_s2_i1_title)
    guard let approvalSpeechView = approvalSpeechView else { return }
    contentView.addSubview(approvalSpeechView)
    approvalSpeechView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-5)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      let height = contentView.frame.width * 160.0/500.0
//      make.height.equalTo(approvalSpeechView.snp.width).multipliedBy(160.0/500.0).priority(750)
      make.height.equalTo(height)
    }
    lastControlBottom = approvalSpeechView.snp.bottom
    
    // OK button to Approve Notifications
    okButton = UIButton.custom("OK", target: self, action: #selector(okButtonTapped))
    guard let button = okButton else { return }
    contentView.addSubview(button)
    
    button.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(0)
      make.centerX.equalTo(self.contentView.snp.centerX)
      make.width.equalTo(160)
      make.height.equalTo(50)
    }
    lastControlBottom = button.snp.bottom
    
  }
  
  @objc func okButtonTapped() {
    
    model.isFirstLaunch = false
    
    // Request authorisation for User Notifications
    if #available(iOS 10.0, *) {
      requestNotificationsAccess(options: [.alert, .badge, .sound]) { [weak self] allowed in
        if !allowed {
          print("Notifications access not granted")
          self?.model.userSettings.isNotificationsEnabled = true
          self?.model.userSettings.saveToDefaults()
        } else {
          print("Notifications access allowed")
          self?.model.userSettings.isNotificationsEnabled = true
          self?.model.userSettings.saveToDefaults()
        }
      }
    } else {
      // Fallback on earlier versions
    }
    /*
    // Request authorisation for Calendar access
    AMGCalendarManager.shared.requestAuthorization { allowed in
      if !allowed {
        print("Calendar access not granted")
      } else {
        print("Calendar access allowed")
      }
    }
 */
 
      /*
      { allowed in
      
      if !allowed {  // prompt to authorise the app in Settings
        
        let alertController = UIAlertController(title: "Calendar Permission", message: "This App requires permission to schedule events in your calendar. Go to Settings ?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) {
          (result : UIAlertAction) -> Void in
          print("You pressed OK")
          
          let settingsURL = URL(string: UIApplicationOpenSettingsURLString)
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsURL!, options: [:]) { success in
              
            }
          } else {
            // Fallback on earlier versions
          }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
          (result : UIAlertAction) -> Void in
          print("You pressed Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
      }
 
     
    }
*/
  
    
    
    if let speechView = self.approvalSpeechView {
      self.scrollToTop(view: speechView)
    }
    animateSection2()
  }
  
  
  // If first launch, after the Skip/Next button is tapped ...
  // Animate in approval section
  func animateApprovalSection() {
    approvalSpeechView?.isHidden = false
    okButton?.isHidden = false
    
    if let player = player {
      self.scrollToTop(view: player)
    }
    
    approvalSpeechView?.center.y += view.bounds.height
    okButton?.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.skipButton?.snp.updateConstraints { (make) in
                      make.height.equalTo(0)
                    }
                    weakSelf.approvalSpeechView?.center.y -= weakSelf.view.bounds.height
                    weakSelf.okButton?.center.y -= weakSelf.view.bounds.height
                    
      }, completion: { _ in

    })
    
  }

  
  
  // MARK: - Section 2 - After Skip/Next is tapped
  func addSection2() {
    
    speechView3 = SpeechBubbleView(type: .primary, text: strings.welcome_s2_i2_title)
    contentView.addSubview(speechView3)
    speechView3.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(Const.VerticalPadding)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(speechView3.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    lastControlBottom = speechView3.snp.bottom
    
    // Approve Notifications and set PIN code
    approvalView = UIView()
    contentView.addSubview(approvalView)
    approvalView.snp.makeConstraints { (make) in
      make.top.equalTo(lastControlBottom).offset(-15)
      make.left.right.equalTo(contentView).inset(Const.HorizontalPadding)
      make.height.equalTo(130).priority(750)
    }
    lastControlBottom = approvalView.snp.bottom
    
    let subTitlesLabel = UILabel()
    subTitlesLabel.text = "Subtitles"
    subTitlesLabel.textColor = Style.primaryTextColor
    subTitlesLabel.font = UIFont.customBody
    approvalView.addSubview(subTitlesLabel)
    subTitlesLabel.snp.makeConstraints { (make) in
      make.left.equalTo(approvalView).offset(40)
      make.top.equalTo(approvalView).offset(10)
      make.height.equalTo(40)
    }
    
    let subTitlesSwitch = UISwitch()
    subTitlesSwitch.isOn = model.userSettings.isSubtitlesEnabled // default is on
    subTitlesSwitch.tag = 0
    subTitlesSwitch.addTarget(self, action: #selector(switchValueDidChange(sender:)), for: .valueChanged)
    approvalView.addSubview(subTitlesSwitch)
    
    subTitlesSwitch.snp.makeConstraints { (make) in
      make.centerY.equalTo(subTitlesLabel)
      make.right.equalTo(approvalView).offset(-40)
      make.height.equalTo(40)
    }
    
    let pinLabel = UILabel()
    pinLabel.text = "Passcode / Touch ID"
    pinLabel.textColor = Style.primaryTextColor
    pinLabel.font = UIFont.customBody
    
    approvalView.addSubview(pinLabel)
    pinLabel.snp.makeConstraints { (make) in
      make.left.equalTo(approvalView).offset(40)
      make.top.equalTo(subTitlesLabel.snp.bottom).offset(20)
      //      make.height.equalTo(40)
    }
    
    let pinSwitch = UISwitch()
    pinSwitch.isOn = model.userSettings.isPasscodeEnabled // default is off
    
    pinSwitch.tag = 2
    pinSwitch.addTarget(self, action: #selector(switchValueDidChange(sender:)), for: .valueChanged)
    approvalView.addSubview(pinSwitch)
    
    pinSwitch.snp.makeConstraints { (make) in
      make.centerY.equalTo(pinLabel)
      make.right.equalTo(approvalView).offset(-40)
      //      make.height.equalTo(40)
    }

    
    let footerLabel = customLabel(text: strings["welcome_s2_i3"])  // Privacy note footer
    footerLabel.numberOfLines = 0
    footerLabel.font = UIFont.customCaption1
    footerLabel.textColor = Style.secondaryTextColor
    approvalView.addSubview(footerLabel)
    footerLabel.snp.makeConstraints { (make) in
      make.top.equalTo(pinLabel.snp.bottom).offset(20)
      make.left.right.equalTo(approvalView).inset(Const.HorizontalPadding)
//      make.left.equalTo(pinLabel.snp.left)
//      make.right.equalTo(pinSwitch.snp.right)
    }
    
    lastControlBottom = approvalView.snp.bottom
  }
  
  // After the Skip/Next button is tapped ...
  // Animate in Section 2
  func animateSection2() {
    
//    self.skipButton?.snp.updateConstraints { (make) in
//      make.height.equalTo(0)
//    }
    
    speechView3.isHidden = false
    approvalView.isHidden = false
    nextButton?.isHidden = false
    self.approvalSpeechView?.isHidden = true
    
    if let player = player {
      self.scrollToTop(view: player)
    }
    speechView3.center.y += view.bounds.height
    approvalView.center.y += view.bounds.height
    nextButton?.center.y += view.bounds.height
    
    UIView.animate(withDuration: Animation.DefaultDuration, delay: 0,
                   usingSpringWithDamping: Animation.DefaultDamping,
                   initialSpringVelocity: Animation.DefaultVelocity,
                   options: [], animations: { [weak self] in
                    guard let weakSelf = self else { return }
                    weakSelf.skipButton?.snp.updateConstraints { (make) in
                      make.height.equalTo(0)
                    }
                    weakSelf.approvalSpeechView?.snp.updateConstraints { (make) in
                      make.height.equalTo(0)
                    }
                    weakSelf.okButton?.snp.updateConstraints { (make) in
                      make.height.equalTo(0)
                    }
                    weakSelf.speechView3.center.y -= weakSelf.view.bounds.height
                    weakSelf.approvalView.center.y -= weakSelf.view.bounds.height
                    weakSelf.nextButton?.center.y -= weakSelf.view.bounds.height
                    
      }, completion: { _ in
//        self.scrollToTop(view: self.player!)
        
        //                self.continueButton.isHidden = false
        //                self.scrollView.scrollRectToVisible(approvalView.frame, animated: true)
        //self.scrollToTop(view: self.speechView3)
        
    })
    
  }

  
  @objc func switchValueDidChange(sender: UISwitch!) {
    
    if sender.tag == 0 { // Subtitles
      print("Welcome subtitles switch changed")
      model.userSettings.isSubtitlesEnabled = sender.isOn
      
    } else if sender.tag == 1 { // Notifications
      print("Welcome notifications switch changed")
      
      if sender.isOn {
        // Request authorisation for User Notifications
        if #available(iOS 10.0, *) {
          requestNotificationsAccess(options: [.alert, .badge, .sound]) { [weak self] allowed in
            if !allowed {
              print("Notifications access not granted")
            } else {
              self?.model.userSettings.isNotificationsEnabled = true
            }
          }
        } else {
          // Fallback on earlier versions
        }
        
      } else {
        self.model.userSettings.isNotificationsEnabled = false
      }
      
    } else if sender.tag == 2 { // Passcode
      
      print("Welcome Passcode switch changed")
      if sender.isOn {  // Set a new passcode - from Injectable
        presentModalPasscodeController(setNewPasscode: true) { [weak self] in
          self?.model.isUnlocked = true
          self?.model.userSettings.isPasscodeEnabled = true
        }
      } else {
        self.model.isUnlocked = true
        self.model.userSettings.isPasscodeEnabled = false
      }
      
    }
    
    self.model.userSettings.saveToDefaults()
        
  }

  override func nextScreen() {
    print("Welcome nextScreen()")
    
    let mainMenuScreen = newViewController(withId: "MainMenu")
    navigationController?.setViewControllers([mainMenuScreen], animated: true)
  }

  
  
}
