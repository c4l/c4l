//
//  SpeechBubbleView.swift
//  WireFraming
//
//  Created by Peter Hunt on 11/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

enum SpeechBubbleType {
  case primary, secondary, tertiary, tertiaryLarge
}

// View with a custom CALayer and Core Animation
class SpeechBubbleView: UIView {
  
  var bubbleType: SpeechBubbleType = .primary
  var speechLabel: UILabel!
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    genericInit()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    genericInit()
  }
  
  init(type: SpeechBubbleType, text: String) {
    super.init(frame: CGRect.zero)
    bubbleType = type
    genericInit()
    speechLabel.text = text
  }
  
  private func genericInit() {
    self.backgroundColor = .clear
    
    speechLabel = UILabel()
    speechLabel.numberOfLines = 0
    speechLabel.adjustsFontSizeToFitWidth = true
    speechLabel.font = UIFont.customBodyItalic
    speechLabel.textColor = Style.inverseTextColor
    speechLabel.textAlignment = .center
//    speechLabel.backgroundColor = UIColor.gray.withAlpha(0.3)
    self.addSubview(speechLabel)
    
    if [.tertiary, .tertiaryLarge].contains(bubbleType) {
      speechLabel.font = UIFont.customBody
      speechLabel.textColor = Style.primaryTextColor
      speechLabel.textAlignment = .left
    }

  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    if bubbleType == .primary {
//      speechLabel.frame = CGRect(x: 25, y: 20, width: self.frame.width - 55, height: self.frame.height - 60)
      speechLabel.frame = CGRect(x: 20, y: 18, width: self.frame.width - 40, height: self.frame.height - 60)
//      speechLabel.textAlignment = .center
      //speechLabel.text = "Welcome to Conversations for Life"
    } else {
      speechLabel.frame = CGRect(x: 20, y: 18, width: self.frame.width - 40, height: self.frame.height - 60)
//      speechLabel.textAlignment = .center
      //speechLabel.text = "This app helps you plan a conversation with a friend. Watch the video to find out how."
    }

  }
  
  override func awakeFromNib() {
  }
  
  
  override func draw(_ rect: CGRect) {
    if bubbleType == .primary {
      Style.drawSpeechBubble(frame: rect, resizing: .aspectFit)
    } else if bubbleType == .secondary {
      Style.drawSpeechBubbleRed(frame: rect, resizing: .aspectFit)
    } else if bubbleType == .tertiary {
      Style.drawSpeechBubbleGrey(frame: rect, resizing: .aspectFit)
    } else if bubbleType == .tertiaryLarge {
      Style.drawSpeechBubbleGreyLarge(frame: rect, resizing: .aspectFit)
    }
  }
  
  /*
  override class var layerClass: AnyClass {
    return SpeechBubbleLayer.self
  }
  */
  /*
  func updateWith(progress : CGFloat) {
    self.progress = progress
  }
  */
}


class SpeechBubbleLayer: CALayer {
  /*
  @NSManaged var progress : CGFloat
  
  override class func needsDisplay(forKey key: String) -> Bool {
    return key == "progress" || super.needsDisplay(forKey: key);
  }
  
  override func action(forKey event: String) -> CAAction? {
    
    if event == "progress" {
      let timing : CAMediaTimingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionDefault)
      let duration = 1.5
      
      let animation = CABasicAnimation(keyPath: event)
      animation.duration = duration
      animation.timingFunction = timing
      animation.fromValue = self.presentation()?.value(forKey: event) ?? 0
      return animation
    }
    
    return super.action(forKey: event)
  }
  */
  
  override func draw(in ctx: CGContext) {
    UIGraphicsPushContext(ctx)
    Style.drawSpeechBubble()
    UIGraphicsPopContext()
  }
  
  /*
   http://stackoverflow.com/questions/8197239/animated-cashapelayer-pie
   let circleRect = self.bounds.insetBy(dx: 1, dy: 1)
   let borderColor = UIColor.white.cgColor
   let backgroundColor = UIColor.red.cgColor
   
   ctx.setFillColor(backgroundColor)
   ctx.setStrokeColor(borderColor)
   ctx.setLineWidth(2)
   
   ctx.fillEllipse(in: circleRect)
   ctx.strokeEllipse(in: circleRect)
   
   let radius = min(circleRect.midX, circleRect.midY)
   let center = CGPoint(x: radius, y: circleRect.midY)
   let startAngle = CGFloat(-(Double.pi/2))
   let endAngle = CGFloat(startAngle + 2 * CGFloat(Double.pi * Double(progress)))
   
   ctx.setFillColor(borderColor)
   ctx.move(to: CGPoint(x:center.x , y: center.y))
   ctx.addArc(center: CGPoint(x:center.x, y: center.y), radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
   ctx.closePath()
   ctx.fillPath()
   }
   }
   */
}

