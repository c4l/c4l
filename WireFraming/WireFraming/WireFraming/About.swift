//
//  About.swift
//  WireFraming
//
//  Created by Peter Hunt on 25/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit

class About: UITableViewController, Injectable {
  
  var dataSource: AboutDataSource!
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  // MARK: - View Configuration
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Setup the dataSource, inject dependencies and populate the table view data
    dataSource = AboutDataSource(strings: strings, model: model)
    dataSource.presentingViewController = self
    
    // setup the tableView
    tableView.dataSource = dataSource
    tableView.delegate = self
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 60
    //    tableView.separatorStyle = .singleLine
    tableView.separatorStyle = .none
    
    self.automaticallyAdjustsScrollViewInsets = true
    
    
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self,
                                                             action: #selector(doneButton))
    
    
    /*
    // Register the custom Banner Table Cell NIB
    let bannerNibName = CustomCellStyle.banner.rawValue
    tableView.register(UINib(nibName: bannerNibName, bundle: nil), forCellReuseIdentifier: bannerNibName)
    
    // Register the custom Tip Table Cell NIB
    let tipNibName = CustomCellStyle.tip.rawValue
    tableView.register(UINib(nibName: tipNibName, bundle: nil), forCellReuseIdentifier: tipNibName)
    */
    
  }
  
  
  /*
   override func viewDidAppear(_ animated: Bool) {
   super.viewDidAppear(animated)
   debugPrint("AboutVC viewDidAppear")
   tableView.reloadData()
   }
   */
  
  @objc func doneButton() {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
  // MARK: - Classy App Rater
  
  func didUpdateRatings() {
    debugPrint(#function)
    
    let imageColor: UIColor = Style.primaryColor
    let imageSize = CGSize(width: 40, height: 40)
    
    let ratingImage = UIImage.fontAwesomeIconWithName(.CheckSquareO, textColor: imageColor, size: imageSize)
    let ratingItem = Item(style: .subtitle, title: "Like the App? Please rate me!",
                          detail: classyRatingString(), image: ratingImage,
                          action: { [weak self] in
                            self?.appstoreRatingAction() })
    
    if dataSource.tableViewData.count > 2 && dataSource.tableViewData[1].items.count > 3 { // check bounds
      dataSource.tableViewData[1].items[2] = ratingItem  // 2nd section, 3rd item - update with new rating string
    }
    self.tableView.reloadData()
  }
  
  func classyRatingString() -> String {
    guard let ratingsCount = ClassyAppRater.numberOfRatings() else {
      return ""
    }
    
    var ratedString: String
    switch ratingsCount {
    case 0 :
      ratedString = NSLocalizedString("No one has rated this version yet 🙁", comment: "")
      
    case 1 :
      ratedString = NSLocalizedString("Only 1 person has rated this version 😐", comment: "")
      
    default:
      ratedString = String(format: NSLocalizedString("%d people have rated this version 🙂", comment: ""), ratingsCount)
    }
    
    return ratedString
  }
  */
  
  
/*
  /// Send an email to feedback email address.
  func sendFeedbackAction() {
    Answers.logCustomEvent(withName: "Feedback Email", customAttributes: nil)  // Crashlytics Answers - log an event
    
    let device: String = Device.deviceVersion() ?? "Unknown"
    let subject = "\(APP_TITLE) \(appVersion()) Feedback (\(device))"
    
    displayMailComposer(to: [FEEDBACK_EMAIL], subject: subject, body: "")
  }
  
  /// Send a recommendation email to a friend
  func tellafriendAction() {
    Answers.logInvite(withMethod: "Email", customAttributes: nil)  // Crashlytics Answers - log an invite
    
    let subject = "Check out \(APP_TITLE)"
    
    let iconURL = "<A HREF='\(APPSTORE_URL)'><img src='\(APPICON_URL)' style='float:middle;width:100px;height:100px;margin:100px;margin-top:25px;margin-bottom:15px;border-radius:12px;'></A>"
    
    var appDescription: String
    #if FREE_VERSION
      appDescription = "a free dictionary App for Words With Friends and Scrabble.";
    #else
      appDescription = "an iOS dictionary App for Words With Friends and Scrabble.";
    #endif
    
    let body = "Hi,<BR>I've been using <A HREF='\(APPSTORE_URL)'>\(APP_TITLE)</A> and thought you might like it.  It's \(appDescription)<BR>\(iconURL)<BR>Check it out in the App Store <A HREF='\(APPSTORE_URL)'>here</A>.<BR>"
    
    displayMailComposer(subject: subject, body: body)
  }
  
  */
  

  

// MARK: - UITableViewDelegate
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
  {
    if section == 0 { return CGFloat.leastNonzeroMagnitude }
    return 50.0
  }
  
  override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    guard let header = view as? UITableViewHeaderFooterView else { return }
    header.contentView.backgroundColor = Style.cardBackgroundColor
    
    header.textLabel?.textColor = Style.primaryTextColor
//    header.textLabel?.backgroundColor = Style.backgroundColor
    header.textLabel?.font = UIFont.customHeadline
    header.textLabel?.frame = header.frame
    header.textLabel?.textAlignment = .center

  }
  
  /*
  /// Fix to hide the top (empty) section header view
  func override tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    if section == 0 { // no header for banner
      return CGFloat.leastNormalMagnitude
    }
    
    return 20  // 40
  }
  
  func override tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let style: CustomCellStyle = self.dataSource.isValid(indexPath: indexPath) ? self.dataSource.tableViewData[indexPath.section].items[indexPath.row].style : .subtitle
    
    if style == .tipPager {
      return 185
    }
    
    return UITableViewAutomaticDimension
  }
  */
  
  /*
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
   let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
   headerView.backgroundColor = .lightGray
   
   let label = UILabel()
   label.translatesAutoresizingMaskIntoConstraints = false
   label.text = "TITLE"
   headerView.addSubview(label)
   label.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 8).isActive = true
   label.rightAnchor.constraint(equalTo: headerView.rightAnchor).isActive = true
   label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
   label.heightAnchor.constraint(equalToConstant: 44).isActive = true
   
   return headerView
   
   }
   */
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    // Get the associated action closure from the datasource, and execute it
    if let action = dataSource.getActionForItem(atIndexPath: indexPath) {
      action()
    }
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  
}

/*

// MARK: - Mail - MFMailComposeViewControllerDelegate
extension About: MFMailComposeViewControllerDelegate {
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true)
  }
  
  func displayMailComposer(to: [String]? = nil, subject: String, body: String) {
    guard MFMailComposeViewController.canSendMail() else { return }
    
    let composer = MFMailComposeViewController()
    composer.mailComposeDelegate = self
    
    // Set the tint color of the mail composer nav bar
    composer.navigationBar.tintColor = currentTheme.colorNamed("tintcolor")
    
    // Set up recipients, subject, body
    composer.setToRecipients(to)
    composer.setSubject(subject)
    composer.setMessageBody(body, isHTML: true)
    
    present(composer, animated: true, completion: nil)
  }
  
 */




