//
//  VideoView.swift
//  WireFraming
//
//  Created by Peter Hunt on 3/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import BMPlayer

class VideoView: UIView {
  
  var player: BMPlayer!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
  func configure(videoName: String, videoFile: String, subtitleFile: String?) {

    player = BMPlayer()
    player.backBlock = { (isFullScreen) in
      if isFullScreen == true {
        return
      }
    }
    
    // Listen to when the player is playing or stopped
    player.playStateDidChange = { (isPlaying: Bool) in
      print("playStateDidChange. Playing: \(isPlaying)")
      /*
       if !isPlaying {
         self.continueButton.setTitle("Continue", for: .normal)
       } else {
         self.continueButton.setTitle("Skip", for: .normal)
       }
       */
    }
    
    // Listen to when the play time changes
    player.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
      print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    
    var subtitle: BMSubtitles?
    
    if let file = subtitleFile {
      let subtitleUrl = Bundle.main.url(forResource: file, withExtension: nil)!
      subtitle = BMSubtitles(url: subtitleUrl)
    } else {
      subtitle = nil
    }
    
    let asset = BMPlayerResource(name: videoName,
                                 definitions: [BMPlayerResourceDefinition(url: videoUrl, definition: "1080p")],
                                 cover: nil,
                                 subtitles: subtitle)
    
    player.setVideo(resource: asset)
    
    self.addSubview(player)
    self.backgroundColor = Style.backgroundColor
    
    setPlayerConstraints()
    
    
    let speechView1 = SpeechBubbleView(type: .primary, text: "Video Information")
    self.addSubview(speechView1)
    
    speechView1.snp.makeConstraints { (make) in
      make.top.equalTo(player.snp.bottom).offset(10)
      make.left.right.equalTo(self)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
      make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
    }
    
    
    //player.play()

  }
  
  
  func setPlayerConstraints() {
    
    player.snp.remakeConstraints { (make) in
      make.top.left.right.equalTo(self)
      // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
      make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
    }
  }
  
  
  /*
  func nextTapped() {
    
    print("VideoView Continue")
    player.pause()
    continueButton.isHidden = true
    
  }
  */
  
  /*
  
  func setupPlayerViewController() {
  
    // *** Using AVPlayerViewController ***
    
    playerViewController = AVPlayerViewController()
    
    if let playerVC = playerViewController {
      // setup AVPlayer
      // let filePath = Bundle.main.path(forResource: "big_buck_bunny", ofType: "mp4")
      let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
      let videoUrl = URL(fileURLWithPath: filePath!)
      let player = AVPlayer(url: videoUrl)
      
      
      playerVC.player = player
      
      
      // ** Show closed captions
      
      // override the default user's system preference for media selection (ie. closed caption settings)
      player.appliesMediaSelectionCriteriaAutomatically = false
      
      
      // From https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/MediaPlaybackGuide/Contents/Resources/en.lproj/RefiningTheUserExperience/RefiningTheUserExperience.html#//apple_ref/doc/uid/TP40016757-CH6-SW1
      //  retrieved an AVMediaSelectionGroup object for a particular media characteristic AVMediaCharacteristicLegible, and select it
      if let asset = player.currentItem?.asset,
        let group = asset.mediaSelectionGroup(forMediaCharacteristic: AVMediaCharacteristicLegible) {
        let options = AVMediaSelectionGroup.playableMediaSelectionOptions(from: group.options)
        
        if let option = options.first {
          player.currentItem?.select(option, in: group)
          print ("AVPlayerViewController Closed Caption Subtitles Found: \(group)")
        }
      }
      
      // Debugging - print all asset characteristics ...
      let asset = AVAsset(url: videoUrl)
      print("AVPlayerViewController Asset characteristics: \(asset.availableMediaCharacteristicsWithMediaSelectionOptions)")
      
      for characteristic in asset.availableMediaCharacteristicsWithMediaSelectionOptions {
        
        print("AVPlayerViewController \(characteristic)")
        
        // Retrieve the AVMediaSelectionGroup for the specified characteristic.
        if let group = asset.mediaSelectionGroup(forMediaCharacteristic: characteristic) {
          // Print its options.
          for option in group.options {
            print("  AVPlayerViewController Option: \(option.displayName)")
          }
        }
      }
      
      
    
    }
  
  }
  
  
  func setupVideoPlayer() {
    
    player = BMPlayer()
    //self.contentView.addSubview(player)
    
    //setVideoPortrait()  // setup constraints for player in portrait mode
    
    player.backBlock = { (isFullScreen) in
      if isFullScreen == true {
        return
      }
    }
    
    // Listen to when the player is playing or stopped
    player.playStateDidChange = { [unowned self] (isPlaying: Bool) in
      print("playStateDidChange \(isPlaying)")
      /*
       if !isPlaying {
       self.continueButton.setTitle("Continue", for: .normal)
       } else {
       self.continueButton.setTitle("Skip", for: .normal)
       }
       */
    }
    
    //Listen to when the play time changes
    player.playTimeDidChange = { (currentTime: TimeInterval, totalTime: TimeInterval) in
      print("playTimeDidChange currentTime: \(currentTime) totalTime: \(totalTime)")
    }
    
    let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
    let videoUrl = URL(fileURLWithPath: filePath!)
    
    let subtitleUrl = Bundle.main.url(forResource: subtitleFile, withExtension: nil)!
    let subtitle = BMSubtitles(url: subtitleUrl)
    
    
//    let asset = BMPlayerResource(url: videoUrl, name: "Welcome")
    
    let asset = BMPlayerResource(name: "Welcome",
                                 definitions: [BMPlayerResourceDefinition(url: videoUrl, definition: "1080p")],
                                 cover: nil,
                                 subtitles: subtitle)
    
    player.setVideo(resource: asset)
    //    player.autoPlay()
    
  }

  
  
  func bindFrameToSuperviewWidth() {
    guard let superview = self.superview else {
      print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
      return
    }
    
    self.translatesAutoresizingMaskIntoConstraints = false
    
    self.preservesSuperviewLayoutMargins = false
    superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    
    let aspectRatioConstraint = NSLayoutConstraint(item: self,
                                                   attribute: .height,
                                                   relatedBy: .equal,
                                                   toItem: self,
                                                   attribute: .width,
                                                   multiplier: (9.0 / 16.0),
                                                   constant: 0)
    aspectRatioConstraint.isActive = true
 
    self.addConstraint(aspectRatioConstraint)
  }
  
  */
  
 

  func setVideoLandscape() {
    
    player.snp.remakeConstraints { (make) in
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      if let rootView = appDelegate.window?.rootViewController?.view {
      
        make.top.left.right.equalTo(rootView)
        // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
        make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
      }
      
    }
    
//    superview?.bringSubview(toFront: self)
//    self.bringSubview(toFront: player)
    
  }
  
  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    /*
//    let orientation = newCollection.verticalSizeClass
    let orientation = traitCollection.verticalSizeClass
    
    switch orientation {
    case .compact:
      print("VideoView now in Landscape")  ///Excluding iPads!!!
      //self.navigationController?.setNavigationBarHidden(true, animated: true)
      //self.navigationController?.setToolbarHidden(true, animated: true)
      
      setVideoLandscape()
      
    default:
      print("VideoView now in Portrait")
      //self.navigationController?.setNavigationBarHidden(false, animated: true)
      //self.navigationController?.setToolbarHidden(false, animated: true)
      
      setVideoPortrait()
    }
 */
  }
  
  /*
   override func layoutSubviews() {
    playerLayer.frame = UIScreen.main.bounds
    
    //containerView.preferredMaxLayoutWidth = frame.size.width
    superview?.setNeedsLayout()
    
   }
  */
  
  /*
   lazy var containerView: UIView = {
   var container = UIView(frame: CGRect(x: 0, y: 0, width: 375, height: 300))
   
   container.translatesAutoresizingMaskIntoConstraints = false
   container.backgroundColor = Style.backgroundColor
   
   
   let filePath = Bundle.main.path(forResource: "big_buck_bunny", ofType: "mp4")
   let videoUrl = URL(fileURLWithPath: filePath!)
   
   let player = AVPlayer(url: videoUrl)
   let playerLayer = AVPlayerLayer(player: player)
   playerLayer.frame = container.frame
   container.layer.addSublayer(playerLayer)
   
   
   //    player.play()
   
   
   return container
   
   }()
   */
  //  var videoFile = "ev1_1080p.mp4"
  //  var subtitleFile = "ev1.srt"
  //var videoFile = "EV1 180517.mov"
  
  /*
   var player2: AVPlayer!
   var playerLayer: AVPlayerLayer!
   var playerViewController: AVPlayerViewController?
   */

  
  
  /*** USING AVPLAYER
   let filePath = Bundle.main.path(forResource: videoFile, ofType: nil)
   let videoUrl = URL(fileURLWithPath: filePath!)
   
   player2 = AVPlayer(url: videoUrl)
   playerLayer = AVPlayerLayer(player: player2)
   playerLayer.frame = self.frame
   playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill  // fill the full width of the screen
   // introView.layer.masksToBounds = true
   self.layer.addSublayer(playerLayer)
   
   bindFrameToSuperviewWidth()
   
   
   // ** Show closed captions
   
   // override the default user's system preference for media selection (ie. closed caption settings)
   player2.appliesMediaSelectionCriteriaAutomatically = false
   
   
   // From https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/MediaPlaybackGuide/Contents/Resources/en.lproj/RefiningTheUserExperience/RefiningTheUserExperience.html#//apple_ref/doc/uid/TP40016757-CH6-SW1
   //  retrieved an AVMediaSelectionGroup object for a particular media characteristic AVMediaCharacteristicLegible, and select it
   if let asset = player2.currentItem?.asset,
   let group = asset.mediaSelectionGroup(forMediaCharacteristic: AVMediaCharacteristicLegible) {
   let options = AVMediaSelectionGroup.playableMediaSelectionOptions(from: group.options)
   
   if let option = options.first {
   player2.currentItem?.select(option, in: group)
   print ("AVPlayerViewController Closed Caption Subtitles Found: \(group)")
   }
   }
   
   // Debugging - print all asset characteristics ...
   let asset = AVAsset(url: videoUrl)
   print("AVPlayerViewController Asset characteristics: \(asset.availableMediaCharacteristicsWithMediaSelectionOptions)")
   
   for characteristic in asset.availableMediaCharacteristicsWithMediaSelectionOptions {
   
   print("AVPlayerViewController \(characteristic)")
   
   // Retrieve the AVMediaSelectionGroup for the specified characteristic.
   if let group = asset.mediaSelectionGroup(forMediaCharacteristic: characteristic) {
   // Print its options.
   for option in group.options {
   print("  AVPlayerViewController Option: \(option.displayName)")
   }
   }
   }
   
   
   player2.play()
   
   
   // *** END OF USING AVPLAYER ***
   */
  
  
  
  /* USING BMPLAYER
   
   setupVideoPlayer()
   self.addSubview(player)
   
   
   
   self.backgroundColor = Style.backgroundColor
   
   setVideoPortrait()
   
   
   let speechView1 = SpeechBubbleView(type: .primary, text: "Video Information")
   self.addSubview(speechView1)
   
   speechView1.snp.makeConstraints { (make) in
   make.top.equalTo(player.snp.bottom).offset(10)
   make.left.right.equalTo(self)
   // Note here, the aspect ratio 16:9 priority is lower than 1000 on the line, because the 4S iPhone aspect ratio is not 16:9
   make.height.equalTo(speechView1.snp.width).multipliedBy(160.0/500.0).priority(750)
   }
   
   
   /*
   
   continueButton = UIButton.custom("Skip", target: self, action: #selector(nextTapped))
   self.addSubview(continueButton)
   
   continueButton.snp.makeConstraints { (make) in
   //      make.bottom.equalTo(self.contentView.snp.bottom).offset(-60)
   make.top.equalTo(speechView1.snp.bottom).offset(10)
   make.centerX.equalTo(speechView1.snp.centerX)
   make.width.equalTo(160)
   make.height.equalTo(50)
   }
   */
   
   
   player.play()
   
   
   /*
   self.bringSubview(toFront: player)
   player.layer.zPosition = CGFloat.greatestFiniteMagnitude
   */
   
   */

}
