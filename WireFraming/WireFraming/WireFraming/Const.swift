//
//  Constants.swift
//  WireFraming
//
//  Created by Peter Hunt on 21/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit


// Default max field length settings
struct Const {
  static let AppName = "Chat for life"
  static let PlansFileName = "plans.data"
  static let PasscodeSalt = Bundle.main.bundleIdentifier ?? AppName  // salt the passcode using the bundleIdentifier string
  static let MaxTextFieldLength = 14   // name field
  static let MaxTextViewLength = 500   // all text views
  static let HorizontalPadding: CGFloat = 10    // standard horizontal padding for controls from left/right of screen
  static let VerticalPadding: CGFloat = 30      // standard vertical padding between controls
}


// Default Animation duration settings
struct Animation {
  static let DefaultDelay: TimeInterval = 1.5 // time between 'receiving' messages
  static let DefaultDuration: TimeInterval = 1.0  //1.5
  static let DefaultDamping: CGFloat = 0.75 // 0 = max oscillation, 1 = no oscillation
  static let DefaultVelocity: CGFloat = 1.8
  static let CheckboxDuration: CGFloat = 0.5
  static let BannerDuration: TimeInterval = 2.5  // time for banner from the top of the screen to display
  
}

