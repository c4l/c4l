//
//  AboutDataSource.swift
//  WireFraming
//
//  Created by Peter Hunt on 25/5/17.
//  Copyright © 2017 appspired. All rights reserved.
//

import Foundation
import UIKit
import SwiftIcons

public enum AboutCustomCellStyle: CustomStringConvertible {
  //case banner = "AboutBannerCell"    // style for a cell with a centred label, a centred label below it in smaller text, and an image
  
  case subtitle // style for a cell with a left-aligned label across the top and a left-aligned label below it in smaller gray text.
  
  case toggle(getValue: () -> (Bool), toggleValue: Selector) // style for a cell with a left-aligned label, and a right-aligned UISwitch. getValue is block to return the current value of the switch. toggleValue is the selector to call when switch is toggled
  
  //case tip = "AboutTipCell" // style for a cell with a left-aligned label, a left-aligned label below it in smaller gray text, and an image on the left.
  //case tipPager = "TipCell"  // a cell with a UIPageViewControl embedded, for paging through tips
  
  public var description: String {
    switch self {
    case .subtitle: return "AboutSubtitleCell"
    case .toggle: return "AboutToggleCell"
    }
  }
}

/// One row in the table view
public struct AboutItem {
  var style : AboutCustomCellStyle   // style of this cell
  var title: String
  var detail: String?
  var image: UIImage?
  var action: (() -> Void)?
}

/// One section in the table view
public struct AboutSection {
  var name  : String?            // title for this section
  var items : [AboutItem]
}


class AboutDataSource: NSObject, UITableViewDataSource, Injectable {
  
  /// All data for the table view
  public var tableViewData = [AboutSection]()
  
  weak var presentingViewController: About?
  
  
  // Injectable
  weak var strings: StringsModel!
  weak var model: DataModel!
  
  func setDependencies(strings: StringsModel, model: DataModel) {
    self.strings = strings
    self.model = model
  }
  
  
  init(strings: StringsModel, model: DataModel) {
    super.init()
    
    setDependencies(strings: strings, model: model)
    
    // Load all data for the datasource
    tableViewData = preloadedData()
  }
  
  /// Return static data to populate the table view data source
  func preloadedData() -> [AboutSection] {
    
    let imageColor: UIColor = Style.primaryColor
    let imageBackgroundColor: UIColor = .clear
    //let watermarkColor: UIColor = Style.backgroundColor.withAlphaComponent(0.75)
    //let watermarkSize = CGSize(width: 80, height: 80)
    let imageSize = CGSize(width: 40, height: 40)
    //let tellAFriendImageSize = CGSize(width: 36, height: 36)  // Envelope looks better a bit smaller
    
    // All items
    
    /*
     // banner
     let bannerImage = UIImage.fontAwesomeIconWithName(.Search, textColor: watermarkColor, size: watermarkSize)
     let bannerItem = Item(style: .banner, title: "\(APP_TITLE) \(appVersion())", detail: "Dictionary for Words With Friends", image: bannerImage,
     action: nil)
     */
    
    // Resources

    
//    let crisisImage = UIImage.init(icon: .fontAwesome(.exclamation), size: imageSize,
//                                   textColor: imageColor, backgroundColor: imageBackgroundColor)
    /*
    let crisisImage = UIImage.init(named: "crisis")
    let crisisItem = AboutItem(style: .subtitle, title: "Contacts in a Crisis", detail: nil, image: crisisImage,
                               action: { [weak self] in
                                self?.crisisAction() })
    */
    
    let replayWelcomeImage = UIImage.init(icon: .fontAwesomeSolid(.playCircle), size: imageSize,
                                          textColor: imageColor, backgroundColor: imageBackgroundColor)
    let replayWelcomeItem = AboutItem(style: .subtitle, title: "Replay Welcome", detail: nil, image: replayWelcomeImage,
                                      action: { [weak self] in
                                        print("Replay Welcome Video")
                                        self?.replayWelcomeAction()
    })
    
    let feedbackImage = UIImage.init(icon: .fontAwesomeSolid(.envelope), size: imageSize,
                                     textColor: imageColor, backgroundColor: imageBackgroundColor)
    let feedbackItem = AboutItem(style: .subtitle, title: "App Feedback", detail: nil, image: feedbackImage,
                                 action: { [weak self] in
                                  print("Email feedback")
                                  self?.feedbackAction()
    })
    
    let privacyImage = UIImage.init(icon: .fontAwesomeSolid(.fileAlt), size: imageSize,
                                     textColor: imageColor, backgroundColor: imageBackgroundColor)
    let privacyItem = AboutItem(style: .subtitle, title: "Privacy and Disclaimer", detail: nil, image: privacyImage,
                                 action: { [weak self] in
                                  self?.privacyStatementAction() })
    
    let aboutImage = UIImage.init(icon: .fontAwesomeSolid(.infoCircle) /*.fontAwesome(.info)*/, size: imageSize,
                                    textColor: imageColor, backgroundColor: imageBackgroundColor)
    let aboutItem = AboutItem(style: .subtitle, title: "About \(strings.app_name)", detail: nil, image: aboutImage,
                                action: { [weak self] in
                                  self?.aboutAction() })
    
    // preferences
    /*
    let audioItem = AboutItem(style: .toggle(getValue: { [unowned self] in self.model.userSettings.isAudioEnabled }, toggleValue: #selector(toggleAudio)), title: "Audio", detail: nil, image: nil,
                              action: { [weak self] in
                                self?.toggleAudio()
    })
    */
    
    // App Preferences

    
    let notificationsItem = AboutItem(style: .toggle(getValue: { [unowned self] in self.model.userSettings.isNotificationsEnabled }, toggleValue: #selector(toggleNotifications)), title: "Notifications", detail: nil, image: nil,
                                  action: { [weak self] in
                                    self?.toggleNotifications()
    })
    
    let calendarItem = AboutItem(style: .toggle(getValue: { [unowned self] in self.model.userSettings.isCalendarEnabled }, toggleValue: #selector(toggleNotifications)), title: "Calendar scheduling", detail: nil, image: nil,
                                      action: { [weak self] in
                                        self?.toggleCalendar()
    })
    
    let subtitlesItem = AboutItem(style: .toggle(getValue: { [unowned self] in self.model.userSettings.isSubtitlesEnabled }, toggleValue: #selector(toggleSubtitles)), title: "Subtitles", detail: nil, image: nil,
                                  action: { [weak self] in
                                    self?.toggleSubtitles()
    })
    
    let passcodeItem = AboutItem(style: .toggle(getValue: { [unowned self] in self.model.userSettings.isPasscodeEnabled }, toggleValue: #selector(togglePasscode)), title: "Passcode / Touch ID", detail: nil, image: nil,
                                 action: { [weak self] in
                                  self?.togglePasscode()
    })
    
    /*
     self?.model.userSettings.isPasscodeEnabled = value
     
     if value == true { // Prompt to set a new passcode
     print("Set new passcode")
     self?.presentModalPasscodeController(setNewPasscode: true) { [weak self] in
     self?.model.isUnlocked = true
     }
     }
     
     */
    
    
    //let settingsImage = UIImage.init(icon: .fontAwesome(.cog), size: imageSize,
    //                                 textColor: imageColor, backgroundColor: imageBackgroundColor)
    //let settingsItem = AboutItem(style: .subtitle, title: "Settings", detail: nil, image: settingsImage,
    //                          action: { [weak self] in
    //                          self?.settingsAction() })
    
    
    /*
     let tellafriendImage = UIImage.fontAwesomeIconWithName(.Envelope, textColor: imageColor, size: tellAFriendImageSize)
     let tellafriendItem = Item(style: .subtitle, title: "Tell a friend", detail: nil, image: tellafriendImage,
     action: { [weak self] in
     self?.tellafriendAction() })
     
     
     let ratingImage = UIImage.fontAwesomeIconWithName(.CheckSquareO, textColor: imageColor, size: imageSize)
     let ratingItem = Item(style: .subtitle, title: "Like the App? Please rate me!",
     detail: classyRatingString(), image: ratingImage,
     action: { [weak self] in
     self?.appstoreRatingAction() })
     */
    
    
    //let feedbackSection = Section(name: "Feedback", items: [feedbackItem, ratingItem])
    
    /*
     let upgradeImage = UIImage.fontAwesomeIconWithName(.Download, textColor: imageColor, size: imageSize)
     let upgradeItem = Item(style: .subtitle, title: "Upgrade to Word Lookup Pro",
     detail: "Remove Ads. Enhanced Search. ♥️", image: upgradeImage,
     action: { [weak self] in
     self?.upgradeToProAction() })
     */
    
    // Sections
    //      let bannerSection = Section(name: nil, items:[bannerItem, upgradeItem])
    
    let resourcesSection = AboutSection(name: "", items: [replayWelcomeItem, feedbackItem, privacyItem, aboutItem])
    let preferencesSection = AboutSection(name: "Preferences", items: [notificationsItem, calendarItem, subtitlesItem, passcodeItem, ])
    
    return [resourcesSection, preferencesSection]
    
    
  }
  
  
  
  // MARK: - Actions
  
  func replayWelcomeAction() {
    print("replayWelcomeAction")
    
    // Dismiss the current modal controller, get the root navigation controller and reset the nav stack
    let navigationController: UINavigationController? = (presentingViewController?.appDelegate.window?.rootViewController as? UINavigationController)
    
    presentingViewController?.dismiss(animated: false) { [weak self] in
      let welcomeScreen = self?.newViewController(withId: "Welcome")
      if let screen = welcomeScreen {
        navigationController?.setViewControllers([screen], animated: false)
      }
    }
    
    /*
    source.navigationController?.setViewControllers([destination], animated: true)
    
    // Show the welcome screen
    let welcomeScreen = newViewController(withId: "Welcome")
    navigationController?.pushViewController(welcomeScreen, animated: false)
    */
    
    /*
     let settingsController = InAppSettingsViewController()
     settingsController.showCreditsFooter = false
     settingsController.showDoneButton = false
     settingsController.delegate = self
     navController.pushViewController(settingsController, animated: true)
     */
  }
  
  func settingsAction() {
    print("settingsAction")
    /*
     let settingsController = InAppSettingsViewController()
     settingsController.showCreditsFooter = false
     settingsController.showDoneButton = false
     settingsController.delegate = self
     navController.pushViewController(settingsController, animated: true)
     */
  }

  func feedbackAction() {
    strings["feedback_url"].openUrl()
  }
  
  func privacyStatementAction() {
    strings["privacy_statement_url"].openUrl()
  }
    
  func crisisAction() {
    print("crisisAction")
    let contactsController = ContactsTableViewController(style: .grouped)
    presentingViewController?.navigationController?.pushViewController(contactsController, animated: true)
  }
  
  
  func aboutAction() {
    print("aboutAction")
    let aboutController = newViewController(withId: "AboutScrolling")
    aboutController.title = "About \(strings.app_name)"
    presentingViewController?.navigationController?.pushViewController(aboutController, animated: true)
  }

  func toggleCalendar() {
    model.userSettings.isCalendarEnabled = !model.userSettings.isCalendarEnabled
    print("Calendar: \(model.userSettings.isCalendarEnabled)")
    model.userSettings.saveToDefaults()
  }

  
  @objc func toggleNotifications() {
    model.userSettings.isNotificationsEnabled = !model.userSettings.isNotificationsEnabled
    print("Notifications: \(model.userSettings.isNotificationsEnabled)")

    // If turned ON, then prompt user to turn on in Settings
    if model.userSettings.isNotificationsEnabled {
      
      let alertController = UIAlertController(title: "Notifications Permission", message: "Please confirm that Notifications access is enabled for this app in Settings. Go to Settings?", preferredStyle: .alert)
      
      let okAction = UIAlertAction(title: "OK", style: .default) {
        (result : UIAlertAction) -> Void in
        print("You pressed OK")
        
        let settingsURL = URL(string: UIApplication.openSettingsURLString)
        if #available(iOS 10.0, *) {
          UIApplication.shared.open(settingsURL!, options: [:]) { success in
          }
        } else {
          // Fallback on earlier versions
        }
      }
      
      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
        (result : UIAlertAction) -> Void in
        print("You pressed Cancel")
        self.model.userSettings.isNotificationsEnabled = false
      }
      
      alertController.addAction(okAction)
      alertController.addAction(cancelAction)
      
      presentingViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    model.userSettings.saveToDefaults()
  }
  
  @objc func toggleSubtitles() {
    model.userSettings.isSubtitlesEnabled = !model.userSettings.isSubtitlesEnabled
    print("Subtitles: \(model.userSettings.isSubtitlesEnabled)")
    model.userSettings.saveToDefaults()
  }
  
  @objc func togglePasscode() {
    model.userSettings.isPasscodeEnabled = !model.userSettings.isPasscodeEnabled
    model.userSettings.saveToDefaults()
    if model.userSettings.isPasscodeEnabled { // Prompt to set a new passcode
      print("Set new passcode")
      if let currentViewController = presentingViewController {
        currentViewController.presentModalPasscodeController(setNewPasscode: true) { [weak self] in
          self?.model.isUnlocked = true
        }
      }
    }
  }
  
    /// Returns corresponding action closure for an item from tableViewData
    ///
    /// - parameter forIndexPath: indexPath for the item to return
    ///
    /// - returns: closure (() -> Void)? for the item retrieved from the data source
    public func getActionForItem(atIndexPath: IndexPath) -> (() -> Void)? {
      guard isValid(indexPath: atIndexPath) else { return nil }
      let item = tableViewData[atIndexPath.section].items[atIndexPath.row]
      return item.action
      
    }
    
    
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
      return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return isValid(section: section) ? tableViewData[section].items.count : 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
      
      if section == 0 { return nil }
      
      if isValid(section: section) {
        return tableViewData[section].name
      }
        
      return nil
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      // get the cell style for this indexpath (default to .subtitle)
      let style: AboutCustomCellStyle = isValid(indexPath: indexPath) ? tableViewData[indexPath.section].items[indexPath.row].style : .subtitle
      
      // dequeue a cell, of the style required, or create a new one
      let cell: UITableViewCell = {
        switch style {
          
          //case .banner:
          //return tableView.dequeueReusableCell(withIdentifier: style.rawValue) ?? AboutBannerCell(style: .subtitle, reuseIdentifier: style.rawValue)
          
        case .subtitle:
          return tableView.dequeueReusableCell(withIdentifier: style.description) ?? UITableViewCell(style: .subtitle, reuseIdentifier: style.description)
          
          //case .tip:
          //return tableView.dequeueReusableCell(withIdentifier: style.rawValue) ?? AboutTipCell(style: .subtitle, reuseIdentifier: style.rawValue)
          
          //case .tipPager:
          //return tableView.dequeueReusableCell(withIdentifier: style.rawValue) ?? TipCell(style: .subtitle, reuseIdentifier: style.rawValue)
          
          
        case .toggle:
          return tableView.dequeueReusableCell(withIdentifier: style.description) ?? UITableViewCell(style: .default, reuseIdentifier: style.description)
          
          
        }
        
      }()
      
      
      // set the cell's contents, colours and fonts from the data source
      setContentsOf(cell: cell, withStyle: style, fromIndexPath: indexPath)
      
      return cell
    }
    
    
    /// Retrieve data from tableViewData and update the contents of the tableview cell
    /// Also set the cell's colours and fonts from the current theme
    ///
    /// - parameter cell:          UITableViewCell to fill
    /// - parameter fromIndexPath: indexPath of cell to fill
    func setContentsOf(cell: UITableViewCell, withStyle: AboutCustomCellStyle, fromIndexPath: IndexPath) {
      
      guard isValid(indexPath: fromIndexPath) else { return }
      let item = tableViewData[fromIndexPath.section].items[fromIndexPath.row]
      
      // Configure the text, image and colours of the cell, depending on the style
      switch withStyle {
        
      case .subtitle:      // standard .subtitle style cell
        // set colours
        cell.textLabel?.textColor = Style.primaryTextColor
        cell.detailTextLabel?.textColor = Style.secondaryTextColor
        
        // set fonts
        let aboutScreenFont = UIFont.customBody
        cell.textLabel?.font = aboutScreenFont
        
        let detailTextFont = UIFont.customSubheadline
        cell.detailTextLabel?.font = detailTextFont
        
        // set contents
        cell.textLabel?.text = item.title
        if let detailText = item.detail {
          cell.detailTextLabel?.text = detailText
          //cell.detailTextLabel?.numberOfLines = 0
        }
        
        if let image = item.image {
          cell.imageView?.image = image
        }
        
        // if there's an action associated with this item, allow interaction and show the disclosure indicator
        if let _ = item.action {
          cell.isUserInteractionEnabled = true
          cell.accessoryType = .disclosureIndicator
        } else {
          cell.isUserInteractionEnabled = false
        }
        
        
      case let .toggle(getValue, toggleValue):
        cell.textLabel?.textColor = Style.primaryTextColor
//        cell.textLabel?.font = getValue() ? UIFont.customBody.bolded : UIFont.customBody
        cell.textLabel?.font = UIFont.customBody
        cell.textLabel?.text = item.title
        
        // if there's an action associated with this item, allow interaction and show the disclosure indicator
        if let _ = item.action {
          cell.isUserInteractionEnabled = true
        } else {
          cell.isUserInteractionEnabled = false
        }
        
        let toggleSwitch = UISwitch(frame: CGRect.zero)
        toggleSwitch.isOn = getValue()
        toggleSwitch.addTarget(self, action: toggleValue, for: .valueChanged)
        cell.accessoryView = toggleSwitch
        
        /*
         case .banner:     // custom AboutBannerCell
         let bannerCell = cell as! AboutBannerCell
         bannerCell.setContent(title: item.title, titleFont: currentTheme.fontNamed("tablecelltextfont"), titleColor: currentTheme.colorNamed("tablecelltextcolor"),
         subTitle: item.detail, subTitleFont: currentTheme.fontNamed("tablecelldetailtextfont"),
         subTitleColor: currentTheme.colorNamed("tablecelldetailtextcolor"), bannerImage: item.image)
         
         case .tip:      // custom AboutTipCell
         let tipCell = cell as! AboutTipCell
         tipCell.setContent(title: item.title, titleFont: currentTheme.fontNamed("tablecelltextfont"), titleColor: currentTheme.colorNamed("tablecelltextcolor"),
         subTitle: item.detail, subTitleFont: currentTheme.fontNamed("tablecelldetailtextfont"), subTitleColor: currentTheme.colorNamed("tablecelldetailtextcolor"), tipImage: item.image)
         
         case .tipPager:      // custom AboutTipCell
         let tipCell = cell as! TipCell
         tipCell.setContent(tips: tipData,
         titleFont: currentTheme.fontNamed("tablecelltextfont"), titleColor: currentTheme.colorNamed("tablecelltextcolor"),
         subTitleFont: currentTheme.fontNamed("tablecelldetailtextfont"), subTitleColor: currentTheme.colorNamed("tablecelldetailtextcolor"))
         */
      }
      
      //    cell.backgroundColor = Style.backgroundColor
      
    }
    
    
    
    /// Helper method - check that the indexPath section is valid for this data source
    func isValid(section: Int) -> Bool {
      if section < 0 || section > tableViewData.count - 1 { return false }
      return true
    }
    
    /// Helper method - check that the indexPath is valid for this data source
    /// (ie. not outside of array bounds)
    /// - parameter indexPath: indexPath to check
    ///
    /// - returns: true if the data source contains data for that indexPath
    func isValid(indexPath: IndexPath) -> Bool {
      // sanity check, and make sure the specified section exists
      if indexPath.row < 0 || !isValid(section: indexPath.section) { return false }
      
      // Make sure the specified row exists
      return indexPath.row <= tableViewData[indexPath.section].items.count - 1
    }
    
    
    
}
